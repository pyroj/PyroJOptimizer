package pyroj.costmodel;



import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.optimizer.Config;

import com.sosnoski.util.array.DoubleArray;
import com.sosnoski.util.array.IntArray;

import static pyroj.costmodel.PhysicalOperatorTagsForSpark.SortExec;

//import edu.uci.ics.hyracks.algebricks.core.algebra.base.ILogicalOperator;
//import edu.uci.ics.hyracks.algebricks.core.algebra.base.IPhysicalOperator;
//import edu.uci.ics.hyracks.algebricks.core.algebra.base.PhysicalOperatorTag;


public class CostModel_RT implements ICostModel {
	private double delta_k=1.2; //1.2; //worst case value of delta_k when k=0.2
	//resource vector size
	public final int RVECTOR_SIZE = 5;   // response time, CPU(processing) , mem, disk, network
	private final int _maxCostId;
	private final int _minCostId;
	private final int _zeroCostId;
	private final IntArray _freeObjectList;
	
	//both these resource vectors together form a resource descriptor i.e. a cost object. (rf,rl) 
	//resource vector for first tuple i.e. rf
	private final DoubleArray _RT_CostObjList_first;
	private final DoubleArray _mem_CostObjList_first;
	private final DoubleArray _processing_CostObjList_first;
	private final DoubleArray _disk_CostObjList_first;
	private final DoubleArray _network_CostObjList_first;

	//resource vector for last tuple i.e. rl
	private final DoubleArray _RT_CostObjList_last;
	private final DoubleArray _mem_CostObjList_last;
	private final DoubleArray _processing_CostObjList_last;
	private final DoubleArray _disk_CostObjList_last;
	private final DoubleArray _network_CostObjList_last;
	/*
	 * this id to hold cost as a single vector representing total work when optimizing for objective=1(total work)
	 * as cost calculus for this is simple just addition so it does not consider 1st tuple and last tuple cost seperately.
	 * so only one component for it, last tuple cost
	 * so now a cost object is made up of 3 components--2 vectors and a scalar--
	 *   {(RT_first,cpu_first, mem_first, disk_first,nw_first), (RT_last,cpu_last,mem_last,disk_last,nw_last), total_work}
	 */
	private final DoubleArray _TotalWork_CostObjList;	
	
	
	
	
	private static MutableDouble _smemoryLatency = new MutableDouble(0);
	private static MutableDouble _sprocessingLatency = new MutableDouble(0);
	private static MutableDouble _sdiskLatency = new MutableDouble(0);
	private static MutableDouble _snetworkLatency = new MutableDouble(0);

	@Override
	public double getCost(int id)
	{
		return 0.0;
	}

	@Override
	public void setCost(int id, double cost)
	{

	}

	/* removed this as I modified another parameterised constructor.
	 *  No need to pass objective approximation_enabled and appro_factor as parameter to this constructor
	 *  Instead moved them to Config.java in PyroJOptimizer,  which is iitialized during initiaize() of RT_Optimizer
	 *   as its more appropriate to make them as property of whole optimizer instea of just cost model
	public CostModel_RT() {
		this(2);
	}*/
	/**
	 * 	
	 *  No need to pass objective approximation_enabled and appro_factor as parameter to this constructor
	 *  Instead moved them to Config.java in PyroJOptimizer,  which is iitialized during initiaize() of RT_Optimizer
	 *   as its more appropriate to make them as property of whole optimizer instea of just cost model
	 *  
	 */
	public CostModel_RT() {	
	
		
			
		_freeObjectList = new IntArray();

		_RT_CostObjList_first = new DoubleArray();
		_RT_CostObjList_first.add(ICostModel.NULL, -1);
		
		_mem_CostObjList_first = new DoubleArray();
		_mem_CostObjList_first.add(ICostModel.NULL, -1);
		
		_processing_CostObjList_first = new DoubleArray();
		_processing_CostObjList_first.add(ICostModel.NULL, -1);
		
		_disk_CostObjList_first = new DoubleArray();
		_disk_CostObjList_first.add(ICostModel.NULL, -1);
		
		_network_CostObjList_first = new DoubleArray();
		_network_CostObjList_first.add(ICostModel.NULL, -1);
		
		_RT_CostObjList_last = new DoubleArray();
		_RT_CostObjList_last.add(ICostModel.NULL, -1);
		
		_mem_CostObjList_last = new DoubleArray();
		_mem_CostObjList_last.add(ICostModel.NULL, -1);
		
		_processing_CostObjList_last = new DoubleArray();
		_processing_CostObjList_last.add(ICostModel.NULL, -1);
		
		_disk_CostObjList_last = new DoubleArray();
		_disk_CostObjList_last.add(ICostModel.NULL, -1);
		
		_network_CostObjList_last = new DoubleArray();
		_network_CostObjList_last.add(ICostModel.NULL, -1);
		
		_TotalWork_CostObjList = new DoubleArray();
		_TotalWork_CostObjList.add(ICostModel.NULL, -1);
		
		_zeroCostId = _RT_CostObjList_first.add(0);
					_mem_CostObjList_first.add(0);
					_processing_CostObjList_first.add(0);
					_disk_CostObjList_first.add(0);
					_network_CostObjList_first.add(0);

					_RT_CostObjList_last.add(0);
					_mem_CostObjList_last.add(0);
					_processing_CostObjList_last.add(0);
					_disk_CostObjList_last.add(0);
					_network_CostObjList_last.add(0);
					_TotalWork_CostObjList.add(0);

		_maxCostId = _RT_CostObjList_first.add(Double.MAX_VALUE);
					_mem_CostObjList_first.add(Double.MAX_VALUE);
					_processing_CostObjList_first.add(Double.MAX_VALUE);
					_disk_CostObjList_first.add(Double.MAX_VALUE);
					_network_CostObjList_first.add(Double.MAX_VALUE);
					
					_RT_CostObjList_last.add(Double.MAX_VALUE);
					_mem_CostObjList_last.add(Double.MAX_VALUE);
					_processing_CostObjList_last.add(Double.MAX_VALUE);
					_disk_CostObjList_last.add(Double.MAX_VALUE);
					_network_CostObjList_last.add(Double.MAX_VALUE);
					_TotalWork_CostObjList.add(Double.MAX_VALUE);
		
		_minCostId = _RT_CostObjList_first.add(Double.MIN_VALUE);
					_mem_CostObjList_first.add(Double.MIN_VALUE);
					_processing_CostObjList_first.add(Double.MIN_VALUE);
					_disk_CostObjList_first.add(Double.MIN_VALUE);
					_network_CostObjList_first.add(Double.MIN_VALUE);
					
					_RT_CostObjList_last.add(Double.MIN_VALUE);
					_mem_CostObjList_last.add(Double.MIN_VALUE);
					_processing_CostObjList_last.add(Double.MIN_VALUE);
					_disk_CostObjList_last.add(Double.MIN_VALUE);
					_network_CostObjList_last.add(Double.MIN_VALUE);
					_TotalWork_CostObjList.add(Double.MIN_VALUE);
		
	}
	

	
	@Override
	/**done_28may'15
	 * this is add_seq i.e operation ';' on resource discriptors
	 * so, (pf,pl);(qf,ql);total_work = (pl;qf, pl;ql, tw1+tw2)=resulting cost object(resource descriptor) 
	 */
	public int Add(int idCostObject, int idCostOther) {
		double[] pl = new double[5];
		double[] qf = new double[5];
		double[] ql = new double[5];
		
		pl=getLastTuple_ResourceVector(idCostObject);
		qf=getFirstTuple_ResourceVector(idCostOther);
		ql=getLastTuple_ResourceVector(idCostOther);
			
		//(pf,pl);(qf,ql) = (pl;qf, pl;ql)
		//totalwork= sum of the two total works
		double tw = min(Double.MAX_VALUE, (getTotalWork_usingCostModelOrd(idCostObject)+getTotalWork_usingCostModelOrd(idCostOther)));
		return Alloc(add_seq(pl,qf), add_seq(pl,ql), tw) ;		
	}

	@Override
	/*
	 * done_28may'15  (non-Javadoc)
	 * @see pyroj.costmodel.ICostModel#AddAndAssign(int, int)
	 */
	public void AddAndAssign(int idCostObject, int idCostOther) {
		double[] pl = new double[5];
		double[] qf = new double[5];
		double[] ql = new double[5];
		
		pl=getLastTuple_ResourceVector(idCostObject);
		qf=getFirstTuple_ResourceVector(idCostOther);
		ql=getLastTuple_ResourceVector(idCostOther);
					
		setFirstTuple_ResourceVector(idCostObject, add_seq(pl,qf));
		setLastTuple_ResourceVector(idCostObject, add_seq(pl,ql));
		
		double tw = min(Double.MAX_VALUE, (getTotalWork_usingCostModelOrd(idCostObject)+getTotalWork_usingCostModelOrd(idCostOther)));
		_TotalWork_CostObjList.set(idCostObject, tw);
		
	}

	@Override
	/*
	 * done_28may'15  (non-Javadoc)
	 * but not used currently anywhere
	 * @see pyroj.costmodel.ICostModel#AddResourceCost(int, pyroj.costmodel.ICostModel.ResourceType, double)
	 */
	public void AddResourceCost(int idCostObject, ResourceType resource, double cost) {
		double old = 0;
		switch(resource){
		case CPU: old = _processing_CostObjList_first.get(idCostObject);
					_processing_CostObjList_first.set(idCostObject, old + cost);
						break;
		case DISK: old = _disk_CostObjList_first.get(idCostObject);
					_disk_CostObjList_first.set(idCostObject, old + cost);
						break;
		case MEMORY: old = _mem_CostObjList_first.get(idCostObject);
					_mem_CostObjList_first.set(idCostObject, old + cost);
						break;
		case NETWORK: old = _network_CostObjList_first.get(idCostObject);
						_network_CostObjList_first.set(idCostObject, old + cost);
						break;
		}
		
		if(_RT_CostObjList_first.get(idCostObject) < (old + cost)){
			_RT_CostObjList_first.set(idCostObject, old + cost);
		}
		double old_work=_TotalWork_CostObjList.get(idCostObject);
		_TotalWork_CostObjList.set(idCostObject, old_work+cost);
		
	}

	@Override
	/*
	 * done_28may'15
	 * @see pyroj.costmodel.ICostModel#Alloc()
	 */
	public int Alloc() {
		double[] temp={0,0,0,0,0};
		return Alloc(temp,temp,0.0);
	}

	/** done_28may'15
	 * All parameters give cost values(latency) for that resource to be allocate to respective cost arrays
	 * @param double[] resourceVector for first tuple
	 * @param double[] resourceVector for last tuple
	 * index 0: response time
	 * index 1: processing cost
	 * index 2: memory cost
	 * index 3: disk cost
	 * index 4: network cost  in both above arrays
	 * @return index of the allocated costs(resource descriptor) obviously this will be same for all the cost arrays,
	 * 			 so we return for array of response time
	 */
//
	private int Alloc(double[] rfirst, double[] rlast, double totalWork) {
		int index;

		// Remove last element, so that it won't have to shift all the elements.
		final int freeListSize = _freeObjectList.size();
		if (freeListSize > 0) {
			index = _freeObjectList.get(freeListSize - 1);
			_freeObjectList.remove(freeListSize - 1);
			
			//add to all arrays at index position
			_RT_CostObjList_first.set(index, rfirst[0]);
			_processing_CostObjList_first.set(index, rfirst[1]);
			_mem_CostObjList_first.set(index, rfirst[2]);
			_disk_CostObjList_first.set(index, rfirst[3]);
			_network_CostObjList_first.set(index, rfirst[4]);
			
			_RT_CostObjList_last.set(index, rlast[0]);
			_processing_CostObjList_last.set(index, rlast[1]);
			_mem_CostObjList_last.set(index, rlast[2]);
			_disk_CostObjList_last.set(index, rlast[3]);
			_network_CostObjList_last.set(index, rlast[4]);
			
			_TotalWork_CostObjList.set(index,totalWork);
		}
		else {
			index = _RT_CostObjList_first.add(rfirst[0]);
			//add to all arrays at index position, as all array indexes are synchronized so following will also add at index position 
			_processing_CostObjList_first.add(rfirst[1]);
			_mem_CostObjList_first.add(rfirst[2]);
			_disk_CostObjList_first.add(rfirst[3]);
			_network_CostObjList_first.add(rfirst[4]);
			
			_RT_CostObjList_last.add(rlast[0]);
			_processing_CostObjList_last.add(rlast[1]);
			_mem_CostObjList_last.add(rlast[2]);
			_disk_CostObjList_last.add(rlast[3]);
			_network_CostObjList_last.add(rlast[4]);
			
			_TotalWork_CostObjList.add(totalWork);
		}
		return index;
	}

	
	
	@Override
	/*TODO :
	 * implement this properly. currently it not used anywhere so not implemented.
	 * (non-Javadoc)
	 * @see pyroj.costmodel.ICostModel#Alloc(java.lang.Object)
	 */
	public int Alloc(Object costStr) {
		assert false: "this method is not implemented in COstModel_RT but called. Implement it properly.";
		if (costStr == null || ((String) costStr).isEmpty())
			return NULL;

		else
			return Alloc(Double.parseDouble((String) costStr));
	}

	@Override
	/**
	 * done_28may'15
	 */
	public int AllocMaxCostObj() {
		double[] temp={Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE,Double.MAX_VALUE};
		return Alloc(temp,temp, Double.MAX_VALUE);
	}

	@Override
	/**
	 * done_28may'15
	 */
	public int AllocZeroCostObj() {
		double[] temp={0,0,0,0,0};
		return Alloc(temp,temp,0.0);
	}
	
	/*
	private boolean IsCostZero(int costId)
	{
		double [] last = getLastTuple_ResourceVector(costId);
		double [] first = getFirstTuple_ResourceVector(costId);
		for(int i=0;i<last.length;i++)
		{
			if(last[i]!=0 && first[i]!=0)
				return false;
		}
		return true;
	}
	*/
	
	/**done_28may'15
	 * assign value of cost at idCostOther to cost object at idCostObject 
	 */
	@Override
	public void Assign(int idCostObject, int idCostOther) {
		_RT_CostObjList_first.set(idCostObject, _RT_CostObjList_first.get(idCostOther));
		_processing_CostObjList_first.set(idCostObject, _processing_CostObjList_first.get(idCostOther));
		_mem_CostObjList_first.set(idCostObject, _mem_CostObjList_first.get(idCostOther));
		_disk_CostObjList_first.set(idCostObject, _disk_CostObjList_first.get(idCostOther));
		_network_CostObjList_first.set(idCostObject, _network_CostObjList_first.get(idCostOther));
		
		_RT_CostObjList_last.set(idCostObject, _RT_CostObjList_last.get(idCostOther));
		_processing_CostObjList_last.set(idCostObject, _processing_CostObjList_last.get(idCostOther));
		_mem_CostObjList_last.set(idCostObject, _mem_CostObjList_last.get(idCostOther));
		_disk_CostObjList_last.set(idCostObject, _disk_CostObjList_last.get(idCostOther));
		_network_CostObjList_last.set(idCostObject, _network_CostObjList_last.get(idCostOther));
		
		_TotalWork_CostObjList.set(idCostObject, _TotalWork_CostObjList.get(idCostOther));
	}

	@Override
	/**
	 * done
	 */
	public int Clone(int idCostObject) {
		final int index = Alloc();
		Assign(index, idCostObject);
		return index;
	}

	@Override
	public int DividedBy(int idCostObject, double divisor) {
		assert false:" 'DividedBy' in CostModel_RTnot Implemented yet in responce time based cost model, but it is called";
		try {
			throw new Exception(" 'DividedBy' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//previous code
		//TODO : need to implement for new cost model, for that understand its use in MQO or greedy, and remove above throw
		final int index = Alloc();
		_RT_CostObjList_first.set(index, _RT_CostObjList_first.get(idCostObject) / divisor);
		return index;
	}

	@Override
	/*
	 * done_28may'15 (non-Javadoc)
	 * @see pyroj.costmodel.ICostModel#Free(int)
	 */
	public void Free(int idCostObject) {
		_RT_CostObjList_first.set(idCostObject, 0.0);
		_processing_CostObjList_first.set(idCostObject, 0.0);
		_mem_CostObjList_first.set(idCostObject, 0.0);
		_disk_CostObjList_first.set(idCostObject, 0.0);
		_network_CostObjList_first.set(idCostObject, 0.0);
		
		_RT_CostObjList_last.set(idCostObject, 0.0);
		_processing_CostObjList_last.set(idCostObject, 0.0);
		_mem_CostObjList_last.set(idCostObject, 0.0);
		_disk_CostObjList_last.set(idCostObject, 0.0);
		_network_CostObjList_last.set(idCostObject, 0.0);
		
		_TotalWork_CostObjList.set(idCostObject, 0.0);
		
		_freeObjectList.add(idCostObject);
	}

	@Override
	public int getNumCostInstance() {
		return _RT_CostObjList_first.size();
	}

	@Override
	//TODO need to think what changes are needed for new cost model, currently it is not used anywhere, so not changed
	public boolean IsAdditionGreaterThanOrEqual(int idCostObject, int idCostToAdd, int idCostAdditionUB) {
		assert false:" 'IsAdditionGreaterThanOrEqual' in CostModel_RTnot Implemented properly yet in responce time based cost model, but it is called. Check whether its implementation goes with requierement";
		final double first = _RT_CostObjList_first.get(idCostObject);
		if (first >= Double.MAX_VALUE)
			return true;

		final double second = _RT_CostObjList_first.get(idCostToAdd);
		if (second >= Double.MAX_VALUE)
			return true;

		if ((first + second) >= _RT_CostObjList_first.get(idCostAdditionUB))
			return true;
		else
			return false;
	}

	@Override
	//TODO need to think what changes are needed for new cost model, currently it is not used anywhere, so not changed
	public boolean IsAdditionLessThan(int idCostObject, int idCostToadd, int idCostAdditionUB) {
		assert false:" 'IsAdditionGreaterThanOrEqual' in CostModel_RTnot Implemented properly yet in responce time based cost model, but it is called. Check whether its implementation goes with requierement";
		final double first = _RT_CostObjList_first.get(idCostObject);
		if (first >= Double.MAX_VALUE)
			return false;

		final double second = _RT_CostObjList_first.get(idCostToadd);
		if (second >= Double.MAX_VALUE)
			return false;

		if ((first + second) < _RT_CostObjList_first.get(idCostAdditionUB))
			return true;
		else
			return false;

	}

	@Override
	public boolean IsEqual(int idCostObject, int idCostOther) {
		return IsLessThanOrEqual(idCostObject, idCostOther) && IsLessThanOrEqual(idCostOther, idCostObject) ; 
		
	}

	@Override
	public boolean IsApproxEqual(int idCostObject, int idCostOther) {
		/*double diff = Math.abs(_RT_CostObjList_first.get(idCostObject) - _RT_CostObjList_first.get(idCostOther));
		if (diff < 1E-7)
			return true;*/
		//TODO
		assert false:"IsApproxEqual method called which is not implemented for CostModel_RT";
		return false;
	}

	//done_28may'15
	@Override
	public boolean IsGreaterThan(int idCostObject, int idCostOther) {
		boolean ans=false;
				
		if(Config.OBJECTIVE == 2 ){
			boolean allEqual = ( _RT_CostObjList_last.get(idCostObject) == _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) == _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) == _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) == _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) == _network_CostObjList_last.get(idCostOther) );
			
			ans = (!allEqual) && ( _RT_CostObjList_last.get(idCostObject)  >= _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) >= _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) >= _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) >= _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) >= _network_CostObjList_last.get(idCostOther) );
		}
		else if(Config.OBJECTIVE == 1 ){
			double temp= compareOnTotalWork(idCostObject, idCostOther);
			return (temp > 0? true : false);
		}
		else{
			assert false: "unknown objective value of Config.OBJECTIVE";
		}
		return ans;
	}

	//done_28may'15
	public double compareOnTotalWork(int idCostObject, int idCostOther) { 
		return _TotalWork_CostObjList.get(idCostObject)-_TotalWork_CostObjList.get(idCostOther);
	}
	
	public double compareOnResponseTime(int idCostObject, int idCostOther) { 
		return _RT_CostObjList_last.get(idCostObject) - _RT_CostObjList_last.get(idCostOther);
	}
	
	//done_28may'15
	public double compareOnObjectiveCost(int idCostObject, int idCostOther) {
		double ans=0.0;
		if(Config.OBJECTIVE ==1){
			ans= compareOnTotalWork(idCostObject, idCostOther);
		}
		else if(Config.OBJECTIVE == 2 ){
			ans= compareOnResponseTime(idCostObject, idCostOther);
		}
		else{
			assert false: "unknown objective value of Config.OBJECTIVE";
			try {
				throw new Exception("unknown objective");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ans;
	}
	
	//done_28may'15
	@Override
	public boolean IsGreaterThanOrEqual(int idCostObject, int idCostOther) {
		boolean ans=false;
		
		if(Config.OBJECTIVE == 2 ){
			ans = ( _RT_CostObjList_last.get(idCostObject)  >= _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) >= _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) >= _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) >= _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) >= _network_CostObjList_last.get(idCostOther) );
		}
		else if(Config.OBJECTIVE == 1 ){
			double temp= compareOnTotalWork(idCostObject, idCostOther);
			return (temp < 0? false : true);
		}
		return ans;

	}

	//done_28may'15
	@Override
	public boolean IsLessThan(int idCostObject, int idCostOther) {	
		boolean ans=false;
		
		if(Config.OBJECTIVE == 2 ){
			boolean allEqual = ( _RT_CostObjList_last.get(idCostObject) == _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) == _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) == _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) == _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) == _network_CostObjList_last.get(idCostOther) );
			
			ans = (!allEqual) && ( _RT_CostObjList_last.get(idCostObject) <= _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) <= _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) <= _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) <= _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) <= _network_CostObjList_last.get(idCostOther) );
		}
		else if(Config.OBJECTIVE == 1 ){
			double temp= compareOnTotalWork(idCostObject, idCostOther);
			return (temp < 0? true : false);
		}
		return ans;

	}

	//done_28may'15
	@Override
	public boolean IsLessThanOrEqual(int idCostObject, int idCostOther) {
		boolean ans=false;
		
		if(Config.OBJECTIVE == 2 ){
			ans =  ( _RT_CostObjList_last.get(idCostObject) <=  _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) <=  _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) <= _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) <=  _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) <=  _network_CostObjList_last.get(idCostOther) );
		}
		else if(Config.OBJECTIVE == 1 ){
			double temp= compareOnTotalWork(idCostObject, idCostOther);
			return (temp > 0? false : true);
		}
		return ans;

	}

	@Override
	public boolean IsNull(int idCostObject) {
		//it null cost object is set in constructor
		return idCostObject <= 0;
	}

	@Override
	/**done_28may'15
	 * checks whether the cost values at id idCostObject are valid nmeric value or not
	 */
	public boolean IsValid(int idCostObject) {
		
			boolean isnan = Double.isNaN(_RT_CostObjList_last.get(idCostObject)) || 
							Double.isNaN(_processing_CostObjList_last.get(idCostObject)) || 
							Double.isNaN(_mem_CostObjList_last.get(idCostObject)) || 
							Double.isNaN(_disk_CostObjList_last.get(idCostObject)) || 
							Double.isNaN(_network_CostObjList_last.get(idCostObject)) ||
							
							Double.isNaN(_RT_CostObjList_first.get(idCostObject)) || 
							Double.isNaN(_processing_CostObjList_first.get(idCostObject)) || 
							Double.isNaN(_mem_CostObjList_first.get(idCostObject)) || 
							Double.isNaN(_disk_CostObjList_first.get(idCostObject)) || 
							Double.isNaN(_network_CostObjList_first.get(idCostObject)) ||
							Double.isNaN(_TotalWork_CostObjList.get(idCostObject));
			
			boolean isInfinity = Double.isInfinite(_RT_CostObjList_last.get(idCostObject)) || 
							Double.isInfinite(_processing_CostObjList_last.get(idCostObject)) || 
							Double.isInfinite(_mem_CostObjList_last.get(idCostObject)) || 
							Double.isInfinite(_disk_CostObjList_last.get(idCostObject)) || 
							Double.isInfinite(_network_CostObjList_last.get(idCostObject)) ||
							
							Double.isInfinite(_RT_CostObjList_first.get(idCostObject)) || 
							Double.isInfinite(_processing_CostObjList_first.get(idCostObject)) || 
							Double.isInfinite(_mem_CostObjList_first.get(idCostObject)) || 
							Double.isInfinite(_disk_CostObjList_first.get(idCostObject)) || 
							Double.isInfinite(_network_CostObjList_first.get(idCostObject))||
							Double.isInfinite(_TotalWork_CostObjList.get(idCostObject));
			
			boolean nonNegative=_RT_CostObjList_last.get(idCostObject) >=0.0  &&
							_processing_CostObjList_last.get(idCostObject) >=0.0 && 
							_mem_CostObjList_last.get(idCostObject) >=0.0 &&
							_disk_CostObjList_last.get(idCostObject) >=0.0 &&
							_network_CostObjList_last.get(idCostObject) >=0.0 &&
							
							_RT_CostObjList_first.get(idCostObject) >=0.0 &&
							_processing_CostObjList_first.get(idCostObject) >=0.0 && 
							_mem_CostObjList_first.get(idCostObject) >=0.0 && 
							_disk_CostObjList_first.get(idCostObject) >=0.0 && 
							_network_CostObjList_first.get(idCostObject) >=0.0 &&
							_TotalWork_CostObjList.get(idCostObject)>=0.0;
			
			
			boolean firstLast = _mem_CostObjList_first.get(idCostObject) <= _mem_CostObjList_last.get(idCostObject) && 
								_processing_CostObjList_first.get(idCostObject) <= _processing_CostObjList_last.get(idCostObject) && 
								_network_CostObjList_first.get(idCostObject) <=_network_CostObjList_last.get(idCostObject) &&
								_disk_CostObjList_first.get(idCostObject) <= _disk_CostObjList_last.get(idCostObject) && 
								_RT_CostObjList_first.get(idCostObject) <= _RT_CostObjList_last.get(idCostObject);
			
			
			return nonNegative && (!isInfinity) && (!isnan) && firstLast ;
			// index 0 corresponds to null
			// return idCostObject > 0 && idCostObject < cost_list.size() &&
		// cost_list.get(idCostObject) >= 0;
	}

	@Override
	/**
	 * done
	 */
	public boolean IsValidPlanCost(int idCostObject) {
		return IsValid(idCostObject) && IsLessThan(idCostObject, MaxCostId());
	}

	@Override
	public int MaxCostId() {
		return _maxCostId; // stored at index 2 of cost_arrays
	}

	@Override
	public int MinCostId() {
		return _minCostId; // stored at index 3 of cost arrays
	}

	@Override
	public void SetToMax(int idCostObject) {
		Assign(idCostObject, _maxCostId);
	}

	@Override
	public void SetToZero(int idCostObject) {
		Assign(idCostObject, _zeroCostId);
	}

	@Override
	public int Subtract(int idCostObject, int idCostOther) {
		assert false:" 'Subtract' not Implemented yet in responce time based cost model, but it is called";
		try {
			throw new Exception(" 'Subtract' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO : need to implement for new cost model, for that understand its use in MQO or greedy, and remove above throw
		//previous code
		final double first = _RT_CostObjList_first.get(idCostObject);
		final double second = _RT_CostObjList_first.get(idCostOther);
		double difference = 0;

		if (first <= Double.MIN_VALUE)
			difference = Double.MIN_VALUE;
		else
			difference = first - second;

		final int index = Alloc();
		_RT_CostObjList_first.set(index, difference);
		return index;
	}

	@Override
	public void SubtractAndAssign(int idCostObject, int idCostOther) {
		assert false:" 'SubtractAndAssign' not Implemented yet in responce time based cost model, but it is called";
		try {
			throw new Exception(" 'Subtract' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO : need to implement for new cost model, for that understand its use in MQO or greedy, and remove above throw
		//previous code
		final double first = _RT_CostObjList_first.get(idCostObject);
		final double second = _RT_CostObjList_first.get(idCostOther);
		double difference = 0;

		if (first <= Double.MIN_VALUE)
			difference = Double.MIN_VALUE;
		else
			difference = first - second;

		_RT_CostObjList_first.set(idCostObject, difference);
	}

	//done_28may'15
	@Override
	public int Times(int idCostObject, double multiplier) {
		/*final double rt = _RT_CostObjList_first.get(idCostObject);
		final double proc = _processing_CostObjList_first.get(idCostObject)*multiplier;
		final double mem = _mem_CostObjList_first.get(idCostObject)*multiplier;
		final double disk = _disk_CostObjList_first.get(idCostObject)*multiplier;
		final double network = _network_CostObjList_first.get(idCostObject)*multiplier;
		
		final double rtl = _RT_CostObjList_first.get(idCostObject)*multiplier;
		final double procl = _processing_CostObjList_first.get(idCostObject)*multiplier;
		final double meml = _mem_CostObjList_first.get(idCostObject)*multiplier;
		final double diskl = _disk_CostObjList_first.get(idCostObject)*multiplier;
		final double networkl = _network_CostObjList_first.get(idCostObject)*multiplier;
		*/
		assert false: "implemented but not used in optimization algorithm(volcano_RTbased) implementation using RT cost model. So check whether the implementation suits requirement before using";
		double [] rl = getLastTuple_ResourceVector(idCostObject);
		double [] rf = getFirstTuple_ResourceVector(idCostObject);
		
		for(int i=0;i<rl.length;i++)
		{
			rl[i]=min(Double.MAX_VALUE,rl[i]*multiplier);
			rf[i]=min(Double.MAX_VALUE,rf[i]*multiplier);			
		}					
		
		return Alloc(rf, rl, min(Double.MAX_VALUE, _TotalWork_CostObjList.get(idCostObject) * multiplier) );
	}

	//done_28may'15
	@Override
	public void TimesAndAssign(int idCostObject, double multiplier) {
		assert false: "implemented but not used in optimization algorithm implementation(volcano_RTbased) using RT cost model. So check whether the implementation suits requirement before using";
		double [] rl = getLastTuple_ResourceVector(idCostObject);
		double [] rf = getFirstTuple_ResourceVector(idCostObject);
		
		for(int i=0;i<rl.length;i++)
		{
			rl[i]=min(Double.MAX_VALUE,rl[i]*multiplier);
			rf[i]=min(Double.MAX_VALUE,rf[i]*multiplier);			
		}
			setFirstTuple_ResourceVector(idCostObject, rf);
			setLastTuple_ResourceVector(idCostObject,rl);
			_TotalWork_CostObjList.set(idCostObject,  min(Double.MAX_VALUE, _TotalWork_CostObjList.get(idCostObject) * multiplier));		
	}

	@Override
	public String toString(int idCostObject) {
		StringBuilder str = new StringBuilder("");
		//str.append("{ Resource vector of first tuple (rf),  Resource vector of last tuple (rl) }: \n");
		str.append("{ (" + Double.toString(_RT_CostObjList_first.get(idCostObject)) + ", ");
		//str.append("\n");
		//str.append("Processing latency: ");
		str.append(Double.toString(_processing_CostObjList_first.get(idCostObject))+ ", ");
		//str.append("\n");
		//str.append("Mamery Latency: ");
		str.append(Double.toString(_mem_CostObjList_first.get(idCostObject))+ ", ");
		//str.append("\n");
		//str.append("Disk latency: ");
		str.append(Double.toString(_disk_CostObjList_first.get(idCostObject))+ ", ");
		//str.append("\n");
		//str.append("Network latency: ");
		str.append(Double.toString(_network_CostObjList_first.get(idCostObject))+ ")");
		str.append(",   ");
		
		
		str.append("(" + Double.toString(_RT_CostObjList_last.get(idCostObject)) + ", ");
		//str.append("\n");
		//str.append("Processing latency: ");
		str.append(Double.toString(_processing_CostObjList_last.get(idCostObject))+ ", ");
		//str.append("\n");
		//str.append("Mamery Latency: ");
		str.append(Double.toString(_mem_CostObjList_last.get(idCostObject))+ ", ");
		//str.append("\n");
		//str.append("Disk latency: ");
		str.append(Double.toString(_disk_CostObjList_last.get(idCostObject))+ ", ");
		//str.append("\n");
		//str.append("Network latency: ");
		str.append(Double.toString(_network_CostObjList_last.get(idCostObject))+ ") }");
				
		str.append(",  \nTotalWork= ");
		str.append(getTotalWork_usingCostModelOrd(idCostObject));
		
		str.append(",  \nSum of resource components= ");
		str.append(getSumOfAllResourseComponents(idCostObject));
		str.append("\n");
		
		return str.toString();
	}

	@Override
	public int ZeroCostId() {
		return _zeroCostId;  //index 1 of the arrays.
	}

	//done_28may'15
	@Override
	public void computeLocalCost(int idCost, Object physicalOperator, Object logicalOperator, 
			Object logProperties, Object[] childrenLogicalProperties, 
			Object reqdProperties, Object[] childrenPhysicalProperties) {

		_smemoryLatency.setValue(0);
		_sprocessingLatency.setValue(0);
		_sdiskLatency.setValue(0);
		_snetworkLatency.setValue(0);
					
		Config.OpHelper.computeLocalCost_FirstTuple(physicalOperator, logicalOperator, logProperties, childrenLogicalProperties, reqdProperties,
				childrenPhysicalProperties, _smemoryLatency, _sprocessingLatency, _sdiskLatency, _snetworkLatency);
		
		double memoryCost_rf = _smemoryLatency.getValue();
		double processingCost_rf = _sprocessingLatency.getValue();
		double diskCost_rf = _sdiskLatency.getValue();
		double networkCost_rf = _snetworkLatency.getValue();
		//first tuple cost
		double[] rf= {max(processingCost_rf,memoryCost_rf,diskCost_rf, networkCost_rf), processingCost_rf, memoryCost_rf, diskCost_rf, networkCost_rf};
		
		
		_smemoryLatency.setValue(0);
		_sprocessingLatency.setValue(0);
		_sdiskLatency.setValue(0);
		_snetworkLatency.setValue(0);
		
		Config.OpHelper.computeLocalCost(physicalOperator, logicalOperator, logProperties, childrenLogicalProperties, reqdProperties,
				childrenPhysicalProperties, _smemoryLatency, _sprocessingLatency, _sdiskLatency, _snetworkLatency);
		

		double memoryCost = _smemoryLatency.getValue();
		double processingCost = _sprocessingLatency.getValue();
		double diskCost = _sdiskLatency.getValue();
		double networkCost = _snetworkLatency.getValue();
		double totalWork=min( (memoryCost+processingCost+diskCost+networkCost), Double.MAX_VALUE);
		
		// last tuple cost
		double[] rl={max(processingCost,memoryCost,diskCost, networkCost), 
				processingCost, memoryCost, diskCost, networkCost};
				
		assert isValidCostFirstLast(rf,rl):"First Greater than last Physical Tag:"+ physicalOperator.getClass().getName()+
		"\n Logical Tag :"+ logicalOperator.getClass().getName()+"\n"+printArray(rf, rl);
				
		setFirstTuple_ResourceVector(idCost,rf);
		setLastTuple_ResourceVector(idCost, rl);	
		_TotalWork_CostObjList.set(idCost, totalWork);
		assert IsValid(idCost):"computed local cost became invalid for "+ physicalOperator.getClass().getName()+
		"\n Logical Tag :"+ logicalOperator.getClass().getName()+"\n"+printArray(rf, rl) +" "+ totalWork;
	}
	
	private String printArray(double [] rf,double[] rl)
	{
		return (rf[0]+"<-->"+rl[0]+"\n"+rf[1]+"<-->"+rl[1]+"\n"+rf[2]+"<-->"+rl[2]+"\n"+rf[3]+"<-->"+rl[3]+"\n"+rf[4]+"<-->"+rl[4]);
		
	}
	

	
	private boolean isValidCostFirstLast(double[] rf, double[] rl) {
			
		return (rf[1]<=rl[1] && rf[2]<=rl[2]&&rf[3]<=rl[3]&&rf[4]<=rl[4]);
	}
	
	//done_28may15 no need to modify for computing total work component. 
	@Override
	public boolean computePartialCost(int idPartialCost, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost) {			

		//here it is assumed that all operators on cost object have zerocostobject as their identity element
		//so for those children whose costid is equal to nullcostid, we set them to zero in this array,
		//so just ignore them by assuming zero and calculate total cost
		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				if ( IsNull(idArrChildrenCost[i])  )        //idArrChildrenCost[i] == ICostModel.NULL) or negative 
					idArrChildrenCost[i]=ZeroCostId();
			}
		}
				
		boolean tmp = computeTotalCost(idPartialCost, physicalOperator, idOperatorLocalCost, idArrChildrenCost);
		assert IsValid(idPartialCost) : "cost became invalid in computePartialCost";
		return tmp;
	}

	/*done_28may'15
	 * The Operation tree(L,R,root)
	 * cost of tree(L,R,root) = t where 
	 * t1 = (Lf || Rf, Lf||Rf)
	 * t2 = t1;(0,Ll#Lf)|(0,Rl#Rf)
	 * 			# is Subtraction of materialized front  rfirst - rlast
	 * t = t2|root 
	 *	if Single Child for root
	 *	t = L|root
	 *
	 *Apply sync(t) if Tree is materialized 
	 * @see pyroj.costmodel.ICostModel#computeTotalCost(int, java.lang.Object, int, int[])
	 */
	@Override
	public boolean computeTotalCost(int idTotalCost, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost) {
		assert IsValid(idTotalCost) : "idTotalCost: " + idTotalCost + " came invalid in computeTotalCost" ;
		
		double[] leftChildFirst;
		double[] leftChildLast;
		double[] rightChildFirst;
		double[] rightChildLast;
		
		int idTotalCostTemp= -1;//AllocZeroCostObj();
		if (idArrChildrenCost != null) 
		{
			leftChildFirst = getFirstTuple_ResourceVector(idArrChildrenCost[0]);
			leftChildLast = getLastTuple_ResourceVector(idArrChildrenCost[0]);
			if(idArrChildrenCost.length==1)
			{
				idTotalCostTemp = computePipelinedExecutionCost(idArrChildrenCost[0],idOperatorLocalCost);
				//--------------compute resulting total work using simple addition
				double totalwork= getTotalWork_usingCostModelOrd(idArrChildrenCost[0])+getTotalWork_usingCostModelOrd(idOperatorLocalCost);
				totalwork=min(totalwork, Double.MAX_VALUE);
				_TotalWork_CostObjList.set(idTotalCostTemp, totalwork);
				//---
				assert IsValid(idTotalCostTemp) : "cost became invalid in computeTotalCost";
			}
			else
			{
				rightChildFirst = getFirstTuple_ResourceVector(idArrChildrenCost[1]);
				rightChildLast = getLastTuple_ResourceVector(idArrChildrenCost[1]);
				
				//t1first and t1last is same
				double[] t1first = add_parallel(leftChildFirst,rightChildFirst);
				
				double [] zero={0,0,0,0,0};
				int idTemp1= Alloc(zero,subtract_materizedFront(leftChildLast, leftChildFirst),0);
				int idTemp2= Alloc(zero,subtract_materizedFront(rightChildLast, rightChildFirst),0);
				int idTemp3 = computePipelinedExecutionCost(idTemp1,idTemp2);
				assert IsValid(idTemp1) : "idTemp1: " + idTemp1 + " became invalid in computeTotalCost" ;
				assert IsValid(idTemp2) : "idTemp2: " + idTemp2 + " became invalid in computeTotalCost" ;
				assert IsValid(idTemp3) : "idTemp3: " + idTemp3 + " became invalid in computeTotalCost" ;
				
				int t2 = Alloc(add_seq(t1first, getFirstTuple_ResourceVector(idTemp3)), add_seq(t1first, getLastTuple_ResourceVector(idTemp3)),0);
				
				assert IsValid(t2) : "t2: " + t2 + " became invalid in computeTotalCost" ;
				
				idTotalCostTemp = computePipelinedExecutionCost(t2, idOperatorLocalCost);
				
				//--------------compute resulting total work using simple addition
				double totalwork= getTotalWork_usingCostModelOrd(idArrChildrenCost[0])+getTotalWork_usingCostModelOrd(idArrChildrenCost[1])+getTotalWork_usingCostModelOrd(idOperatorLocalCost);
				totalwork=min(totalwork, Double.MAX_VALUE);
				_TotalWork_CostObjList.set(idTotalCostTemp, totalwork);
				//---
				
				
				assert IsValid(idTotalCostTemp) : "cost became invalid in computeTotalCost";
			
				//idTotalCost = t;
				Free(idTemp1);
				Free(idTemp2);
				Free(idTemp3);
			}
			if((physicalOperator.getClass().getName().equals(SortExec)) || physicalOperator.getClass().getName().equals(PhysicalOperatorTagsForSpark.HashAggregateExec) )
			{
				sync(idTotalCostTemp);
				assert IsValid(idTotalCostTemp) : "cost became invalid in computeTotalCost";
			}
			
			/*
			if(IsEqual(idTotalCost, idTotalCostTemp))
			{
				Free(idTotalCostTemp);
				return false;
			}*/
			
			Assign(idTotalCost, idTotalCostTemp);
			
			assert IsValid(idTotalCost) : "cost became invalid in computeTotalCost";
			
			Free(idTotalCostTemp);
			return true;
		}
		
		return false;
		
		// Here add Sync operator if Tree is materialized, 
		
		//TODO: We may have ArrayIndexOutofBound here, need to check if that case arises.
		//In IsEqual to function, if idTotalCostTemp remains =-1
		
		// Also we also need to clarify if we need to compare whole vector or just totalCost.
		
		/*
		
		double totalCost = _sCostObjList.get(idOperatorLocalCost);

		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				totalCost += _sCostObjList.get(idArrChildrenCost[i]);
			}
		}
		
		if (_sCostObjList.get(idTotalCost) == totalCost)
			return false;
		_sCostObjList.set(idTotalCost, totalCost);
		return true;
		*/
	}
	
	
	public boolean isCorrectMaxRt(double [] list){
		
		double maxRT = list[0];
		double max=0.0;
		for(int i=1;i<5;i++){
			if(list[i] > max)
				max=list[i];
		}
		return maxRT<=max;
	}
	
	/*done_28may'15
	 * rewritten by Mayuri on 17 Feb'15 to avoid problem of result having -ve values
	 * Note: if value of any component in remainign cost comes <0 after calculation, then it should be considered as zero, 
	 * as it is remaining UB, so -ve UB means zero remaining cost allowed
	 * 
	 * For details of formulae used, please Refer to MTP Stage 1 Report of Mayuri.
	 * Section 4.2
	 * (non-Javadoc)
	 * @see pyroj.costmodel.ICostModel#computeRemainingCostUB(int, java.lang.Object, int, int[], int)
	 */
	@Override
	public boolean computeRemainingCostUB(int idRemainingCostUB, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost, int idCostUB) {

		//TODO
		//in response the based cost model, cost for base cases as scan or join, cost is calculated as total work done
		//but its not the case with other operators. so need to check physical operator & calculate using our cost model operators
		//so to calculate remaining cost we need not subtract cost of root and all children from UB.
		
		assert ((CostModel_RT)(Config.CostModel)).IsValid(idRemainingCostUB) : "cost came invalid in computeRemainingCostUB for costid: " +idRemainingCostUB;
		
		//double k = 0.2;
		//as we dont know the exact value of d_k , as we dont have value for (a,b)||(e,f), since (a,b) is still to be calculated
		// We will consider thet max value d_k = 1+k; k is to be read from config file. Currently we will set it to 0.2 by defualt
		double d_k = getDelta_k(); //1+k; //TODO: write and call this function calculate_d_k();
		double [] localFirst = getFirstTuple_ResourceVector(idOperatorLocalCost);  // e
		double [] localLast = getLastTuple_ResourceVector(idOperatorLocalCost); // f
		double [] costUBLast = getLastTuple_ResourceVector(idCostUB); // v
		double[] remainingCostLast = new double[RVECTOR_SIZE];
		double remainingWork=0.0;
		if(idArrChildrenCost[0]==-1){ 	//calculate remaining cost UB for left child
					//referring to formula in report, remainingCostLast  is b here
					for(int i=1;i<remainingCostLast.length;i++)
					{	//if value comes -ve then should be considered as zero as it is remaining UB
						remainingCostLast[i]= max(0.0, ((costUBLast[i]-localFirst[i])/(d_k))-(localLast[i]-localFirst[i]) ); 
					}
					if(idArrChildrenCost.length==2){ //means root operator has two children and we are calculating for 1st(left) of them
						for(int i=1;i<remainingCostLast.length;i++)
						{
							remainingCostLast[i]= remainingCostLast[i] / d_k; 
						}
					}
					remainingCostLast[0]=max(remainingCostLast); //b computed
					
					//--------------compute remaining UB for total work 
					remainingWork = max(0.0, (getTotalWork_usingCostModelOrd(idCostUB)-getTotalWork_usingCostModelOrd(idOperatorLocalCost)) );										
					//---
		}
		else if(idArrChildrenCost[0]>0){	//calculate remaining cost UB for right child and idArrChildrenCost[0] has costid of left child

					double [] leftFirst = getFirstTuple_ResourceVector(idArrChildrenCost[0]);  // a
					double [] leftLast = getLastTuple_ResourceVector(idArrChildrenCost[0]); // b
					
					//refering to formula in report, remainingCostLast  is y here
					for(int i=1;i<remainingCostLast.length;i++)
					{				//if value comes -ve then should be considered as zero as it is remaining UB
						double y_i = ( (  ((costUBLast[i]-leftFirst[i]-localFirst[i])/d_k) - (localLast[i]-localFirst[i])   )/ d_k) - (leftLast[i]-leftFirst[i]);
						remainingCostLast[i]= max(0.0, y_i ); 
					}
					remainingCostLast[0]=max(remainingCostLast); //y has been computed now
					
					//--------------compute remaining UB for total work 
					remainingWork = max(0.0, (getTotalWork_usingCostModelOrd(idCostUB)-getTotalWork_usingCostModelOrd(idOperatorLocalCost)-getTotalWork_usingCostModelOrd(idArrChildrenCost[0])) );										
					//---
		}
		else{
			//it should never come here
			assert false : "invalid cost id has come as parameter in idArrChildrenCost to function computeRemainingCostUB";
		}
		
		/*int tempid = AllocZeroCostObj();
		setLastTuple_ResourceVector(tempid, remainingCostLast);
		_TotalWork_CostObjList.set(tempid, remainingWork);
		
		
		if(IsEqual(idRemainingCostUB, tempid))
		{
			Free(tempid);
			return false;
		}*/
		setLastTuple_ResourceVector(idRemainingCostUB, remainingCostLast); //resulting remaining cost is (0,b) or (0,y)
		_TotalWork_CostObjList.set(idRemainingCostUB, remainingWork);
		//Free(tempid);
		

		assert ((CostModel_RT)(Config.CostModel)).IsValid(idRemainingCostUB) : "cost became invalid in computeRemainingCostUB for costid: " +idRemainingCostUB;
		return true;
		
	}

	//done_28may'15
	@Override
	public int computeUseCost(Object logicalProperties,Object physicalProperty) {
		
		MutableDouble memoryLatency 	= new MutableDouble(0);
		MutableDouble processingLatency = new MutableDouble(0);
		MutableDouble diskLatency 		= new MutableDouble(0);
		MutableDouble networkLatency 	= new MutableDouble(0);
		
		Config.OpHelper.computeUseCost(logicalProperties,physicalProperty, memoryLatency, processingLatency, diskLatency, networkLatency);
		double memoryCost = memoryLatency.getValue();
		double processingCost = processingLatency.getValue();
		double diskCost = diskLatency.getValue();
		double networkCost = networkLatency.getValue();
		
		
		//Response time = maximum (all resource cost)
		// for last tuple
		double[] rl={max(processingCost,memoryCost,diskCost, networkCost), 
				processingCost, memoryCost, diskCost, networkCost};
				
		//for first tuple, we are setting hack as first tuple with cost zero. we can add 
		// function to get first tuple materialization cost.Cost will be equal to processing one block. 
		double[] rf = {0,0,0,0,0};
		
		//--compute total work using simple total sum
		double totalwork=memoryCost+processingCost+diskCost+networkCost;
		totalwork = min(totalwork, Double.MAX_VALUE);
		//--
		
		return Alloc(rf, rl, totalwork);
		//Response time = maximum (all resource cost)
		
	}

	//done_28may'15
	@Override
	public int computeMaterializationCost(Object logicalOperator,
			Object logProperties, Object physicalOperator, Object physicalProperty) {
		
		MutableDouble memoryLatency 	= new MutableDouble(0);
		MutableDouble processingLatency = new MutableDouble(0);
		MutableDouble diskLatency 		= new MutableDouble(0);
		MutableDouble networkLatency 	= new MutableDouble(0);
		
		Config.OpHelper.computeMaterializationCost(logicalOperator, logProperties, physicalOperator, physicalProperty , memoryLatency,
					processingLatency, diskLatency, networkLatency);
		
		double memoryCost = memoryLatency.getValue();
		double processingCost = processingLatency.getValue();
		double diskCost = diskLatency.getValue();
		double networkCost = networkLatency.getValue();
		
		
		//Response time = maximum (all resource cost)
		// for last tuple
		double[] rl={max(processingCost,memoryCost,diskCost, networkCost), 
				processingCost, memoryCost, diskCost, networkCost};
		
		Config.OpHelper.computeMaterializationCost_FirstTuple(logicalOperator, logProperties, physicalOperator, physicalProperty , memoryLatency,
				processingLatency, diskLatency, networkLatency);
	
		memoryCost = memoryLatency.getValue();
		processingCost = processingLatency.getValue();
		diskCost = diskLatency.getValue();
		networkCost = networkLatency.getValue();
	
		//for first tuple, we are setting hack as first tuple with cost zero. we can add 
		// function to get first tuple materialization cost.Cost will be equal to processing one block. 
		
		double[] rf={max(processingCost,memoryCost,diskCost, networkCost), 
			processingCost, memoryCost, diskCost, networkCost};

		//--compute total work using simple total sum
		double totalwork=memoryCost+processingCost+diskCost+networkCost;
		totalwork = min(totalwork, Double.MAX_VALUE);
		//--
		
		return Alloc(rf, rl, totalwork);
	}
	
	public double max(double a, double... args){
		double max = a;
		for(double arg : args){
			if(arg > max)
				max=arg;
		}
		
		return max; 
	}
	
	public double max(double [] list)
	{
		double max = list[0];
		
		for(double arg : list){
			if(arg > max)
				max=arg;
		}
		
		return max;
		
	}
	public double min(double a, double... args){
		double min = a;
		for(double arg : args){
			if(arg < min)
				min=arg;
		}
		return min; 
	}
	
	/**done_28may'15
	 * operator | i.e Dependent parallel execution on resource descriptor
	 * @param idCostObject_P
	 * @param idCostObject_Q
	 * @return id of resulting cost object
	 * P=(pf,pl) C=(cf,cl) are two resource descriptors for producer and consumer in pipelined execution
	 * So result = P|C = (rf,rl)
	 * rf = pf;cf
	 * rl = pf;cf;d(k)*((pl-pf) || (cl-cf))
	 * d(k) = 1+ k*(t' - max(t1,t2))/(t1+t2-max(t1,t2))
	 * where (t1, Wp)=(pl-pf), (t2, Wc) = (cl-cf) and (t', W) = ((pl-pf) || (cl-cf)) 
	 */
	private int computePipelinedExecutionCost(int idCostObject_P, int idCostObject_C){
		// TODO : set value of k in config file and read it from there, also decde for its appropriate value
		
		assert IsValid(idCostObject_P) : "producer cost came invalid in computePipelinedExecutionCost:" + idCostObject_P;
		assert IsValid(idCostObject_C) : "consumer cost came invalid in computePipelinedExecutionCost:" +idCostObject_C;
		
		//--------------compute resulting total work using simple addition
		double totalwork= getTotalWork_usingCostModelOrd(idCostObject_P)+getTotalWork_usingCostModelOrd(idCostObject_C);
		totalwork=min(totalwork, Double.MAX_VALUE);
		//---
		
		if(Is_VectorCost_Equal(idCostObject_P, _zeroCostId)) { 
			int tempCostId = AllocZeroCostObj();
			Assign(tempCostId, idCostObject_C);
			_TotalWork_CostObjList.set(tempCostId, totalwork);
			return tempCostId;
			//if idCostObject_P(producer) is zero then the total pipelined cost will be equal to cost value of idCostObject_C, 
			//so we are returning directly. It handles both cases: 1. P=0 and C=0  and 2. P=0 and C is non zero positive cost value
			//If we apply following formula to calculate then it gives NaN value. so this check should be done first
		}
		
		
		
		double k=0.2;
		double[] pf = new double[5];
		double[] pl = new double[5];
		double[] cf = new double[5];
		double[] cl = new double[5];
		
		pf=getFirstTuple_ResourceVector(idCostObject_P);
		pl=getLastTuple_ResourceVector(idCostObject_P);
		cf=getFirstTuple_ResourceVector(idCostObject_C);
		cl=getLastTuple_ResourceVector(idCostObject_C);
			
		if (pl[0] >= Double.MAX_VALUE || cl[0] >= Double.MAX_VALUE){
			int idCost= AllocMaxCostObj();			
			_TotalWork_CostObjList.set(idCost, totalwork);
			return idCost;
		}
			
		else{		
			double[] s1 = subtract_materizedFront(pl, pf); //(t1, Wp)=(pl-pf), t1=s1[0]
			assert isCorrectMaxRt(s1): "Response time is not valid here S1"+s1;
			double[] s2 = subtract_materizedFront(cl, cf); //(t2, Wc) = (cl-cf) , t2=s2[0]
			assert isCorrectMaxRt(s2): "Response time is not valid here S2"+s2;
			double[] s3 = add_parallel(s1, s2); //(t', W) = ((pl-pf) || (cl-cf)), t'=s3[0]
			assert isCorrectMaxRt(s3): "Response time is not valid here S3"+s3;
			double d_k = 1 + k*(s3[0] - Math.max(s1[0], s2[0])) / ( s1[0]+s2[0] - Math.max(s1[0], s2[0]) ); //getDelta_k();

			//--
			//this is just a bad hack to remove null pointer exception due to NaN coming due to -ve values
			if(Double.isNaN(d_k)){
				d_k = 1;
			}
			//--
			
			double[] rf = add_seq(pf, cf);
			// rl = add_seq(rf, scalar_multiplication(s3, d_k));
			int resultCostID = Alloc(rf, add_seq(rf, scalar_multiplication(s3, d_k)), totalwork);
			
			assert isCorrectMaxRt(getFirstTuple_ResourceVector(resultCostID)): "Response time is not valid here S4"+getFirstTuple_ResourceVector(resultCostID);
			assert isCorrectMaxRt(getLastTuple_ResourceVector(resultCostID)): "Response time is not valid here S4"+getLastTuple_ResourceVector(resultCostID);
			/*----- as a experiment, multiplying only RT element in s3 by d_k instead of scalar_multiplication(s3, d_k) 
			s3[0]= s3[0]*d_k;
			int resultCostID = Alloc(rf, add_seq(rf, s3), totalwork);
			-----*/
			assert IsValid(resultCostID) : "cost(id: "+resultCostID+") became invalid in computePipelinedExecutionCost for producer"+ idCostObject_P + "consumer: "+ idCostObject_C;
			return resultCostID;
		}
	}
	
	private double getDelta_k() {
		double k =0.2; // later declare k in config file and read it from there
		//Currently using worst case value of delta_K. Later use proper formula given in ganguly to calculate value of delta_k dynamically
		delta_k = 1+k;
		return delta_k;
	}



	/**
	 * Should be called for already existing cost object present at index idCostObject
	 * @param idCostObject_P is index of resource descriptor (rf,rl)
	 * @return index of resulting resource descriptor as (rl,rl) i.e rf=rl
	 */
	
	private void sync(int idCostObject){
		//TODO do error exception handling
		double[] rl = getLastTuple_ResourceVector(idCostObject);
		setFirstTuple_ResourceVector(idCostObject, rl);
	}
	public static void main1(String[] args) throws Exception {
		CostModel_RT rt = new CostModel_RT();
		/*System.out.println("zerocostId: "+ rt._zeroCostId + "maxcostId:  "+ rt._maxCostId + " mincostId: " + rt._minCostId);
		System.out.println(" values at zerocost Id: " + 
		rt._RT_CostObjList_first.get(rt._zeroCostId)+ 
		rt._disk_CostObjList_first.get(rt._zeroCostId) +
		rt._mem_CostObjList_first.get(rt._zeroCostId) +
		rt._processing_CostObjList_first.get(rt._zeroCostId)+
		rt._network_CostObjList_first.get(rt._zeroCostId));
		
		System.out.println("max is: " + rt.max(4,11.56,1.1,Double.MAX_VALUE));
		*/
		double nan = 0.0/0.0;
		double[] rf = {nan,nan,nan,nan,nan };
		int tempcostid = rt.Alloc(rf, rf, nan);
		System.out.println("cost object tempcostid in main isvalid  : "+ rt.IsValid(tempcostid));
		//assert rt.IsValid(tempcostid) : "cost object tempcostid in main is invalid";
		
		double a=1.4980776123852631E308;
		System.out.println("a: "+a);
		double b=a+a;
		
		System.out.println("b=a+a: "+b);
		b+=a;
		System.out.println("b=a+a: "+b);
		if (b >= Double.MAX_VALUE){
			System.out.println("b>=maxvalue: "+Double.MAX_VALUE);
		}else{
			System.out.println("b=a+a: "+b);
		}
		
		System.out.println("is minval > 0"+ Double.MIN_VALUE + " "+( Double.MIN_VALUE > 0.0));
	}
	public static void main(String[] args){
		System.out.println(Double.MAX_VALUE/2+Double.MAX_VALUE/2);
		double a=Double.MAX_VALUE;
		double b=1;
		int c=1;
		double d=a+100;
		System.out.println("a==d? "+ (a==d));
		System.out.println((Double.MAX_VALUE+1));
		System.out.println(a+b);
		System.out.println(a+c);
		System.out.println("------------");
		for(double i=10; ;i*=10){
			System.out.println(i+ " : "+ (a+i)+ "  "+(a==(a+i)));
		}
		
	}
	//-----------------------------utility functions-----------------------------------	

	public double getResponseTime(int idCostObj){
		return _RT_CostObjList_last.get(idCostObj);
	}
	
	/*
	 * done_28may'15
	 * to calculate total work to be stored in _TotalWork_CostObjList, always use this function NOT  'getTotalWork_usingCostModel_RT'
	 */
	public double getTotalWork_usingCostModelOrd(int idCostObject){
		return _TotalWork_CostObjList.get(idCostObject);		
	}
	
	/*
	 * done_28may'15
	 */
	public double getSumOfAllResourseComponents(int idCostObject){
		double tw=0.0;
		tw += _processing_CostObjList_last.get(idCostObject) + _mem_CostObjList_last.get(idCostObject);			
		tw += _disk_CostObjList_last.get(idCostObject) + _network_CostObjList_last.get(idCostObject);
		tw=min(tw, Double.MAX_VALUE);
		return tw;
	}
	public double getObjectiveCost(int idCostObj){
		if(Config.OBJECTIVE == 1){ //total cost
			return getTotalWork_usingCostModelOrd(idCostObj);
		}
		else if(Config.OBJECTIVE==2){ //Responce time
			return getResponseTime(idCostObj);
		}
		else{
			assert false:"undefined objective "+Config.OBJECTIVE;
			return -9999999;
		}
	}
	
	//-----------------------------operations on resource vector-------------all these operations are specifically used for objective=2, so it need not be modified for computing totalWOrk component of cost object---------------------	
	/**seq execution i.e. ;
	 * modified on 17Feb'15 to remove problem of infinity values
	 *@param rfirst (t1, w1)
	 * @param rsec (t2, w2)
	 * @return result resource vector = ( t1+t2, w1+w2 )
	 */
	private double[] add_seq(double[] rfirst, double[] rsec) {
		double[] result = new double[5];
		double max =0.0;
		//if rfirst and rsec had values Double.MAX_VALUE, then their additio will become infinity, so coded as below
		for(int i=1;i<5;++i){
			result[i] = min(rfirst[i]+rsec[i], Double.MAX_VALUE);
			if(max<result[i])
				max=result[i];
		}
		result[0]=max;
		return result;
		
	}

	/**IPE i.e. ||
	 * modified on 17Feb'15 to remove problem of infinity values
	 * @param rfirst (t1, w1)
	 * @param rsec (t2, w2)
	 * @return result resource vector rfirst || rsec = ( max(t1,t2,max(w1i+w2i)), w1+w2 ) 
	 */

	private double[] add_parallel(double[] rfirst, double[] rsec) {
		double[] result = new double[5];
		//if rfirst and rsec had values Double.MAX_VALUE, then their additio will become infinity, so coded as below
		double mx=min(rfirst[1]+rsec[1], Double.MAX_VALUE);
		for(int i=1;i<=4;++i){
			result[i] = min(rfirst[i]+rsec[i], Double.MAX_VALUE);
			if(result[i] > mx)
				mx=result[i];
		}
		if(rfirst[0] > mx)
			mx=rfirst[0];
		if(rsec[0] > mx)
			mx=rsec[0];
		result[0] = mx;
		return result;
	}

	
	/**CORRECT
	 * subtraction of materialized front  rfirst - rsec
	 * 
	 *@param rfirst (t1, w1)
	 * @param rsec (t2, w2)
	 * @return result resource vector = (t,w)
	 * where, w=w1-w2 and t=max(t1-t2, max(wi))
	 */
	private double[] subtract_materizedFront(double[] rfirst, double[] rsec) {
		double[] result = new double[RVECTOR_SIZE];
		result[1] = rfirst[1]-rsec[1];
		double mx=result[1];
		for(int i=2;i<RVECTOR_SIZE;++i){
			result[i] = rfirst[i]-rsec[i];
			if(result[i]>mx){
				mx = result[i];
			}
		}
		result[0]=mx;
		return result;
	}
	
	/**scalar multiplicatio rvector * k
	 * 
	 *@param rfirst (t1, w1)
	 * @return result resource vector = ( t1*k, w1*k )
	 */
	private double[] scalar_multiplication(double[] rfirst, double k) {
		double[] result = new double[5];
		for(int i=0;i<5;++i){
			result[i]=rfirst[i]*k;
		}
		return result;
	}

	
	/**
	 * @param cost object at idCostObject = (rf, rl)
	 * @return rf
	 */
	private double[] getFirstTuple_ResourceVector(int idCostObject) {
		double[] qf = new double[5];
		qf[0] = _RT_CostObjList_first.get(idCostObject);
		qf[1] = _processing_CostObjList_first.get(idCostObject);
		qf[2] = _mem_CostObjList_first.get(idCostObject);
		qf[3] = _disk_CostObjList_first.get(idCostObject);
		qf[4] = _network_CostObjList_first.get(idCostObject);
		return qf;
	}
	
	/**
	 * @param cost object at idCostObject = (rf, rl)
	 * @return rl
	 */
	private double[] getLastTuple_ResourceVector(int idCostObject) {
		double[] pl = new double[5];
		pl[0] = _RT_CostObjList_last.get(idCostObject);
		pl[1] = _processing_CostObjList_last.get(idCostObject);
		pl[2] = _mem_CostObjList_last.get(idCostObject);
		pl[3] = _disk_CostObjList_last.get(idCostObject);
		pl[4] = _network_CostObjList_last.get(idCostObject);
		return pl;
	}
		
	/**
	 * Should be called with already existing cost object present at index idCostObject 
	 * @param cost object at idCostObject = (rf, rl)
	 * @return set rf to result_f
	 */
	private void setFirstTuple_ResourceVector(int idCostObject, double[] result_f) {
		_RT_CostObjList_first.set(idCostObject, result_f[0]);
		_processing_CostObjList_first.set(idCostObject, result_f[1]);
		_mem_CostObjList_first.set(idCostObject, result_f[2]);
		_disk_CostObjList_first.set(idCostObject, result_f[3]);
		_network_CostObjList_first.set(idCostObject, result_f[4]);		
	}
	
	/**
	 * Should be called with already existing cost object present at index idCostObject
	 * @param cost object at idCostObject = (rf, rl)
	 * @return sets rl to result_l
	 */
	private void setLastTuple_ResourceVector(int idCostObject, double[] result_l) {
		_RT_CostObjList_last.set(idCostObject, result_l[0]);
		_processing_CostObjList_last.set(idCostObject, result_l[1]);
		_mem_CostObjList_last.set(idCostObject, result_l[2]);
		_disk_CostObjList_last.set(idCostObject, result_l[3]);
		_network_CostObjList_last.set(idCostObject, result_l[4]);		
	}
	
	//--Some extra functions needed which are not in ICOstModel interface
	/**
	 * 1st parameter idCostObject is id of cost LB object, 
	 * so value of all component in it represent Lower bound on that component
	 * so compare value of each component in idCostObject with corr. component in idCostOther, and set it to minimum of the two
	 * @param idCostObject  
	 * @param idCostOther :  id of cost object to be compared with
	 */
	public void setComponentToLowerValues(int idCostObject, int idCostOther){
		
		assert IsValid(idCostObject) : "cost object coming as parameter to setComponentToLowerValues function is invalid";
		
		double temp1 = min(_processing_CostObjList_first.get(idCostObject), _processing_CostObjList_first.get(idCostOther));
		_processing_CostObjList_first.set(idCostObject, temp1);
		
		double temp2 = min(_mem_CostObjList_first.get(idCostObject), _mem_CostObjList_first.get(idCostOther));
		_mem_CostObjList_first.set(idCostObject, temp2);
		
		double temp3 = min(_disk_CostObjList_first.get(idCostObject), _disk_CostObjList_first.get(idCostOther));
		_disk_CostObjList_first.set(idCostObject, temp3);
		
		double temp4 = min(_network_CostObjList_first.get(idCostObject), _network_CostObjList_first.get(idCostOther));
		_network_CostObjList_first.set(idCostObject, temp4);
		
		_RT_CostObjList_first.set(idCostObject, max(temp1,temp2,temp3,temp4));

		temp1 = min(_processing_CostObjList_last.get(idCostObject), _processing_CostObjList_last.get(idCostOther));
		_processing_CostObjList_last.set(idCostObject, temp1);
		
		temp2 = min(_mem_CostObjList_last.get(idCostObject), _mem_CostObjList_last.get(idCostOther));
		_mem_CostObjList_last.set(idCostObject, temp2);
		
		temp3 = min(_disk_CostObjList_last.get(idCostObject), _disk_CostObjList_last.get(idCostOther));
		_disk_CostObjList_last.set(idCostObject, temp3);
		
		temp4 = min(_network_CostObjList_last.get(idCostObject), _network_CostObjList_last.get(idCostOther));
		_network_CostObjList_last.set(idCostObject, temp4);
		
		_RT_CostObjList_last.set(idCostObject, max(temp1,temp2,temp3,temp4));
		
		//--total work
		temp4 = min(_TotalWork_CostObjList.get(idCostObject), _TotalWork_CostObjList.get(idCostOther));
		_TotalWork_CostObjList.set(idCostObject, temp4);
		//--
		
		assert IsValid(idCostObject) : "cost object became invalid due to (in) setComponentToLowerValues function";
	}
	
	public void setComponentToLowerValuesAndSync(int idCostObject, int idCostOther){
		setComponentToLowerValues(idCostObject, idCostOther);
		//sync(idCostObject); 
	}
	
	
	
	/**
	 * 1st parameter idCostObject is id of cost object whose component to be set to upper bound 
	 * 
	 * so compare value of each component in idCostObject with corr. component in idCostOther, and set it to maximum of the two
	 * @param idCostObject  
	 * @param idCostOther :  id of cost object to be compared with
	 */
	public void setComponentToLargerValues(int idCostObject, int idCostOther){
		
		assert IsValid(idCostObject) : "cost object coming as parameter to setComponentToLargerValues function is invalid";
		
		double temp1 = max(_processing_CostObjList_first.get(idCostObject), _processing_CostObjList_first.get(idCostOther));
		_processing_CostObjList_first.set(idCostObject, temp1);
		
		double temp2 = max(_mem_CostObjList_first.get(idCostObject), _mem_CostObjList_first.get(idCostOther));
		_mem_CostObjList_first.set(idCostObject, temp2);
		
		double temp3 = max(_disk_CostObjList_first.get(idCostObject), _disk_CostObjList_first.get(idCostOther));
		_disk_CostObjList_first.set(idCostObject, temp3);
		
		double temp4 = max(_network_CostObjList_first.get(idCostObject), _network_CostObjList_first.get(idCostOther));
		_network_CostObjList_first.set(idCostObject, temp4);
		
		_RT_CostObjList_first.set(idCostObject, max(temp1,temp2,temp3,temp4));		
		
		temp1 = max(_processing_CostObjList_last.get(idCostObject), _processing_CostObjList_last.get(idCostOther));
		_processing_CostObjList_last.set(idCostObject, temp1);
		
		temp2 = max(_mem_CostObjList_last.get(idCostObject), _mem_CostObjList_last.get(idCostOther));
		_mem_CostObjList_last.set(idCostObject, temp2);
		
		temp3 = max(_disk_CostObjList_last.get(idCostObject), _disk_CostObjList_last.get(idCostOther));
		_disk_CostObjList_last.set(idCostObject, temp3);
		
		temp4 = max(_network_CostObjList_last.get(idCostObject), _network_CostObjList_last.get(idCostOther));
		_network_CostObjList_last.set(idCostObject, temp4);
		
		_RT_CostObjList_last.set(idCostObject, max(temp1,temp2,temp3,temp4));
		
		//--total work
		temp4 = max(_TotalWork_CostObjList.get(idCostObject), _TotalWork_CostObjList.get(idCostOther));
		_TotalWork_CostObjList.set(idCostObject, temp4);
		//--
		
		assert IsValid(idCostObject) : "cost object became invalid in setComponentToLargerValues function ";
	}
	
	public void setComponentToLargerValuesAndSync(int idCostObject, int idCostOther){
		setComponentToLargerValues(idCostObject, idCostOther);
		//sync(idCostObject);
	}
	
	
	//---------------------------------These functions are written to compare two cost objects based on their vector cost past only irrespective of Config.OBJCTIVE value
	private boolean Is_VectorCost_Equal(int idCostObject, int idCostOther) {
		return Is_VectorCost_LessThanOrEqual(idCostObject, idCostOther) && Is_VectorCost_LessThanOrEqual(idCostOther, idCostObject) ; 
		
	}
	private boolean Is_VectorCost_LessThanOrEqual(int idCostObject, int idCostOther) {
				
		boolean ans =  ( _RT_CostObjList_last.get(idCostObject) <=  _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) <=  _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) <= _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) <=  _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) <=  _network_CostObjList_last.get(idCostOther) );
		
		return ans;
	}
	
	
	//--------------------------------------Implemented for Introducing Approximation------------------
	
	/**IMP NOTE:
	 * This function should be called only when APPROXIMATION_ENABLED is 'true' and using OBJECTIVE=2(response time).
	 * In case of OBJECTIVE=1, there is only one best cost for each node and not set of best costs(pareto set). 
	 * So no point in using approximation there. So this function does only vector comparison and NOT comparison based on total work
	 * So it should not be used when optimizing for  OBJECTIVE=1 i.e. total work 
	 * This function does not make any such check, it is responsibility of user of this function to check this
	 * 
	 * returns whether idCostObject < (1+e)idCostOther
	 * @param idCostObject
	 * @param idCostOther
	 * @return
	 */
	public boolean IsApproximatelyLessThan(int idCostObject, int idCostOther) {	
		boolean ans=false;
		double e = Config.APPROXIMATION_FACTOR;
		
		if(Config.OBJECTIVE == 2 ){
			boolean allEqual = ( _RT_CostObjList_last.get(idCostObject) == _RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) == _processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) == _mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) == _disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) == _network_CostObjList_last.get(idCostOther) );
			
			ans = (!allEqual) && ( _RT_CostObjList_last.get(idCostObject) <= (1+e)*_RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) <= (1+e)*_processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) <= (1+e)*_mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) <= (1+e)*_disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) <= (1+e)*_network_CostObjList_last.get(idCostOther) );
		}
		else if(Config.OBJECTIVE == 1 ){
			assert false: "IsApproximatelyLessThan() function called for OBJECTIVE =1. Read its function level comments to understand its usage";			
		}
		return ans;

	}
	
	/**IMP NOTE:
	 * This function should be called only when APPROXIMATION_ENABLED is 'true' and using OBJECTIVE=2(response time).
	 * In case of OBJECTIVE=1, there is only one best cost for each node and not set of best costs(pareto set). 
	 * So no point in using approximation there. So this function does only vector comparison and NOT comparison based on total work
	 * So it should not be used when optimizing for  OBJECTIVE=1 i.e. total work 
	 * This function does not make any such check, it is responsibility of user of this function to check this
	 * 
	 * returns whether idCostObject <= (1+e)idCostOther
	 * @param idCostObject
	 * @param idCostOther
	 * @return
	 */
	public boolean IsApproximatelyLessThanOrEqualTo(int idCostObject, int idCostOther) {	
		boolean ans=false;
		double e = Config.APPROXIMATION_FACTOR;
		
		if(Config.OBJECTIVE == 2 ){
			ans = ( _RT_CostObjList_last.get(idCostObject) <= (1+e)*_RT_CostObjList_last.get(idCostOther) && 
					_processing_CostObjList_last.get(idCostObject) <= (1+e)*_processing_CostObjList_last.get(idCostOther) &&
					_mem_CostObjList_last.get(idCostObject) <= (1+e)*_mem_CostObjList_last.get(idCostOther) &&
					_disk_CostObjList_last.get(idCostObject) <= (1+e)*_disk_CostObjList_last.get(idCostOther) &&
					_network_CostObjList_last.get(idCostObject) <= (1+e)*_network_CostObjList_last.get(idCostOther) );
		}
		else if(Config.OBJECTIVE == 1 ){
			assert false: "IsApproximatelyLessThan() function called for OBJECTIVE =1. Read its function level comments to understand its usage";			
		}
		return ans;

	}

}



