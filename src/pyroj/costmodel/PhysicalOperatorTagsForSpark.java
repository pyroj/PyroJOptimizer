package pyroj.costmodel;

/**
 * Created by kpunjabi on 21/6/17.
 */
public enum PhysicalOperatorTagsForSpark {
    HiveTableScanExec,
    FilterExec,
    ProjectExec,
    BroadcastNestedLoopJoinExec,
    CartesianProductExec,
    SortMergeJoinExec,
    BroadcastHashJoinExec,
    ShuffledHashJoinExec,
    HashAggregateExec,
    SortAggregateExec,
    GlobalLimitExec,
    LocalLimitExec,
    SortExec,
    UnionExec,
    InsertIntoHiveTable,
    TakeOrderedAndProjectExec,
    ShuffleExchange,
    BroadcastExchangeExec;

    private PhysicalOperatorTagsForSpark() {
    }
}