package pyroj.costmodel;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.optimizer.Config;

import com.sosnoski.util.array.DoubleArray;
import com.sosnoski.util.array.IntArray;

// ordinary cost
public class ResponseTimeBasedCostModel implements ICostModel {

	private final int _maxCostId;
	private final int _minCostId;
	private final int _zeroCostId;
	private final IntArray _sFreeObjectList;
	private final DoubleArray _sRT_CostObjList;
	private final DoubleArray _sMem_CostObjList;
	private final DoubleArray _sProcessing_CostObjList;
	private final DoubleArray _sDisk_CostObjList;
	private final DoubleArray _sNetwork_CostObjList;
	
	private static MutableDouble _smemoryLatency = new MutableDouble(0);
	private static MutableDouble _sprocessingLatency = new MutableDouble(0);
	private static MutableDouble _sdiskLatency = new MutableDouble(0);
	private static MutableDouble _snetworkLatency = new MutableDouble(0);

	public ResponseTimeBasedCostModel() {
		_sFreeObjectList = new IntArray();

		_sRT_CostObjList = new DoubleArray();
		_sRT_CostObjList.add(ICostModel.NULL, -1);
		
		_sMem_CostObjList = new DoubleArray();
		_sMem_CostObjList.add(ICostModel.NULL, -1);
		
		_sProcessing_CostObjList = new DoubleArray();
		_sProcessing_CostObjList.add(ICostModel.NULL, -1);
		
		_sDisk_CostObjList = new DoubleArray();
		_sDisk_CostObjList.add(ICostModel.NULL, -1);
		
		_sNetwork_CostObjList = new DoubleArray();
		_sNetwork_CostObjList.add(ICostModel.NULL, -1);
		
		
		_zeroCostId = _sRT_CostObjList.add(0);
		_sMem_CostObjList.add(0);
		_sProcessing_CostObjList.add(0);
		_sDisk_CostObjList.add(0);
		_sNetwork_CostObjList.add(0);

		_maxCostId = _sRT_CostObjList.add(Double.MAX_VALUE);
		_sMem_CostObjList.add(Double.MAX_VALUE);
		_sProcessing_CostObjList.add(Double.MAX_VALUE);
		_sDisk_CostObjList.add(Double.MAX_VALUE);
		_sNetwork_CostObjList.add(Double.MAX_VALUE);
		
		_minCostId = _sRT_CostObjList.add(Double.MIN_VALUE);
		_sMem_CostObjList.add(Double.MIN_VALUE);
		_sProcessing_CostObjList.add(Double.MIN_VALUE);
		_sDisk_CostObjList.add(Double.MIN_VALUE);
		_sNetwork_CostObjList.add(Double.MIN_VALUE);
		
	}
	
	@Override
	public double getCost(int id)
	{
		return _sRT_CostObjList.get(id);
	}

	@Override
	public void setCost(int id, double cost)
	{

	}

	@Override
	public int Add(int idCostObject, int idCostOther) {
		final double rt = _sRT_CostObjList.get(idCostObject);
		final double proc = _sProcessing_CostObjList.get(idCostObject);
		final double mem = _sMem_CostObjList.get(idCostObject);
		final double disk = _sDisk_CostObjList.get(idCostObject);
		final double network = _sNetwork_CostObjList.get(idCostObject);
		
		final double rt1 = _sRT_CostObjList.get(idCostOther);
		final double proc1 = _sProcessing_CostObjList.get(idCostOther);
		final double mem1 = _sMem_CostObjList.get(idCostOther);
		final double disk1 = _sDisk_CostObjList.get(idCostOther);
		final double network1 = _sNetwork_CostObjList.get(idCostOther);

		//currently written for Add_sequential, as hyracks schedules its stages sequentially
		//so r1+r1 = (t1+t1, W1+W2)
		//for Add_par r1+r1 = (max(W1+W2), W1+W2), need to implement it later, how to include it, should we change IcotModel for that?
		
		if (rt >= Double.MAX_VALUE || rt1 >= Double.MAX_VALUE)
			return AllocMaxCostObj();
		else
			return Alloc(rt + rt1, proc + proc1, mem + mem1, disk + disk1, network + network1) ;
	}

	@Override
	public void AddAndAssign(int idCostObject, int idCostOther) {

		final double rt = _sRT_CostObjList.get(idCostObject);
		final double proc = _sProcessing_CostObjList.get(idCostObject);
		final double mem = _sMem_CostObjList.get(idCostObject);
		final double disk = _sDisk_CostObjList.get(idCostObject);
		final double network = _sNetwork_CostObjList.get(idCostObject);
		
		final double rt1 = _sRT_CostObjList.get(idCostOther);
		final double proc1 = _sProcessing_CostObjList.get(idCostOther);
		final double mem1 = _sMem_CostObjList.get(idCostOther);
		final double disk1 = _sDisk_CostObjList.get(idCostOther);
		final double network1 = _sNetwork_CostObjList.get(idCostOther);
		
		//currently written for Add_sequential, as hyracks schedules its stages sequentially
		//so r1+r1 = (t1+t1, W1+W2)
		//for Add_par r1+r1 = (max(W1+W2), W1+W2), need to implement it later, how to include it, should we change IcotModel for that?
		
		if (rt >= Double.MAX_VALUE || rt1 >= Double.MAX_VALUE){
			SetToMax(idCostObject);
		}
		else{
			_sRT_CostObjList.set(idCostObject, rt+rt1);
			_sProcessing_CostObjList.set(idCostObject, proc+proc1);
			_sMem_CostObjList.set(idCostObject, mem+mem1);
			_sDisk_CostObjList.set(idCostObject, disk+disk1);
			_sNetwork_CostObjList.set(idCostObject, network+network1);
		}

	}

	@Override
	public void AddResourceCost(int idCostObject, ResourceType resource, double cost) {
		double old = 0;
		switch(resource){
		case CPU: old = _sProcessing_CostObjList.get(idCostObject);
					_sProcessing_CostObjList.set(idCostObject, old + cost);
						break;
		case DISK: old = _sDisk_CostObjList.get(idCostObject);
					_sDisk_CostObjList.set(idCostObject, old + cost);
						break;
		case MEMORY: old = _sMem_CostObjList.get(idCostObject);
					_sMem_CostObjList.set(idCostObject, old + cost);
						break;
		case NETWORK: old = _sNetwork_CostObjList.get(idCostObject);
						_sNetwork_CostObjList.set(idCostObject, old + cost);
						break;
		}
		
		if(_sRT_CostObjList.get(idCostObject) < (old + cost)){
			_sRT_CostObjList.set(idCostObject, old + cost);
		}
		
	}

	@Override
	public int Alloc() {
		return Alloc(0,0,0,0,0);
	}

	/**
	 * All parameters give cost values(latency) for that resource to be allocate to respective cost arrays
	 * @param responseTime
	 * @param proc
	 * @param mem
	 * @param disk
	 * @param network
	 * @return index of the allocated costs obviously this will be same for all the cost arrays,
	 * 			 so we return for array of response time
	 */
	private int Alloc(double responseTime, double proc, double mem, double disk, double network) {
		int index;

		// Remove last element, so that it won't have to shift all the elements.
		final int freeListSize = _sFreeObjectList.size();
		if (freeListSize > 0) {
			index = _sFreeObjectList.get(freeListSize - 1);
			_sFreeObjectList.remove(freeListSize - 1);
			
			//add to all arrays at index position
			_sRT_CostObjList.set(index, responseTime);
			_sProcessing_CostObjList.set(index, proc);
			_sMem_CostObjList.set(index, mem);
			_sDisk_CostObjList.set(index, disk);
			_sNetwork_CostObjList.set(index, network);
		}
		else {
			index = _sRT_CostObjList.add(responseTime);
			//add to all arrays at index position, as all array indexes are synchronized so following will also add at index position 
			_sProcessing_CostObjList.add(proc);
			_sMem_CostObjList.add(mem);
			_sDisk_CostObjList.add(disk);
			_sNetwork_CostObjList.add(network);
		}
		return index;
	}

	
	//TODO implement this properly. currently it not used anywhere so not impelmented.
	@Override
	public int Alloc(Object costStr) {
		if (costStr == null || ((String) costStr).isEmpty())
			return NULL;

		else
			return Alloc(Double.parseDouble((String) costStr));
	}

	@Override
	public int AllocMaxCostObj() {
		//TODO check if is it fine to return _maxCostId directly?
		//currently have not don't it by thinking that someone calling 
		//this may deallocate it later but we dont want our original object to lose
		return Alloc(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
	}

	@Override
	public int AllocZeroCostObj() {
		//TODO check if is it fine to return _zeroCostId directly?
		//currently have not don't it by thinking that 
		//someone calling this may deallocate it later but we dont want our original object to lose
		return Alloc(0,0,0,0,0);
	}

	
	/**
	 * assign value of cost at idCostOther to cost object at idCostObject 
	 */
	@Override
	public void Assign(int idCostObject, int idCostOther) {
		_sRT_CostObjList.set(idCostObject, _sRT_CostObjList.get(idCostOther));
		_sProcessing_CostObjList.set(idCostObject, _sProcessing_CostObjList.get(idCostOther));
		_sMem_CostObjList.set(idCostObject, _sMem_CostObjList.get(idCostOther));
		_sDisk_CostObjList.set(idCostObject, _sDisk_CostObjList.get(idCostOther));
		_sNetwork_CostObjList.set(idCostObject, _sNetwork_CostObjList.get(idCostOther));
	}

	@Override
	public int Clone(int idCostObject) {
		final int index = Alloc();
		Assign(index, idCostObject);
		return index;
	}

	@Override
	public int DividedBy(int idCostObject, double divisor) {
		try {
			throw new Exception(" 'DividedBy' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//previous code
		//TODO : need to implement for new cost model, for that understand its use in MQO or greedy, and remove above throw
		final int index = Alloc();
		_sRT_CostObjList.set(index, _sRT_CostObjList.get(idCostObject) / divisor);
		return index;
	}

	@Override
	public void Free(int idCostObject) {
		_sRT_CostObjList.set(idCostObject, 0.0);
		_sProcessing_CostObjList.set(idCostObject, 0.0);
		_sMem_CostObjList.set(idCostObject, 0.0);
		_sDisk_CostObjList.set(idCostObject, 0.0);
		_sNetwork_CostObjList.set(idCostObject, 0.0);
		_sFreeObjectList.add(idCostObject);
	}

	@Override
	public int getNumCostInstance() {
		return _sRT_CostObjList.size();
	}

	@Override
	//TODO need to think what changes are needed for new cost model, currently it is not used anywhere, so not changed
	public boolean IsAdditionGreaterThanOrEqual(int idCostObject, int idCostToAdd, int idCostAdditionUB) {

		final double first = _sRT_CostObjList.get(idCostObject);
		if (first >= Double.MAX_VALUE)
			return true;

		final double second = _sRT_CostObjList.get(idCostToAdd);
		if (second >= Double.MAX_VALUE)
			return true;

		if ((first + second) >= _sRT_CostObjList.get(idCostAdditionUB))
			return true;
		else
			return false;
	}

	@Override
	//TODO need to think what changes are needed for new cost model, currently it is not used anywhere, so not changed
	public boolean IsAdditionLessThan(int idCostObject, int idCostToadd, int idCostAdditionUB) {
		final double first = _sRT_CostObjList.get(idCostObject);
		if (first >= Double.MAX_VALUE)
			return false;

		final double second = _sRT_CostObjList.get(idCostToadd);
		if (second >= Double.MAX_VALUE)
			return false;

		if ((first + second) < _sRT_CostObjList.get(idCostAdditionUB))
			return true;
		else
			return false;

	}

	@Override
	public boolean IsEqual(int idCostObject, int idCostOther) {
		return _sRT_CostObjList.get(idCostObject) == _sRT_CostObjList.get(idCostOther);
	}

	@Override
	public boolean IsApproxEqual(int idCostObject, int idCostOther) {
		double diff = Math.abs(_sRT_CostObjList.get(idCostObject) - _sRT_CostObjList.get(idCostOther));
		if (diff < 1E-7)
			return true;
		return false;
	}

	@Override
	public boolean IsGreaterThan(int idCostObject, int idCostOther) {
		return _sRT_CostObjList.get(idCostObject) > _sRT_CostObjList.get(idCostOther);
	}

	@Override
	public boolean IsGreaterThanOrEqual(int idCostObject, int idCostOther) {
		return _sRT_CostObjList.get(idCostObject) >= _sRT_CostObjList.get(idCostOther);
	}

	@Override
	public boolean IsLessThan(int idCostObject, int idCostOther) {
		return _sRT_CostObjList.get(idCostObject) < _sRT_CostObjList.get(idCostOther);
	}

	@Override
	public boolean IsLessThanOrEqual(int idCostObject, int idCostOther) {
		return _sRT_CostObjList.get(idCostObject) <= _sRT_CostObjList.get(idCostOther);
	}

	@Override
	public boolean IsNull(int idCostObject) {
		//it null cost oblect is set in constructor
		return idCostObject <= 0;
	}

	@Override
	public boolean IsValid(int idCostObject) {
		
		try {
			throw new Exception(" 'IsValid' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO : need to implement for new cost model, for that understand its use in MQO or greedy, and remove above throw
		//previous code
		return _sRT_CostObjList.get(idCostObject) >= 0;
		// index 0 corresponds to null
		// return idCostObject > 0 && idCostObject < cost_list.size() &&
		// cost_list.get(idCostObject) >= 0;
	}

	@Override
	public boolean IsValidPlanCost(int idCostObject) {
		return _sRT_CostObjList.get(idCostObject) < Double.MAX_VALUE;
	}

	@Override
	public int MaxCostId() {
		return _maxCostId; // stored at index 2 of cost_arrays
	}

	@Override
	public int MinCostId() {
		return _minCostId; // stored at index 3 of cost arrays
	}

	@Override
	public void SetToMax(int idCostObject) {
		Assign(idCostObject, _maxCostId);
	}

	@Override
	public void SetToZero(int idCostObject) {
		Assign(idCostObject, _zeroCostId);
	}

	@Override
	public int Subtract(int idCostObject, int idCostOther) {
		try {
			throw new Exception(" 'Subtract' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO : need to implement for new cost model, for that understand its use in MQO or greedy, and remove above throw
		//previous code
		final double first = _sRT_CostObjList.get(idCostObject);
		final double second = _sRT_CostObjList.get(idCostOther);
		double difference = 0;

		if (first <= Double.MIN_VALUE)
			difference = Double.MIN_VALUE;
		else
			difference = first - second;

		final int index = Alloc();
		_sRT_CostObjList.set(index, difference);
		return index;
	}

	@Override
	public void SubtractAndAssign(int idCostObject, int idCostOther) {
		try {
			throw new Exception(" 'Subtract' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO : need to implement for new cost model, for that understand its use in MQO or greedy, and remove above throw
		//previous code
		final double first = _sRT_CostObjList.get(idCostObject);
		final double second = _sRT_CostObjList.get(idCostOther);
		double difference = 0;

		if (first <= Double.MIN_VALUE)
			difference = Double.MIN_VALUE;
		else
			difference = first - second;

		_sRT_CostObjList.set(idCostObject, difference);
	}

	@Override
	public int Times(int idCostObject, double multiplier) {
		final double rt = _sRT_CostObjList.get(idCostObject);
		final double proc = _sProcessing_CostObjList.get(idCostObject);
		final double mem = _sMem_CostObjList.get(idCostObject);
		final double disk = _sDisk_CostObjList.get(idCostObject);
		final double network = _sNetwork_CostObjList.get(idCostObject);
		
		if (rt >= Double.MAX_VALUE)
			return AllocMaxCostObj();
		else
			return Alloc(rt * multiplier, proc * multiplier, mem * multiplier, disk * multiplier, network * multiplier) ;
	}

	@Override
	public void TimesAndAssign(int idCostObject, double multiplier) {
		final double rt = _sRT_CostObjList.get(idCostObject);
		final double proc = _sProcessing_CostObjList.get(idCostObject);
		final double mem = _sMem_CostObjList.get(idCostObject);
		final double disk = _sDisk_CostObjList.get(idCostObject);
		final double network = _sNetwork_CostObjList.get(idCostObject);
		
		if (rt >= Double.MAX_VALUE){
			SetToMax(idCostObject);
		}
		else{
			_sRT_CostObjList.set(idCostObject, rt*multiplier);
			_sProcessing_CostObjList.set(idCostObject, proc*multiplier);
			_sMem_CostObjList.set(idCostObject, mem*multiplier);
			_sDisk_CostObjList.set(idCostObject, disk*multiplier);
			_sNetwork_CostObjList.set(idCostObject, network*multiplier);
		}

	}

	@Override
	public String toString(int idCostObject) {
		StringBuilder str = new StringBuilder("");
		str.append("Response time: ");
		str.append(Double.toString(_sRT_CostObjList.get(idCostObject)));
		str.append("\n");
		str.append("Processing latency: ");
		str.append(Double.toString(_sProcessing_CostObjList.get(idCostObject)));
		str.append("\n");
		str.append("Mamery Latency: ");
		str.append(Double.toString(_sMem_CostObjList.get(idCostObject)));
		str.append("\n");
		str.append("Disk latency: ");
		str.append(Double.toString(_sDisk_CostObjList.get(idCostObject)));
		str.append("\n");
		str.append("Network latency: ");
		str.append(Double.toString(_sNetwork_CostObjList.get(idCostObject)));
		str.append("\n");
		
		return str.toString();
	}

	@Override
	public int ZeroCostId() {
		return _zeroCostId;
	}

	@Override
	public void computeLocalCost(int idCost, Object physicalOperator, Object logicalOperator, 
			Object logProperties, Object[] childrenLogicalProperties, 
			Object reqdProperties, Object[] childrenPhysicalProperties) {

		_smemoryLatency.setValue(0);
		_sprocessingLatency.setValue(0);
		_sdiskLatency.setValue(0);
		_snetworkLatency.setValue(0);

		Config.OpHelper.computeLocalCost(physicalOperator, logicalOperator, logProperties, childrenLogicalProperties, reqdProperties,
				childrenPhysicalProperties, _smemoryLatency, _sprocessingLatency, _sdiskLatency, _snetworkLatency);

		double memoryCost = _smemoryLatency.getValue();
		double processingCost = _sprocessingLatency.getValue();
		double diskCost = _sdiskLatency.getValue();
		double networkCost = _snetworkLatency.getValue();
		
		//Response time = maximum (all resource cost)
		
		_sProcessing_CostObjList.set(idCost, processingCost);
		_sMem_CostObjList.set(idCost, memoryCost);
		_sDisk_CostObjList.set(idCost, diskCost);
		_sNetwork_CostObjList.set(idCost, networkCost);
		_sRT_CostObjList.set(idCost, max(processingCost,memoryCost,diskCost, networkCost));
	
	}

	@Override
	public boolean computePartialCost(int idPartialCost, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost) {

		//TODO
		//in response the based cost model, cost for base cases as scan or join, cost is calculated as total work done
		//but its not the case with other operators. so need to check physical operator & calculate using our cost model operators
		
		try {
			throw new Exception(" 'computePartialCost' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//previous code
		double partialCost = _sRT_CostObjList.get(idOperatorLocalCost);

		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				if (idArrChildrenCost[i] != ICostModel.NULL)
					partialCost += _sRT_CostObjList.get(idArrChildrenCost[i]);
			}
		}
		if (_sRT_CostObjList.get(idPartialCost) == partialCost)
			return false;
		_sRT_CostObjList.set(idPartialCost, partialCost);
		return true;
	}

	@Override
	public boolean computeTotalCost(int idTotalCost, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost) {
		
		//TODO
		//in response the based cost model, cost for base cases as scan or join, cost is calculated as total work done
		//but its not the case with other operators. so need to check physical operator & calculate using our cost model operators
		
		try {
			throw new Exception(" 'computePartialCost' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//previous code
		double totalCost = _sRT_CostObjList.get(idOperatorLocalCost);

		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				totalCost += _sRT_CostObjList.get(idArrChildrenCost[i]);
			}
		}
		if (_sRT_CostObjList.get(idTotalCost) == totalCost)
			return false;
		_sRT_CostObjList.set(idTotalCost, totalCost);
		return true;
	}

	@Override
	public boolean computeRemainingCostUB(int idRemainingCostUB, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost, int idCostUB) {

		//TODO
		//in response the based cost model, cost for base cases as scan or join, cost is calculated as total work done
		//but its not the case with other operators. so need to check physical operator & calculate using our cost model operators
		//so to calculate remaining cost we need not subtract cost of root and all children from UB.
		try {
			throw new Exception(" 'computePartialCost' not Implemented yet in responce time based cost model, but it is called");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//previous code
		double remainingCostUB = _sRT_CostObjList.get(idCostUB) - _sRT_CostObjList.get(idOperatorLocalCost);

		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				if (idArrChildrenCost[i] != ICostModel.NULL)
					remainingCostUB -= _sRT_CostObjList.get(idArrChildrenCost[i]);
			}
		}
		if (_sRT_CostObjList.get(idRemainingCostUB) == remainingCostUB)
			return false;
		_sRT_CostObjList.set(idRemainingCostUB, remainingCostUB);
		return true;
	}

	@Override
	public int computeUseCost(Object logicalProperties,Object physicalProperty) {
		
		MutableDouble memoryLatency 	= new MutableDouble(0);
		MutableDouble processingLatency = new MutableDouble(0);
		MutableDouble diskLatency 		= new MutableDouble(0);
		MutableDouble networkLatency 	= new MutableDouble(0);
		
		Config.OpHelper.computeUseCost(logicalProperties,physicalProperty, memoryLatency, processingLatency, diskLatency, networkLatency);
		double memoryCost = memoryLatency.getValue();
		double processingCost = processingLatency.getValue();
		double diskCost = diskLatency.getValue();
		double networkCost = networkLatency.getValue();
		double rt=max(processingCost,memoryCost,diskCost, networkCost);
		
		//Response time = maximum (all resource cost)
		return Alloc(rt, processingCost, memoryCost, diskCost, networkCost);
	}

	@Override
	public int computeMaterializationCost(Object logicalOperator,
			Object logProperties, Object physicalOperator, Object physicalProperty) {
		
		MutableDouble memoryLatency 	= new MutableDouble(0);
		MutableDouble processingLatency = new MutableDouble(0);
		MutableDouble diskLatency 		= new MutableDouble(0);
		MutableDouble networkLatency 	= new MutableDouble(0);
		
		Config.OpHelper.computeMaterializationCost(logicalOperator, logProperties, physicalOperator, physicalProperty , memoryLatency,
					processingLatency, diskLatency, networkLatency);
		
		double memoryCost = memoryLatency.getValue();
		double processingCost = processingLatency.getValue();
		double diskCost = diskLatency.getValue();
		double networkCost = networkLatency.getValue();
		double rt=max(processingCost,memoryCost,diskCost, networkCost);
		
		//Response time = maximum (all resource cost)
		return Alloc(rt, processingCost, memoryCost, diskCost, networkCost);
	}
	
	public double max(double a, double... args){
		double max = a;
		for(double arg : args){
			if(arg > max)
				max=arg;
		}
		
		return max; 
		
	}
	
	public static void main(String[] args) throws Exception {
		ResponseTimeBasedCostModel rt = new ResponseTimeBasedCostModel();
		System.out.println("zerocostId: "+ rt._zeroCostId + "maxcostId:  "+ rt._maxCostId + " mincostId: " + rt._minCostId);
		System.out.println(" values at zerocost Id: " + 
		rt._sRT_CostObjList.get(rt._zeroCostId)+ 
		rt._sDisk_CostObjList.get(rt._zeroCostId) +
		rt._sMem_CostObjList.get(rt._zeroCostId) +
		rt._sProcessing_CostObjList.get(rt._zeroCostId)+
		rt._sNetwork_CostObjList.get(rt._zeroCostId));
		
		System.out.println("max is: " + rt.max(4,11.56,1.1,Double.MAX_VALUE));
	}

}

