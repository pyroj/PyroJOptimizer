 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.costmodel;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.optimizer.Config;

import com.sosnoski.util.array.DoubleArray;
import com.sosnoski.util.array.IntArray;

// ordinary cost
public class CostModelOrd implements ICostModel {

	private final int _maxCostId;
	private final int _minCostId;
	private final DoubleArray _sCostObjList;
	private final IntArray _sFreeObjectList;
	private final int _zeroCostId;

	private static MutableDouble _smemoryLatency = new MutableDouble(0);
	private static MutableDouble _sprocessingLatency = new MutableDouble(0);
	private static MutableDouble _sdiskLatency = new MutableDouble(0);
	private static MutableDouble _snetworkLatency = new MutableDouble(0);

	public CostModelOrd() {
		_sFreeObjectList = new IntArray();

		_sCostObjList = new DoubleArray();
		_sCostObjList.add(ICostModel.NULL, -1);
		_zeroCostId = _sCostObjList.add(0);
		_maxCostId = _sCostObjList.add(Double.MAX_VALUE);
		_minCostId = _sCostObjList.add(Double.MIN_VALUE);
	}

	@Override
	public double getCost(int id)
	{
		return _sCostObjList.get(id);
	}

	public void setCost(int id, double cost)
	{
		_sCostObjList.set(id, cost);
	}

	@Override
	public int Add(int idCostObject, int idCostOther) {
		final double first = _sCostObjList.get(idCostObject);
		final double second = _sCostObjList.get(idCostOther);
		double sum = 0;

		if (first >= Double.MAX_VALUE || second >= Double.MAX_VALUE)
			sum = Double.MAX_VALUE;
		else
			sum = first + second;

		final int index = Alloc();
		_sCostObjList.set(index, sum);
		return index;
	}

	@Override
	public void AddAndAssign(int idCostObject, int idCostOther) {
		final double first = _sCostObjList.get(idCostObject);
		final double second = _sCostObjList.get(idCostOther);
		double sum = 0;

		if (first >= Double.MAX_VALUE || second >= Double.MAX_VALUE)
			sum = Double.MAX_VALUE;
		else
			sum = first + second;

		_sCostObjList.set(idCostObject, sum);
	}

	@Override
	public void AddResourceCost(int idCostObject, ResourceType resource, double cost) {
		_sCostObjList.set(idCostObject, _sCostObjList.get(idCostObject) + cost);
	}

	@Override
	public int Alloc() {
		return Alloc(0);
	}

	private int Alloc(double value) {
		int index;

		// Remove last element, so that it won't have to shift all the elements.
		final int freeListSize = _sFreeObjectList.size();
		if (freeListSize > 0) {
			index = _sFreeObjectList.get(freeListSize - 1);
			_sFreeObjectList.remove(freeListSize - 1);
			_sCostObjList.set(index, value);
		}
		else {
			index = _sCostObjList.add(value);
		}
		return index;
	}

	@Override
	public int Alloc(Object costStr) {
		if (costStr == null || ((String) costStr).isEmpty())
			return NULL;

		else
			return Alloc(Double.parseDouble((String) costStr));
	}

	@Override
	public int AllocMaxCostObj() {
		return Alloc(Double.MAX_VALUE);
	}

	@Override
	public int AllocZeroCostObj() {
		return Alloc(0);
	}

	@Override
	public void Assign(int idCostObject, int idCostOther) {
		_sCostObjList.set(idCostObject, _sCostObjList.get(idCostOther));
	}

	@Override
	public int Clone(int idCostObject) {
		final int index = Alloc();
		_sCostObjList.set(index, _sCostObjList.get(idCostObject));
		return index;
	}

	@Override
	public int DividedBy(int idCostObject, double divisor) {
		final int index = Alloc();
		_sCostObjList.set(index, _sCostObjList.get(idCostObject) / divisor);
		return index;
	}

	@Override
	public void Free(int idCostObject) {
		_sCostObjList.set(idCostObject, 0.0);
		_sFreeObjectList.add(idCostObject);
	}

	@Override
	public int getNumCostInstance() {
		return _sCostObjList.size();
	}

	@Override
	public boolean IsAdditionGreaterThanOrEqual(int idCostObject, int idCostToAdd, int idCostAdditionUB) {

		final double first = _sCostObjList.get(idCostObject);
		if (first >= Double.MAX_VALUE)
			return true;

		final double second = _sCostObjList.get(idCostToAdd);
		if (second >= Double.MAX_VALUE)
			return true;

		if ((first + second) >= _sCostObjList.get(idCostAdditionUB))
			return true;
		else
			return false;
	}

	@Override
	public boolean IsAdditionLessThan(int idCostObject, int idCostToadd, int idCostAdditionUB) {
		final double first = _sCostObjList.get(idCostObject);
		if (first >= Double.MAX_VALUE)
			return false;

		final double second = _sCostObjList.get(idCostToadd);
		if (second >= Double.MAX_VALUE)
			return false;

		if ((first + second) < _sCostObjList.get(idCostAdditionUB))
			return true;
		else
			return false;

	}

	@Override
	public boolean IsEqual(int idCostObject, int idCostOther) {
		return _sCostObjList.get(idCostObject) == _sCostObjList.get(idCostOther);
	}

	@Override
	public boolean IsApproxEqual(int idCostObject, int idCostOther) {
		double diff = Math.abs(_sCostObjList.get(idCostObject) - _sCostObjList.get(idCostOther));
		if (diff < 1E-7)
			return true;
		return false;
	}

	@Override
	public boolean IsGreaterThan(int idCostObject, int idCostOther) {
		return _sCostObjList.get(idCostObject) > _sCostObjList.get(idCostOther);
	}

	@Override
	public boolean IsGreaterThanOrEqual(int idCostObject, int idCostOther) {
		return _sCostObjList.get(idCostObject) >= _sCostObjList.get(idCostOther);
	}

	@Override
	public boolean IsLessThan(int idCostObject, int idCostOther) {
		return _sCostObjList.get(idCostObject) < _sCostObjList.get(idCostOther);
	}

	@Override
	public boolean IsLessThanOrEqual(int idCostObject, int idCostOther) {
		return _sCostObjList.get(idCostObject) <= _sCostObjList.get(idCostOther);
	}

	@Override
	public boolean IsNull(int idCostObject) {
		return idCostObject <= 0;
	}

	@Override
	public boolean IsValid(int idCostObject) {
		return _sCostObjList.get(idCostObject) >= 0;
		// index 0 corresponds to null
		// return idCostObject > 0 && idCostObject < cost_list.size() &&
		// cost_list.get(idCostObject) >= 0;
	}

	@Override
	public boolean IsValidPlanCost(int idCostObject) {
		return _sCostObjList.get(idCostObject) < Double.MAX_VALUE;
	}

	@Override
	public int MaxCostId() {
		return _maxCostId; // stored at index 1 of cost_array
	}

	@Override
	public int MinCostId() {
		return _minCostId; // stored at index 2 of cost array
	}

	@Override
	public void SetToMax(int idCostObject) {
		_sCostObjList.set(idCostObject, Double.MAX_VALUE);
	}

	@Override
	public void SetToZero(int idCostObject) {
		_sCostObjList.set(idCostObject, 0.0);
	}

	@Override
	public int Subtract(int idCostObject, int idCostOther) {
		final double first = _sCostObjList.get(idCostObject);
		final double second = _sCostObjList.get(idCostOther);
		double difference = 0;

		if (first <= Double.MIN_VALUE)
			difference = Double.MIN_VALUE;
		else
			difference = first - second;

		final int index = Alloc();
		_sCostObjList.set(index, difference);
		return index;
	}

	@Override
	public void SubtractAndAssign(int idCostObject, int idCostOther) {
		final double first = _sCostObjList.get(idCostObject);
		final double second = _sCostObjList.get(idCostOther);
		double difference = 0;

		if (first <= Double.MIN_VALUE)
			difference = Double.MIN_VALUE;
		else
			difference = first - second;

		_sCostObjList.set(idCostObject, difference);
	}

	@Override
	public int Times(int idCostObject, double multiplier) {
		final double first = _sCostObjList.get(idCostObject);
		double product = 1;

		if (first >= Double.MAX_VALUE)
			product = Double.MAX_VALUE;
		else
			product = first * multiplier;

		final int index = Alloc();
		_sCostObjList.set(index, product);
		return index;
	}

	@Override
	public void TimesAndAssign(int idCostObject, double multiplier) {
		final double first = _sCostObjList.get(idCostObject);
		double product = 1;

		if (first >= Double.MAX_VALUE)
			product = Double.MAX_VALUE;
		else
			product = first * multiplier;

		_sCostObjList.set(idCostObject, product);
	}

	@Override
	public String toString(int idCostObject) {
		return Double.toString(_sCostObjList.get(idCostObject));
	}

	@Override
	public int ZeroCostId() {
		return _zeroCostId;
	}

	@Override
	public void computeLocalCost(int idCost, Object physicalOperator, Object logicalOperator, Object logProperties,
			Object[] childrenLogicalProperties, Object reqdProperties, Object[] childrenPhysicalProperties) {

		_smemoryLatency.setValue(0);
		_sprocessingLatency.setValue(0);
		_sdiskLatency.setValue(0);
		_snetworkLatency.setValue(0);

		Config.OpHelper.computeLocalCost(physicalOperator, logicalOperator, logProperties, childrenLogicalProperties, reqdProperties,
				childrenPhysicalProperties, _smemoryLatency, _sprocessingLatency, _sdiskLatency, _snetworkLatency);

		double memoryCost = _smemoryLatency.getValue();
		double processingCost = _sprocessingLatency.getValue();
		double diskCost = _sdiskLatency.getValue();
		double networkCost = _snetworkLatency.getValue();
		if (memoryCost >= Double.MAX_VALUE || processingCost >= Double.MAX_VALUE || diskCost >= Double.MAX_VALUE
				|| networkCost >= Double.MAX_VALUE)
			_sCostObjList.set(idCost, Double.MAX_VALUE);
		else
			_sCostObjList.set(idCost, memoryCost + processingCost + diskCost + networkCost);
	}

	@Override
	public boolean computePartialCost(int idPartialCost, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost) {

		double partialCost = _sCostObjList.get(idOperatorLocalCost);

		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				if (idArrChildrenCost[i] != ICostModel.NULL)
					partialCost += _sCostObjList.get(idArrChildrenCost[i]);
			}
		}
		if (_sCostObjList.get(idPartialCost) == partialCost)
			return false;
		_sCostObjList.set(idPartialCost, partialCost);
		return true;
	}

	@Override
	public boolean computeTotalCost(int idTotalCost, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost) {
		double totalCost = _sCostObjList.get(idOperatorLocalCost);

		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				totalCost += _sCostObjList.get(idArrChildrenCost[i]);
			}
		}
		if (_sCostObjList.get(idTotalCost) == totalCost)
			return false;
		_sCostObjList.set(idTotalCost, totalCost);
		return true;
	}

	@Override
	public boolean computeRemainingCostUB(int idRemainingCostUB, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost, int idCostUB) {

		double remainingCostUB = _sCostObjList.get(idCostUB) - _sCostObjList.get(idOperatorLocalCost);

		if (idArrChildrenCost != null) {
			for (int i = 0; i < idArrChildrenCost.length; i++) {
				if (idArrChildrenCost[i] != ICostModel.NULL)
					remainingCostUB -= _sCostObjList.get(idArrChildrenCost[i]);
			}
		}
		if (_sCostObjList.get(idRemainingCostUB) == remainingCostUB)
			return false;
		_sCostObjList.set(idRemainingCostUB, remainingCostUB);
		return true;
	}

	@Override
	public int computeUseCost(Object logicalProperties,Object physicalProperty) {
		int costId = AllocZeroCostObj();
		MutableDouble memoryLatency 	= new MutableDouble(0);
		MutableDouble processingLatency = new MutableDouble(0);
		MutableDouble diskLatency 		= new MutableDouble(0);
		MutableDouble networkLatency 	= new MutableDouble(0);
		Config.OpHelper.computeUseCost(logicalProperties,physicalProperty, memoryLatency, processingLatency, diskLatency, networkLatency);
		double memoryCost = memoryLatency.getValue();
		double processingCost = processingLatency.getValue();
		double diskCost = diskLatency.getValue();
		double networkCost = networkLatency.getValue();
		if (memoryCost >= Double.MAX_VALUE || processingCost >= Double.MAX_VALUE || diskCost >= Double.MAX_VALUE
				|| networkCost >= Double.MAX_VALUE)
			_sCostObjList.set(costId, Double.MAX_VALUE);
		else
			_sCostObjList.set(costId, memoryCost + processingCost + diskCost + networkCost);
		
		return costId;
	}

	@Override
	public int computeMaterializationCost(Object logicalOperator,
			Object logProperties, Object physicalOperator, Object physicalProperty) {
		int costId = AllocZeroCostObj();
		MutableDouble memoryLatency 	= new MutableDouble(0);
		MutableDouble processingLatency = new MutableDouble(0);
		MutableDouble diskLatency 		= new MutableDouble(0);
		MutableDouble networkLatency 	= new MutableDouble(0);
		Config.OpHelper.computeMaterializationCost(logicalOperator, logProperties, physicalOperator, physicalProperty , memoryLatency,
					processingLatency, diskLatency, networkLatency);
		double memoryCost = memoryLatency.getValue();
		double processingCost = processingLatency.getValue();
		double diskCost = diskLatency.getValue();
		double networkCost = networkLatency.getValue();
		if (memoryCost >= Double.MAX_VALUE || processingCost >= Double.MAX_VALUE || diskCost >= Double.MAX_VALUE
				|| networkCost >= Double.MAX_VALUE)
			_sCostObjList.set(costId, Double.MAX_VALUE);
		else
			_sCostObjList.set(costId, memoryCost + processingCost + diskCost + networkCost);
		
		return costId;
	}

}
