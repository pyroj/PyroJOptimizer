 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.costmodel;

public interface ICostModel {

	public enum ResourceType {
		CPU, DISK, MEMORY, NETWORK
	};

	static int NULL = 0;

	/**
	 * Computes this + other [ won't modify this object]
	 * 
	 * @param other
	 * @return idCostObject of (this + other)
	 */
	int Add(int idCostObject, int idCostOther);

	/**
	 * this = this + other
	 * 
	 * @param other
	 */
	void AddAndAssign(int idCostObject, int idCostOther);

	/**
	 * Add a resource cost to the current object
	 * 
	 * @param resource
	 * @param cost
	 */
	void AddResourceCost(int idCostObject, ResourceType resource, double cost);

	/**
	 * Allocate a new object
	 * 
	 * @param
	 * @return idCostObject for newly allocated object
	 */
	int Alloc();

	/**
	 * Allocate a new object with value encoded in Object
	 * 
	 * @param costStr
	 *            Object
	 * @return idCostObject for newly allocated object
	 */
	int Alloc(Object costStr);

	/**
	 * Allocate a cost object and initialize to to max
	 * 
	 * @return idCostObject for newly allocated object
	 */
	int AllocMaxCostObj();

	/**
	 * Allocate cost object and initialize to zero
	 * 
	 * @return idCostObject for newly allocated object
	 */
	int AllocZeroCostObj();

	/**
	 * this = other
	 * 
	 * @param other
	 */
	void Assign(int idCostObject, int idCostOther);

	/**
	 * Creates a copy of current object and returns.
	 * 
	 * @return idCostObject of clone
	 */
	int Clone(int idCostObject);

	/**
	 * Computes this / divisor; [ won't modify this]
	 * 
	 * @param divisor
	 * @return idCostObject of answer
	 */
	int DividedBy(int idCostObject, double divisor);

	/**
	 * Free up the cost object pointed by idCostObject
	 * 
	 * @param idCostObject
	 * @return
	 */
	void Free(int idCostObject);

	/**
	 * Get Number of cost objects created - FOR DEBUG
	 */
	int getNumCostInstance();

	/**
	 * Check if current cost can be added with the new cost add with cost limit
	 * provided.
	 * 
	 * @param costToadd
	 *            , additionUB
	 * @return true if it can be added, false otherwise
	 */
	boolean IsAdditionGreaterThanOrEqual(int idCostObject, int idCostToAdd, int idCostAdditionUB);

	/**
	 * Check if current cost can be added with the new cost add with cost limit
	 * provided.
	 * 
	 * @param costToadd
	 *            , additionUB
	 * @return true if it can be added, false otherwise
	 */
	boolean IsAdditionLessThan(int idCostObject, int idCostToAdd, int idCostAdditionUB);

	/**
	 * Check if this == other
	 * 
	 * @param other
	 * @return
	 */
	boolean IsEqual(int idCostObject, int idCostOther);

	boolean IsApproxEqual(int idCostObject, int idCostOther);

	/**
	 * Check if this > other
	 * 
	 * @param other
	 * @return
	 */
	boolean IsGreaterThan(int idCostObject, int idCostOther);

	/**
	 * Check if this >= other
	 * 
	 * @param other
	 * @return
	 */
	boolean IsGreaterThanOrEqual(int idCostObject, int idCostother);

	/**
	 * Check if this < other
	 * 
	 * @param other
	 * @return
	 */
	boolean IsLessThan(int idCostObject, int idCostOther);

	/**
	 * Check if this <= other
	 * 
	 * @param other
	 * @return
	 */
	boolean IsLessThanOrEqual(int idCostObject, int idCostOther);

	/**
	 * Check if current cost is null
	 * 
	 * @return
	 */
	boolean IsNull(int idCostObject);

	/**
	 * Check if current cost is a valid cost for a plan / operator.
	 * 
	 * @return
	 */
	boolean IsValid(int idCostObject);

	boolean IsValidPlanCost(int idCostObject);

	public int MaxCostId();

	public int MinCostId();

	void SetToMax(int idCostObject);

	void SetToZero(int idCostObject);

	/**
	 * Computes this - other [won't modify this object]
	 * 
	 * @param other
	 * @return idCostObject of (this - other)
	 */
	int Subtract(int idCostObject, int idCostOther);

	/**
	 * this = this - other
	 * 
	 * @param other
	 */
	void SubtractAndAssign(int idCostObject, int idCostOther);

	/**
	 * Computes multiplier * this [ won't modify this]
	 * 
	 * @param multiplier
	 * @return idCostObject of (multiplier * this)
	 */
	int Times(int idCostObject, double multiplier);

	/**
	 * this = multiplier * this;
	 * 
	 * @param multiplier
	 */
	void TimesAndAssign(int idCostObject, double multiplier);

	/**
	 * String representation of object
	 * 
	 * @return
	 */
	String toString(int idCostObject);

	public int ZeroCostId();

	public void computeLocalCost(int idCost, Object physicalOperator, Object logicalOperator, Object logProperties,
			Object[] childrenLogicalProperties, Object reqdProperties, Object[] childrenPhysicalProperties);

	
	/**
	 * Given local cost and cost of some of the children, compute the partial
	 * cost of tree rooted at the physical operator. Assign the computed partial
	 * cost in the last parameter "partialCost".
	 * 
	 * @param physicalOperator
	 * @param operatorLocalCost
	 * @param childrenCost
	 * @param partialCost
	 *            - output parameter.
	 * @return true: if new computed cost is different than old partial cost,
	 *         false otherwise
	 */
	boolean computePartialCost(int idPartialCost, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost);

	/**
	 * Given the local cost and cost of children, compute the total cost of tree
	 * rooted at the physical operator.
	 * 
	 * @param physicalOperator
	 * @param operatorLocalCost
	 * @param childrenCost
	 * @param totalCost
	 * @return true: if new computed cost is different than old partial cost,
	 *         false otherwise
	 */
	boolean computeTotalCost(int idTotalCost, Object physicalOperator, int idOperatorLocalCost, int[] idArrChildrenCost);

	/**
	 * Compute the cost of using materialized results for given logical properties.
	 * Note that use cost does not depend on actual logical or physical operator
	 * since it only needs to know size of data represented by the logical properties.
	 * As the physical properties (Partitioned/Unpartitioned) can affect the use cost.
	 * @param logicalProperties
	 * 		: Logical properties of the LogicalEquivalenceNode whose use cost is to be
	 * 		computed.
	 * @param physical Properties
	 * 		: Physical properties of the physical equivalence node
	 * @return 
	 * 		Id of the cost object which represents use cost. The ownership of the cost
	 * 		object lies with the optimizer
	 * */
	int computeUseCost(Object logicalProperties, Object physicalProperty);
	
	
	/**
	 * Given local cost, cost of some of the children, and cost upper bound,
	 * compute the cost upper bound of child.
	 * 
	 * @param physicalOperator
	 * @param operatorLocalCost
	 * @param childrenCost
	 * @param costUB
	 * @param remainingCostUB
	 * @return true: if new computed cost is different than old partial cost,
	 *         false otherwise
	 */
	boolean computeRemainingCostUB(int idRemainingCostUB, Object physicalOperator, int idOperatorLocalCost,
			int[] idArrChildrenCost, int idCostUB);

	/**
	 * Compute the cost of materializing the physicalOperator. LogicalOperator is also passed 
	 * because materialization cost may depend on type of operator. For e.g. base table scan
	 * materialization will have a cost of zero.
	 * Matetialization cost also depends on the physical properties of operator(or physical equivalence node).
	 * As in case of partitioned and unpartitioned materialization cost will change
	 * @param logicalOperator
	 * 		: Logical operator representing the node to be considered for materialization
	 * @param logProperties
	 * 		: Logical properties of the operator
	 * @param physicalOperator
	 * 		: Physical operator used to compute the node
	 * @param : physical Property
	 * 		: Physical properties of operator. 
	 * @return
	 * 		cost object id representing computed cost. The ownership of this cost object
	 * 		resides with the optimizer 
	 * */
	//int computeMaterializationCost(Object logicalOperator, Object logProperties, Object physicalOperator);
	int computeMaterializationCost(Object logicalOperator,
			Object logProperties, Object physicalOperator,
			Object physicalProperty);

	double getCost(int id);
	void setCost(int id, double cost);
	
}
