 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.runtime;

public interface IImplementationRule {

	/**
	 * Check if the rule is applicable. It should be very efficient as it will be called multiple times.
	 * @param logicalOperator
	 * @return
	 */
	boolean isApplicable(Object logicalOperator);

	/**
	 * Get corresponding physical operator
	 * @param logicalOperator
	 * @param logicalProperties
	 * @param childrenlogicalProps
	 * @param reqdProperties
	 * @return physical operator, if it can satisfy the required properties, otherwise null.
	 */
	Object getOutputOperator(Object logicalOperator, Object logicalProperties, Object[] childrenlogicalProps,
			Object reqdProperties);

	/**
	 * Get rule name.
	 * @return
	 */
	String getRuleName();
}
