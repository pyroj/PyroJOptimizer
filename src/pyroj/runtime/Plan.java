 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.runtime;

import pyroj.optimizer.PhysicalOperatorNode;

public class Plan {
	private final Object _logicalOperator;
	private final Object _physicalOperator;
	private final Object _logicalProperties;
	private final Object _physicalProperties;
	private final String _localCost;
	private final String _totalCost;
	private final Plan[] _children;
	private final PhysicalOperatorNode _physicalOpNode; // reference to PhysicalOperatorNode that is at the root of the plan
	private boolean materialize = false; // indicate whether plan should be materialized
	
	public Plan (
			Object logicalOperator, 
			Object physicalOperator,
			Object logicalProperties,
			Object physicalProperties,
			String localCost,
			String totalCost,
			Plan[] children,
			PhysicalOperatorNode op) {
		_logicalOperator = logicalOperator;
		_physicalOperator = physicalOperator;
		_logicalProperties = logicalProperties;
		_physicalProperties = physicalProperties;
		_localCost = localCost;
		_totalCost = totalCost;
		_children = children;
		_physicalOpNode = op;
	}
	
	
	/**
	 * Indicates whether plan should be materialized
	 * @return true if the result this plan represents
	 * should be materialized
	 */
	public boolean isMaterialized() {
		return materialize;
	}
	
	/**
	 * Changes the materialization status of the node.
	 * @param n
	 * 		: Value to be assigned to materialization status
	 */
	public void setMaterialization(boolean n){
		materialize = n;
	}
	
	public Object getLogicalOperator() {
		return _logicalOperator;
	}
	
	public Object getPhysicalOperator() {
		return _physicalOperator;
	}
	
	public Object getLogicalProperties() {
		return _logicalProperties;
	}
	
	public Object getPhysicalProperties() {
		return _physicalProperties;
	}
	
	public String getLocalCost() {
		return _localCost;
	}
	
	public String getTotalCost() {
		return _totalCost;
	}
	
	public Plan[] getChildren() {
		return _children;
	}
	/** returns PhysicalOperatorNode that is at the root of the Plan*/
	public PhysicalOperatorNode getPhysicalOpNode() {
		return _physicalOpNode;
	}
	
}
