package pyroj.runtime;

public interface IClusterConfig {
	
	/*
	 * This function is to be implemented to returns the number of active
	 * nodes in the cluster which is sent to the optimizer.
	 */

	int getActiveNodeCount();

}