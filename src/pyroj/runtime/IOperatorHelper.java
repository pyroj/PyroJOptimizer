 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.runtime;

import java.io.PrintStream;
import java.util.ArrayList;

import org.apache.commons.lang3.mutable.MutableDouble;

/**
 * Interface to return all the helper functions required for IOperator.
 * 
 */
public interface IOperatorHelper {

	// Functions related to logical operator
	
	/**
	 * Used while adding logical operator in memo to avoid duplicate operators
	 * @param logicalOperator
	 * @param otherLogicalOperator
	 * @return
	 */
	boolean isLogOperatorEqual(Object logicalOperator, Object otherLogicalOperator);

	boolean isLogOperatorNotEqualHive(Object logicalOperator, Object otherLogicalOperator, int grp1, int grp2);
	
	/**
	 * Used while adding logical operator in memo to avoid duplicate operators
	 * @param logicalOperator
	 * @return
	 */
	int getLogOperatorLocalHashCode(Object logicalOperator);
	
	/**
	 * Compute logical property delivered by a logical operator given children
	 * logical properties. The returned object can be modified by optimizer,
	 * thus, logical operator must create a new object for output logical
	 * properties instead of sharing to avoid side effects of modification
	 * 
	 * @param logicalOperator
	 * @param childrenLogicalProperties
	 * @return
	 */
	Object computeOutputLogicalProperties(Object logicalOperator, Object[] childrenLogicalProperties);

	/**
	 * put logical property object returned by computeOutputLogicalProperties back in memory object pool
	 * @param logicalProperty
	 */
	void putLogicalPropertyObject (Object logicalProperty);
	
	/**
	 * Get array of logical properties
	 * @param numElements
	 * @return
	 */
	Object[] getLogicalPropertiesArray (int numElements);
	
	/**
	 * put the array of logical properties back in memory pool
	 * @param logicalPropertyArray
	 */
	void putLogicalPropertiesArray(Object[] logicalPropertyArray);

	/**
	 * Print the logical operator
	 * @param ps 
	 * @param logicalOperator
	 * @param logicalProperties
	 */
	void printLogicalOperator(PrintStream ps, Object logicalOperator, Object logicalProperties);

	// Functions related to physical operator

	/**
	 * Compute child required properties for the given required properties of
	 * physical operator.
	 * @param physicalOperator
	 * @param logicalOperator
	 * @param logicalProperties
	 * @param childrenLogicalProperties
	 * @param reqdProperties
	 * @return array of children required physical properties
	 */
	ArrayList<Object[]> getChildReqdProperties(Object physicalOperator, Object logicalOperator, Object logicalProperties, Object[] childrenLogicalProperties, Object reqdProperties);

	/**
	 * put physical property array returned by getChildReqdProperties back in memory object pool
	 * @param physicalPropertyArray
	 */
	void putPhysicalPropertiesArray(ArrayList<Object[]> physicalPropertyArray);
	
	/**
	 * compute the local cost of the physical operator and cache it for future
	 * use.
	 * 
	 * @param physicalOperator
	 * @param logicalOperator 
	 * @param logicalProperties
	 * @param childrenLogicalProperties
	 * @param reqdProperties
	 * @param childrenPhysicalProperties TODO
	 * @param memoryLatency
	 *            : output parameter - memoryLatency of physical operator
	 * @param processingLatency
	 *            : output parameter - processingLatency of physical operator
	 * @param diskLatency
	 *            : output parameter - diskLatency of physical operator
	 * @param networkLatency
	 *            : output parameter - networkLatency of physical operator
	 */
	void computeLocalCost(Object physicalOperator, Object logicalOperator, Object logicalProperties, Object[] childrenLogicalProperties,
			Object reqdProperties, Object[] childrenPhysicalProperties, MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency);

	/**
	 * compute the local cost of the physical operator and cache it for future
	 * use.
	 * 
	 * @param physicalOperator
	 * @param logicalOperator 
	 * @param logicalProperties
	 * @param childrenLogicalProperties
	 * @param reqdProperties
	 * @param childrenPhysicalProperties TODO
	 * @param memoryLatency
	 *            : output parameter - memoryLatency of physical operator
	 * @param processingLatency
	 *            : output parameter - processingLatency of physical operator
	 * @param diskLatency
	 *            : output parameter - diskLatency of physical operator
	 * @param networkLatency
	 *            : output parameter - networkLatency of physical operator
	 */
	void computeLocalCost_FirstTuple(Object physicalOperator, Object logicalOperator, Object logicalProperties, Object[] childrenLogicalProperties,
			Object reqdProperties, Object[] childrenPhysicalProperties, MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency);

	
	/**
	 * Print the physical operator.
	 * @param ps 
	 * @param physicalOperator
	 * @param logicalOperator TODO
	 * @param logicalProperties
	 */
	void printPhysicalOperator(PrintStream ps, Object physicalOperator, Object logicalOperator, Object logicalProperties);

	// Functions related to logical properties

	/**
	 * Return number of blocks in corresponding logical property, 
	 * It is used to prioritize different logical operators
	 * @param logicalProperties
	 * @return
	 */
	double getNumBlocks(Object logicalProperties);

	/**
	 * Merge logicalPropertyToMerge into srcLogiclProperty
	 * @param srcLogicalProperties
	 * @param logicalPropertiesToMerge
	 */
	void mergeProperty(Object srcLogicalProperties, Object logicalPropertiesToMerge);

	/**
	 * print the logical properties
	 * @param ps
	 * @param logicalProperties
	 */
	void printLogicalProperties (PrintStream ps, Object logicalProperties);

	// Functions related to physical properties

	/**
	 * Are the physical properties equivalent ?
	 * @param phyProp1
	 * @param phyProp2
	 * @return
	 */
	boolean isPhysicalPropertyEqual(Object phyProp1, Object phyProp2);

	boolean isPhysicalPropertyEqualHive(Object phyProp1, Object phyProp2, Object logOp);

	/**
	 * print the physical property
	 * @param ps
	 * @param physicalProperties
	 */
	void printPhysicalProperties (PrintStream ps, Object physicalProperties);
	
	//Functions related to rules
	
	/**
	 * List of all transformation rules
	 * @return
	 */
	ITransformationRule[] getTransformationRules();

	/**
	 * List of all implementation rules
	 * @return
	 */
	IImplementationRule[] getImplementationRules();

	/**
	 * List of all local property enforcer rules
	 * @return
	 */
	ILocalEnforcerRule[] getLocalEnforcerRules();

	/**
	 * List of all partition property enforcer rule.
	 * @return
	 */
	IPartitionEnforcerRule[] getPartitionEnforcerRules();

	// Utilities functions.
	
	/**
	 * Clear state
	 */
	void clearState();
	
	/**
	 * print number of object created
	 */
	void printInstanceCounts();
	//TODO: Add physical properties as argument to this method
	/**
	 * Compute the cost of using materialized results for logicalProperties.
	 * use cost also depends on the physical properties(Partitioned/Unpartitioned) of physical equivalence node
	 * @param logicalProperties
	 * 			  : Logical properties of the operator being 
	 * considered for materialization
	 * @param physicalProperties
	 * 				: Physical properties of the physical equivalence node 
	 * @param memoryLatency
	 *            : output parameter - memoryLatency of physical operator
	 * @param processingLatency
	 *            : output parameter - processingLatency of physical operator
	 * @param diskLatency
	 *            : output parameter - diskLatency of physical operator
	 * @param networkLatency
	 *            : output parameter - networkLatency of physical operator
	 */ 
	void computeUseCost(Object logicalProperties, Object physicalProperties ,MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency);
	
	/**
	 * Compute the cost of materializing an intermediate result. Properties
	 * of the result are indicated by logicalProperties. LogicalOperator and
	 * PhysicalOperator at the root of the node are also specified in case
	 * materialization cost depends on their types. For e.g. base table will
	 * often have materialization cost as 0 since it is already stored.
	 * @param logicalOperator
	 * 		: Logical operator at the root of the node
	 * @param logicalProperties
	 * 		: Logical properties of the intermediate result
	 * @param physicalOperator
	 * 		: Physical operator at the root of the node
	 * @param physicalProperty 
	 * 		: Physical properties of the physical equivalence node to which physical operator belongs   
	 * @param memoryLatency
	 * 		: output parameter - memoryLatency of physical operator
	 * @param processingLatency
	 * 		: output parameter - processingLatency of physical operator
	 * @param diskLatency
	 * 		:  output parameter - diskLatency of physical operator
	 * @param networkLatency
	 * 		: output parameter - networkLatency of physical operator
	 */
	void computeMaterializationCost(Object logicalOperator, Object logicalProperties, Object physicalOperator, Object physicalProperty, MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency);
	
	//first Tuple materialiczation cost
	void computeMaterializationCost_FirstTuple(Object logicalOperator, Object logicalProperties, Object physicalOperator, Object physicalProperty, MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency);
	
	/**
	 * unifyOperators method is called when Optimizer finds that two logical
	 * operators are equivalent and only one of them is to be preserved in 
	 * memo. This method gives runtime a chance to update any operator 
	 * specific state that is dependent on operator being replace. For e.g
	 * parent operator may hold references to the child operator.
	 * @param fromLogOp: Operator that is being replaced
	 * @param toLogOp  : Operator that is preserved
	 */
	void unifyOperators(Object fromLogOp, Object toLogOp);
	
	/** --------------------------------------------------------------------------------------------------------------------------------------------------------------------
	 	Following interface are related to Greedy Optimization
	 **/
	
	/**
	 * This method return true is the phyProp1 is covered by the phyProp2
	 * @param phyProp1
	 * @param phyProp2
	 * @return
	 */
    boolean isCovered(Object phyProp1, Object phyProp2) ;

    /**
     * This method return the empty physical property <null, null>
     * @return
     */
    Object getEmptyPropery();

    /**
     * Returns true is logical equivalence node related to the operator should not be considered for the sharing 
     * @param operator
     * @return
     */
	boolean isExcludedForSharing(Object operator);
	
	/**----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	 Following interfaces are related to the select subsumption derivation 
	 **/
	/**
	 * This method check whether a given operator is select operator (logical)
	 *   
	 * @param op
	 * @return
	 */
	public boolean isSelectOperator(Object op);
	
	/**
	 * implies relation checks whether predicate expression of first operator implies the  
	 * predicate expression of second operator of select operator. 
	 * 
	 *  Example :  x < 4 implies the x < 5 because first can be generated from second
	 *  
	 * This method should be implemented for the MQO   
	 * 
	 * @param operator1
	 * @param operator2
	 * @return true - if implies relation holds
	 * 			false - otherwise
	 */
	boolean implies(Object operator1, Object operator2);
	
	/**
	 * This method returns clone of the predicate expression of logical select operator. 
	 * Passed operator must be logical select.
	 * @param operator
	 * @return
	 */
	Object cloneSelectPredicate(Object operator);
	
	/**
	 * This predicate merge two predicate and return a new predicate.  
	 * @param condExpr1
	 * @param condExpr2
	 * @return  ( condExp1 OR condExpr2 )
	 */
	
	Object mergePredicates(Object condExpr1, Object condExpr2);
	
	/** 
	 * Check whether given select operator is correlated 
	 */
	boolean isCorrelated(Object ob);
	
	/**
	 * Check whether a given logical operator is Aggregate Operator
	 * @param op
	 * @return
	 */
	public boolean isAggregateOperator(Object op);


	/**
	 * impliesAggr checks whether first operator implies second operator
	 * Ex Grouping on {A,B} implies Grouping on {A}
	 * @param op1
	 * @param op2
	 * @return true - if implies relation holds
	 * 			false - otherwise
	 *
	 */
	public boolean impliesAggr(Object op1, Object op2);

	/**
	 * This method return the clone of operator. 
	 * If newchild argument is null then clone will have old child reference else newChild will replace the old child reference  
	 * if predicate is null clone will have old predicate ( two operator should not refer same predicate, predicates should be cloned ) else newPredicate will replace the old predicate    
	 * @param operator
	 * @param newChild
	 * @param newPredicate
	 * @return
	 */
	Object cloneSelectOperator(Object operator, Object newChild, Object newPredicate);
	//-----------------------------------------------------------------------------------------------------------------------


	Object cloneAggregateOperator(Object operator, Object newChild, Object newGrpbyExp);


	Object getnewOp(Object expr);

	Object cloneAggregateGroupByAttributes(Object operator);

	Object mergeGroupByAttributes(Object set1, Object set2);

	public boolean canMergeAggr(Object operator1, Object operator2);

	Object getAggregateOperator(Object operator1, Object operator2);

	Object getProjectOperator(Object operator1, Object operator2);

	boolean IsVirtualOperator(Object op);

	Object getFilterPhysicalOperator(Object logOp);
}
