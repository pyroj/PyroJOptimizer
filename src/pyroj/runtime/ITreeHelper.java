 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.runtime;

public interface ITreeHelper {

	boolean isRootMatches(Object patternTree, Object logicalOperator);

	boolean shouldRootBeLogicalOperator(Object patternTree);

	int getNumChildren(Object tree);

	Object[] getChildren(Object tree);

	Object getChild(Object tree, int index);

	Object getParent(Object patternTree);

	int getId(Object tree);

	int getNumNodes(Object patternTree);

	Object getRootOperator(Object exprTree);

	void print(Object exprTree);
	
	int getRootOperatorType(Object patternTree);
}
