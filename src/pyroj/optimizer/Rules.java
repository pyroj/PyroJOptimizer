 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import pyroj.runtime.IImplementationRule;
import pyroj.runtime.ILocalEnforcerRule;
import pyroj.runtime.IPartitionEnforcerRule;
import pyroj.runtime.ITransformationRule;

public class Rules {

	private final ILocalEnforcerRule[] _localEnforcerRules;
	private final IPartitionEnforcerRule[] _partitionEnforcerRules;
	private final IImplementationRule[] _implementationRules;
	private final ITransformationRule[] _transformationRules;

	/**
	 * Rules will be applied in the order on their position in list.
	 * 
	 * @param rules
	 */
	public Rules(ITransformationRule[] transformationRules,
			IImplementationRule[] implementationRules,
			ILocalEnforcerRule[] localEnforcerRules,
			IPartitionEnforcerRule[] partitionEnforcerRules) {
		_transformationRules = transformationRules;
		_implementationRules = implementationRules;
		_localEnforcerRules = localEnforcerRules;
		_partitionEnforcerRules = partitionEnforcerRules;
	}

	public ILocalEnforcerRule[] getLocalEnforcerRules() {
		return _localEnforcerRules;
	}
	
	public IPartitionEnforcerRule[] getPartitionEnforcerRules() {
		return _partitionEnforcerRules;
	}

	public IImplementationRule[] getImplementationRules() {
		return _implementationRules;
	}

	public ITransformationRule[] getTransformationRules() {
		return _transformationRules;
	}
}