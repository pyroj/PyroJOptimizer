


	/**
	 * @param args
	 */
/*	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
*/
package pyroj.optimizer.responceTimeBased;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

import pyroj.optimizer.Config;
import pyroj.optimizer.DegreeShare;

public class LogicalOperatorNode_RT {

	public static int NumInstances = 0;

	private final Object _rootOperator;
	private final LogicalEquivalenceNode_RT[] _children;

	private LogicalEquivalenceNode_RT _grp;

	private final int _id;
	private final BitSet _rulesApplied = new BitSet();
	private LogicalOperatorNode_RT _unifiedTo = null;

	// Changes for Greedy -------
	/** it is true if the degree list for the Eq node has been calculated  	 
	 * this variable shows that the degree list for all the parent has been calcuated
	 * so it is safe to delete the degree list associated with current Node
	 * TODO : Add this feature, (Not added yet)
	 * */
	private boolean isDegreeCalculated = false;
	
	/** degree list contains the child equivalence node and there degree
	 * degree list always sorted by the topological number
	 * (No separate sorting is performed , creation method inserts the element they
	 * are automatically in sorted order) 
	 */
	private List<DegreeShare> degreeList = null;
	
	/** mark that degree list for the equivalece node has been calculated 
	 * @param flag
	 */
	public void setDegreeCalculated(boolean flag) {
		isDegreeCalculated = flag;
	}
	
	public boolean isDegreeCalculated() {
		return isDegreeCalculated ;
	}
	
	/**
	 * return the degree list associated with the  this node
	 * @return 
	 */
	public List<DegreeShare> getDegreeList() {
		if (degreeList == null) {
			degreeList = createDegreeList();
		}
		return degreeList;
	}
	
	
	/**
	 * Thise method create the degree list of not yet created
	 * See Prasan thesis for details
	 * @return
	 */
	private List<DegreeShare> createDegreeList() {
		List <DegreeShare> list = new ArrayList<DegreeShare>();
		if (degreeList != null)
			return degreeList;
		
		int numChildren = getNumChildren();
		if ( numChildren > 0 ) {
			// input lists
			List<List<DegreeShare>> inpList = new ArrayList<List<DegreeShare>>(numChildren);
			
			// iterator for each input list
			List<Iterator<DegreeShare>>  inpIter = new ArrayList<Iterator<DegreeShare>>(numChildren);
			
			// set iterator for each inputList
			for (int i = 0; i< numChildren ; i++ ) {
				LogicalEquivalenceNode_RT logEqNode = getChild(i);
				inpList.add(i, logEqNode.getDegreeList());
				inpIter.add(i, inpList.get(i).iterator());
 			}
			
			// initialize the currentEntries
			// currEntry point the DegreeShare in each inpList  which has the lowest topNo
			// (Notice by construction of algo we ensure that each degree list is sorted 
			// according to the topological No)
			List<DegreeShare> curEntry = new ArrayList<DegreeShare>(numChildren);
			for (int i = 0 ; i < numChildren ; i++ ) {
				if (inpIter.get(i).hasNext()) {
					curEntry.add(i, inpIter.get(i).next());
				} else {
					curEntry.add(i, null);
				}
			}
			
			// traverse all the child degree list and take sum of all logical eq node with same topological no.
			for (;;) {
				int minTopNo = -1;
				
				// get the minimum topological number
				for ( int i = 0 ; i < numChildren ; i++  ) {
					if (curEntry.get(i) == null) 
						continue;
					
					LogicalEquivalenceNode_RT childEq = curEntry.get(i).getLogicalEquivalenceNode();
					int eqTopNo = childEq.getToplogocalNo();
					if ( minTopNo == -1 || eqTopNo < minTopNo ) 
						minTopNo = eqTopNo;
					
				}
				
				// if no more eq node left 
				if ( minTopNo == -1 )
					break;
				
				int sumDegree = 0 ;
				DegreeShare degShar = null;
				
				for ( int i = 0 ; i < numChildren; i++) {
					if (curEntry.get(i) == null) 
						continue;
					LogicalEquivalenceNode_RT eqNode = curEntry.get(i).getLogicalEquivalenceNode();
					
					int eqTopNo = eqNode.getToplogocalNo();
					
					if (eqTopNo != minTopNo) 
						continue;
					
					int currDegree = curEntry.get(i).getDegree();
					
					sumDegree += currDegree;
					degShar = curEntry.get(i);
					
					
					if (inpIter.get(i).hasNext()) {
						curEntry.set(i, inpIter.get(i).next());
					} else {
						curEntry.set(i, null);
					}
						
				}
				
				if (sumDegree == degShar.getDegree()) {
					list.add(degShar);
				} else {
					DegreeShare newDegShar = new DegreeShare(degShar.getLogicalEquivalenceNode(), sumDegree);
					list.add(newDegShar);
				}	
			}	
		}
		
		// TODO :Check if all the parent have calculated the dgree list delete existing
		// degree list
		setDegreeCalculated(true);
		degreeList = list;
		return list;
	}
	//------------------------
	
	// Changes of subsumption derivation
	
	/**
	 * isDrived Denotes whether the logical expression node is
	 * denerated during subsumption derivation, this is used in MQO
	 * true = drived by sumsumption
	 */
	private boolean _isDrived = false;
	
	public boolean isDrived() {
		return _isDrived;
	}
	
	public void markDrived(boolean flag) {
		_isDrived = false;
	}
	
	// ------------------------
	
	public LogicalOperatorNode_RT(Object root) {
		_rootOperator = root;
		_children = null;
		_id = NumInstances++;
	}

	public LogicalOperatorNode_RT(Object root, LogicalEquivalenceNode_RT child0) {
		_rootOperator = root;
		_children = new LogicalEquivalenceNode_RT[1];
		_children[0] = child0;
		_id = NumInstances++;
	}

	public LogicalOperatorNode_RT(Object root, LogicalEquivalenceNode_RT child0, LogicalEquivalenceNode_RT child1) {
		_rootOperator = root;
		_children = new LogicalEquivalenceNode_RT[2];
		_children[0] = child0;
		_children[1] = child1;
		_id = NumInstances++;
	}

	public LogicalOperatorNode_RT(Object root, LogicalEquivalenceNode_RT[] children) {
		_rootOperator = root;
		_children = children;
		_id = NumInstances++;
	}

	public Object computeLogicalProperties() {
		final Object[] childrenLogicalProperties = getChildrenProps();
		Object logProp = Config.OpHelper.computeOutputLogicalProperties(_rootOperator, childrenLogicalProperties);
		Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);  //TODO We dont have body implementation of this function, why we need this 
		return logProp;
	}

	@Override
	/*public boolean equals(Object obj) {
		if (!(obj instanceof LogicalOperatorNode_RT))
			return false;

		final LogicalOperatorNode_RT other = (LogicalOperatorNode_RT) obj;
		
		if (!Config.OpHelper.isLogOperatorEqual(_rootOperator, other._rootOperator))
			return false;
		
		if (_children != null) {
			for (int i = 0; i < _children.length; i++) {
				if (_children[i].getId() != other._children[i].getId()) {
					return false;
				}
			}
		}
		
		return true;
	}*/
	
	public boolean equals(Object obj) {
		if (!(obj instanceof LogicalOperatorNode_RT))
			return false;

		final LogicalOperatorNode_RT other = (LogicalOperatorNode_RT) obj;
		
		if (!Config.OpHelper.isLogOperatorEqual(_rootOperator, other._rootOperator))
			return false;
		
		if (_children != null) {
			for (int i = 0; i < _children.length; i++) {
				if(_children[i].GetCurrentActiveGroup().getId() != other._children[i].GetCurrentActiveGroup().getId()){
					return false;
				}
			}
		}
		
		return true;
	}
	
	
	

	
	public LogicalEquivalenceNode_RT getChild(int index) {
		return _children[index];
	}

	public LogicalEquivalenceNode_RT[] getChildren() {
		return _children;
	}

	public Object[] getChildrenProps() {
		if (_children == null)
			return null;

		final Object[] childProps = Config.OpHelper.getLogicalPropertiesArray(_children.length);

		for (int i = 0; i < _children.length; i++)
			childProps[i] = _children[i].getLogicalProperties();

		return childProps;
	}

	public LogicalOperatorNode_RT getCurrentActiveOperator() {
		if (_unifiedTo == null)
			return this;
		return _unifiedTo.getCurrentActiveOperator();
	}

	public int getFirstUnAppliedRuleIndex() {
		return _rulesApplied.nextClearBit(0);
	}

	public int getFirstUnAppliedRuleIndex(int startIndex) {
		return _rulesApplied.nextClearBit(startIndex);
	}

	public LogicalEquivalenceNode_RT getGroup() {
		return _grp;
	}

	public int getId() {
		return _id;
	}

	public double getInputSizeBlocks() {
		if (_children == null)
			return 0;

		double inputSize = 0;
		for (int i = 0; i < _children.length; i++) {
			inputSize += Config.OpHelper.getNumBlocks(_children[i].getLogicalProperties());
		}

		return inputSize;
	}

	public int getNumChildren() {
		if (_children == null)
			return 0;
		return _children.length;
	}

	public Object getOperator() {
		return _rootOperator;
	}

	public boolean getRuleApplied(int ruleIndex) {
		return _rulesApplied.get(ruleIndex);
	}

	@Override
	public int hashCode() {
		int hashCode = Config.OpHelper.getLogOperatorLocalHashCode(_rootOperator);
		if (_children != null) {
			for (int i = 0; i < _children.length; i++)
				hashCode *= _children[i].getId();
		}
		
		return hashCode;
	}

	public void print() {
		int grpId = -1;
		if (_grp != null)
			grpId = _grp.getId();

		System.out.print(grpId + "." + _id + ".");
		Config.OpHelper.printLogicalOperator(System.out, _rootOperator, _grp.getLogicalProperties());
	}
	

	@Override
	public String toString() {
		return " ";
	}

	public void printDAG(int indentLevel) {
		System.out.println();
		for (int i = 0; i < indentLevel; i++)
			System.out.print("| ");
		print();

		if (_children != null) {
			for (int i = 0; i < _children.length; i++) {
				_children[i].printDAG(indentLevel + 1);
			}
		}
	}

	public void printExpr(PriorityQueue<LogicalEquivalenceNode_RT> childGroups) {
		print();

		if (_children != null) {
			System.out.println();
			System.out.print("  <");
			for (int i = 0; i < _children.length; i++) {
				if (i > 0)
					System.out.print(", ");
				final LogicalEquivalenceNode_RT child = _children[i];
				child.printshort();
				if (childGroups != null)
					childGroups.add(child);
			}
			System.out.print(">");
		}
		System.out.println();
	}

	public void setChild(int index, LogicalEquivalenceNode_RT child) {
		_children[index] = child;
	}

	public void setGroup(LogicalEquivalenceNode_RT grp) {
		_grp = grp;
	}

	public void setRuleApplied(int ruleIndex) {
		_rulesApplied.set(ruleIndex);
	}

	public void setUnifiedTo(LogicalOperatorNode_RT op) {
		_unifiedTo = op;
	}
	
	
	private BitSet get_rulesApplied() {
		return _rulesApplied;
	}

	
	// For Unification
	public void updateAppliedRules(LogicalOperatorNode_RT other){
		BitSet _rulesApplied_other = other.get_rulesApplied();
		for (int i = _rulesApplied_other.nextSetBit(0); i >= 0; i = _rulesApplied_other.nextSetBit(i+1)) {
			     this._rulesApplied.set(i);
			 }
	}
}


