package pyroj.optimizer.responceTimeBased;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import pyroj.costmodel.CostModel_RT;
import pyroj.optimizer.Config;
import pyroj.optimizer.Rules;
import pyroj.runtime.IImplementationRule;
import pyroj.runtime.ILocalEnforcerRule;
import pyroj.runtime.IPartitionEnforcerRule;

public class PhysicalEquivalenceNode_RT {
	//stores total sum of #pareto plans in all children + #pareto plans for itself (i.e. size of own bestcost cover set)
	protected int numberOfParetoPlansInSubtree=0;
	public static int numOfPlansDeleted=0; 
	/** isOptimized is true if the physical Eq node has been optimized completely, 
	 * considering algorithmic as well as enforcer operators as child operators i.e 
	 * by applying partition enforcer rues also*/
	private boolean _isOptimized = false;
	/**
	 * _isOptimizedForOnlyAlgPlan is true if the physical Eq node has been optimized by considering its 
	 * algorithmic (or local enforcer child operators only), partition enforcer operators are not considered,
	 * i.e. while expanding, applyPartitionEnforcerRule is not done
	 */
	private boolean _isOptimizedForOnlyAlgPlan = false;
	static public int NumInstances = 0;

	private final int _id;

	private final LogicalEquivalenceNode_RT _group;
	private final Object _physicalProperties;

	//they should be null, as it is checked in function expand, so do not initialize in constructor 
	private ArrayList<PhysicalOperatorNode_RT> _algOperators;
	private ArrayList<PhysicalOperatorNode_RT> _enfOperators;
	
	/**
	 * Cost of current best plan (algorithmic / enforcer) for the phyEqNode.
	 * Reference to index of _currentBestPlanCost List of operator with min
	 *
	 * Stores multiple non comparable best plan costs. 
	 * As each child PhysicalOperatorNode has its best cost stored as List<int[]> (_currentBestPlanCost)
	 * So, in each pair in the list below, 
	 * int represents the index in List _currentBestPlanCost of PhysicalOperatorNode_RT  
	 * previous PhyEqNode class has _currentBestPlanOp and _idCurrentBestPlanCost, now this alone can give both
	 * Imp***: this integer in pair is not cost id, it's index in _currentBestPlanCost of PhyOpNode from that we can get costObject's Id
	 * So, in constructor, when it is set to -1 as no child PhyOpNode is there yet
	 **/
	private final List< MutablePair<PhysicalOperatorNode_RT, Integer>> _currentBestPlanOpAndIndex;
	
	private final List< MutablePair<PhysicalOperatorNode_RT, Integer>> _currentOnlyAlgoBestPlanOpAndIndex;
	
	private List<Pair<Integer, Plan_RT>> cachedBestPlan = null;

	public static int NumEqNodesDuringLastUpdate = 0;

	public static int NumOpNodesDuringLastUpdate = 0;
	
	private boolean _isRoot = false;

	private boolean _isEnfPlanActive = false;
	/**
	 * cost lower bound among algorithm operators only. _algCostLB >= _costLB
	 * Reference to costLB object of algorithmic operator with minimum costLB
	 */
	private int _idAlgCostLB = Config.CostModel.AllocZeroCostObj();

	/**
	 * Overall cost lower bound. Minimum cost lower bound among algorithm and
	 * enforcer operators. Reference to costLB object of operator with minimum
	 * costLB
	 */
	private int _idCostLB = Config.CostModel.AllocZeroCostObj();

	/**
	 * Cost upper from from only algorithmic parents. It is the upper bound for
	 * enforcer operators. _costUBFromAlgParents <= _costUBFromParents.
	 * Reference to childCostUB object of algorithmic parent with maximum
	 * childCostUB
	 */
	private int _idCostUBFromAlgParents = Config.CostModel.MaxCostId();

	/**
	 * overall cost upper bound from all parents, it is cost upper bound for
	 * algorithm as well as enforcer operators. Reference to childCostUB object
	 * of parent with maximum childCostUB.
	 */
	private int _idCostUBFromParent = Config.CostModel.MaxCostId();	
	
	/** Number of times current PhysicalEquivalenceNode_RT 
	 *  is used in plan
	 * */
	private int useCount = 0;
	private final ArrayList<PhysicalOperatorNode_RT> _algParents = new ArrayList<PhysicalOperatorNode_RT>();
	private final ArrayList<PhysicalOperatorNode_RT> _enfParents = new ArrayList<PhysicalOperatorNode_RT>();
	private boolean shared;
	
	/** Indicates which equivalence nodes are in best plan.
	 *  This can be used for other purposes too.
	 * */
	private boolean mark = false;

	// Add for Greedy ----	
	/** this method refer to the bestPlanObj in case the plan is revisited this object would be returned 
	 * At present this is used only in greedy optimization. 
	 * TODO : for SharSh we have same kind of technique. We can merge two methods (if possible)  
	 * */
	private Plan_RT bestPlanObj = null;
	// -------	

		
	public PhysicalEquivalenceNode_RT(LogicalEquivalenceNode_RT group, Object physicalProperties) {
		_group = group;
		_physicalProperties = physicalProperties;
		_id = NumInstances++;
		_currentBestPlanOpAndIndex = new ArrayList<MutablePair<PhysicalOperatorNode_RT, Integer>>();
		_currentBestPlanOpAndIndex.add(new MutablePair<PhysicalOperatorNode_RT, Integer>(null, -1));
		_currentOnlyAlgoBestPlanOpAndIndex = new ArrayList<MutablePair<PhysicalOperatorNode_RT, Integer>>();
		_currentOnlyAlgoBestPlanOpAndIndex.add(new MutablePair<PhysicalOperatorNode_RT, Integer>(null, -1));
	}
	
	
	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	/** returns whether current PhysicalEquivalenceNode_RT is shared*/
	public boolean isShared() {
		return shared;
	}

	/** Sets the current PhysicalEquivalenceNode_RT as shared or unshared*/
	public void setShared(boolean shared) {
		this.shared = shared;
	}

	public void setRoot() {
		_isRoot = true;
	}

	/**
	 * Returns algorithmic parents of the PhysicalEquivalenceNode_RT
	 * */
	public List<PhysicalOperatorNode_RT> getAlgParents() {
		return new ArrayList<PhysicalOperatorNode_RT>(_algParents);
	}
	
	
	/** Mark the node. The purpose of marking is dependent
	 * on user of this class.
	 * */
	public void setMarked(boolean n) {
		mark = n;
	}
	
	/** Returns true if marked is true
	 * */
	public boolean isMarked() {
		return mark;
	}
	
	/** Indicates which equivalence nodes are visited in
	 * current traversal. This can be used for other 
	 * purposes too. TraversalId is useful because
	 * otherwise, we would need to reset visited for
	 * each node in between two traversals for all nodes
	 * */
	private int traversal = 0;
	
	/** Mark the node as visited. The purpose of marking is dependent
	 * on user of this class
	 * */
	public void setTraversal(int n) {
		traversal = n;
	}
	
	/** Returns traversal id of the latest traversal
	 * */
	public boolean isTraversed(int currentTraversal) {
		return traversal == currentTraversal;
	}

	
	public boolean isOptimized() {
		return _isOptimized;
	}
	
	public void setOptimized(boolean flag) {
		_isOptimized = flag;
	}
	
	public boolean isOptimizedForOnlyAlgPlan() {
		return _isOptimizedForOnlyAlgPlan;
	}


	public void setOptimizedForOnlyAlgPlan(boolean flag) {
		this._isOptimizedForOnlyAlgPlan = flag;
	}

	/**
	 * m done
	 * @param onlyAlgo
	 * @return
	 */
	public List<MutablePair<PhysicalOperatorNode_RT, Integer>> getCurrentBestPlanOpAndIndex(boolean onlyAlgo) {
		if(onlyAlgo){
			return _currentOnlyAlgoBestPlanOpAndIndex;
		}
		else{
			return _currentBestPlanOpAndIndex;
		}
	}
	
	// Added for Greedy-----------------
	/**
	 * This method only maintain the list of the _algParent and _enfParent. In future this method can be merged with addParent method
	 */
	public void addOnlyParent(PhysicalOperatorNode_RT parent , boolean isAlgOperator ) {
		if (isAlgOperator) 
			_algParents.add(parent);
		else 
			_enfParents.add(parent);

	}
	
	//----------
	
	/*public void addParent(PhysicalOperatorNode_RT parent, int idCostUBFromParent, boolean isAlgOperator) {

		if (isAlgOperator) {
			_algParents.add(parent);

			if (_algParents.size() == 1 || Config.CostModel.IsLessThan(_idCostUBFromAlgParents, idCostUBFromParent)) {
				_idCostUBFromAlgParents = idCostUBFromParent;

				if (_enfOperators != null) {
					for (int i = 0; i < _enfOperators.size(); i++)
						_enfOperators.get(i).updateCostUB();
				}
			}

		}
		else {
			_enfParents.add(parent);
		}
		if (_enfParents.size() + _algParents.size() == 1
				|| Config.CostModel.IsLessThan(_idCostUBFromParent, idCostUBFromParent)) {
			_idCostUBFromParent = idCostUBFromParent;

			if (_algOperators != null) {
				for (int i = 0; i < _algOperators.size(); i++)
					_algOperators.get(i).updateCostUB();
			}
		}
	}
	*/

	/**
	 * m Done
	 * @param rules
	 */
	private void applyPartitionEnforcerRules(IPartitionEnforcerRule[] rules) {
		if (_enfOperators == null)
		_enfOperators = new ArrayList<PhysicalOperatorNode_RT>();
		
		//not need as _idCostLB already updated during localEnf application
		if (Config.isPruningEnabled == true)
			Config.CostModel.Assign(_idCostLB, _idAlgCostLB);

		if (_physicalProperties == null) {
			return;
		}

		//((IPhysicalPropertiesVector)_physicalProperties).getPartitioningProperty();
		//((IPhysicalPropertiesVector)_physicalProperties).getLocalProperties();
		
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		//print();
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		//Config.OpHelper.printPhysicalProperties(System.out, _physicalProperties);
		
		for (int ruleIndex = 0; ruleIndex < rules.length; ruleIndex++) {
		

			
			final Pair<Object, Object> outputOperator = rules[ruleIndex].getOutputOperator(_physicalProperties);

			if (outputOperator == null)
				continue;
			
			final Object outputLogOp = outputOperator.getLeft();
			final Object outputPhyOp = outputOperator.getRight();
			rules[ruleIndex].putPair(outputOperator);

			final Object logicalProperties = _group.getLogicalProperties();

			final ArrayList<Object[]> childrenReqdPropertiesArray = Config.OpHelper.getChildReqdProperties(outputPhyOp,
					 outputLogOp, logicalProperties, null, _physicalProperties);

			for (Object[] childrenReqdProperties : childrenReqdPropertiesArray) {
				final PhysicalEquivalenceNode_RT[] children = new PhysicalEquivalenceNode_RT[] { _group
						.getPhysicalEqNode(childrenReqdProperties[0]) };
				//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				//Config.OpHelper.printPhysicalProperties(System.out, childrenReqdProperties[0]);	
				final PhysicalOperatorNode_RT phyOpNode = new PhysicalOperatorNode_RT(false, outputPhyOp,outputLogOp, this, children, childrenReqdProperties);
				_enfOperators.add(phyOpNode);
	
				final int idOpCostLB = phyOpNode.getCostLBId();
				// changed to disable pruning
			//	if (Config.isPruningEnabled == true) {  //TODO Sandeep Pruning
			//		((CostModel_RT)(Config.CostModel)).setComponentToLowerValuesAndSync(_idCostLB, idOpCostLB);
			//	}
				
				// update currentBestPlan
				/* decided not to update best plan cost here. 
				 * it will be updated only when the child op not is optimized */			
	
				if (_isRoot) {
					NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode_RT.NumInstances;
					NumOpNodesDuringLastUpdate = PhysicalOperatorNode_RT.NumInstances;
				}
				
			}
			Config.OpHelper.putPhysicalPropertiesArray(childrenReqdPropertiesArray);
		}
	}

	// old method
	/**
	 * m Done
	 * @param rules
	 */
	private void applyLocalEnforcerRules(ILocalEnforcerRule[] rules) {

		if (_physicalProperties == null) {
			return;
		}

		for (int ruleIndex = 0; ruleIndex < rules.length; ruleIndex++) {

			final Pair<Object, Object> outputOperator = rules[ruleIndex].getOutputOperator(_physicalProperties);

			if (outputOperator == null)
				continue;

			final Object outputLogOp = outputOperator.getLeft();
			final Object outputPhyOp = outputOperator.getRight();		
			rules[ruleIndex].putPair(outputOperator);
			
			final Object logicalProperties = _group.getLogicalProperties();
			final ArrayList<Object[]> childrenReqdPropertiesArray = Config.OpHelper.getChildReqdProperties(outputPhyOp,
					outputLogOp, logicalProperties, null, _physicalProperties);

			for (Object[] childrenReqdProperties : childrenReqdPropertiesArray) {
				final PhysicalEquivalenceNode_RT[] children = new PhysicalEquivalenceNode_RT[] { _group.getPhysicalEqNode(childrenReqdProperties[0]) };
	
				final PhysicalOperatorNode_RT phyOpNode = new PhysicalOperatorNode_RT(true, outputPhyOp, outputLogOp, this, children, childrenReqdProperties);
				// changed as localEnf was being added in the _agl
				_algOperators.add(phyOpNode);
				phyOpNode.setLocalEnforcer(true);
				//_enfOperators.add(phyOpNode);
	
				final int idOpCostLB = phyOpNode.getCostLBId();
				// changed to disable pruning
	//			if (Config.isPruningEnabled == true) {  // TODO Sandeep Pruning
		//		((CostModel_RT)(Config.CostModel)).setComponentToLowerValuesAndSync(_idAlgCostLB, idOpCostLB);
		//		}
				
				// update currentBestPlan
				/* decided not to update best plan cost here. 
				 * it will be updated only when the child op not is optimized */
	
				if (_isRoot) {
					NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode_RT.NumInstances;
					NumOpNodesDuringLastUpdate = PhysicalOperatorNode_RT.NumInstances;
				}
			}
			Config.OpHelper.putPhysicalPropertiesArray(childrenReqdPropertiesArray);
		}	
		//Config.CostModel.Assign(_idCostLB, _idAlgCostLB);
	}
	
	
		
	/**m Done
	 * This method apply all set of rules passed on all the logical expressions present in logicalEq node.
	 * @param rules
	 */
	private void applyImplementationRules(IImplementationRule[] rules) {

		_algOperators = new ArrayList<PhysicalOperatorNode_RT>();
		Config.CostModel.SetToMax(_idAlgCostLB);

		final int numLogicalExpressions = _group.getNumLogExpr();
		
		// process all logical expression 
		for (int logExprIndex = 0; logExprIndex < numLogicalExpressions; logExprIndex++) {

			final LogicalOperatorNode_RT expr = _group.getLogicalExpression(logExprIndex);
			if (expr == null)
				continue;

			// apply all implementation rules
			for (int implementationRuleIndex = 0; implementationRuleIndex < rules.length; implementationRuleIndex++) {

				// check if rule is for the root operator of the expression 
				if (!rules[implementationRuleIndex].isApplicable(expr.getOperator()))
					continue;

				final Object logicalOperator = expr.getOperator();
				final Object logicalProperties = _group.getLogicalProperties();
				final Object[] childrenLogicalProperties = expr.getChildrenProps();
				final Object outputOperator = rules[implementationRuleIndex].getOutputOperator(logicalOperator,
						logicalProperties, childrenLogicalProperties, _physicalProperties);

				if (outputOperator == null) {
					Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);
					continue;
				}

				// has no more child
				if (expr.getNumChildren() == 0) {
					Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);

					final PhysicalOperatorNode_RT phyOpNode = new PhysicalOperatorNode_RT(true, outputOperator, logicalOperator, this, null, null);
					_algOperators.add(phyOpNode);
					
					// update algorithmic lower bound
					final int idOpCostLB = phyOpNode.getCostLBId();
					((CostModel_RT)(Config.CostModel)).setComponentToLowerValuesAndSync(_idAlgCostLB, idOpCostLB);
					
					// update currentBestPlan
					/* decided not to update best plan cost here. 
					 * it will be updated only when the child op not is optimized */
					
					if (_isRoot) {
						NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode_RT.NumInstances;
						NumOpNodesDuringLastUpdate = PhysicalOperatorNode_RT.NumInstances;
					}
				}
				
				else {
					// expr have children
					
					// calculate the children required property
					final ArrayList<Object[]> childrenReqdPropertiesArray = Config.OpHelper.getChildReqdProperties(outputOperator,
							logicalOperator, logicalProperties, childrenLogicalProperties, _physicalProperties);
	
					Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);
					
					for (Object[] childrenReqdProperties : childrenReqdPropertiesArray) {
						
						// get all child PhysicalEquivalence Node
						final PhysicalEquivalenceNode_RT[] children = computePhyEqNodes(expr.getChildren(), childrenReqdProperties);
						final PhysicalOperatorNode_RT phyOpNode = new PhysicalOperatorNode_RT(true, outputOperator, logicalOperator, this, children, childrenReqdProperties);
						_algOperators.add(phyOpNode);
						
						// update algLowerBound
						final int idOpCostLB = phyOpNode.getCostLBId();
						
						// changed to disable pruning
		//				if (Config.isPruningEnabled == true) {
			//				((CostModel_RT)(Config.CostModel)).setComponentToLowerValuesAndSync(_idAlgCostLB, idOpCostLB);
			//			}
						
						// update currentBestPlan
						/* decided not to update best plan cost here. 
						 * it will be updated only when the child op not is optimized */
						
						if (_isRoot) {
							NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode_RT.NumInstances;
							NumOpNodesDuringLastUpdate = PhysicalOperatorNode_RT.NumInstances;
						}
					}
					Config.OpHelper.putPhysicalPropertiesArray(childrenReqdPropertiesArray);
				}
			}
		}
		Config.CostModel.Assign(_idCostLB, _idAlgCostLB);
	}

	/**
	 * m done
	 * @param groups
	 * @param reqdPropertiescurrentBestPlanOpAndIndex
	 * @return
	 */
	private PhysicalEquivalenceNode_RT[] computePhyEqNodes(LogicalEquivalenceNode_RT[] groups, Object[] reqdProperties) {
		if (groups == null || groups.length == 0)
			return null;

		final PhysicalEquivalenceNode_RT[] children = new PhysicalEquivalenceNode_RT[groups.length];
		for (int i = 0; i < groups.length; i++)
			children[i] = groups[i].getPhysicalEqNode(reqdProperties[i]);

		return children;
	}

	/**m Done
	 * This method apply the all the rules passed in rule set on this physicalEqNode and 
	 * generate all the possible physical plans 
	 * @param onlyAlgPlan : true if only generate algorithmic plan
	 * @param rules : set of implementation rule to be applied
	 */
	public void expand(boolean onlyAlgPlan, Rules rules) {
		
		// check if already expanded   
		if (_algOperators != null && _enfOperators != null)
			return;
		//TODO Sandeep Pruning : Check for this flag and also for condition of calling these rules
		
		//if (onlyAlgPlan && _algOperators != null && Config.isPruningEnabled == true)
		//if (onlyAlgPlan && _algOperators != null)
		//	return;
		
		// generate algorithmic plan
		
		
		
		if (_algOperators == null) {
			applyImplementationRules(rules.getImplementationRules());
			applyLocalEnforcerRules(rules.getLocalEnforcerRules());
		}
		
		if (!onlyAlgPlan && _enfOperators == null){
			applyPartitionEnforcerRules(rules.getPartitionEnforcerRules());
		}
		// generate enforcer plans
		//if ((!onlyAlgPlan && _enfOperators == null) || (_enfOperators == null && Config.isPruningEnabled == false)) {
			//Mohit: I have comment the OR part of the above condition before taking the temp results without pruning. Check if it is required
		

		if (Config.PrintPhysicalNodeCreation)
			//printLong();
				printOperatorNode();
	}

	public PhysicalOperatorNode_RT getChild(int index) {

		if (index < _algOperators.size())
			return _algOperators.get(index);
		index -= _algOperators.size();

		return _enfOperators.get(index);
	}

	/**
	 * m Done
	 * @param onlyAlgPlan
	 * @return
	 */
	public int getCostLBId(boolean onlyAlgPlan) {
	//	if (onlyAlgPlan && Config.isPruningEnabled == true) // TODO Sandeep Pruning : why flag in first cond.
	if (onlyAlgPlan)	
		return _idAlgCostLB;
	else
		return _idCostLB;
	}

	/*public int getCostUBId(boolean forAlgOperator) {

		final int idCostUBFromParent = (forAlgOperator) ? _idCostUBFromParent : _idCostUBFromAlgParents;

		if (Config.CostModel.IsLessThan(idCostUBFromParent, _idCurrentBestPlanCost))
			return idCostUBFromParent;
		else
			return _idCurrentBestPlanCost;

	}*/

	public int getId() {
		return _id;
	}

	public LogicalEquivalenceNode_RT getLogicalEqNode() {
		return _group;
	}

	public Object getLogicalProperties() {
		return _group.getLogicalProperties();
	}

	public int getNumberOfChildren(boolean onlyAlgOperators) {
		int numOperators = 0;
		if (_algOperators != null)
			numOperators += _algOperators.size();

		if (!onlyAlgOperators && _enfOperators != null)
			numOperators += _enfOperators.size();

		return numOperators;
	}

	public Object getPhysicalProperties() {
		return _physicalProperties;
	}

	public boolean isEnfPlanActive() {
		return _isEnfPlanActive;
	}

	/**
	
	
	
	public void printId() {
		System.out.print(_group.getId() + "." + _id);
	}

	/**
	 * Print all the operators contained with in a physicalEq node 
	 */
	private void printOperatorNode() {
		if (_algOperators != null ) {
			for (PhysicalOperatorNode_RT phyOpNode : _algOperators) {
				System.out.print("\t\t");
				phyOpNode.printLong();
			}
		}
		if (_enfOperators != null ) {
			for (PhysicalOperatorNode_RT phyOpNode : _enfOperators) {
				System.out.print("\t\t");
				phyOpNode.printLong();
			}
		}
	}
	
	
	public void resetEnfPlanActive() {
		_isEnfPlanActive = false;
	}

	public void setEnfPlanActive() {
		_isEnfPlanActive = true;
	}

	/**m done
	 * recomputes the Algorithmic or overall lower bound and set to min components values among all current LB of children 
	 * @param onlyAlgoLB
	 */
	public void recomputeCostLB(boolean onlyAlgoLB) {
		final int numAlgoOperators = getNumberOfChildren(true);
		final int numOperators = getNumberOfChildren(false); //total number of children operator nodes
		if(numOperators == 0){
			return;
		}
		// update algLowerBound
		Config.CostModel.Assign(_idAlgCostLB, getChild(0).getCostLBId());
		for (int i = 1; i < numAlgoOperators; i++) {
			final int idOpCostLB = getChild(i).getCostLBId();			
			((CostModel_RT)(Config.CostModel)).setComponentToLowerValuesAndSync(_idAlgCostLB, idOpCostLB);		
		}
		if(onlyAlgoLB){
			return;
		}
		//update overall lower bound
		Config.CostModel.Assign(_idCostLB,_idAlgCostLB);
		for (int i = numAlgoOperators; i <numOperators ; i++) {
			final int idOpCostLB = getChild(i).getCostLBId();			
			((CostModel_RT)(Config.CostModel)).setComponentToLowerValuesAndSync(_idCostLB, idOpCostLB);
		}
		assert ((CostModel_RT)(Config.CostModel)).IsValid(_idAlgCostLB) : "_idCostLB become invalid in recomputeCostLB";
		assert ((CostModel_RT)(Config.CostModel)).IsValid(_idCostLB) : "_idCostLB become invalid in recomputeCostLB";
		
	}
	
	
	
	
	
	/**m Done It works fine for obj=1 also. no change required for computing total work component of cost obj. So done_28may'15
	 * This method should be called by child operator after the child operator is completely optimized
	 * means the assumption is that before calling this child op must have calculated its bestPlanCosts coverset
	 * and thus this method should be called by each child op "atmost once" 
	 * @param childBestCosts : chilgren operator's bestcosts, which need to be merged to bestCostCover set 
	 * of this phyEqNode
	 * @param childOp
	 * @param isAlgoOp : if algoOp, then merge to _currentOnlyAlgoBestPlanOpAndIndex 
	 * else merge to overallBestCost list i.e. _currentBestPlanOpAndIndex
	 */
	public void mergeIntoBestCostCoverSet(int[] childBestCosts, PhysicalOperatorNode_RT childOp, 
			boolean isAlgoOp) {
		List<MutablePair<PhysicalOperatorNode_RT, Integer>> myCostList;
		if(isAlgoOp){
			myCostList = _currentOnlyAlgoBestPlanOpAndIndex;
		}
		else{
			myCostList = _currentBestPlanOpAndIndex;
		}
		
		//System.out.println("IN mergeIntoBestCostCoverSet  _currentBestPlanOpAndIndex size is "+myCostList.size());
	//	if(myCostList.size())
		//TODO: ERROR: What to do if  NUll Pointer Exception here. 
		
		// --by Mayuri -- 
		if(myCostList.get(0).getLeft() == null)
			//if(myCostList.size()==0)   // Sandeep
			{ 
				myCostList.clear();
				for(int i=0; i<childBestCosts.length; i++){
					myCostList.add(new MutablePair<PhysicalOperatorNode_RT, Integer>(childOp, i));
				}
				if(childBestCosts.length==0)
					myCostList.add((new MutablePair<PhysicalOperatorNode_RT, Integer>(null, -1)));
				return;
		}
		for(int i=0; i < childBestCosts.length; i++){
			boolean isAdded = false;
			int j=0;
			for(j=0; j<myCostList.size(); j++){
				int index = myCostList.get(j).getRight();
				int xCostId = myCostList.get(j).getLeft().get_currentBestPlanCost().get(index)[2];
				
				assert ((CostModel_RT)(Config.CostModel)).IsValid(xCostId) : "coming cost is invalid";
				assert ((CostModel_RT)(Config.CostModel)).IsValid(childBestCosts[i]) : "coming cost is invalid";
				
				if(Config.CostModel.IsGreaterThanOrEqual(childBestCosts[i], xCostId)){
					break;
				}
				else if(Config.CostModel.IsLessThan(childBestCosts[i], xCostId)){
					PhysicalOperatorNode_RT.numOfPlansBeforeMerge += myCostList.get(j).getLeft().get_currentBestPlanCost().size();
					myCostList.set(j, null);
					numOfPlansDeleted++;
					
					if(!isAdded){
						myCostList.add(new MutablePair<PhysicalOperatorNode_RT, Integer>(childOp, i));
						isAdded=true;
					}
				}
				
			}
			if(isAdded){
				//previous cost objects which are covered by this idCostObject, are set to null, so remove all those
				
				List<MutablePair<PhysicalOperatorNode_RT, Integer>> temp=  new ArrayList<MutablePair<PhysicalOperatorNode_RT, Integer>>();  
				temp.add(null);
				myCostList.removeAll(temp);
			}
			else if(j>=myCostList.size() && !isAdded){
				//it is incomparable to all costs in myCostList. so need to be added as new best cost
				myCostList.add(new MutablePair<PhysicalOperatorNode_RT, Integer>(childOp, i));
			}
		}
		//System.out.println("IN mergeIntoBestCostCoverSet EXIT  _currentBestPlanOpAndIndex size is "+myCostList.size());
		assert (!(Config.OBJECTIVE==1 && myCostList.size()>1)) : "for obj=1 only 1 best plan should be remained in bestcostlist "; 
	}
	


		/**
		 * @param args
		 */
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			PhysicalEquivalenceNode_RT P= new PhysicalEquivalenceNode_RT(null, null);
			if (P._algOperators == null && P._enfOperators == null){
				System.out.println("yeah both are null");
			}
			
			System.out.println("thank u :) ");
			

		}


		public String printId() {
			System.out.print(_id);
			return ""+_id;
			
		}

		public void printGroupAndId() {
			_group.printshort(); 
			System.out.print(" : PhyEqNodeId: " +_id);
			
		}

		
		public void printShort() {
			printId();
			System.out.print(":PhyEqNode, properties = <");
			Config.OpHelper.printPhysicalProperties(System.out, _physicalProperties);
			System.out.print(">");
		}
		

		/**
		 * This method is used to print all the plans present in this Physical Equivalence node
		 */
		//modified on 3 feb'15 by Mayuri
		public void print() {		
			System.out.println();
			printShort();
			System.out.println();
			System.out.println("\t\t\t\t\t##############");
			System.out.println("\t\t\t\t\tOnlyAlgo Best plan cover set:");
			printBestCostCoverSet(true, 5);
			System.out.println("\t\t\t\t\t overall Best plan cover set:");
			printBestCostCoverSet(false, 5);
			System.out.println("\t\t\t\t\t##############");
			System.out.println("Operators as follows:>>>>>");
			System.out.println();
			final int numOperators = getNumberOfChildren(false);
			for (int i = 0; i < numOperators; i++) {
				getChild(i).print();
				System.out.println();
			}
			
		}
		
		/*
		public void printCurrentPlan(int indentLevel)
		{
			if(_currentBestPlanOpAndIndex == null)
				return;

			PhysicalOperatorNode_RT currentBestPlan_op = null;
			
			int index=0;
			//find the child operator node which has min response time among the cover set
			for(MutablePair<PhysicalOperatorNode_RT, Integer> PhyOPindexPair : _currentBestPlanOpAndIndex)
			{
					currentBestPlan_op = PhyOPindexPair.left;
					if(currentBestPlan_op == null)  // TODO: ERROR sandeep
						return;
					index = PhyOPindexPair.right;
					currentBestPlan_op.printPlan(index,indentLevel);
					System.out.println();
					for (int i = 0; i < indentLevel; i++)
						System.out.print("computeCurrentBestPlanCost| ");
					currentBestPlan_op.print(index);
					PhysicalEquivalenceNode_RT[] children = currentBestPlan_op.getChildren();
					if (children == null) 
						return;
					for (int i = 0; i < children.length; i++) {
						
						children[i].printCurrentPlan(indentLevel + 1);
			}
			
		}
			
		}
		*/
		
		//mofified on 3 Feb'15 by Mayuri
		public String printCurrentBestPlan(int indentLevel, StringBuilder detailed_bestPlanStr) {
			/*
			System.out.println();
			for (int i = 0; i < indentLevel; i++)
				System.out.print(" ");
			*/
			if(_currentBestPlanOpAndIndex == null)
				return "";

			PhysicalOperatorNode_RT currentBestPlan_op = null;
			
			int minvalId =Config.CostModel.MaxCostId();//this is correct :)
			int index=0;
			//find the child operator node which has min objective cost (response time or total work) among the cover set
			for(MutablePair<PhysicalOperatorNode_RT, Integer> PhyOPindexPair : _currentBestPlanOpAndIndex)
			{
				if(PhyOPindexPair.left == null){
					break; //added to remove null pointer exception
					//this can be the condition when the phyEqNode is at leaf so it does not have any child. 
					//so we are adding entry (null,-1) in its _currentBestPlanOpAndIndex in mergeIntoBestCostCoverSet function
				}
				int tempcostID= PhyOPindexPair.left.get_currentBestPlanCost().get(PhyOPindexPair.right)[2];
				
				double cmp = ((CostModel_RT)Config.CostModel).compareOnObjectiveCost(minvalId,tempcostID );
				if(cmp > 0){
					minvalId = tempcostID;
					currentBestPlan_op = PhyOPindexPair.left;
					index = PhyOPindexPair.right;
				}				
			}
			
			if(currentBestPlan_op==null)
				return "";
			currentBestPlan_op.printPlan(index,indentLevel, detailed_bestPlanStr);
			
			//--returning so that we can print in output file for testing & graph plotting
			StringBuilder str = new StringBuilder("");
			//str.append(currentBestPlan_op.getBestCostAt(index));
			double[] temp=currentBestPlan_op.getBestCostvaluesAt(index);
			//str.append(" totalWOrk: " +temp[0]);
			//str.append(" RT: "+ temp[1]);
			str.append(" #pareto_plans=");
			//str.append(" {at_root: "+_currentBestPlanOpAndIndex.size()+" ,Total: "+numberOfParetoPlansInSubtree+"," +PhysicalOperatorNode_RT.numOfPlansBeforeMerge+ " ," +PhysicalEquivalenceNode_RT.numOfPlansDeleted+"} ");
			str.append(" {at_root: "+_currentBestPlanOpAndIndex.size()+" ,Total: "+numberOfParetoPlansInSubtree+" } ");
			str.append(" TotalWork: ");
			str.append(temp[0]);
			str.append(" Sum_of_all_resourceComponents: ");
			str.append(temp[1]);
			str.append(" RT: ");
			str.append(temp[2]);
			
			return str.toString();
			//--
		}


		public Plan_RT getCurrentBestPlan() {
	
			if(_currentBestPlanOpAndIndex == null)
				return null;
				
			PhysicalOperatorNode_RT currentBestPlan_op = null;
			
			int minvalId =Config.CostModel.MaxCostId();//this is correct :)
			int index=0;
			//find the child operator node which has min response time among the cover set
			for(MutablePair<PhysicalOperatorNode_RT, Integer> PhyOPindexPair : _currentBestPlanOpAndIndex)
			{
				if(PhyOPindexPair.left == null){
					break; //added to remove null pointer exception
					//this can be the condition when the phyEqNode is at leaf so it does not have any child. 
					//so we are adding entry (null,-1) in its _currentBestPlanOpAndIndex in mergeIntoBestCostCoverSet function
				}
				int tempcostID= PhyOPindexPair.left.get_currentBestPlanCost().get(PhyOPindexPair.right)[2];
				
				double cmp = ((CostModel_RT)Config.CostModel).compareOnObjectiveCost(minvalId,tempcostID );
				if(cmp > 0){
					minvalId = tempcostID;
					currentBestPlan_op = PhyOPindexPair.left;
					index = PhyOPindexPair.right;
				}				
			}
			
			if(currentBestPlan_op==null)
			{
				System.out.println("Plan_RT getCurrentBestPlan() returns null. Size of _currentBestPlanOpAndIndex is "+_currentBestPlanOpAndIndex.size());
				return null;
			}
					
			//--
			Plan_RT bestplan = currentBestPlan_op.getCurrentBestPlan(index);
			//int bestCostId = currentBestPlan_op.get_currentBestPlanCost().get(index)[2];
			//cachedBestPlan = Pair.of(_idCurrentBestPlanCost, bestplan);
			//return cachedBestPlan.getRight();
			return bestplan;
		}

		

		//--mayuri added on 10feb'15 
		public void printPlan(int index, int indentLevel, StringBuilder bestPlanStr) {
			if(index >=0){
				int childindex = _currentBestPlanOpAndIndex.get(index).right;
				PhysicalOperatorNode_RT phyOpNode = _currentBestPlanOpAndIndex.get(index).left;
				phyOpNode.printPlan(childindex, indentLevel, bestPlanStr);
			}
		}


		public Plan_RT getCurrentBestPlanAtIndex(int index) {
			assert (index>=0) : "getCurrentBestPlanAtIndex for phyEqNode got called for negative index value";
			
			if(index >=0){
				int childindex = _currentBestPlanOpAndIndex.get(index).right;
				PhysicalOperatorNode_RT phyOpNode = _currentBestPlanOpAndIndex.get(index).left;
				return phyOpNode.getCurrentBestPlan(childindex);
			}
			else
				return null;
		}


		
		private void printBestCostCoverSet(boolean onlyAlgo, int indentLevel){
			List<MutablePair<PhysicalOperatorNode_RT, Integer>> coverSet = getCurrentBestPlanOpAndIndex(onlyAlgo);
			if(coverSet==null || coverSet.size()==0 || coverSet.get(0).left==null){
				return;
			}
			
			StringBuffer str=new StringBuffer("");
			for(int i=0;i<indentLevel;i++){
				str.append("\t");
			}
			for(int i=0;i<coverSet.size();i++){
				PhysicalOperatorNode_RT op = coverSet.get(i).left;
				int opBestCostIndex=coverSet.get(i).right;
				System.out.print(str.toString());
				System.out.print("(");
				op.getParent().printGroupAndId();
				
				System.out.print("." + op.getId() +" , ");
				System.out.print(opBestCostIndex+ " th best cost");
				System.out.println(")");
				
			}
		}
		
		
}


