package pyroj.optimizer.responceTimeBased;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.costmodel.CostModel_RT;
import pyroj.optimizer.Config;
import pyroj.optimizer.MemoryManager;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.optimizer.PhysicalOperatorNode;

public class Volcano_RTbased extends RT_Optimizer {

	
	private int numOfPlansPrivate=0;
	@Override
	public Plan_RT optimize(Object logicalExpressionTree) {
		clearState();
		PhysicalOperatorNode_RT.numOfPlansBeforeMerge=0;
		PhysicalEquivalenceNode_RT.numOfPlansDeleted=0;
		System.out.println("In volcano_RT based................YAHHOOOOOO...its printing....... :):):)");
		if (Config.PrintQueryExpressionTree) {
			System.out.println("\nQuery Expression Tree");
			System.out.println("=====================");
			Config.TreeHelper.print(logicalExpressionTree);
			System.out.println();
		}

		final LogicalOperatorNode_RT rootOp = addExprTree(logicalExpressionTree, null);

		if (Config.PrintInitialMemo) {
			System.out.println("\nInitial memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		applyTransformationRules(rootOp.getGroup());

		if (Config.PrintMemoAfterTrasformation) {
			System.out.println("\nMemo after transformation");
			System.out.print("===========================");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		final int idCostUB = Config.CostModel.MaxCostId();

		final PhysicalEquivalenceNode_RT phyEqNode = rootOp.getGroup().getPhysicalEqNode(null);
		phyEqNode.setRoot();
		
		optimizePhysicalEquivalenceNode(phyEqNode, false, idCostUB);
		// TODO Auto-generated method stub
		if (Config.PrintFinalMemo) {
			System.out.println("\n\nFinal memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
		}


		System.out.println(" \n\nPlan Count for the Final Memo ");
		System.out.println("============");
		int plancount = getPlanCountPhysicaLEqNode(phyEqNode);
		System.out.println(plancount);
		System.out.println("");

		/*try {
			BufferedWriter out = new BufferedWriter(new FileWriter("/home/kpunjabi/Desktop/Spark-PyroJ-RT-Integration-TPCH-Queries-Plans/all_plans_countResults.txt", true));
			out.write("Plan count for the query is : ");
			out.write(String.valueOf(plancount));
			out.newLine();
			out.close();
		}
		catch (IOException e) {
			System.out.println("exception occured"+ e);
		}*/


		String bestCost = "";
		StringBuilder detailed_bestPlanStr = new StringBuilder("");
		if (Config.PrintFinalPlan) {
			System.out.print("\nFinal plan (Objective: "+ Config.OBJECTIVE + ")");
			System.out.println("\n==========");
			
			bestCost= phyEqNode.printCurrentBestPlan(0, detailed_bestPlanStr);
			System.out.println();
		}	
		
		//System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Eq "+phyEqNode.NumEqNodesDuringLastUpdate);
	//	System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Op "+phyEqNode.NumOpNodesDuringLastUpdate);
		//---printing detailed best plan for debugging purpose
		if (Config.Printdetailed_plan_in_outputFile){
			writeToOutputFile(Config.querryResultOutputBW, "\n*********detailed best plan************:\n");
			writeToOutputFile(Config.querryResultOutputBW, detailed_bestPlanStr.toString());
			writeToOutputFile(Config.querryResultOutputBW, "\n***************************************:\n");
		}			
		//-----
		
		StringBuilder str = new StringBuilder("");
		//str.append("\nCost of best plan(Objective: ");
		//str.append(((CostModel_RT)Config.CostModel).OBJECTIVE + ") is : \n");
		str.append(" Objective:");
		str.append(Config.OBJECTIVE );
		str.append(bestCost);
		//str.append("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		if(Config.querryResultOutputBW != null ){
			writeToOutputFile(Config.querryResultOutputBW, str.toString());
		}
		

		return phyEqNode.getCurrentBestPlan();
	}

	//--
	public void writeToOutputFile(BufferedWriter bw, String content) {
		if(bw==null){
			return;
		}
		try {
			bw.write(content);
			bw.flush();
			System.out.println("Done");
		} catch (IOException e) {
			System.out.println("IOException in writeToOutputFile function");
			e.printStackTrace();
		}
	}


	//----------------- Functions added to caluculate plan count---------------

	private int getPlanCountPhysicalOpNode(PhysicalOperatorNode_RT phyOpNode){
		PhysicalEquivalenceNode_RT[] children = phyOpNode.getChildren();
		if(children==null || children.length==0){
			return 1;
		}
		int sum = 1;
		for(int i=0;i<children.length;i++){
			sum=sum*getPlanCountPhysicaLEqNode(children[i]);
		}
		return sum;
	}
	private int getPlanCountPhysicaLEqNode(PhysicalEquivalenceNode_RT phyEqNode){
		int numchildren = phyEqNode.getNumberOfChildren(false);

		int sum=0;
		for( int i=0;i<numchildren;i++){
			sum+=getPlanCountPhysicalOpNode(phyEqNode.getChild(i));
		}
		return sum;
	}
	
	//--

	
	
	//done_28may'15
	private void optimizePhysicalOperatorNode(PhysicalOperatorNode_RT phyOpNode, int idCostUB) {
		if (Config.PrintTaskExecuting) {
			System.out.print("^^^^^^^^^^^^^Find best plan - ");
			phyOpNode.printShort();
			System.out.print(", " + Config.CostModel.toString(idCostUB));
			System.out.println();
			System.out.print("initial optimization status: isOptimized =" + phyOpNode.isOptimized());
		}
		if(!phyOpNode.isOptimized()){
			final Object phyOp = phyOpNode.getOperator();
			final int idLocalCost = phyOpNode.getLocalCostID();
			final PhysicalEquivalenceNode_RT[] children = phyOpNode.getChildren();
			//here children cannot be null, because if it was null, then its already optimized so won't enter here
			final boolean isEnfOperator = phyOpNode.isEnfOperator();
			final int[] idChildrenCost = MemoryManager.getCostArray(children.length);
			final int[] idChildCostUB = MemoryManager.getCostArray(children.length);
			for (int i = 0; i < children.length; i++) {
				idChildrenCost[i]=-1;
			}
			
			
			//-this allocation is must otherwise values of null cost object will get changed (null cost object id= 0 in cost model)
			allocateCostObjects(idChildCostUB);
			
			//-
			for (int i = 0; i < children.length; i++) {
				//
				if(Config.isPruningEnabled){
					//Mohit: there can be at most two children of a physicalOperatorNode and idChildrenCost points to a cost array which holds the
					//cost of the children
					//Mohit: idChildCostUB[i] points to the RemainingCostUB for i-th child which is used in pruning
					Config.CostModel.computeRemainingCostUB(idChildCostUB[i], phyOp, idLocalCost, idChildrenCost, idCostUB);
				}

				final int idCostLB = children[i].getCostLBId(isEnfOperator);
				
				/* Mohit: Think if this is required
				if(Config.isPruningEnabled){
					phyOpNode.computeCostLB(); //Mohit Commented on 22march
				}
				*/
				if(Config.isPruningEnabled){	
					if (Config.CostModel.IsGreaterThan(idCostLB, idChildCostUB[i])) {
						//  As its child's LB overshoots its cost UB, so no point in considering this operator node. So just compute its LB and return
						//so this operator node's best cost will not be updated it will be still be max(as initialized)
						// TODO  Need to set optimized flag as we are sure of LB overshooting UB
						phyOpNode.computeCostLB();
						phyOpNode.setOptimized(true);
						freeCostObjects(idChildrenCost);
						freeCostObjects(idChildCostUB);
						MemoryManager.putCostArray(idChildrenCost);
						MemoryManager.putCostArray(idChildCostUB);		
						return;
					}
				}
				
				optimizePhysicalEquivalenceNode(children[i], isEnfOperator, idChildCostUB[i]);
				idChildrenCost[i] = Config.CostModel.AllocZeroCostObj(); //must step as, utill now it has value -1
				
				//Mohit Commented on 22march next 1 line
				if(Config.isPruningEnabled){
					calculateComponentLB_amongChildBestCosts(idChildrenCost[i], children[i], isEnfOperator);
				}
				phyOpNode.numberOfParetoPlansInSubtree += children[i].numberOfParetoPlansInSubtree; 
				
				if(Config.isPruningEnabled){
					//check if this is needed
					if (Config.CostModel.IsGreaterThan(idChildrenCost[i], idChildCostUB[i])) {
						// as its component LB among best cost cover set of the child's overshoots its cost UB, 
						//so no point in considering this operator node. So just compute its LB and return
						//so this operator node's best cost will not be updated it will be still be max(as initialized)
						//whether to set it as optimized or not? need to decide
						phyOpNode.computeCostLB();
						phyOpNode.setOptimized(true);
						freeCostObjects(idChildrenCost);
						freeCostObjects(idChildCostUB);
						MemoryManager.putCostArray(idChildrenCost);
						MemoryManager.putCostArray(idChildCostUB);				
						return;
					}
				}
			}
			if(Config.isPruningEnabled){
				phyOpNode.computeCostLB(); //Mohit Commented on 22march
			}
			//currently passing idChildCostUB to computeCurrentBestPlanCost, as was thinking of pruning costs if some child cost go above UB.
			//but was not sure if it is correct or not, so just passed it as parameter, but not used in that function
			phyOpNode.computeCurrentBestPlanCost(idChildCostUB);
			phyOpNode.setOptimized(true);
			
			phyOpNode.numberOfParetoPlansInSubtree += phyOpNode.get_currentBestPlanCost().size();
			
			freeCostObjects(idChildrenCost);
			freeCostObjects(idChildCostUB);
			MemoryManager.putCostArray(idChildrenCost);
			MemoryManager.putCostArray(idChildCostUB);
		}
		phyOpNode.mergeIntoParentBestCostCoverSet();
		//System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Op "+phyOpNode.numberOfParetoPlansInSubtree);
		return;
	}

	

	private void allocateCostObjects(int[] emptyCostArray) {
		for(int i=0;i<emptyCostArray.length;i++){
			emptyCostArray[i]= Config.CostModel.AllocZeroCostObj();
		}	
	}

	private void freeCostObjects(int[] costObjectArray) {
		for(int i=0;i<costObjectArray.length;i++){
			if(costObjectArray[i]>3){ // if it is -1 means not at all allocated costobject else 0,1,2,3 are predefined costobjects so should not be freed
				Config.CostModel.Free(costObjectArray[i]);
			}
		}	
	}

	/**done_28may'15
	 * m Done
	 * @param phyEqNode
	 * @param onlyAlgPlan
	 * @param idCostUB
	 */
	private void optimizePhysicalEquivalenceNode(PhysicalEquivalenceNode_RT phyEqNode, boolean onlyAlgPlan, int idCostUB) {
		if (Config.PrintTaskExecuting) {
			System.out.print("^^^^^^^^^^^^^Find best plan - ");
			phyEqNode.printShort();
			System.out.print(", " + Config.CostModel.toString(idCostUB));
			System.out.println();
			System.out.print("initial optimization status: isOptimizedForOnlyAlgPlan =" + phyEqNode.isOptimizedForOnlyAlgPlan());
			System.out.println(",     isOptimized =" + phyEqNode.isOptimized());
		}

		if(phyEqNode.isOptimized()){
			return;
		}
		else if(onlyAlgPlan && phyEqNode.isOptimizedForOnlyAlgPlan()){
			return;
		}

		phyEqNode.expand(onlyAlgPlan, getRules());
		final int numAlgoOperators = phyEqNode.getNumberOfChildren(true);
		final int numOperators = phyEqNode.getNumberOfChildren(onlyAlgPlan);
		//optimize all algorithmic children
		if(!phyEqNode.isOptimizedForOnlyAlgPlan()){
			for (int i = 0; i < numAlgoOperators; i++) {
				/* previous code by seniors using ordinary cost based cost model
				final int idCurrentBestPlanCost = phyEqNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsGreaterThan(idCostUB, idCurrentBestPlanCost))
					idCostUB = idCurrentBestPlanCost;
				 */

				if(Config.isPruningEnabled){
					recomputeCostUB_fromCurrentBestPlanCosts(idCostUB, phyEqNode, true);
				}

				final PhysicalOperatorNode_RT phyOpNode = phyEqNode.getChild(i);

				//Mohit: In pruning the cost upper bound for the child is same as the upper bound for the equivalence node
				//Mohit: Before calling optimize physical node it checks if LB < UB or not, it is optimized only if LB< UB
				//Mohit: LB is equal to the local cost of operator when it is created. But once an operator node is optimized its
				//		best cost cover set is merged with the parents best cost cover set. And for other children the UB is recomputed
				//		as component wise upper bound from the pareto set of the parent.
				//Mohit: Lower bound for equivalence node is calculated once all its operator node are optimized
				//Mohit: LB for equivalence node will help us prune some operator node whose child is this equivalence node
				
				//phyOpNode.computeCostLB(); //Mohit Commented on 22march

				if(Config.isPruningEnabled){
					if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
						continue;
				}
				
				optimizePhysicalOperatorNode(phyOpNode, idCostUB);

				phyEqNode.numberOfParetoPlansInSubtree += phyOpNode.numberOfParetoPlansInSubtree;

				if (Config.PrintTaskExecuting) {
					System.out.print("Find best plan - ");
					phyEqNode.printShort();
					System.out.print(", " + Config.CostModel.toString(idCostUB));
					System.out.println();
				}
			}
			List<MutablePair<PhysicalOperatorNode_RT, Integer>> bestAlgoPlanOpAndIndex = 
					phyEqNode.getCurrentBestPlanOpAndIndex(true);
			phyEqNode.getCurrentBestPlanOpAndIndex(false).clear();
			phyEqNode.getCurrentBestPlanOpAndIndex(false).addAll(bestAlgoPlanOpAndIndex);
		}
		
		//optimize all enforcer children operators
		//if onlyAlgPlan is true then numOperators and numAlgoOperators will be equal so, will not enter in this loop
		for (int i = numAlgoOperators; i <numOperators ; i++) {
			if(Config.isPruningEnabled){
				recomputeCostUB_fromCurrentBestPlanCosts(idCostUB, phyEqNode, false);
			}

			final PhysicalOperatorNode_RT phyOpNode = phyEqNode.getChild(i);
			if(Config.isPruningEnabled){
				phyOpNode.computeCostLB(); 
			}
			if(Config.isPruningEnabled){
				if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
					continue;
			}
			optimizePhysicalOperatorNode(phyOpNode, idCostUB);

			phyEqNode.numberOfParetoPlansInSubtree += phyOpNode.numberOfParetoPlansInSubtree;

			if (Config.PrintTaskExecuting) {
				System.out.print("Find best plan - ");
				phyEqNode.printShort();
				System.out.print(", " + Config.CostModel.toString(idCostUB));
				System.out.println();
			}
		}
		if(Config.isPruningEnabled){
			phyEqNode.recomputeCostLB(onlyAlgPlan); 
		}
		if(onlyAlgPlan){
			phyEqNode.setOptimizedForOnlyAlgPlan(true);
		}
		else{
			phyEqNode.setOptimizedForOnlyAlgPlan(true);
			phyEqNode.setOptimized(true);
		}

		phyEqNode.numberOfParetoPlansInSubtree += phyEqNode.getCurrentBestPlanOpAndIndex(false).size();
		//System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Eq "+phyEqNode.numberOfParetoPlansInSubtree);
	}//end of function OptPhyEqNode
	
	/**
	 * written for assertion check to validate whether _currentBestPlanOpAndIndex of PhuEqNode have valid state
	 * @param bestCostsListofEqNode:  _currentBestPlanOpAndIndex or _currentOnlyAlgoBestPlanOpAndIndex of phyEqNode
	 * @return
	 */
	private boolean checkStateValid(List<MutablePair<PhysicalOperatorNode_RT, Integer>> bestCostsListofEqNode){
		//_currentBestPlanOpAndIndex set (for algo and overall both) of PhyEqNode is initialized to (null,-1) in constructor. so this list will always have min 1 element
		if(bestCostsListofEqNode==null || bestCostsListofEqNode.size()==0){
			return false;
		}
		for (int i = 0; i < bestCostsListofEqNode.size(); i++) {
			if (bestCostsListofEqNode.size()>1 && bestCostsListofEqNode.get(i).getLeft() == null) {
				return false;
			}
		}
		return true;
	}
	/**m Done. done_28may'15
	 * if obj=2(RT) then Looks at all best costs of childEqNode and finds maximum value for each resource component among all, 
	 * 				and such maximum value of each component among all best costs is stored in idCostUB
	 * if obj=1(total work) then just compares the only best cost of childEqNode with idCostUB and if lesser then updates idCOstUB's total work value
	 *  
	 * @param idCostUB : here it contains the original costUB, so its components also need to be compared with other's
	 * @param currentEqNode
	 * @param onlyAlgo
	 */
	private void recomputeCostUB_fromCurrentBestPlanCosts(int idCostUB, PhysicalEquivalenceNode_RT currentEqNode, boolean onlyAlgo) {
		assert ((CostModel_RT)(Config.CostModel)).IsValid(idCostUB) : "recomputeCostUB_fromCurrentBestPlanCosts got invalid cost as parameter";
		
		List<MutablePair<PhysicalOperatorNode_RT, Integer>> currentBestCostsList = currentEqNode.getCurrentBestPlanOpAndIndex(onlyAlgo);	

		assert checkStateValid(currentBestCostsList): "Invalid state of _currentBestPlanOpAndIndex of PhyEqNode ";
				
		if(currentBestCostsList.get(0).getLeft() == null){//means no best cost in best cost cover set yet as it has only one initial value (null,-1)
				return; // don't change value of idCostUB
		}
		
		int[] childBestCosts = new int[currentBestCostsList.size()];
		for (int i = 0; i < currentBestCostsList.size(); i++) {
			/* no need to check this as this condition is covered in above assert and if
			if(childBestCostsList.get(i).getLeft() == null){
				childBestCosts[i] = Config.CostModel.MaxCostId();
			}*/		
			int index =currentBestCostsList.get(i).getRight();
			childBestCosts[i] = currentBestCostsList.get(i).getLeft().get_currentBestPlanCost().get(index)[2];		
		}	
		
		//--find component wise max value among current best cost cover set
		int upperBound_amongBestCosts = Config.CostModel.AllocZeroCostObj();
		for (int i = 0; i < childBestCosts.length ; i++) {	
			((CostModel_RT)(Config.CostModel)).setComponentToLargerValues(upperBound_amongBestCosts, childBestCosts[i]);
		}
		//--
		
		((CostModel_RT)(Config.CostModel)).setComponentToLowerValues(idCostUB, upperBound_amongBestCosts);
		Config.CostModel.Free(upperBound_amongBestCosts);						
				
		assert ((CostModel_RT)(Config.CostModel)).IsValid(idCostUB) : "cost became invalid in recomputeCostUB_fromCurrentBestPlanCosts";		
	}
	
	/**m Done.It works fine for obj=1 also. no change required for computing total work component of cost obj. So done_28may'15
	 * 	Looks at all best costs of childEqNode and finds minimum value for each resource component among all, 
	 * and such minimum value of each component among all best costs is stored in idBestCostLB 
	 * @param idBestCostLB: its initial components values should not be considered for comparison. 
	 * component values among best cost objects only need to be compared, and result is hold in idBestCostLB
	 * @param childEqNode : child phyEqNode
	 * @param onlyAlgo
	 */
	private void calculateComponentLB_amongChildBestCosts(int idBestCostLB, PhysicalEquivalenceNode_RT childEqNode, boolean onlyAlgo) {
		
		assert ((CostModel_RT)(Config.CostModel)).IsValid(idBestCostLB) : "cost came invalid in calculateComponentLB_amongChildBestCosts";
		
		List<MutablePair<PhysicalOperatorNode_RT, Integer>> childBestCostsList = childEqNode.getCurrentBestPlanOpAndIndex(onlyAlgo);	
		
		assert checkStateValid(childBestCostsList): "Invalid state of _currentBestPlanOpAndIndex of PhyEqNode ";
		
		if(childBestCostsList.get(0).getLeft() == null){//means no best cost in best cost cover set yet as it has only one initial value (null,-1)
				return;
		}
		
		int[] childBestCosts = new int[childBestCostsList.size()];
		
		for (int i = 0; i < childBestCostsList.size(); i++) {
			/* no need to check this as this condition is covered in above assert and if
			if(childBestCostsList.get(i).getLeft() == null){
				childBestCosts[i] = Config.CostModel.MaxCostId();
			}*/		
			int index =childBestCostsList.get(i).getRight();
			childBestCosts[i] = childBestCostsList.get(i).getLeft().get_currentBestPlanCost().get(index)[2];			
		}	
		
		Config.CostModel.Assign(idBestCostLB, childBestCosts[0]);
		for (int i = 1; i < childBestCosts.length ; i++) {	
			((CostModel_RT)(Config.CostModel)).setComponentToLowerValues(idBestCostLB, childBestCosts[i]);
		}
		
		assert ((CostModel_RT)(Config.CostModel)).IsValid(idBestCostLB) : "cost became invalid in calculateComponentLB_amongChildBestCosts";
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

