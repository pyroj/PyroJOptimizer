package pyroj.optimizer.responceTimeBased;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import pyroj.costmodel.ICostModel;
import pyroj.optimizer.Config;

public class MemoryManager_RT {

	public static int NumCostArrays = 0;
	private static ArrayList<ArrayList<int[]>> _childCostArray = new ArrayList<ArrayList<int[]>>();

	public static int[] getCostArray(int length) {

		if (length <= 0)
			return null;

		for (int i = _childCostArray.size(); i < length; i++) {
			_childCostArray.add(new ArrayList<int[]>());
		}

		final ArrayList<int[]> costArray = _childCostArray.get(length - 1);

		if (!costArray.isEmpty()) {
			return costArray.remove(costArray.size() - 1);
		}

		NumCostArrays++;
		return new int[length];
	}

	public static void putCostArray(int[] costIDArr) {
		if (costIDArr == null)
			return;

		final int length = costIDArr.length;
		for (int i = 0; i < length; i++)
			costIDArr[i] = ICostModel.NULL;

		final ArrayList<int[]> costArray = _childCostArray.get(length - 1);
		costArray.add(costIDArr);
	}

	public static int NumBindingPairs = 0;
	private static ArrayList<MutablePair<Object, Object>> _bindingPairs = new ArrayList<MutablePair<Object, Object>>();

	public static Pair<Object, Object> getTempBindingPair(Object left, Object right) {
		MutablePair<Object, Object> pair = null;
		if (!_bindingPairs.isEmpty()) {
			pair = _bindingPairs.remove(_bindingPairs.size() - 1);
			pair.setLeft(left);
			pair.setRight(right);
		}
		else {
			NumBindingPairs++;
			pair = new MutablePair<Object, Object>(left, right);
		}
		return pair;
	}

	public static void putTempBindingPair(Pair<Object, Object> pair) {
		final MutablePair<Object, Object> bndPair = (MutablePair<Object, Object>) pair;
		if (bndPair == null)
			return;

		bndPair.setLeft(null);
		bndPair.setRight(null);
		_bindingPairs.add(bndPair);
	}

	public static int NumBindingCreated = 0;
	private static ArrayList<ArrayList<Pair<Object, Object>[]>> _bindings = new ArrayList<ArrayList<Pair<Object, Object>[]>>();

	@SuppressWarnings("unchecked")
	public static Pair<Object, Object>[] getTempBinding(int length) {

		for (int i = _bindings.size(); i < length; i++) {
			_bindings.add(new ArrayList<Pair<Object, Object>[]>());
		}

		final ArrayList<Pair<Object, Object>[]> bindingArray = _bindings.get(length - 1);

		if (!bindingArray.isEmpty()) {
			return bindingArray.remove(bindingArray.size() - 1);
		}

		NumBindingCreated++;
		return new MutablePair[length];
	}

	public static void putTempBindingsRecurse(Pair<Object, Object>[] binding) {

		if (binding == null)
			return;

		final int length = binding.length;
		for (int i = 0; i < length; i++) {
			putTempBindingPair(binding[i]);
			binding[i] = null;
		}

		final ArrayList<Pair<Object, Object>[]> bindingArray = _bindings.get(length - 1);
		bindingArray.add(binding);
	}

	public static int NumBindingArrayCreated = 0;
	private static ArrayList<ArrayList<Pair<Object, Object>[]>> _bindingArrays = new ArrayList<ArrayList<Pair<Object, Object>[]>>();

	public static ArrayList<Pair<Object, Object>[]> getTempBindingArray() {
		if (!_bindingArrays.isEmpty()) {
			return _bindingArrays.remove(_bindingArrays.size() - 1);
		}

		NumBindingArrayCreated++;
		return new ArrayList<Pair<Object, Object>[]>();
	}

	public static void putTempBindingArrayRecurse(ArrayList<Pair<Object, Object>[]> bindings) {
		for (int i = 0; i < bindings.size(); i++) {
			putTempBindingsRecurse(bindings.get(i));
		}
		bindings.clear();
		_bindingArrays.add(bindings);
	}

	public static int NumBindingGroupArrayCreated = 0;
	private static ArrayList<ArrayList<LogicalEquivalenceNode_RT>> _bindingGroupArrays = new ArrayList<ArrayList<LogicalEquivalenceNode_RT>>();

	public static ArrayList<LogicalEquivalenceNode_RT> getTempBindingGroupArray() {
		if (!_bindingGroupArrays.isEmpty()) {
			return _bindingGroupArrays.remove(_bindingGroupArrays.size() - 1);
		}

		NumBindingGroupArrayCreated++;
		return new ArrayList<LogicalEquivalenceNode_RT>();
	}

	public static void putTempBindingGroupArray(ArrayList<LogicalEquivalenceNode_RT> bindings) {
		bindings.clear();
		_bindingGroupArrays.add(bindings);
	}

	public static int NumCostInstanceCreated = 0;

	public static int getTempCostObject() {
		NumCostInstanceCreated++;
		return Config.CostModel.Alloc();
	}

	public static void putTempCostObject(int costID) {
		Config.CostModel.Free(costID);
	}

	public static int NumChildEqNodes = 0;
	private static ArrayList<ArrayList<PhysicalEquivalenceNode_RT[]>> _childEqNodeArray = new ArrayList<ArrayList<PhysicalEquivalenceNode_RT[]>>();

	public static PhysicalEquivalenceNode_RT[] getTempChildEqNodeArray(int length) {

		for (int i = _childEqNodeArray.size(); i < length; i++) {
			_childEqNodeArray.add(new ArrayList<PhysicalEquivalenceNode_RT[]>());
		}

		final ArrayList<PhysicalEquivalenceNode_RT[]> childEqNodeArray = _childEqNodeArray.get(length - 1);

		if (!childEqNodeArray.isEmpty()) {
			return childEqNodeArray.remove(childEqNodeArray.size() - 1);
		}

		NumChildEqNodes++;
		return new PhysicalEquivalenceNode_RT[length];
	}

	public static void putTempChildEqNodeArray(PhysicalEquivalenceNode_RT[] childEqNodeArray) {

		final int length = childEqNodeArray.length;
		for (int i = 0; i < length; i++)
			childEqNodeArray[i] = null;

		final ArrayList<PhysicalEquivalenceNode_RT[]> childEqNodes = _childEqNodeArray.get(length - 1);
		childEqNodes.add(childEqNodeArray);
	}

	public static void initialize() {
	}
}

