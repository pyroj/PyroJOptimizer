package pyroj.optimizer.responceTimeBased;

import java.io.BufferedWriter;
import java.util.ArrayList;

import org.apache.commons.lang3.tuple.Pair;

import pyroj.costmodel.ICostModel;
import pyroj.optimizer.Config;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.optimizer.PhysicalOperatorNode;
import pyroj.optimizer.Rules;
import pyroj.optimizer.cascade.Task;
import pyroj.runtime.IOperatorHelper;
import pyroj.runtime.ITransformationRule;
import pyroj.runtime.ITreeHelper;

public abstract class RT_Optimizer {

	static public void printInstanceCounts(long timeTaken) {
		System.out.println();
		System.out.printf("%1$-35s = %2$d ms\n", "Time taken", timeTaken);
		System.out.printf("%1$-35s = %2$d \n", "LogicalEquivalenceNode_RT instances", LogicalEquivalenceNode_RT.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "LogicalOperatorNode_RT instances", LogicalOperatorNode_RT.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "PhysicalEquivalenceNode instances",
				PhysicalEquivalenceNode.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "PhysicalOperatorNode instances", PhysicalOperatorNode.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "CostOrd instances", Config.CostModel.getNumCostInstance());
		System.out.printf("%1$-35s = %2$d \n", "Binding array instances", MemoryManager_RT.NumBindingArrayCreated);
		System.out.printf("%1$-35s = %2$d \n", "Binding grouparray instances",
				MemoryManager_RT.NumBindingGroupArrayCreated);
		System.out.printf("%1$-35s = %2$d \n", "Binding instances", MemoryManager_RT.NumBindingCreated);
		System.out.printf("%1$-35s = %2$d \n", "Binding pair instances", MemoryManager_RT.NumBindingPairs);
		
		Config.OpHelper.printInstanceCounts();
	}

	static protected void clearState() {
		LogicalEquivalenceNode_RT.NumInstances = 0;
		LogicalOperatorNode_RT.NumInstances = 0;
		PhysicalEquivalenceNode.NumInstances = 0;
		PhysicalOperatorNode.NumInstances = 0;
		Task.NumInstances = 0;
		MemoryManager_RT.NumBindingArrayCreated = 0;
		MemoryManager_RT.NumBindingGroupArrayCreated = 0;
		MemoryManager_RT.NumBindingCreated = 0;
		MemoryManager_RT.NumBindingPairs = 0;
		Config.OpHelper.clearState();
	}

	protected Memo_RT _memo = null;

	protected Rules _rules = null;
	
	//--mayuri  
	/**
	 * @param objective : We are working on two objective 1=Total work, 2= Response time
	 * @param enable_approximation  true or false
	 * @param appromation_factor : need to be considered only when enable_approximation is true. else ignore this value
	 */
	public void initialize(Rules rules, IOperatorHelper opHelper, ICostModel costModel, ITreeHelper treeHelper, BufferedWriter bw,
			int objective, boolean enable_approximation, double approximation_factor) {
		Config.querryResultOutputBW = bw;
		Config.OBJECTIVE = objective;
		Config.APPROXIMATION_ENABLED = enable_approximation;
		Config.APPROXIMATION_FACTOR = approximation_factor;
		
		_memo = new Memo_RT();
		_rules = rules;
		
		Config.OpHelper = opHelper;
		Config.setCostModel(costModel);
		Config.TreeHelper = treeHelper;
		MemoryManager_RT.initialize();
	}
	//--
	
	

	protected LogicalOperatorNode_RT addExprTree(Object exprTree, LogicalEquivalenceNode_RT grp) {

		final Object expr = Config.TreeHelper.getRootOperator(exprTree);
			
		LogicalEquivalenceNode_RT[] childrenGroup = null;

		if (Config.TreeHelper.getNumChildren(exprTree) > 0) {
			final Object[] children = Config.TreeHelper.getChildren(exprTree);
			childrenGroup = new LogicalEquivalenceNode_RT[children.length];

			for (int i = 0; i < children.length; i++) {
				final Object childRootOp = Config.TreeHelper.getRootOperator(children[i]);
				if (childRootOp instanceof LogicalEquivalenceNode_RT)
					childrenGroup[i] = (LogicalEquivalenceNode_RT) childRootOp;
				else
					childrenGroup[i] = addExprTree(children[i], null).getGroup();
			}
		}

		return addOperator(expr, childrenGroup, grp);
	}
	
	protected LogicalOperatorNode_RT addOperator(LogicalOperatorNode_RT op) {

		//System.out.println("in sigle addOperator*******************************************************************************************************");
		LogicalEquivalenceNode_RT grp = op.getGroup();
		final LogicalOperatorNode_RT opMemo = _memo.find(op);
		
		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode_RT(op.computeLogicalProperties());
				grp.addExpr(op);
				op.setGroup(grp);
			}

			_memo.insert(op);
			return op;
		}else {
			if (grp != null) {
				grp.removeExpr(op);
				for (int i = 0; i < op.getNumChildren(); i++) {
					op.getChild(i).removeParent(op);
				}

				
				//-------------------------
				//if(!op.equals(opMemo))
				//if(op != opMemo)
					Config.OpHelper.unifyOperators(op.getOperator(), opMemo.getOperator());
					op.setUnifiedTo(opMemo);
				//-------------------------
				final LogicalEquivalenceNode_RT grpMemo = opMemo.getGroup();
				if (grp.getId() != grpMemo.getId()) {
					 //grpMemo.unify(grp, this);
					 final LogicalEquivalenceNode_RT deletedGrp = grpMemo.unify(grp, this);
						if(deletedGrp.getId() == grpMemo.getId())
						{
							//-------------------------
						//	if(!op.equals(opMemo))
								Config.OpHelper.unifyOperators(opMemo.getOperator(), op.getOperator());
							//-------------------------
							op.setGroup(grp);
							return op;
						}
				}
			}else{
			 System.out.println("Group is null in single para addOperator");
			}
			return opMemo;
		}
	}

	
	/**creates new LogicalOperatorNode_RT from root given in expr and its childrens in childrenGroup and adds it to grp
	and adds this operator to Memo_RT. as grp is associated with this added op in Memo_RT, so grp is also part of Memo_RT 
	or if the op is already present in Memo_RT but associated with diff group, then unification of the two groups is done.
	and finally returns the LogicalOperatorNode_RT formed with expr */
	protected LogicalOperatorNode_RT addOperator(Object expr, LogicalEquivalenceNode_RT[] childrenGroup,
			LogicalEquivalenceNode_RT grp) {

		final LogicalOperatorNode_RT op = new LogicalOperatorNode_RT(expr, childrenGroup);
		final LogicalOperatorNode_RT opMemo = _memo.find(op);

		

		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode_RT(op.computeLogicalProperties());
			}

			grp.addExpr(op);
			op.setGroup(grp);
			if (childrenGroup != null) {
				for (int i = 0; i < childrenGroup.length; i++)
					childrenGroup[i].addParent(op,true);
			}

			// TODO This operator may trigger more rules on ancestor operator,
			// so
			// need to add task for that.
			_memo.insert(op);
			//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			//_memo.print(op.getGroup());
			//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			return op;
		}
		else {
			final LogicalEquivalenceNode_RT grpMemo = opMemo.getGroup();
			if (grp != null && grp.getId() != grpMemo.getId()) {
				final LogicalEquivalenceNode_RT deletedGrp = grp.unify(grpMemo, this);
				if(deletedGrp.getId() == grpMemo.getId())
				{
					//-------------------------
					//if(!op.equals(opMemo))
						Config.OpHelper.unifyOperators(opMemo.getOperator(), op.getOperator());
					//-------------------------
					op.setGroup(grp);
					_memo.insert(op);
					removeOp(opMemo);
					
					return op;
				}
					
				if (deletedGrp != null) {
					
					// following code is to catch bug. commented to improve the performance
					// TODO Sandeep, plz uncomment this code. after testing all
					final LogicalOperatorNode_RT deletedOp = _memo.isGroupExist(deletedGrp);
					if (deletedOp != null) {
						System.out.println("Bug - - ");
						deletedOp.print();
					}
					
				}
			} 
			
			//System.out.println("################################################################################\n");
			//_memo.print(op.getGroup());
			//System.out.println("################################################################################\n");
			//-------------------------
			//if(!op.equals(opMemo))
				Config.OpHelper.unifyOperators(op.getOperator(), opMemo.getOperator());
			//-------------------------
			return opMemo;
		}

	}

	public Rules getRules() {
		return _rules;
	}

	public abstract Plan_RT optimize(Object logicalExpressionTree);

	public LogicalOperatorNode_RT removeOp(LogicalOperatorNode_RT op) {
		return _memo.remove(op);
	}

	private LogicalEquivalenceNode_RT applyRule(ITransformationRule rule, LogicalOperatorNode_RT logExpr,
			LogicalEquivalenceNode_RT group, int ruleIndex) {
		final Object rulePattern = rule.getInputPattern();

		if(logExpr.getRuleApplied(ruleIndex))
			return group;
		boolean ruleApplied = false;
		
		final ArrayList<Pair<Object, Object>[]> bindings = MemoryManager_RT.getTempBindingArray();
		final ArrayList<LogicalEquivalenceNode_RT> bindingGroup = MemoryManager_RT.getTempBindingGroupArray();
		PatternTree_RT.getBindings(rulePattern, logExpr, bindings, bindingGroup);
		for (int bndIndex = 0; bindings != null && bndIndex < bindings.size(); bndIndex++) {

			final Pair<Object, Object>[] binding = bindings.get(bndIndex);

			final Object[] outputExpr = rule.getOutputExpr(binding);

			if (outputExpr == null)
				continue;

			for (int i = 0; i < outputExpr.length; i++) {

				group =	addExprTree(outputExpr[i], group).getGroup();

			}
			ruleApplied = true;
			
		}

		MemoryManager_RT.putTempBindingArrayRecurse(bindings);
		MemoryManager_RT.putTempBindingGroupArray(bindingGroup);

		if (ruleApplied)
			logExpr.setRuleApplied(ruleIndex);
		return group;
	}

	protected void applyTransformationRules(LogicalEquivalenceNode_RT group) {

		if (group.isTransformationRulesApplied())
			return;

		final ITransformationRule[] rules = _rules.getTransformationRules();
		//System.out.println("*****************************************************************************************************************"+group.getId()+" Start");
		//group.printAllExpr(null);
		
		for (int exprIndex = 0; exprIndex < group.getNumLogExpr(); exprIndex++) {
			final LogicalOperatorNode_RT logExpr = group.getLogicalExpression(exprIndex);

			if (logExpr == null)
				continue;

			final int numChildren = logExpr.getNumChildren();
			for (int childIndex = 0; childIndex < numChildren; childIndex++) {
				final LogicalEquivalenceNode_RT childGroup = logExpr.getChild(childIndex);
				if (!childGroup.isTransformationRulesApplied())
					applyTransformationRules(childGroup);
			}

			int ruleIndex = logExpr.getFirstUnAppliedRuleIndex(0);
			
			while (ruleIndex < rules.length) {

				group = applyRule(rules[ruleIndex], logExpr, group, ruleIndex);
				logExpr.setRuleApplied(ruleIndex);
				//System.out.println(" Group "+group.getId()+" Rule Index "+ruleIndex +" expr Index "+exprIndex + "Total Expr "+group.getNumLogExpr());
				//group.printAllExpr(null);
				ruleIndex = logExpr.getFirstUnAppliedRuleIndex(ruleIndex + 1);
			}
		}
	//	System.out.println("*****************************************************************************************************************"+group.getId()+ "End");
		group.setTransformationRulesApplied();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
}
