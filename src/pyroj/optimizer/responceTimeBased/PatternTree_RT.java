package pyroj.optimizer.responceTimeBased;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import pyroj.optimizer.Config;

public class PatternTree_RT {

	@SuppressWarnings("unused")
	public static void getBindingsNonRoot(Object patternTree,
			LogicalOperatorNode_RT opNode,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode_RT> bindingGroup) throws Exception {

		if (Config.TreeHelper.isRootMatches(patternTree, opNode.getOperator()))
			getBindings(patternTree, opNode,
					new ImmutablePair<Integer, LogicalOperatorNode_RT>(
							Config.TreeHelper.getId(patternTree), opNode),
					bindings, bindingGroup);

		final Object[] children = Config.TreeHelper.getChildren(patternTree);

		if (children != null) {
			for (int i = 0; i < children.length; i++)
				getBindingsNonRoot(children[i], opNode, bindings, bindingGroup);
		}
	}

	public static void getBindings(Object patternTree,
			LogicalOperatorNode_RT opNode,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode_RT> bindingGroup) {

		getBindingsDesc(patternTree, opNode,
				Config.TreeHelper.getNumNodes(patternTree), null, bindings,
				bindingGroup);
	}

	protected static void getBindings(Object patternTree,
			LogicalOperatorNode_RT opNode,
			Pair<Integer, LogicalOperatorNode_RT> fixedBinding,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode_RT> bindingGroup) throws Exception {

		if (Config.TreeHelper.getParent(patternTree) == null) {
			getBindingsDesc(patternTree, opNode,
					Config.TreeHelper.getNumNodes(patternTree), fixedBinding,
					bindings, bindingGroup);
			return;
		}

		if (opNode == null
				|| !Config.TreeHelper.isRootMatches(patternTree,
						opNode.getOperator()))
			return;

		final LogicalEquivalenceNode_RT eqNode = opNode.getGroup();

		for (int i = 0; i < eqNode.getNumParents(); i++) {
			final LogicalOperatorNode_RT parentExpr = eqNode
					.getParentExpression(i);
			if (parentExpr != null)
				getBindings(Config.TreeHelper.getParent(patternTree),
						parentExpr, fixedBinding, bindings, bindingGroup);
		}
	}

	private static void crossProductAndAssignToFirst(
			ArrayList<Pair<Object, Object>[]> left,
			ArrayList<Pair<Object, Object>[]> right) {

		final int numLeftBnd = left.size();
		// Multiple remaining right bnd.
		for (int i = 1; i < right.size(); i++) {
			final Pair<Object, Object>[] rightBnd = right.get(i);

			for (int j = 0; j < numLeftBnd; j++) {
				final Pair<Object, Object>[] leftBnd = left.get(j);

				final Pair<Object, Object>[] resultBnd = MemoryManager_RT
						.getTempBinding(leftBnd.length);
				crossProductAndAssignToFirst(resultBnd, leftBnd);
				crossProductAndAssignToFirst(resultBnd, rightBnd);

				left.add(resultBnd);
			}
		}

		// Multiply first element of rightBnd with all left
		final Pair<Object, Object>[] firstRightBnd = right.get(0);
		for (int k = 0; k < numLeftBnd; k++) {
			crossProductAndAssignToFirst(left.get(k), firstRightBnd);
		}
	}

	private static void crossProductAndAssignToFirst(
			Pair<Object, Object>[] first, Pair<Object, Object>[] second) {

		if (first.length != second.length)
			throw new IllegalArgumentException("Binding length must be same in left and right");

		for (int i = 0; i < first.length; i++) {
			if (second[i] == null)
				continue;
			if (first[i] == null)
				first[i] = MemoryManager_RT.getTempBindingPair(
						second[i].getLeft(), second[i].getRight());
			else if (second[i] != null && first[i] != second[i])
				throw new IllegalArgumentException("Different binding in left and right array");
		}
	}

	private static void getBindingsDesc(Object patternTree,
			LogicalOperatorNode_RT opNode, int numElements,
			Pair<Integer, LogicalOperatorNode_RT> fixedBinding,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode_RT> bindingGroup) {

		if (opNode == null
				|| !Config.TreeHelper.isRootMatches(patternTree,
						opNode.getOperator()))
			return;

		final int id = Config.TreeHelper.getId(patternTree);

		// if we have fixed binding for this
		if (fixedBinding != null && id == fixedBinding.getKey()
				&& opNode != fixedBinding.getValue())
			return;

		final Object[] children = Config.TreeHelper.getChildren(patternTree);
		if (children == null) {
			final Pair<Object, Object>[] binding = MemoryManager_RT
					.getTempBinding(numElements);
			binding[id] = MemoryManager_RT.getTempBindingPair(
					opNode.getOperator(), opNode.getGroup()
							.getLogicalProperties());
			if (bindingGroup != null)
				bindingGroup.add(opNode.getGroup());
			bindings.add(binding);
			return;
		}
		//else if(Config.TreeHelper.getRootOperatorType(patternTree) != -1 && opNode.getNumChildren() != children.length)
		else if(opNode.getNumChildren() != children.length)
			return;
		else {

			final ArrayList<Pair<Object, Object>[]> allBindings = MemoryManager_RT
					.getTempBindingArray();

			for (int i = 0; i < children.length; i++) {

				if (children[i] == null)
					continue;

				final ArrayList<Pair<Object, Object>[]> childResult = MemoryManager_RT
						.getTempBindingArray();
				final LogicalEquivalenceNode_RT childEq = opNode.getChild(i);

				if (!Config.TreeHelper.shouldRootBeLogicalOperator(children[i])) {
					final Pair<Object, Object>[] binding = MemoryManager_RT
							.getTempBinding(numElements);
					binding[Config.TreeHelper.getId(children[i])] = MemoryManager_RT
						.getTempBindingPair(childEq,
									childEq.getLogicalProperties());
					//binding[Config.TreeHelper.getId(children[i])] = MemoryManager_RT
					//		.getTempBindingPair(opNode.getGroup(),
					//				opNode.getGroup().getLogicalProperties());
					childResult.add(binding);
				} else {
					for (int exprIndex = 0; exprIndex < childEq.getNumLogExpr(); exprIndex++) {
						final LogicalOperatorNode_RT childExpr = childEq
								.getLogicalExpression(exprIndex);
						if (childExpr == null)
							continue;
						getBindingsDesc(children[i], childExpr, numElements,
								fixedBinding, childResult, null);
					}
				}

				if (childResult == null || childResult.size() == 0) {
					MemoryManager_RT.putTempBindingArrayRecurse(childResult);
					MemoryManager_RT.putTempBindingArrayRecurse(allBindings);
					return;
				}
				if (allBindings.size() == 0) {
					allBindings.addAll(childResult);
					childResult.clear();
				} else {
					crossProductAndAssignToFirst(allBindings, childResult);
				}

				MemoryManager_RT.putTempBindingArrayRecurse(childResult);

			}
			for (int i = 0; i < allBindings.size(); i++) {
				allBindings.get(i)[id] = MemoryManager_RT.getTempBindingPair(
						opNode.getOperator(), opNode.getGroup()
								.getLogicalProperties());
				if (bindingGroup != null)
					bindingGroup.add(opNode.getGroup());
			}

			bindings.addAll(allBindings);
			allBindings.clear();
			MemoryManager_RT.putTempBindingArrayRecurse(allBindings);
		}

	}
}