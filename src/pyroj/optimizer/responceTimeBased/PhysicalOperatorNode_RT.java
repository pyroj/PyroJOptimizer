package pyroj.optimizer.responceTimeBased;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.costmodel.CostModel_RT;
import pyroj.costmodel.ICostModel;
import pyroj.optimizer.Config;
import pyroj.optimizer.MemoryManager;
//import edu.uci.ics.hyracks.algebricks.core.algebra.base.IPhysicalOperator;

public class PhysicalOperatorNode_RT {
	//stores total sum of #pareto plans in all children + #pareto plans for itself (i.e. size of own bestcost cover set)
	protected int numberOfParetoPlansInSubtree=0;
	public static int numOfPlansBeforeMerge=0;
	
	private boolean isOptimized=false;
	public static int NumInstances;

	private final int _id;

	private final boolean _isAlgOrLocalEnfOperator;
	private final Object _rootOperator;
	private final Object _logicalOperator;
	private final PhysicalEquivalenceNode_RT _parent;
	private final PhysicalEquivalenceNode_RT[] _children;
	
	// Added for Greedy --- 
	private PhysicalEquivalenceNode_RT[] _actualChildren =null ;
	
	private boolean _isLocalEnf = false;
	//---

	private final int _idLocalCost = Config.CostModel.AllocZeroCostObj();

	private final int _idCostLB;

	/**
	 * it stores multiple on comparable best plan costs ,So it is cover set of best plan costs
	 * each as: <leftchildIndex, rightChildIndex, idCostObject> 
	 */ 
	private final List<int[]> _currentBestPlanCost=new ArrayList<int[]>();  

	private final int[] _idChildrenCostUB;

	public List<int[]> get_currentBestPlanCost() {
		return _currentBestPlanCost;
	}
	public boolean isOptimized() {
		return isOptimized;
	}
	public void setOptimized(boolean isOptimized) {
		this.isOptimized = isOptimized;
	}

	public PhysicalOperatorNode_RT(
				boolean isAlgOrLocalEnfOperator, 
				Object rootOperator,
				Object logicalOperator,
				PhysicalEquivalenceNode_RT parent,
				PhysicalEquivalenceNode_RT[] children,
				Object[] childrenPhysicalProperties) {
		_id = NumInstances++;
		_isAlgOrLocalEnfOperator = isAlgOrLocalEnfOperator;
		_rootOperator = rootOperator;
		_logicalOperator = logicalOperator;
		_parent = parent;
		_children = children;

		//Assumed that each Operator has max two children, 
		//all further cost calculation functions are based on this assumption
		//and this is quite reasonable, as currently all op have atmost two children
		assert (!(_children!=null && _children.length>2)) : "*********** _children.length > 2 for PhysicalOperaterNode";
		/*if (_children != null)
		{
			if(_children.length > 2){
				printShort();
				try {
					throw new Exception("*********** _children.length > 2 for PhysicalOperaterNode");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}*/
		
		computeLocalCost(childrenPhysicalProperties);

		if (_children == null) {
			_idChildrenCostUB = null;
			_idCostLB = _idLocalCost;
			
			final int[] temp = new int[3];
			temp[0]=-1;
			temp[1]=-1;
			temp[2]=_idLocalCost;
			_currentBestPlanCost.add(temp);
			//as the opnode does not have any children, so this is its final best cost, no need not optimize again
			isOptimized = true; 
		}
		else {
			//this must be the 1st line here ( creating new isCostLB) 
			_idCostLB = Config.CostModel.Clone(_idLocalCost);
			
			_currentBestPlanCost.add(new int[3]);
			_currentBestPlanCost.get(0)[0]=-1;
			_currentBestPlanCost.get(0)[1]=-1;
			_currentBestPlanCost.get(0)[2]=Config.CostModel.MaxCostId();
			
			boolean allChildOptimized=true;
			for (int i = 0; i < _children.length; i++){
				if(!isChildOptimized(_children[i], !isAlgOrLocalEnfOperator)){
					allChildOptimized=false;
					break;
				}
			}
			if(allChildOptimized){
				computeCostLB(); //Mohit: commented on 30th march
				_idChildrenCostUB = new int[_children.length];
				for (int i = 0; i < _children.length; i++){
					_idChildrenCostUB[i] = Config.CostModel.MaxCostId();
				}
				computeCurrentBestPlanCost(_idChildrenCostUB);
				//_idChildrenCostUB = null;
				isOptimized = true; 
			}
			else{	
				_idChildrenCostUB = new int[_children.length];
				for (int i = 0; i < _children.length; i++)
					_idChildrenCostUB[i] = Config.CostModel.AllocZeroCostObj();
	
				for (int i = 0 ; i < _children.length ; i++ ) { 
					_children[i].addOnlyParent(this,_isAlgOrLocalEnfOperator);
				}
			}
			
		}
		if(isOptimized == true){
			numberOfParetoPlansInSubtree = _currentBestPlanCost.size();
		}
	}
	/**
	 * m
	 * @param child
	 * @param onlyAlgoPlan: true; if this OpNode is enforcer op, so it want only algorithmic plan from child
	 * 					: false: this phyOpNode is algo op, so it want overall best plan from child,
	 * 							so it want overall optimized child
	 * @return
	 */
	private boolean isChildOptimized(PhysicalEquivalenceNode_RT child, boolean onlyAlgoPlan){
		/*if(onlyAlgoPlan){
			return (child.isOptimizedForOnlyAlgPlan() || child.isOptimized());
		}
		else{  // this phyOpNode is algo op, so it want overall optimized child
			return child.isOptimized();
		}*/
		// Done By Sandeep TODO
		return child.isOptimized();
	}

	// Added for Greedy --
	
	
	public void setLocalEnforcer(boolean flag) {
		this._isLocalEnf = true;
	}

	public boolean isLocalEnf() {
		return _isLocalEnf;
	}
	
	
	// ---
	
	public int getId() {
		return _id;
	}

	public Object getOperator() {
		return _rootOperator;
	}
	
	public Object getLogicalOperator() {
		return _logicalOperator;
	}

	public boolean isAlgOrLocalEnfOperator() {
		return _isAlgOrLocalEnfOperator;
	}

	public boolean isEnfOperator() {
		return !_isAlgOrLocalEnfOperator;
	}

	public int getLocalCostID() {
		return _idLocalCost;
	}

	public PhysicalEquivalenceNode_RT getParent() {
		return _parent;
	}

	public PhysicalEquivalenceNode_RT[] getChildren() {
		return _children;
	}

	public int getCostLBId() {
		return _idCostLB;
	}

	/**m
	 * Done
	 * @param b 
	 * @return
	 */
	public boolean computeCostLB() {
		//TODO: check if _children can be null here
		if(_children==null)
			return false;
		final int[] idChildCostLB = MemoryManager.getCostArray(_children.length);

		for (int i = 0; i < _children.length; i++)
			idChildCostLB[i] = _children[i].getCostLBId(!_isAlgOrLocalEnfOperator);

		boolean modified = Config.CostModel.computePartialCost(_idCostLB, _rootOperator, _idLocalCost, idChildCostLB);
		MemoryManager.putCostArray(idChildCostLB);

		assert ((CostModel_RT)(Config.CostModel)).IsValid(_idCostLB) : "cost became invalid in computeCostLB";
		
		return modified;
	}

	/*
	public void updateCostLB() {
		boolean modified = computeCostLB();
		if (modified)
			_parent.updateCostLB(_idCostLB, _isAlgOrLocalEnfOperator);
		// else
		// System.out.println("costLB");
	}
	*/
	
	/**mDone Done.It works fine for obj=1 also. no change required for computing total work component of cost obj. So done_28may'15
	 * assumption is that this is called only once for a PhyOpNode, when all its children has been optimized and 
	 * This method assumes all child equivalence nodes have already updated there currentBestPlanCost. 
	 * so when this is called, _currentBestPlanCost has just one initial entry <-1,-1, maxCostID>
	 * @param idChildCostUB : array of UB for the children,initially thought that it should be used, but currently it is not used 
	 * think properly if it is proper to child best plan based on this upper bound
	 * 
	 * @return it always returns true as the while cost is newly computed. true false was previous convention		   
	 */
	public boolean computeCurrentBestPlanCost(int[] idChildCostUB) {
		if(_children == null){
			//actually it should not be called for op having no children as its bestcost is already set in constructor
			//but still written just to avoid null pointer exception, in case called by mistake
			return true;
		}
		
		boolean onlyAlgo = (!_isAlgOrLocalEnfOperator);
		
		List<MutablePair<PhysicalOperatorNode_RT, Integer>> child1BestCostsList;
		child1BestCostsList = _children[0].getCurrentBestPlanOpAndIndex(onlyAlgo);
		int[] child1BestCosts = new int[child1BestCostsList.size()];
		for (int i = 0; i < child1BestCostsList.size(); i++) {
			if(child1BestCostsList.get(i).getLeft() == null){
				child1BestCosts[i] = Config.CostModel.MaxCostId();
			}
			else{
				int index =child1BestCostsList.get(i).getRight();
				child1BestCosts[i] = child1BestCostsList.get(i).getLeft().get_currentBestPlanCost().get(index)[2];
			}
		}
		
		List<MutablePair<PhysicalOperatorNode_RT, Integer>> child2BestCostsList = null;
		int[] child2BestCosts = null;
		if(_children.length ==1){	
						
        _currentBestPlanCost.clear(); //TODO: see whether this clearing is correct or not,
			//TODO: no need to deallocate objects as assumption is that this is called only once for a opnode 
			//when all its child has optimized and thus computed final best plan costs
			// so when this is called, _currentBestPlanCost has just one initial entry <-1,-1, maxCostID>
			final int[] idChildrenBestPlanCosts = new int[_children.length];
			for (int i = 0; i < child1BestCosts.length; i++) {
				if(!Config.CostModel.IsValidPlanCost(child1BestCosts[i])){
					continue;
				}
				idChildrenBestPlanCosts[0] = child1BestCosts[i];
				int idCostObject = Config.CostModel.AllocZeroCostObj();
				boolean modified = Config.CostModel.computeTotalCost(idCostObject, _rootOperator, _idLocalCost,
						idChildrenBestPlanCosts);
				
				assert ((CostModel_RT)(Config.CostModel)).IsValid(idCostObject) : "cost became invalid in computeCurrentBestPlanCost";
				
				if(_currentBestPlanCost.size()==0){
					_currentBestPlanCost.add(new int[3]);
					_currentBestPlanCost.get(0)[0]=i;
					_currentBestPlanCost.get(0)[1]=-1;
					_currentBestPlanCost.get(0)[2]=idCostObject;
				}
				else{
					assert (Config.OBJECTIVE==2): "Control flow sould not come here for objective: "+Config.OBJECTIVE;
					//now check whether this idCostObject should be part of bestcosts cover set of this PhyOpNode
					boolean isAddedtoCoverset=false;
					int k=0;
					for(k=0;k<_currentBestPlanCost.size(); k++){
						if(Config.CostModel.IsLessThan(idCostObject, _currentBestPlanCost.get(k)[2])){
							if(!isAddedtoCoverset){
								int n=_currentBestPlanCost.size();
								_currentBestPlanCost.add(n,new int[3]);
								_currentBestPlanCost.get(n)[0]=i;
								_currentBestPlanCost.get(n)[1]=-1;
								_currentBestPlanCost.get(n)[2]=idCostObject;
								Config.CostModel.Free(_currentBestPlanCost.get(k)[2]);
								numOfPlansBeforeMerge++;
								_currentBestPlanCost.set(k, null);
								isAddedtoCoverset=true;
							}
							else{
								Config.CostModel.Free(_currentBestPlanCost.get(k)[2]);
								numOfPlansBeforeMerge++;
								_currentBestPlanCost.set(k, null);
							}
						}	
						else if(!isAddedtoCoverset && Config.APPROXIMATION_ENABLED &&  
								((CostModel_RT)Config.CostModel).IsApproximatelyLessThanOrEqualTo(_currentBestPlanCost.get(k)[2], idCostObject)){
							//this idCostObject cost is approximately  dominated by some cost in _currentBestPlanCost, so no need to consider it
							Config.CostModel.Free(idCostObject);
							break;								
						}
						else if(!isAddedtoCoverset && !Config.APPROXIMATION_ENABLED && Config.CostModel.IsLessThanOrEqual(_currentBestPlanCost.get(k)[2], idCostObject)){
							//this idCostObject cost is dominated by some cost in _currentBestPlanCost, so no need to consider it
							Config.CostModel.Free(idCostObject);
							break;								
						}	
						
					}
					if(isAddedtoCoverset){
						//previous cost objects which are covered by this idCostObject, are set to null, so remove all those
						ArrayList<int[]> temp = new ArrayList<int[]>();
						temp.add(null);
						_currentBestPlanCost.removeAll(temp);
					}
					else if(k>=_currentBestPlanCost.size() && !isAddedtoCoverset){
						//it is incomparable to all costs so need to be added as new best cost
						int n=_currentBestPlanCost.size();
						_currentBestPlanCost.add(n,new int[3]);
						_currentBestPlanCost.get(n)[0]=i;
						_currentBestPlanCost.get(n)[1]=-1;
						_currentBestPlanCost.get(n)[2]=idCostObject;
					}
				}	
			}
		}
		else if(_children.length ==2){	
			child2BestCostsList = _children[1].getCurrentBestPlanOpAndIndex(onlyAlgo);
			child2BestCosts = new int[child2BestCostsList.size()];
			for (int i = 0; i < child2BestCostsList.size(); i++) {
				if(child2BestCostsList.get(i).getLeft() == null){
					child2BestCosts[i] = Config.CostModel.MaxCostId();
				}
				else{
					int index =child2BestCostsList.get(i).getRight();
					child2BestCosts[i] = child2BestCostsList.get(i).getLeft().get_currentBestPlanCost().get(index)[2];
				}
			}	
			
			_currentBestPlanCost.clear(); //TODO: see whether this clearing is correct or not,
			
			final int[] idChildrenBestPlanCosts = new int[_children.length];
			for (int i = 0; i < child1BestCosts.length; i++) {
				if(!Config.CostModel.IsValidPlanCost(child1BestCosts[i])){
					continue;
				}
				for (int j = 0; j < child2BestCosts.length; j++) {
					if(!Config.CostModel.IsValidPlanCost(child2BestCosts[j])){
						continue;
					}
					idChildrenBestPlanCosts[0] = child1BestCosts[i];
					idChildrenBestPlanCosts[1] = child2BestCosts[j];
					int idCostObject = Config.CostModel.AllocZeroCostObj();
					boolean modified = Config.CostModel.computeTotalCost(idCostObject, _rootOperator, _idLocalCost,
							idChildrenBestPlanCosts);
				
					assert ((CostModel_RT)(Config.CostModel)).IsValid(idCostObject) : "cost became invalid in computeCurrentBestPlanCost";
					
					if(_currentBestPlanCost.size()==0){
						_currentBestPlanCost.add(new int[3]);
						_currentBestPlanCost.get(0)[0]=i;
						_currentBestPlanCost.get(0)[1]=j;
						_currentBestPlanCost.get(0)[2]=idCostObject;
					}
					else{
						assert (Config.OBJECTIVE==2): "Control flow sould not come here for objective: "+Config.OBJECTIVE;
						//now check whether this idCostObject should be part of bestcosts cover set of this PhyOpNode
						boolean isAddedtoCoverset=false;
						int k=0;
						for(k=0;k<_currentBestPlanCost.size(); k++){
							if(Config.CostModel.IsLessThan(idCostObject, _currentBestPlanCost.get(k)[2])){
								if(!isAddedtoCoverset){
									int n=_currentBestPlanCost.size();
									_currentBestPlanCost.add(n,new int[3]);
									_currentBestPlanCost.get(n)[0]=i;
									_currentBestPlanCost.get(n)[1]=j;
									_currentBestPlanCost.get(n)[2]=idCostObject;
									Config.CostModel.Free(_currentBestPlanCost.get(k)[2]);
									_currentBestPlanCost.set(k, null);
									numOfPlansBeforeMerge++;
									isAddedtoCoverset=true;
								}
								else{
									Config.CostModel.Free(_currentBestPlanCost.get(k)[2]);
									_currentBestPlanCost.set(k, null);
									numOfPlansBeforeMerge++;
								}
							}
							else if(!isAddedtoCoverset && Config.APPROXIMATION_ENABLED &&	
									((CostModel_RT)Config.CostModel).IsApproximatelyLessThanOrEqualTo(_currentBestPlanCost.get(k)[2], idCostObject)){
								//this idCostObject cost is approximately dominated by some cost in _currentBestPlanCost, so no need to consider it
								Config.CostModel.Free(idCostObject);
								break;								
							}
							else if(!isAddedtoCoverset && !Config.APPROXIMATION_ENABLED &&	Config.CostModel.IsLessThanOrEqual(_currentBestPlanCost.get(k)[2], idCostObject)){
								//this idCostObject cost is dominated by some cost in _currentBestPlanCost, so no need to consider it
								Config.CostModel.Free(idCostObject);
								break;								
							}
							
						}
						if(isAddedtoCoverset){
							//previous cost objects which are covered by thid idCostObject, are set to null, so remove all those
							ArrayList<int[]> temp = new ArrayList<int[]>();
							temp.add(null);
							_currentBestPlanCost.removeAll(temp);
						}
						else if(k>=_currentBestPlanCost.size() && !isAddedtoCoverset){
							//it is incomparable to all costs so need to be added as new best cost
							int n=_currentBestPlanCost.size();
							_currentBestPlanCost.add(n,new int[3]);
							_currentBestPlanCost.get(n)[0]=i;
							_currentBestPlanCost.get(n)[1]=j;
							_currentBestPlanCost.get(n)[2]=idCostObject;
						}
					}			
				}
				
			}
			if(_currentBestPlanCost.size() ==0){
				_currentBestPlanCost.add(new int[3]);
				_currentBestPlanCost.get(0)[0]=-1;
				_currentBestPlanCost.get(0)[1]=-1;
				_currentBestPlanCost.get(0)[2]=Config.CostModel.MaxCostId();
			}
		}
		return true;	
	}

	
	/**
	 * This method recompute the currentBestPlanCostId. In case the cost of currentBestPlanChanged 
	 * in that case it recursively calls update method for that parent equivalence node.
	 * This method should be called in case of children cost has  been changes
	 */
/*	public void updateCurrentBestPlanCost() {

		boolean modified = computeCurrentBestPlanCost();
		if (modified)
			_parent.updateCurrentBestPlanCost(_idCurrentBestPlanCost, this);
		// else
		// System.out.println("currentBestPlanCost");
	}
*/
/*	public int getCostUBId() {
		return _parent.getCostUBId(_isAlgOrLocalEnfOperator);
	}

	public void updateCostUB() {
		if (_children != null) {
			final int idCostUB = _parent.getCostUBId(_isAlgOrLocalEnfOperator);
			final int[] idChildCostLB = MemoryManager.getCostArray(_children.length);

			for (int i = 0; i < _children.length; i++)
				idChildCostLB[i] = _children[i].getCostLBId(!_isAlgOrLocalEnfOperator);

			for (int i = 0; i < _children.length; i++) {
				final int idCurrentChildCostLB = idChildCostLB[i];
				idChildCostLB[i] = ICostModel.NULL;

				boolean modified = Config.CostModel.computeRemainingCostUB(_idChildrenCostUB[i], _rootOperator,
						_idLocalCost, idChildCostLB, idCostUB);

				if (!Config.CostModel.IsValid(_idChildrenCostUB[i]))
					Config.CostModel.SetToZero(_idChildrenCostUB[i]);

				idChildCostLB[i] = idCurrentChildCostLB;

				if (modified)
					_children[i].updateCostUBFromParent(_idChildrenCostUB[i], _isAlgOrLocalEnfOperator);
				// else
				// System.out.println("UpdateChildCostUB");
			}

			MemoryManager.putCostArray(idChildCostLB);
			computeCostUBForChild();
		}
	}
*/
	public int getCostUBForChildId(int childId) {
		for (int i = 0; i < _children.length; i++)
			if (_children[i].getId() == childId)
				return _idChildrenCostUB[i];

		return ICostModel.NULL;
	}

	/*  old code
	public void computeCostUBForChild() {

		final int idCostUB = _parent.getCostUBId(_isAlgOrLocalEnfOperator);
		final int[] idChildCostLB = MemoryManager.getCostArray(_children.length);

		for (int i = 0; i < _children.length; i++)
			idChildCostLB[i] = _children[i].getCostLBId(!_isAlgOrLocalEnfOperator);

		for (int i = 0; i < _children.length; i++) {
			final int idCurrentChildCostLB = idChildCostLB[i];
			idChildCostLB[i] = ICostModel.NULL;

			Config.CostModel.computeRemainingCostUB(_idChildrenCostUB[i], _rootOperator, _idLocalCost, idChildCostLB,
					idCostUB);

			if (!Config.CostModel.IsValid(_idChildrenCostUB[i]))
				Config.CostModel.SetToZero(_idChildrenCostUB[i]);

			idChildCostLB[i] = idCurrentChildCostLB;
		}

		MemoryManager.putCostArray(idChildCostLB);
	}
	*/
	public double getInputSizeBlocks() {
		if (_children == null)
			return 0;

		double inputSize = 0;
		for (int i = 0; i < _children.length; i++) {
			inputSize += Config.OpHelper.getNumBlocks(_children[i].getLogicalProperties());
		}

		return inputSize;
	}

	public StringBuilder printShort() {
		StringBuilder str=new StringBuilder("");
		str.append(_parent.printId());
		System.out.print("." + _id + ".");
		str.append("." + _id + ".");
		Config.OpHelper.printPhysicalOperator(System.out, _rootOperator, _logicalOperator, _parent.getLogicalProperties());
		str.append(" PhysicalOPeartor : ");
		//str.append(((IPhysicalOperator) _rootOperator).getOperatorTag().name());
		if (isEnfOperator()) {
			System.out.print("\t :EnforcerOperator  ");
			str.append("\t :EnforcerOperator  ");
		}else if (isAlgOrLocalEnfOperator()) {
			System.out.print("\t :Algorithmic ");
			str.append("\t :Algorithmic ");
		}else {
			System.out.print("\t :Local Enforcer ");
			str.append("\t :Local Enforcer ");
		}
		return str;
	}

	public void printId() {
		System.out.print(_id);
	}
	
	public void printLong() {
		printShort();
		System.out.println();
		System.out.println("\t\t\t\t _localCost = " + Config.CostModel.toString(_idLocalCost));
		System.out.println("\t\t\t\t _costLB = " + Config.CostModel.toString(_idCostLB));
	//	System.out.println("\t\t\t\t _costUB = " + Config.CostModel.toString(getCostUBId()));
	//	System.out.println("\t\t\t\t _currentBestPlanCost = " + Config.CostModel.toString(_idCurrentBestPlanCost));
		if (_idChildrenCostUB != null) {
			System.out.print("\t\t\t\t _childCostUB = ");
			for (int i = 0; i < _idChildrenCostUB.length; i++)
				System.out.print(Config.CostModel.toString(_idChildrenCostUB[i]) + " ");
			System.out.println();
		}
	}

	/* old method not needed now
	public void print() {
		printShort();
	System.out.print(" <totalCost = " + Config.CostModel.toString(_idCurrentBestPlanCost) + ", localCost = "
				+ Config.CostModel.toString(_idLocalCost) + ", ");
		Config.OpHelper.printPhysicalProperties(System.out, _parent.getPhysicalProperties());
		System.out.print(",");
		Config.OpHelper.printLogicalProperties(System.out, _parent.getLogicalProperties());
		System.out.print(">");
	}*/
	
	
	public void print() {
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		printShort();

		System.out.println("\n Best cost cover set: ");
		for(int i=0;i<_currentBestPlanCost.size();i++){
				System.out.println();
				System.out.println(i + "-------->");
				System.out.println("_currentBestPlanCost.get(" + i+ ")" + _currentBestPlanCost.get(i));
				System.out.println(" totalCost = " + Config.CostModel.toString(_currentBestPlanCost.get(i)[2]) );
				System.out.println(" localCost = "  + Config.CostModel.toString(_idLocalCost) );
				
				if(_children==null){
					System.out.println(" _children = null ");
				}
				else{
					System.out.println(" _children : ");
					
					for(int j=0;j<_children.length; j++){
						_children[j].printGroupAndId();
						System.out.println(" :  "+_currentBestPlanCost.get(i)[j] + "th best plan");
					}
				}
				
		}
		
		/*Config.OpHelper.printPhysicalProperties(System.out, _parent.getPhysicalProperties());
		System.out.print(",");
		Config.OpHelper.printLogicalProperties(System.out, _parent.getLogicalProperties());
		System.out.print(">");
		*/
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	
	public void print(int index, StringBuilder bestPlanStr) {
		bestPlanStr.append(printShort());
		bestPlanStr.append(" \n totalCost = " + Config.CostModel.toString(_currentBestPlanCost.get(index)[2]));
		System.out.print(" \n totalCost = " + Config.CostModel.toString(_currentBestPlanCost.get(index)[2]));
		System.out.print(" \n  localCost = " + Config.CostModel.toString(_idLocalCost) ); 
		
		/*Config.OpHelper.printPhysicalProperties(System.out, _parent.getPhysicalProperties());
		System.out.print(",");
		Config.OpHelper.printLogicalProperties(System.out, _parent.getLogicalProperties());*/
		//bestPlanStr.append("> \n");
		//System.out.print("> \n");
	}

	public String getBestCostAt(int index) {
		//printShort();
		StringBuilder str = new StringBuilder("");
		str.append(_rootOperator);
		str.append(" : ");
		str.append(" \n < totalCost : ");
		str.append(Config.CostModel.toString(_currentBestPlanCost.get(index)[2]));
		return str.toString();
	}
	
	public String getBestCostAt_short(int index) {
		//printShort();
		StringBuilder str = new StringBuilder("");
		str.append(_rootOperator);
		str.append(" : ");
		str.append("\ntotalCost : ");
		
		double rt=((CostModel_RT)Config.CostModel).getResponseTime(_currentBestPlanCost.get(index)[2]);
		double work=((CostModel_RT)Config.CostModel).getTotalWork_usingCostModelOrd(_currentBestPlanCost.get(index)[2]);
		str.append("Response time = ");
		str.append(rt);
		str.append(",   Total work = ");
		str.append(work);
		return str.toString();
	}
	/**MSK
	 * added for printing in file for graph plotting
	 * @param index
	 * @return total work and response time of a cost value at index 'index' in best cost cover set of this PhyOPNode
	 * returns double array as follows: 0th index =total work, 1st index=response time
	 */
	public double[] getBestCostvaluesAt(int index) {
		assert (Config.OBJECTIVE==1 | Config.OBJECTIVE==2): "unknown objective"+Config.OBJECTIVE;
		double[] costvals = new double[3];
		/*if(Config.OBJECTIVE==1){
			costvals[0]=((CostModel_RT)Config.CostModel).getTotalWork_usingCostModelOrd(_currentBestPlanCost.get(index)[2]);
		}else{ //means objective=2
			costvals[0]=((CostModel_RT)Config.CostModel).getSumOfAllResourseComponents(_currentBestPlanCost.get(index)[2]);
				
		}*/

		costvals[0]=((CostModel_RT)Config.CostModel).getTotalWork_usingCostModelOrd(_currentBestPlanCost.get(index)[2]);
		costvals[1]=((CostModel_RT)Config.CostModel).getSumOfAllResourseComponents(_currentBestPlanCost.get(index)[2]);
		
		costvals[2]=((CostModel_RT)Config.CostModel).getResponseTime(_currentBestPlanCost.get(index)[2]);
		return costvals;
	}
	/** 
	 * This method traverse the DAG in bottom up manner and return the Plan_RT-DAG.
	 * Plan DAG is different from AND or graph.  
	 * MSK
	 */
	public Plan_RT getCurrentBestPlan(int index) {

		if (_children == null)
			return new Plan_RT(
					_logicalOperator,
					_rootOperator,
					_parent.getLogicalProperties(),
					_parent.getPhysicalProperties(),
					Config.CostModel.toString(_idLocalCost),
					Config.CostModel.toString(_currentBestPlanCost.get(index)[2]),
					null,
					this
					);

		Plan_RT[] childrenBestPlans = new Plan_RT[_children.length];

		for (int i = 0; i < _children.length; i++) {
			childrenBestPlans[i] = _children[i].getCurrentBestPlanAtIndex(_currentBestPlanCost.get(index)[i]);
		}

		return new Plan_RT(
				_logicalOperator,
				_rootOperator,
				_parent.getLogicalProperties(),
				_parent.getPhysicalProperties(),
				Config.CostModel.toString(_idLocalCost),
				Config.CostModel.toString(_currentBestPlanCost.get(index)[2]),
				childrenBestPlans,
				this
				);
	}

	/**
	 * MSK
	 * Print the best plan.
	 * @param index is the index of the particular entry in currentbestplancost cover set of this opnode which we want to print
	 * it is passed by the parent eqNOde by comparing the response time of various costs among the children of parent eqNode. and then parent 
	 * selects the one with minimum response time 
	 * @param indentLevel
	 * @param bestPlanStr 
	 */
	//mofified on 3 Feb'15 by Mayuri
    //mofified on 10 Feb'15 by Mayuri
	public void printPlan(int index, int indentLevel, StringBuilder bestPlanStr) {
		//System.out.println();
		
		StringBuffer temp = new StringBuffer("");
		for (int i = 0; i < indentLevel; i++){
			System.out.print("  ");
			//bestPlanStr.append("---");
			temp.append("---");			
		}
		
		//--print #pareto plans for parent and myself ---
		bestPlanStr.append("\n");
		bestPlanStr.append(temp.toString());
		bestPlanStr.append("#pareto plans in parentEqNode("+_parent.printId()+"): "+_parent.getCurrentBestPlanOpAndIndex(false).size());
		bestPlanStr.append("\n");
		bestPlanStr.append(temp.toString());
		bestPlanStr.append("#pareto plans for me: "+_currentBestPlanCost.size());
		//---
		
		bestPlanStr.append("\n");
		bestPlanStr.append(temp.toString());
		print(index, bestPlanStr);		
		if (_children == null) 
			return;
		else{
			for (int i = 0; i < _children.length; i++) {	
				_children[i].printPlan(_currentBestPlanCost.get(index)[i], indentLevel+1, bestPlanStr);
			}
		}
	}

	
	
	private void computeLocalCost(Object[] childrenPhysicalProperties) {
		Object[] childrenLogicalProperties = null;
		if (_children != null) {
			childrenLogicalProperties = Config.OpHelper.getLogicalPropertiesArray(_children.length);
			for (int i = 0; i < _children.length; i++)
				childrenLogicalProperties[i] = _children[i].getLogicalProperties();
		}

		Config.CostModel.computeLocalCost(_idLocalCost, _rootOperator, _logicalOperator,
				_parent.getLogicalProperties(), childrenLogicalProperties, _parent.getPhysicalProperties(), childrenPhysicalProperties);

		Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);
	}


	public static void test(){
		List<int[]> _current = new ArrayList<int[]>();
		_current.clear();
		if(_current.size()==0){
			_current.add(new int[3]);
			_current.get(0)[0]=1;
			_current.get(0)[1]=2;
			_current.get(0)[2]=0;
		}
		int n=_current.size();
		_current.add(n,new int[3]);
		_current.get(n)[0]=1;
		_current.get(n)[1]=2;
		_current.get(n)[2]=0;
		 n=_current.size();
		_current.add(n,new int[3]);
		_current.get(n)[0]=1;
		_current.get(n)[1]=2;
		_current.get(n)[2]=0;
		 n=_current.size();
		_current.add(n,new int[3]);
		_current.get(n)[0]=1;
		_current.get(n)[1]=2;
		_current.get(n)[2]=0;
		 n=_current.size();
		_current.add(n,new int[3]);
		_current.get(n)[0]=1;
		_current.get(n)[1]=2;
		_current.get(n)[2]=0;
		 n=_current.size();
		_current.add(n,new int[3]);
		_current.get(n)[0]=1;
		_current.get(n)[1]=2;
		_current.get(n)[2]=0;
		
		n=_current.size();
		System.out.println("before setting null: size: " + n);
		for(int i=0;i<n;i++){
			System.out.println(_current.get(i));
		}
		
		_current.set(1, null);
		_current.set(3, null);
		_current.set(5, null);
		n=_current.size();
		System.out.println("after setting null: size: " + n);
		for(int i=0;i<n;i++){
			System.out.println(_current.get(i));
		}
		
		ArrayList<int[]> temp = new ArrayList<int[]>();
		temp.add(null);
		_current.removeAll(temp);
		n=_current.size();
		System.out.println("after removeall: size: " + n);
		for(int i=0;i<n;i++){
			System.out.println(_current.get(i));
		}
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		test();

	}
	
	
	/**
	 * m Done
	 * this method should be called only after this PhyOpNode is totally optimized, 
	 * and thus its bestplan costs have been completely calculated
	 * It propagates this best plan costs to its parent phyEqNode
	 * sequence of cost ids in _currentBestPlanCost(class variable) and myBestCosts(local variable) must be same 
	 * 
	 */
	public void mergeIntoParentBestCostCoverSet() {
		
		int n =_currentBestPlanCost.size();
		int[] myBestCosts = new int[n];
		for(int i=0;i<n;i++){
			myBestCosts[i]=_currentBestPlanCost.get(i)[2];
			
			assert ((CostModel_RT)(Config.CostModel)).IsValid(myBestCosts[i]) : "coming cost of PhyOpNOde is invalid in mergeIntoParentBestCostCoverSet";
		}
		_parent.mergeIntoBestCostCoverSet(myBestCosts, this, _isAlgOrLocalEnfOperator);
		
		
		
	}
	
}
