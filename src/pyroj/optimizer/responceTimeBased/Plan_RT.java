package pyroj.optimizer.responceTimeBased;


public class Plan_RT {
	private final Object _logicalOperator;
	private final Object _physicalOperator;
	private final Object _logicalProperties;
	private final Object _physicalProperties;
	private final String _localCost;
	private final String _totalCost;
	private final Plan_RT[] _children;
	private final PhysicalOperatorNode_RT _physicalOpNode; // reference to PhysicalOperatorNode that is at the root of the plan
	private boolean materialize = false; // indicate whether plan should be materialized
	
	public Plan_RT (
			Object logicalOperator, 
			Object physicalOperator,
			Object logicalProperties,
			Object physicalProperties,
			String localCost,
			String totalCost,
			Plan_RT[] children,
			PhysicalOperatorNode_RT op) {
		_logicalOperator = logicalOperator;
		_physicalOperator = physicalOperator;
		_logicalProperties = logicalProperties;
		_physicalProperties = physicalProperties;
		_localCost = localCost;
		_totalCost = totalCost;
		_children = children;
		_physicalOpNode = op;
	}
	
	
	/**
	 * Indicates whether plan should be materialized
	 * @return true if the result this plan represents
	 * should be materialized
	 */
	public boolean isMaterialized() {
		return materialize;
	}
	
	/**
	 * Changes the materialization status of the node.
	 * @param n
	 * 		: Value to be assigned to materialization status
	 */
	public void setMaterialization(boolean n){
		materialize = n;
	}
	
	public Object getLogicalOperator() {
		return _logicalOperator;
	}
	
	public Object getPhysicalOperator() {
		return _physicalOperator;
	}
	
	public Object getLogicalProperties() {
		return _logicalProperties;
	}
	
	public Object getPhysicalProperties() {
		return _physicalProperties;
	}
	
	public String getLocalCost() {
		return _localCost;
	}
	
	public String getTotalCost() {
		return _totalCost;
	}
	
	public Plan_RT[] getChildren() {
		return _children;
	}
	/** returns PhysicalOperatorNode that is at the root of the Plan*/
	public PhysicalOperatorNode_RT getPhysicalOpNode() {
		return _physicalOpNode;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
