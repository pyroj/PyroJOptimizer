package pyroj.optimizer.responceTimeBased;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;

//TODO Should memo contain only logical operator or physical operators as well ?
public class Memo_RT {
	static final int MEMOSIZE = 1000;

	private final HashMap<LogicalOperatorNode_RT, LogicalOperatorNode_RT> _operators = new HashMap<LogicalOperatorNode_RT, LogicalOperatorNode_RT>(
			MEMOSIZE, 10);

	public Memo_RT() {
	}

	/**
	 * searches for the logical operator
	 * 
	 * @param op
	 * @return returns the existing operator if found; NULL otherwise
	 */
	public final LogicalOperatorNode_RT find(LogicalOperatorNode_RT op) {
		assert null != op;

		final LogicalOperatorNode_RT retval = _operators.get(op);
		return retval;
	}

	/**
	 * inserts the operator into the table
	 * 
	 * @param op
	 */
	public final void insert(LogicalOperatorNode_RT op) {
		if (_operators.put(op, op) == null) {
			// System.out.println("Operator " + op.getGroup().getId() + "." +
			// op.getId() + " added to memo");
		}
	}

	public LogicalOperatorNode_RT isGroupExist(LogicalEquivalenceNode_RT grp) {
		for (final Iterator<LogicalOperatorNode_RT> opIter = _operators.values()
				.iterator(); opIter.hasNext();) {
			final LogicalOperatorNode_RT op = opIter.next();
			if (op.getGroup().getId() == grp.getId())
				return op;
		}

		return null;
	}


	public void print(LogicalEquivalenceNode_RT root) {
		final BitSet groupsPrinted = new BitSet(
				LogicalEquivalenceNode_RT.NumInstances);

		final PriorityQueue<LogicalEquivalenceNode_RT> groupsToPrint = new PriorityQueue<LogicalEquivalenceNode_RT>();
		groupsToPrint.add(root);

		while (!groupsToPrint.isEmpty()) {
			final LogicalEquivalenceNode_RT grp = groupsToPrint.remove();

			if (groupsPrinted.get(grp.getId()))
				continue;

			groupsPrinted.set(grp.getId());
			grp.printAllExpr(groupsToPrint);
		}

	}

	/**
	 * removes the logical operator
	 * 
	 * @param op
	 */
	public final LogicalOperatorNode_RT remove(LogicalOperatorNode_RT op) {
		op = _operators.remove(op);
	//	System.out.println("Operator " + op.getGroup().getId() + "." +
	//	op.getId() + " removed from memo");
		return op;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
