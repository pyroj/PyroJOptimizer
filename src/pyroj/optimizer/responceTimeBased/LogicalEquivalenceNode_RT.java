package pyroj.optimizer.responceTimeBased;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

import pyroj.optimizer.Config;
import pyroj.optimizer.Config.DebugLevel;
import pyroj.optimizer.DegreeShare;

/**
 * Group node of and / or graph.
 * 
 */
public class LogicalEquivalenceNode_RT implements
		Comparable<LogicalEquivalenceNode_RT> {

	public static int NumInstances = 0;

	private boolean _isTransformationRulesApplied = false;

	public final boolean isTransformationRulesApplied() {
		return _isTransformationRulesApplied;
	}

	public void setTransformationRulesApplied() {
		_isTransformationRulesApplied = true;
	}

	private static void addExpr(ArrayList<LogicalOperatorNode_RT> exprList,
			LogicalOperatorNode_RT expr) {
		final int size = exprList.size();
		for (int i = 0; i < size; i++) {
			if (exprList.get(i) != null && exprList.get(i).equals(expr)) {
					//&& exprList.get(i).getId() == expr.getId())
				exprList.get(i).updateAppliedRules(expr);
			     		return;
			   }
		}

		exprList.add(expr);
	}
		
	private final int _id;

	/**
	 * Array of logical expressions belong to the group.
	 */
	private final ArrayList<LogicalOperatorNode_RT> _logexpr = new ArrayList<LogicalOperatorNode_RT>();

	private Object _logProps = null;

	private final ArrayList<LogicalOperatorNode_RT> _parents = new ArrayList<LogicalOperatorNode_RT>();

	/**
	 * TODO Change it to lattice.
	 */
	protected final ArrayList<PhysicalEquivalenceNode_RT> _plans = new ArrayList<PhysicalEquivalenceNode_RT>();

	private boolean _unificationInProgress = false;

	private LogicalEquivalenceNode_RT _unifiedTo = null;
	
	public LogicalEquivalenceNode_RT UnifiedTo(){
		return _unifiedTo;
	}
	
	// Changes for Greedy----
	
	private int traversal;
	
	/** points to the physical equivalence node with no physical property */
	private PhysicalEquivalenceNode_RT planGroupBase = null;
	
	/** topological ordering number */
	private int topNo;  
	
	// this is topological counter which monotonically increases  
	private static int TopCount =0;

	/** degree denotes the sharability of a node */
	private int degree;
	
	/** it is true if the degree list for the Eq node has been calculated ,
	 * TODO : Add this feature 	 */
	private boolean isDegreeCalculated = false;
	
	/** isAffected is true if the cost of this node might be affected because of change in the cost of some child node */
	private boolean isAffected = false;
	
	// degree list contains the child equivalence node and there degree
	// degree list always sorted by the topological number
	private List<DegreeShare> degreeList = null;
	
	/** refer to use cost of the logical Eq node. Initailized to invalid value */
	//private int _useCostId = -10;

	
	
	public boolean isTraversed(int currentTraversal) {
		return traversal == currentTraversal;
	}
	
	public void setTraversal(int currentTraversal) {
		traversal = currentTraversal;
	}
	// use cost must be calculated once the physical DAg has been expanded
	public int getUseCostId(Object physicalProperties) {
		return Config.CostModel.computeUseCost(_logProps, physicalProperties);
	}
	
	public PhysicalEquivalenceNode_RT getPlanGroupBase() {
		return planGroupBase;
	}
	
	/** this method decides that a logical eq node is shared on the basis of the degree, this 
	 * method particularly used in sharability optimization (see MQO parer for more detail) */
	public boolean isSharable() {
		if (getNumParents() >1  && degree > 1) 
			return true;
		return false;
	}
	
	public int getToplogocalNo(){
		return topNo;
	}
	
	public void setTopologicalNo(){
		this.topNo = TopCount++;
	}
	
	public int getDegree() {
		return degree;
	}
	
	public void setDegree(int degree_t){
		this.degree = degree_t;
	}
	
	public void setDegreeCalculated(boolean flag) {
		isDegreeCalculated = flag;
	}
	
	public boolean isDegreeCalculated() {
		return isDegreeCalculated ;
	}
	
	public boolean isAffected() {
		return isAffected;
	}
	
	public void markIsAffected(boolean flag) {
		this.isAffected = flag;
	}
	
	public int getNumPhysicalEqivalenceNode (){
		return _plans.size();
	}
	
	public List<PhysicalEquivalenceNode_RT> getPhysicalEquivalenceNodeList(){
		return _plans;
	}
	
	/** during the greedy optimization once the the whole physicalDAG is optimizeed we do
	 * not add any new physicalEq node so the Index of each physicalEqNode would be same
	 * 
	 * This method gives the index of the physicalEq node  
	 * @param phyEqNode
	 * @return
	 */
	public int getIndex(PhysicalEquivalenceNode_RT phyEqNode){
		return _plans.indexOf(phyEqNode);
	}

	/**
	 * This method returns the degree list associated with this eq node 
	 * @return
	 */
	public List<DegreeShare> getDegreeList() {
		if (degreeList == null) {
			degreeList = createDegreeList();
		}
		return degreeList;
	}
	
	/** This method look at all child phsyicalEq nodes and return the minimum lb.
	 * The return cost id is just a reference. and that should not be freed by caller */
	// As it is not promised that he LB of each phyEq is updated we will directly look at the 
	/*
	public int getLowerBound() {
		
		for (PhysicalEquivalenceNode_RT phyEq : _plans) {
			
		}
		return -1;
	}
	*/

	
	// This method create the degree list for a given equivalence node
	private List<DegreeShare> createDegreeList() {
		if (degreeList != null)
			return degreeList;
		
		List <DegreeShare> list = new ArrayList<DegreeShare>();
		int numLogExpr = getNumLogExpr();
		if ( numLogExpr > 0 ) {
		
			// input lists
			List<List<DegreeShare>> inpList = new ArrayList<List<DegreeShare>>(numLogExpr);
			
			// iterator for each input list
			List<Iterator<DegreeShare>>  inpIter = new ArrayList<Iterator<DegreeShare>>(numLogExpr);
			
			// initialize each iterator to the 
			for (int i = 0; i< numLogExpr ; i++ ) {
				LogicalOperatorNode_RT logOp = getLogicalExpression(i);
				if (logOp != null ) {
					inpList.add(i, logOp.getDegreeList());
					inpIter.add(i, inpList.get(i).iterator());
				} else {
					inpList.add(i, null);
					inpIter.add(i,null);
				}
 			}
			
			// initialize the currentEntries
			// currEntry point the DagShar in each inpList  which has the lowest topNo 
			List<DegreeShare> curEntry = new ArrayList<DegreeShare>(numLogExpr);
			for (int i = 0 ; i < numLogExpr ; i++ ) {
				if (inpIter.get(i) != null && inpIter.get(i).hasNext()) {
					curEntry.add(i, inpIter.get(i).next());
				} else {
					curEntry.add(i, null);
				}
			}
			
			// scan all list together and process all child logical eq with same topNo in all input list
			for (;;) {
				int minTopNo = -1;
				
				// find out the minimum topological number
				for ( int i = 0 ; i < numLogExpr ; i++  ) {
					if (curEntry.get(i) == null) 
						continue;
					
					LogicalEquivalenceNode_RT childEq = curEntry.get(i).getLogicalEquivalenceNode();
					int eqTopNo = childEq.getToplogocalNo();
					if ( minTopNo == -1 || eqTopNo < minTopNo ) 
						minTopNo = eqTopNo;
					
				}
				
				// if no more eq node left 
				if ( minTopNo == -1 )
					break;
				
				int maxDegree = 0 ;
				DegreeShare maxDegShar = null;
				
				for ( int i = 0 ; i < numLogExpr; i++) {
					if (curEntry.get(i) == null) 
						continue;
					LogicalEquivalenceNode_RT eqNode = curEntry.get(i).getLogicalEquivalenceNode();
					
					int eqTopNo = eqNode.getToplogocalNo();
					
					if (eqTopNo != minTopNo) 
						continue;
					
					int currDegree = curEntry.get(i).getDegree();
					
					if (maxDegree < currDegree) {
						maxDegree= currDegree;
						maxDegShar = curEntry.get(i);
					}
					
					if (inpIter.get(i) != null && inpIter.get(i).hasNext()) {
						curEntry.set(i, inpIter.get(i).next());
					} else {
						curEntry.set(i, null);
					}
				}
				list.add(maxDegShar);
			}
		}
		
		// TODO :Check if all the parent have calculated the degree list delete existing
		// degree list
		setTopologicalNo();
		DegreeShare newDegShar = new DegreeShare(this,1);
		list.add(newDegShar);
		setDegreeCalculated(true);
		degreeList = list;
		return list;
	}
	
		

	/**
	 * This method takes a base physicalEq node and newly created PhysicalEq and insert 
	 * that at the proper place in subList and superList 
	 * @param currentTraversal
	 * @param base
	 * @param requiredProp
	 * @param newPg
	 */
	
	// This is not used in Volcano
	/*private void AddPlan(int currentTraversal, PhysicalEquivalenceNode_RT base, Object requiredProp, PhysicalEquivalenceNode_RT newPg ) {
		assert(newPg != null);
		
		// iterate over all the node in the superlist of the base
		List<PhysicalEquivalenceNode_RT> baseSuperList = base.getSuperList();
		Iterator<PhysicalEquivalenceNode_RT> baseSupIter = baseSuperList.iterator();
		
		boolean inserted = false;
		while (baseSupIter.hasNext()) {
			PhysicalEquivalenceNode_RT nextPG = baseSupIter.next();
			Object nextProp = nextPG.getPhysicalProperties();
			
			if ( Config.OpHelper.isCovered(nextProp,requiredProp) ) {
				// nextProp is covered by the requireProp so we need to go further
				AddPlan(currentTraversal, nextPG, requiredProp, newPg);
				inserted = true;
			} else if ( Config.OpHelper.isCovered(requiredProp,nextProp) ) {
				// as requireProp is covered by the nextProp and 
				// nextProp is not covered by requiredProp 
				// and requireProp covers the baseProp
				// this is the right place to insert the newPG
				
				// collect the nodes present in the super list of the this PhysicalEquivalence Node which covers the current required property
				List<PhysicalEquivalenceNode_RT> nextCoveringList =new ArrayList<PhysicalEquivalenceNode_RT>();
				nextCoveringList.add(nextPG);
				baseSupIter.remove();
				while (baseSupIter.hasNext()){
					nextPG = baseSupIter.next();
					if (Config.OpHelper.isCovered(requiredProp,nextProp)){
						nextCoveringList.add(nextPG);
						baseSupIter.remove();
					}
				}
				
				
				// delete basePG from all the nextPG present in the nextCoveringList
				
				for (PhysicalEquivalenceNode_RT t_nextPG : nextCoveringList) {
					List <PhysicalEquivalenceNode_RT> subList = t_nextPG.getSubList();
					Iterator<PhysicalEquivalenceNode_RT> subListIter = subList.iterator();
					while ( subListIter.hasNext()) {
						if ( subListIter.next().getId() == base.getId() ) {
							subListIter.remove();
							break;
						}
					}
				}
				
				// insert the newPlan Group 
				// collect all the remaining groups under the current iterator which might covered  by the 
				insertPhysicalEquivalenceNode(base,nextCoveringList,newPg);
				inserted = true;
				break;
			}
		}
 
		if (inserted == false) {
			// if there is both of above cases failed for all nodes then
			insertPhysicalEquivalenceNode(base,null,newPg);
		}
	}
*/
	/** This method insert the newPG group assuming that newPG covers the basePG, and  nextPG covers the newPG
	*/
	// This is not used 
	/*private void insertPhysicalEquivalenceNode(PhysicalEquivalenceNode_RT basePG,
			List<PhysicalEquivalenceNode_RT> nextCoveringList, PhysicalEquivalenceNode_RT newPG) {
		assert (basePG != null);
		assert (newPG != null);
		
		// insert PG in base superList 
		basePG.getSuperList().add(newPG);
		
		
		// insert base in the newpG subList
		newPG.getSubList().add(basePG);
		
		// if next Node not null insert in newPG super list and 
		// insert newPG in nextPG superList
		if (nextCoveringList != null) {
			for(PhysicalEquivalenceNode_RT nextPG : nextCoveringList) {
				newPG.getSuperList().add(nextPG);
				nextPG.getSubList().add(newPG);
			}
		}
	}*/
	
	//-------------------------------------------

	
	// Changes for Subsumption derivation
	
	private boolean _isSubExpaned;
	
	/** return the subsumption expanded status **/
	public boolean isSubExpanded () {
		return _isSubExpaned;
	}
	 public void setSubExpanded (boolean isSubExpanded) {
		 this._isSubExpaned = isSubExpanded;
	 }

	 boolean _isShareable = false;
	 public boolean isShareable() {
		 return _isShareable;
	 }
	 
	 public void markSharable(boolean flag) {
		 _isShareable = flag;
	 }
	//-------------------------------------------
	
	public LogicalEquivalenceNode_RT(Object logProp) {
		_id = NumInstances++;
		_logProps = logProp;
	}
	
	

	public void addExpr(LogicalOperatorNode_RT expr) {
		addExpr(_logexpr, expr);
		
		//TODO : Confirm with sapna. is it possible that the logical properties  can change
		
		final Object newLogProp = expr.computeLogicalProperties();
		
		Config.OpHelper.mergeProperty(_logProps, newLogProp);
		Config.OpHelper.putLogicalPropertyObject(newLogProp);
		// TODO:whenever logical property is changed, we need to recompute local
		// cost of each physical operator.
	}

	

	public void addParent(LogicalOperatorNode_RT expr) {
		addParent(expr, false);
	}
	
	public void addParent(LogicalOperatorNode_RT expr, boolean skip) {
		if (!skip) {
	 		for (LogicalOperatorNode_RT parent : _parents) {
				if (parent.getId() == expr.getId())
					return;
			}
		}
		_parents.add(expr);
	}

	@Override
	public int compareTo(LogicalEquivalenceNode_RT arg0) {
		//return Integer.compare(arg0._id, _id);
		return Integer.valueOf(arg0._id).compareTo(Integer.valueOf(_id));
		
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof LogicalEquivalenceNode_RT))
			return false;
		if (((LogicalEquivalenceNode_RT) other).getId() != _id)
			return false;
		return true;
	}
	


	/**
	 * This method returns the physical equivalence node which satisfy the required property.
	 * In case node such physical equivalence node exist it simply add a new equivalence node.
	 * Apart from this is also insert the node in the subList and superList of physical Equivalence node
	 * 
	 * TODO: We should add some insertPhysicalEquivalenceNodeflag in-order to enable and disable subList and superList features.
	 *  
	 * @param reqdProperties
	 * @return
	 */
	public PhysicalEquivalenceNode_RT getPhysicalEqNode(Object reqdProperties) {

		// if the planGroupBase does not exist then add that. 
		if (planGroupBase == null ) {
			// plan group base have null reqd prop
			//Object phyProp = Config.OpHelper.getEmptyPropery();
			planGroupBase = new PhysicalEquivalenceNode_RT(this, null);
			_plans.add(planGroupBase);
		}

		// TODO : Check in case of the Cascade optimization are we considering the subsumtion of the nodes.
		// if the node already present with exactly same property then return the physical equivalence node		
		for(PhysicalEquivalenceNode_RT eqNode : _plans){
			if(Config.OpHelper.isPhysicalPropertyEqual(eqNode.getPhysicalProperties(), reqdProperties))
				return eqNode;
		}
		
		
		PhysicalEquivalenceNode_RT node = new PhysicalEquivalenceNode_RT(this, reqdProperties);
		_plans.add(node);
		
		// TODO : Prasan implementation avoid the cyclic dependency by counter. But  
		// current implementation show that is not needed ensure while debugging
		//
		// Sandeep : I think not required to call this. Currently parameter is set as false
		/*if (Config.getCreateSubAndSuperList() == true ) {
			int currentTraversal = Config.nextCounter();
			AddPlan(currentTraversal, planGroupBase, reqdProperties, node);
		}*/
		return node;
	}

	public LogicalEquivalenceNode_RT GetCurrentActiveGroup() {
		if (_unifiedTo == null)
			return this;
		return _unifiedTo.GetCurrentActiveGroup();
	}

	public int getId() {
		return _id;
	}

	public LogicalOperatorNode_RT getLogicalExpression(int index) {
		if (index >= _logexpr.size())
			return null;
		return _logexpr.get(index);
	}

	public LogicalOperatorNode_RT getParentExpression(int index) {
		if (index >= _parents.size())
			return null;
		return _parents.get(index);
	}

	public Object getLogicalProperties() {
		return _logProps;
	}

	public int getNumLogExpr() {
		return _logexpr.size();
	}

	public int getNumParents() {
		return _parents.size();
	}

	public void print() {
		System.out.print("[");
		printshort();
		System.out.print("]");
		if (_logProps != null) {
			System.out.print(" ");
			Config.OpHelper.printLogicalProperties(System.out, _logProps);
		}
	}

	public void printAllExpr(PriorityQueue<LogicalEquivalenceNode_RT> childGroups) {
		System.out.println();
		System.out
				.println("----------------------------------------------------------------------------------------------------------------");
		print();
		System.out.println();
		System.out
				.println("----------------------------------------------------------------------------------------------------------------");
		for (LogicalOperatorNode_RT expr: _logexpr) {
			if (expr != null)
				expr.printExpr(childGroups);
		}

		System.out.println();

		for (PhysicalEquivalenceNode_RT plan : _plans) {
			plan.print();
		}

	}

	public void printDAG(int indentLevel) {
		System.out.println();
		for (int i = 0; i < indentLevel; i++)
			System.out.print("| ");
		print();

		for (int i = 0; i < _logexpr.size(); i++)
			_logexpr.get(i).printDAG(indentLevel + 1);

	}

	public void printshort() {
		System.out.printf("Group:%1$3d", _id);

	}

	public void removeExpr(LogicalOperatorNode_RT expr) {
		final int size = _logexpr.size();
		for (int i = 0; i < size; i++) {
			if (_logexpr.get(i) != null
					&& _logexpr.get(i).getId() == expr.getId()) {
				_logexpr.set(i, null);
			}
		}
	}

	public void removeParent(LogicalOperatorNode_RT expr) {
		for (int i = 0; i < _parents.size(); i++) {
			if (_parents.get(i).getId() == expr.getId())
				_parents.remove(i);
		}
	}

	// unify other node to this node.
	public LogicalEquivalenceNode_RT unify(LogicalEquivalenceNode_RT obj,
			RT_Optimizer optimizer) {

		final LogicalEquivalenceNode_RT other = obj.GetCurrentActiveGroup();

		if (_unificationInProgress || obj._unificationInProgress)
			return null;

		if (obj._unifiedTo != null) {
			System.out.println("Object has been unified to oter");
		}

		// unify higher group node to lower id group node.
		if (_id > other._id) {
			return other.unify(this, optimizer);
		}
		_unificationInProgress = true;
		other._unificationInProgress = true;

		
		if (Config.IsDebug(DebugLevel.ALL)) {
			System.out.println();
			System.out.print("Unifying ");
			other.printshort();
			System.out.print(" to ");
			printshort();
			if (Config.IsDebug(DebugLevel.ALL)) {
				printAllExpr(null);
				other.printAllExpr(null);
			}
		}
		if (_unifiedTo != null) {
			System.out.println("It has been unified to other");
		}

		if (_id == other._id) {
			System.out.println("unifying same object");
		}
		//moveExprsFrom(other);
		moveParentsFrom(other, optimizer);

		moveExprsFrom(other);

		other._unifiedTo = this;

		if (Config.IsDebug(DebugLevel.ALL)) {
			System.out.print("\nAfter unifying ");
			other.printshort();
			System.out.print(" into ");
			printshort();
			if (Config.IsDebug(DebugLevel.ALL)) {
				printAllExpr(null);
				other.printAllExpr(null);
				System.out.print("");
			}
		}

		_unificationInProgress = false;
		other._unificationInProgress = false;
		return other;
	}

	private void moveExprsFrom(LogicalEquivalenceNode_RT other) {
		for (LogicalOperatorNode_RT expr: other._logexpr) {
			if (expr != null) {
				expr.setGroup(this);
				addExpr(_logexpr, expr);
			}
		}
		other._logexpr.clear();
	}

	private void moveParentsFrom(LogicalEquivalenceNode_RT other,
			RT_Optimizer optimizer) {

		while (!other._parents.isEmpty()) {
			LogicalOperatorNode_RT parent = other._parents.remove(0);
			// As we are changing the child the parent, we have to remove it
			// from memo and re-insert it.
			final LogicalOperatorNode_RT prevParent = optimizer.removeOp(parent);

			// change child pointer of parent
			final int numChildren = parent.getNumChildren();
			for (int i = 0; i < numChildren; i++) {
				final LogicalEquivalenceNode_RT parentChild = parent.getChild(i);
				if (parentChild.getId() == other.getId())
					//parent = optimizer._memo.remove(parent);
					parent.setChild(i, this);
					//optimizer._memo.insert(parent);
			}

			// change parent pointer of this.
			if (prevParent != null) {
				parent = optimizer.addOperator(parent);
			}
			else{
				System.out.println("Nice prevParent is null");
			}
			addParent(parent);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}


