package pyroj.optimizer;

import pyroj.optimizer.responceTimeBased.LogicalEquivalenceNode_RT;

public class DegreeShare {
	LogicalEquivalenceNode_RT logEqNode;
	int degree;
	
	public DegreeShare( LogicalEquivalenceNode_RT  logEq , int d ) {
		logEqNode  = logEq;
		degree = d;
	}
	
	public LogicalEquivalenceNode_RT getLogicalEquivalenceNode() {
		return logEqNode;
	}
	
	public int getDegree() {
		return degree;
	}

}
