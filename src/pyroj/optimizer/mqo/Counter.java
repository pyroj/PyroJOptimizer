 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.mqo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *  Counter class implements a generic counter. The counter
 *  implements its own synchronization and can be called
 *  from more than one thread concurrently. Counter accepts
 *  initial value and then increments by d where d is a
 *  user defined integer value
 *  TODO: Check if there is a standard implementation of
 *  the counter that we can use.
 * */
public class Counter {
	private AtomicInteger count;
	private int d;
	
	/**
	 * Create a new instance of the counter.
	 * @param initial
	 * 		: Initial value of the counter
	 * @param difference 
	 * 		: Difference value. Each get increments counter by difference value
	 * */
	public Counter(int initial, int difference) {
		count = new AtomicInteger(initial);
		d = difference;
	}
	
	/** returns value of the counter and increments the counter*/
	public synchronized int next() {
		return count.getAndAdd(d);
	}
	
}
