 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.mqo;

import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableInt;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import pyroj.costmodel.ICostModel;
import pyroj.optimizer.*;
import pyroj.optimizer.cascade.Cascade;
import pyroj.runtime.IOperatorHelper;
import pyroj.runtime.ITreeHelper;
import pyroj.runtime.Plan;

import java.util.List;


public class MQO extends Optimizer {

	/** Internal optimizer used to optimize each query
	 * */
	private Cascade opt;
	
	private final Counter counter;
	
	public MQO() {
		opt = new CascadeWithState();
		counter = new Counter(1, 1);
	}
	
	/**
	 * Marks the nodes in the Equivalence nodes that
	 * are in the best plan for queries
	 */
	private void markPlan(PhysicalEquivalenceNode peqNode) {
		PhysicalOperatorNode bestPlanRoot = peqNode.getCurrentBestPlanOperator();
		peqNode.setMarked(true);

		if(bestPlanRoot.getChildren()!=null)
		 for(PhysicalEquivalenceNode childPeqNode: bestPlanRoot.getChildren())
			markPlan(childPeqNode);
	}
	
	/** Computes number of times a PhysicalEquivalenceNode is used
	 *  given some nodes in plan are materialized. Number of uses
	 *  of a given PhysicalEquivalenceNode is set to 
	 * */
	private void computeUseCount( int traversal, PhysicalEquivalenceNode peqNode) {
		
		// visiting the node first time
		if( !peqNode.isTraversed(traversal) ) {
			peqNode.setTraversal(traversal);
			peqNode.setUseCount(0);
		}
		// found another use for peqNode
		peqNode.setUseCount(peqNode.getUseCount() + 1);
		
		// processed the node already. Return if useCount
		// is greater than 1 because we only need to know
		// if a node is used more than once
		if(peqNode.isShared() && peqNode.getUseCount() > 1) 
			return;
			// some operators such as EmptyTupleSource may not have
			// any physical plan
			if(peqNode.getCurrentBestPlanOperator() != null && peqNode.getCurrentBestPlanOperator().getChildren() != null) {
				for(PhysicalEquivalenceNode peqChild : peqNode.getCurrentBestPlanOperator().getChildren())
					computeUseCount(traversal, peqChild);
			}
	}
	
	// mark nodes as shared based on their use counts
	private void markShared(PhysicalEquivalenceNode peqNode) {
		// mark child nodes
		if(peqNode.getCurrentBestPlanOperator() != null && peqNode.getCurrentBestPlanOperator().getChildren() != null)
			for(PhysicalEquivalenceNode peqChild : peqNode.getCurrentBestPlanOperator().getChildren())
				markShared(peqChild);
		
		if(peqNode.getUseCount() > 1 && peqNode.getCurrentBestPlanOperator() != null) {
			Object logicalOp = peqNode.getCurrentBestPlanOperator().getLogicalOperator();
			Object logicalProp = peqNode.getLogicalEqNode().getLogicalProperties();
			int totalMatCost = Config.CostModel.computeMaterializationCost(logicalOp, logicalProp, 
					peqNode.getCurrentBestPlanOperator().getOperator(),peqNode.getPhysicalProperties());
			
			totalMatCost = Config.CostModel.DividedBy(totalMatCost, peqNode.getUseCount() - 1);
			int useCost = Config.CostModel.computeUseCost(logicalProp,peqNode.getPhysicalProperties());
			
			Config.CostModel.AddAndAssign(totalMatCost, useCost);
			// cost of the current best plan rooted at operator
			int computeCostId = peqNode.getCurrentBestPlanCostId();
			// see MQO paper for explanation of this plan
			if(Config.CostModel.IsLessThan(totalMatCost, computeCostId))
				peqNode.setShared(true);				
		}
	}
	
	/** select actual set of nodes to materialize
	 * @param traversal
	 * @param peqNode
	 * 		: PhysicalEquivalenceNode considered for materialization
	 * @return costId
	 * 		: CostId is owned by the caller
	 */
	private int sharCostTrav(int traversal, PhysicalEquivalenceNode peqNode, Mutable<Number> matCost) {
		
		Object logicalProperties = peqNode.getLogicalProperties();
		PhysicalOperatorNode phyOpNode = peqNode.getCurrentBestPlanOperator();
		//TODO: Verify cost expression for materialization cost compare 
		if(peqNode.isTraversed(traversal)) {
			int useCostId = Config.CostModel.Clone(Config.CostModel.computeUseCost(logicalProperties,peqNode.getPhysicalProperties()));
			Object logicalOperator = phyOpNode.getLogicalOperator();
			int materializeCostId = Config.CostModel.computeMaterializationCost(logicalOperator, logicalProperties, phyOpNode.getOperator(),phyOpNode.getParent().getPhysicalProperties());
			if(!peqNode.isShared()){
				// useCost = useCost + MatCost() / UseCount()
				Config.CostModel.AddAndAssign(useCostId, Config.CostModel.DividedBy(materializeCostId, peqNode.getUseCount()));
			}
			// materialize
			if(Config.CostModel.IsGreaterThan(peqNode.getCurrentBestPlanCostId(), useCostId)) {
				peqNode.setShared(true);
				int matCostId = (Integer)matCost.getValue();
				Config.CostModel.AddAndAssign(matCostId, materializeCostId);
				matCost.setValue(matCostId);
				peqNode.getCurrentBestPlan().setMaterialization(true);
				peqNode.set_materialize(true);

				System.out.println("Setting Materialization true");
				Config.OpHelper.printPhysicalOperator(System.out, phyOpNode.getOperator(), logicalOperator, logicalProperties);
				Config.OpHelper.printPhysicalProperties(System.out, peqNode.getPhysicalProperties());
				phyOpNode.printShort();

				return useCostId;
			}
			
			return peqNode.getCurrentBestPlanCostId(); 
		} 
		/* This else statement is questionable. For a node that is
		 * set to shared (meaning duplicate not materialized) then
		 * that node will be marked as not shared during its first
		 * traversal. Thus, some cost will be added to usecost 
		 * when it's traversed second time which is not correct.
		 * Thus, a node should be marked as not shared only if it
		 * is not shared already. Thus, the statement is 
		 * redundant.
		 * TODO: Review
		else {
			peqNode.setShared(false);
		}*/
		
		peqNode.setTraversal(traversal);
		
		int costId = Config.CostModel.Clone(peqNode.getCurrentBestPlanOperator().getLocalCostID());
		
		if(peqNode.getCurrentBestPlanOperator() != null && peqNode.getCurrentBestPlanOperator().getChildren() != null)
		{
			for(PhysicalEquivalenceNode childPeqNode : peqNode.getCurrentBestPlanOperator().getChildren()) {
				int childCostId = sharCostTrav(traversal, childPeqNode, matCost);
				Config.CostModel.AddAndAssign(costId, childCostId);
				Config.CostModel.Free(childCostId);
			}
		}
		
		// update plan cost
		// TODO: How to update actual plan cost ? It is of type Object
		int oldBestPlanCostId = peqNode.getCurrentBestPlanCostId();
		Config.CostModel.Assign(oldBestPlanCostId, costId);
		
		return costId;
	}
	
	
	// returns costId of the cost of best plan for peqNode
	// calling routine owns the cost object
	public int sharCost(PhysicalEquivalenceNode peqNode) {
		// add unmarkPlan here if markPlan in optimize method is used
		int traversal = counter.next();
		computeUseCount(traversal, peqNode);
		
		// mark shared nodes
		traversal = counter.next();
		markShared(peqNode);
		
		traversal = counter.next();
		computeUseCount(traversal, peqNode);
		
		traversal = counter.next();
		// TODO: Check if cost arithmetic is necessary here. PyroJ optimizer does not return the cost of the best plan
		Mutable<Number> matCostId = new MutableInt(Config.CostModel.AllocZeroCostObj()); 
		int costId = sharCostTrav(traversal, peqNode, matCostId);
		Config.CostModel.AddAndAssign(costId, (Integer)matCostId.getValue());
		Config.CostModel.Free((Integer)matCostId.getValue());
		return costId;
	}
	
	/** Currently applyLogicalSubsumption only decides whether to materialize
	 *  a PhysicalEquivalenceNode.
	 *
	private void applyLocalLogicalSubsumptions(PhysicalEquivalenceNode peqNode) {
		// we are interested in Alg parents only as VolcanoShar in MQO considers only select subsumption
		List<PhysicalOperatorNode> algParents = peqNode.getAlgParents();	// parent operators for current PhysicalEquivalenceNode
		for(PhysicalOperatorNode parent: algParents) {
			PhysicalEquivalenceNode parPeqNode = parent.getParent();
			
			// parPeqNode is not in best plan. Don't consider this for subsumption.
			if(!parPeqNode.isMarked())
				continue;
			
			// if parent PhysicalOperatorNode is not in the best plan of
			// parent's PhysicalEquivalenceNode
			if(parPeqNode.getCurrentBestPlanOperator().getId() != parent.getId()) 
				continue;

			
			// find out benefit of materializing current Node to parents
			// Add the node to "Materialized state" if it is cheaper 
			// to materialize the node and re-use the results
		}
		 
		
		
		
		// TODO: Free this cost object ?
		/*int cost = Config.CostModel.Clone(pRoot.getLocalCostID());
		for(PhysicalEquivalenceNode childPeqNode: pRoot.getChildren())
				
	}
	
	private void applyLogicalSubsumptions(PhysicalEquivalenceNode peqNode) {
		
		if(!peqNode.isMarked() || peqNode.isVisited())
			return;
		
		peqNode.setvisited(true);
		
		PhysicalOperatorNode pRoot = peqNode.getCurrentBestPlanOperator();
		// apply subsumptions to child nodes
		for(PhysicalEquivalenceNode childPeqNode: pRoot.getChildren())
			applyLogicalSubsumptions(childPeqNode);
			
	}*/
	
	@Override
	public void initialize(Rules rules, IOperatorHelper opHelper, ICostModel costModel, ITreeHelper treeHelper) {
		// See if MQO itself needs handle to Memo and other related structures
		opt.initialize(rules, opHelper, costModel, treeHelper);
	}

	@Override
	public void initialize(Rules rules, IOperatorHelper opHelper, ITreeHelper treeHelper) {
		// See if MQO itself needs handle to Memo and other related structures
		opt.initialize(rules, opHelper, treeHelper);
	}
	
	// TODO: Find a way to let Optimizer convey the nodes to be materialized
	// Possible solution: Add a variable in plan to indicate whether the
	// current node should be materialized. Plan has a reference to 
	// PhysicalEquivalenceNode which is marked appropriately. Thus, if
	// a best plan for a particular node says it is marked for materialization
	// then the runtime should materialize it. Note that Volcano-SH does not
	// change the best plans for individual queries. Thus, we do not need 
	// to change actual plans. We only need to add materialization information.

	protected void markAllTranformationRulesApplied(LogicalOperatorNode op) {
		int numOfTransformationRules = Config.OpHelper.getTransformationRules().length;
		for (int i =0 ; i < numOfTransformationRules; i++ ) {
			op.setRuleApplied(i);
		}
	}

	private LogicalOperatorNode addDerivedOperator(Object expr,
												   LogicalEquivalenceNode[] childrenGroup,
												   LogicalEquivalenceNode grp) {


		final LogicalOperatorNode op = new LogicalOperatorNode(expr, childrenGroup);
		op.markDrived(true);
		// set that LON already expanded
		markAllTranformationRulesApplied(op);

		final LogicalOperatorNode opMemo = _memo.find(op);

		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode(op.computeLogicalProperties());
				grp.markSharable(true);
				grp.setSubExpanded(true);
			}

			grp.addExpr(op);
			op.setGroup(grp);
			if (childrenGroup != null) {
				for (int i = 0; i < childrenGroup.length; i++)
					childrenGroup[i].addParent(op,true);
			}

			if (Config.IsDebug(Config.DebugLevel.HIGH)) {
				System.out.println("Added new Subsumtion Node   ");
				Config.OpHelper.printLogicalOperator(System.out, op.getOperator(), op.getGroup().getLogicalProperties());
				System.out.println();
				//op.printExpr(childGroups);
			}
			// TODO This operator may trigger more rules on ancestor operator,
			// so
			// need to add task for that.
			_memo.insert(op);
			return op;
		}
		else {
			// found matching operators. TODO:  Check if we need to consider which group is unified.
			// TODO:  Check if the root can be an expression
			if(op != opMemo)
				Config.OpHelper.unifyOperators(op.getOperator(), opMemo.getOperator());
			final LogicalEquivalenceNode grpMemo = opMemo.getGroup();
			if (grp != null && grp.getId() != grpMemo.getId()) {
				/*
				final LogicalEquivalenceNode deletedGrp = grp.unify(grpMemo, this);
				if (deletedGrp != null) {
					final LogicalOperatorNode deletedOp = _memo.isGroupExist(deletedGrp);
					if (deletedOp != null) {
						System.out.println("Bug - - ");
						deletedOp.print();
					}
				}
				*/
			}
			return opMemo;
		}

	}

	int AddAggregateScan(PhysicalEquivalenceNode prevInpPG, PhysicalEquivalenceNode newInpPG, PhysicalOperatorNode op)
	{
		Plan prevInpBestPlan = prevInpPG.getCurrentBestPlan();
		Plan newInpBestPlan = newInpPG.getCurrentBestPlan();

		//calculate benefit

		double benefit = 0;
		if(Config.CostModel.IsLessThan(prevInpPG.getUseCostId(), prevInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId()) )
			benefit = Config.CostModel.getCost(prevInpPG.getUseCostId());
		else
			benefit = Config.CostModel.getCost(prevInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId());

		if(Config.CostModel.IsLessThan(newInpPG.getUseCostId(), newInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId()) )
			benefit -= Config.CostModel.getCost(newInpPG.getUseCostId());
		else
			benefit -= Config.CostModel.getCost(newInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId());

		if(benefit > 0)
		{
			// Inconsistent DAG

			//addDerivedPhysicalOperator()
			//Object newLogOp = Config.OpHelper.cloneSelectOperator(lop1, lop2, null);
			//Object newRootOp = Config.OpHelper.getNewPhyOp(newLogOp);
			//PhysicalOperatorNode newOp = new PhysicalOperatorNode(true, newRootOp, newLogOp, );

			op.setChild(0, newInpPG);

			PhysicalEquivalenceNode selPG = op.getParent();
			Plan selBestPlan = selPG.getCurrentBestPlan();
			int cost = selPG.getCurrentBestPlanCostId();

			System.out.println(cost + " " + Config.CostModel.toString(cost));


			int clone_costVal = Config.CostModel.Clone(op.getLocalCostID());

			System.out.println(clone_costVal + " OP Local Cost" + Config.CostModel.toString(clone_costVal));


			if(Config.CostModel.IsLessThan(newInpPG.getUseCostId(), selBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId() ))
				Config.CostModel.AddAndAssign(clone_costVal, newInpPG.getUseCostId());
			else
				Config.CostModel.AddAndAssign(clone_costVal, selBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId());

			System.out.println(Config.CostModel.toString(newInpPG.getUseCostId()));
			System.out.println(Config.CostModel.toString(selBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId()));
			System.out.println(clone_costVal + " OP Local Cost" + Config.CostModel.toString(clone_costVal));
			Config.CostModel.Assign(cost, clone_costVal);

			//int id = op.getCurrentBestPlanCostId();
			//Config.CostModel.Assign(id, clone_costVal);

			System.out.println(cost + " " + Config.CostModel.toString(cost));

			prevInpPG.setShared(false);
			newInpPG.setShared(true);


			return 1;
		}

		return 0;
	}


	int AddSelectScan(PhysicalEquivalenceNode prevInpPG, PhysicalEquivalenceNode newInpPG, PhysicalOperatorNode op)
	{
		Plan prevInpBestPlan = prevInpPG.getCurrentBestPlan();
		Plan newInpBestPlan = newInpPG.getCurrentBestPlan();

		//calculate benefit

		double benefit = 0;
		if(Config.CostModel.IsLessThan(prevInpPG.getUseCostId(), prevInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId()) )
			benefit = Config.CostModel.getCost(prevInpPG.getUseCostId());
		else
			benefit = Config.CostModel.getCost(prevInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId());

		if(Config.CostModel.IsLessThan(newInpPG.getUseCostId(), newInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId()) )
			benefit -= Config.CostModel.getCost(newInpPG.getUseCostId());
		else
			benefit -= Config.CostModel.getCost(newInpBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId());

		if(benefit > 0)
		{
			// Inconsistent DAG

			//addDerivedPhysicalOperator()
			//Object newLogOp = Config.OpHelper.cloneSelectOperator(lop1, lop2, null);
			//Object newRootOp = Config.OpHelper.getNewPhyOp(newLogOp);
			//PhysicalOperatorNode newOp = new PhysicalOperatorNode(true, newRootOp, newLogOp, );

			op.setChild(0, newInpPG);

			PhysicalEquivalenceNode selPG = op.getParent();
			Plan selBestPlan = selPG.getCurrentBestPlan();
			int cost = selPG.getCurrentBestPlanCostId();

			System.out.println(cost + " " + Config.CostModel.toString(cost));


			int clone_costVal = Config.CostModel.Clone(op.getLocalCostID());

			System.out.println(clone_costVal + " OP Local Cost" + Config.CostModel.toString(clone_costVal));


			if(Config.CostModel.IsLessThan(newInpPG.getUseCostId(), selBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId() ))
				Config.CostModel.AddAndAssign(clone_costVal, newInpPG.getUseCostId());
			else
				Config.CostModel.AddAndAssign(clone_costVal, selBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId());

			System.out.println(Config.CostModel.toString(newInpPG.getUseCostId()));
			System.out.println(Config.CostModel.toString(selBestPlan.getPhysicalOpNode().getCurrentBestPlanCostId()));
			System.out.println(clone_costVal + " OP Local Cost" + Config.CostModel.toString(clone_costVal));
			Config.CostModel.Assign(cost, clone_costVal);

			//int id = op.getCurrentBestPlanCostId();
			//Config.CostModel.Assign(id, clone_costVal);

			System.out.println(cost + " " + Config.CostModel.toString(cost));

			prevInpPG.setShared(false);
			newInpPG.setShared(true);


			return 1;
		}

		return 0;
	}

	// apply logical subsumption postpass on the optimal plan
	void ApplyLocalLogicalSubs(int travID, PhysicalEquivalenceNode pg)
	{
		if(!pg.isMarked())
			return;


		Plan pgBestPlan = pg.getCurrentBestPlan();

		if(pgBestPlan==null)
			return;

		List<PhysicalOperatorNode> parents = pg.getAlgParents();
		int numPars = parents.size();

		PhysicalOperatorNode [] selList = new PhysicalOperatorNode[numPars + 1];
		int[] isCovered = new int[numPars + 1];

		PhysicalOperatorNode [] aggList = new PhysicalOperatorNode[numPars + 1];
		int [] iscoveredAgg = new int[numPars + 1];

		int numSel = 0;
		int numAgg = 0;

		for(int i=0; i<numPars; i++)
		{
			PhysicalOperatorNode op = parents.get(i);
			PhysicalEquivalenceNode parPG = op.getParent();

			if(!parPG.isMarked())
				continue;

			if(Config.OpHelper.IsVirtualOperator(parPG.getCurrentBestPlanOperator().getLogicalOperator()))
				return;

			//check if op is included in best plan
			if(parPG.getCurrentBestPlanOperator().getId() != op.getId())
				continue;

			//op is part of best plan

			//update the cost of parPG's bestplan
			//int costVal = op.getCurrentBestPlanCostId();

			int costVal = op.getLocalCostID();//should it be total cost??


			int n = op.getChildren().length;
			PhysicalEquivalenceNode [] children = op.getChildren();

			for(int j=0;j<n;j++)
			{
				PhysicalEquivalenceNode inpPG = children[j];
				Plan inpBestPlan = inpPG.getCurrentBestPlan();

				if(!inpPG.isMarked())
					return;//continue;

				//not adding mat cost here
				int useCost = inpPG.getUseCostId();

				//int matCost = ;


				if(inpPG.isShared())
					Config.CostModel.AddAndAssign(costVal, useCost);

				int parCost = parPG.getCurrentBestPlanCostId();

				//Config.CostModel.Assign(parCost, costVal);

			}


			if(Config.OpHelper.isSelectOperator(op.getLogicalOperator()))
				selList[numSel++] = op ;

			if(Config.OpHelper.isAggregateOperator(op.getLogicalOperator()))
				aggList[numAgg++] = op;

		}

		for( int i = 0 ; i < numSel ; i++ )
			isCovered[i] = 0;

		for(int k=0;k<numSel;k++)
		{
			for(int l=k+1;l<numSel;l++)
			{
				Object lop1 = selList[k].getLogicalOperator();
				Object lop2 = selList[l].getLogicalOperator();

				Plan leftPlan = selList[k].getCurrentBestPlan();
				PhysicalEquivalenceNode leftPG = selList[k].getParent();

				Plan rightPlan = selList[l].getCurrentBestPlan();
				PhysicalEquivalenceNode rightPG = selList[l].getParent();

				if(Config.OpHelper.implies(lop1, lop2))
				{
					isCovered[k] = AddSelectScan(pg, rightPG, selList[k]);
					System.out.println(isCovered[k]);
				}
				else if(Config.OpHelper.implies(lop2, lop1))
				{
					isCovered[l] = AddSelectScan(pg, leftPG, selList[l]);
					System.out.println(isCovered[l]);
				}
			}
		}


		for(int i=0; i < numAgg; i++)
		{
			for(int j=i+1; j< numAgg; j++)
			{
				Object lop1 = aggList[i].getLogicalOperator();
				Object lop2 = aggList[j].getLogicalOperator();

				Plan leftPlan = aggList[i].getCurrentBestPlan();
				PhysicalEquivalenceNode leftPG = aggList[i].getParent();

				Plan rightPlan = aggList[j].getCurrentBestPlan();
				PhysicalEquivalenceNode rightPG = aggList[j].getParent();

				if(Config.OpHelper.impliesAggr(lop1, lop2))
				{
					iscoveredAgg[i] = AddAggregateScan(pg, leftPG, aggList[j]);
				}
				else if(Config.OpHelper.impliesAggr(lop2, lop1))
				{
					iscoveredAgg[j] = AddAggregateScan(pg, rightPG, aggList[i]);
				}
			}
		}


		//the covering predicate

		Object pred = null;
		boolean newPred = false;

		/*for(int i=0;i<numSel;i++)
		{
			if(isCovered[i]==0)
			{
				PhysicalEquivalenceNode selPg = selList[i].getParent();
				if(!selPg.isMarked())
					continue;

				if (pred == null) {
					// get a clone of condition Expression
					pred = Config.OpHelper.cloneSelectPredicate(selList[i].getLogicalOperator());
				} else {
					Object otherExpr = Config.OpHelper.cloneSelectPredicate(selList[i].getLogicalOperator());
					// TODO :can merge one predicate in other, that can avoid creation of unnecessary objects
					pred = Config.OpHelper.mergePredicates(pred,otherExpr);
					newPred = true;
				}


			}
		}

		if(newPred)
		{
			System.out.println("In New Pred");

			Object subSelOp = Config.OpHelper.cloneSelectOperator(selList[0].getOperator(), null, pred);
			LogicalOperatorNode subSelLogOpNode = addDerivedOperator(subSelOp, new LogicalEquivalenceNode[] {pg.getLogicalEqNode()}, null);
			LogicalEquivalenceNode newLogEq = subSelLogOpNode.getGroup();

			PhysicalEquivalenceNode selOp = selList[0].getParent();
			PhysicalEquivalenceNode newPhy = newLogEq.getPhysicalEqNode(selOp.getPhysicalProperties());
			Object rootOp = Config.OpHelper.getFilterPhysicalOperator(subSelOp);
			PhysicalOperatorNode Pop = new PhysicalOperatorNode(true, rootOp , subSelOp, newPhy, selList[0].getChildren(), new Object[]{pg.getPhysicalProperties()});


		}*/

	}

	void ApplyLogicalSubs(int travID, PhysicalEquivalenceNode pg)
	{
		if(!pg.isMarked())
			return;

		if(pg.isTraversed(travID))
			return;

		pg.setTraversal(travID);


		//should do it only for best plan operator
		List<PhysicalOperatorNode> planList = pg.getAlgPlanList();


		for(int i=0; i<planList.size(); i++)
		{
			PhysicalOperatorNode op = planList.get(i);

			if(op!=null && op.getChildren()!=null)
			{
				int n = op.getChildren().length;
				PhysicalEquivalenceNode [] children = op.getChildren();
				for(int j=0;j<n;j++)
				{
					PhysicalEquivalenceNode childPG = children[j];
					ApplyLogicalSubs(travID, childPG);
				}

			}

		}

		ApplyLocalLogicalSubs(travID, pg);

	}
	/**
	 *  MQO optimizer expects an ExprTree with virtual 
	 *  root node at the top. The representation of this
	 *  virtual root node is to be decided by operator 
	 *  implementor. The requirement for the virtual 
	 *  root is that, it should be a dummy node whose 
	 *  purpose is to combine ExprTrees of all queries.
	 * */
	@Override
	public Plan optimize(Object logicalExpressionTree) {
		Plan plan = opt.optimize(logicalExpressionTree);
		PhysicalEquivalenceNode peqNode = plan.getPhysicalOpNode().getParent();
		
		// TODO: Add Select subsumption here
		markPlan(peqNode);

		int travID = counter.next();
		ApplyLogicalSubs(travID, peqNode);




		System.out.println("\n\nFinal memo after physical subsumption");
		System.out.print("============");
		Config.savedMemo.print(peqNode.getLogicalEqNode());

		System.out.println("Plan after applying subsumption but before sharecost traversal");
		peqNode.printCurrentBestPlan(0);
		System.out.println("\n");

		//Plan t = peqNode.getCurrentBestPlan1();
		//PhysicalEquivalenceNode p = t.getPhysicalOpNode().getParent();
		
		sharCost(peqNode);

		/*System.out.println("\n\nFinal memo after share cost traversal");
		System.out.print("============");
		Config.savedMemo.print(peqNode.getLogicalEqNode());*/


		//t = peqNode.getCurrentBestPlan1();
		//p = t.getPhysicalOpNode().getParent();

		System.out.println("Plan after applying subsumption on best plan DAG");
		peqNode.printCurrentBestPlan(0);
		System.out.println("\n");




		return peqNode.getCurrentBestPlan();
	}
	

}
