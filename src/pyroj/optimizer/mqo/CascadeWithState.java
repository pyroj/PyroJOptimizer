 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.mqo;

import java.util.Stack;

import pyroj.optimizer.*;
import pyroj.optimizer.cascade.Cascade;
import pyroj.optimizer.cascade.OptimizePhysicalEquivalenceNode;
import pyroj.optimizer.cascade.Task;
import pyroj.runtime.Plan;

/**
 * Cascade with State does not clear state between query optimization.
 */
public class CascadeWithState extends Cascade {

	private Stack<Task> _tasks;

	/**
	 * Find best plan for the group with the costUB
	 * 
	 * @param group
	 * @param costUB
	 * @return Expression tree of physical operators.
	 * @throws Exception
	 */

	// TODO: Consider if direct changes to Optimizer.java would be appropriate
	@Override
	protected LogicalOperatorNode addOperator(LogicalOperatorNode op) {

		LogicalEquivalenceNode grp = op.getGroup();
		final LogicalOperatorNode opMemo = _memo.find(op);

		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode(op.computeLogicalProperties());
				grp.addExpr(op);
				op.setGroup(grp);
			}

			_memo.insert(op);
			return op;
		}
		else {
			// found a matching operator TODO: Check if we need to consider which group is retained
			if(op != opMemo)
				Config.OpHelper.unifyOperators(op.getOperator(), opMemo.getOperator());
			if (grp != null) {
				grp.removeExpr(op);
				for (int i = 0; i < op.getNumChildren(); i++) {
					op.getChild(i).removeParent(op);
				}

				op.setUnifiedTo(opMemo);

				final LogicalEquivalenceNode grpMemo = opMemo.getGroup();
				if (grp.getId() != grpMemo.getId()) {

					grpMemo.unify(grp, this);
				}
			}
			return opMemo;
		}
	}
	
	protected LogicalOperatorNode addOperator(Object expr, LogicalEquivalenceNode[] childrenGroup,
			LogicalEquivalenceNode grp) {

		Object t = Config.OpHelper.getnewOp(expr);

		Object tmp = null;
		if(t!=null)
			tmp = Config.TreeHelper.getRootOperator(t);

		final LogicalOperatorNode op;
		if(tmp!=null)
			op = new LogicalOperatorNode(tmp, childrenGroup);
		else
			op = new LogicalOperatorNode(expr, childrenGroup);


		final LogicalOperatorNode opMemo = _memo.find(op);

		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode(op.computeLogicalProperties());

				if(grp.getId() > Config.cur_max_grp)
					Config.setCur_max_grp(grp.getId());
			}

			grp.addExpr(op);
			op.setGroup(grp);
			if (childrenGroup != null) {
				for (int i = 0; i < childrenGroup.length; i++)
					childrenGroup[i].addParent(op,true);
			}

			// TODO This operator may trigger more rules on ancestor operator,
			// so
			// need to add task for that.
			_memo.insert(op);

			grp.setSubExpanded(false);
			System.out.println("GRP SUBSUMPTION");
			System.out.println(grp.getId());
			return op;
		}
		else {
			// found matching operators. TODO:  Check if we need to consider which group is unified.
			// TODO:  Check if the root can be an expression

			if(op != opMemo)
				Config.OpHelper.unifyOperators(op.getOperator(), opMemo.getOperator());
			final LogicalEquivalenceNode grpMemo = opMemo.getGroup();


			grpMemo.setSubExpanded(false);
			//System.out.println("GRP SUBSUMPTION");
			//System.out.println(grpMemo.getId());
			/*
			if(grp == null)
			{
				grp = new LogicalEquivalenceNode(op.computeLogicalProperties());
				grp.addExpr(op);
				op.setGroup(grp);
			}*/


			if (grp != null && grp.getId() != grpMemo.getId())
			{

				final LogicalEquivalenceNode deletedGrp = grp.unify(grpMemo, this);
				if (deletedGrp != null) {
					// following code is just to check for bug should be disabled during the 
					// experiments
					/*
					final LogicalOperatorNode deletedOp = _memo.isGroupExist(deletedGrp);
					if (deletedOp != null) {
						System.out.println("Bug - - ");
						deletedOp.print();
					}
					*/
				}
			}

			return opMemo;
		}

	}

	
	
	@Override
	public void addTask(Task task) {
		_tasks.push(task);
	}
	
	public void printMemo(LogicalEquivalenceNode rootLogEq) {
		this._memo.print(rootLogEq);
	}
	
	/**
	 * This Method apply the LogicalSubSumption on a logical Equivalence Node. It check all the parent select nodes 
	 * based on the implies relation it adds the new path in the memo
	 *  
	 * TODO : Add Aggregate Subsumption
	 * @param logEqNode
	 */
	private void ApplyLogicalSub(LogicalEquivalenceNode logEqNode) {
		// if already expanded
		if (logEqNode.isSubExpanded())
				return;


		//logEqNode.printshort();
		//System.out.println("\n");

		if (Config.IsDebug(Config.DebugLevel.HIGH)) {
			System.out.print("Applying SubExpand on ");
			logEqNode.printshort();
			System.out.println();
		}
		
		int numParents = logEqNode.getNumParents();
		
		// ArrayList for select Operator node , +1 to consider the case when the numParents = 0
		LogicalOperatorNode[] selList = new LogicalOperatorNode[numParents+1];
		
		// ArrayList for aggregate operator node
		LogicalOperatorNode[] aggOpList = new LogicalOperatorNode[numParents+1];
		

		
		// number of Parent Select 
		int numSel = 0;
				
		// number of Parent Aggregate
		int numAggr = 0;
		
		// add all the parent select and aggregate operator in their respective list
		for ( int i = 0 ; i < numParents ; i++   )
		    {
			LogicalOperatorNode logOpNode  = logEqNode.getParentExpression(i);
			Object op = logOpNode.getOperator();


			if ( Config.OpHelper.isSelectOperator(op) )
			{
				// hive does not support correlation so all the operator would be should be added  
				if (! Config.OpHelper.isCorrelated(op) ){
					// not correlated 
					selList[numSel++] = logOpNode ;
				} else {
					// this must be removed , just added to catch bug -- as at present correlation is not supported by hive so this case should not be catched
					assert (false);
				}
			}

			if (Config.OpHelper.isAggregateOperator(op))
				aggOpList[numAggr++] = logOpNode; 
		}


		/********************** SELECT SUBSUMPTION **************************/

		// flag to show that parent operator is covered by some other parent operator
		boolean[] isCovered = new boolean[numSel+1];

		// Set isCovered false;
		for ( int i = 0  ; i < isCovered.length ; i++ ) {
			isCovered[i] = false;
		}
		
		// for each pair of select check if one subsumes other add 
		// path connecting to them
		LogicalEquivalenceNode[] chilEquivalenceNodes = new LogicalEquivalenceNode[1];
			
		Object selPred = null;
		for (int i = 0 ; i  < numSel ; i++ ) {
			for (int j = i+1 ; j < numSel ; j++) {
				if ( Config.OpHelper.implies(selList[i].getOperator(), selList[j].getOperator()) ) {
					chilEquivalenceNodes[0] = selList[j].getGroup();
							
					addDerivedOperator(Config.OpHelper.cloneSelectOperator(selList[i].getOperator()
							,selList[j].getOperator(), null),	chilEquivalenceNodes, selList[i].getGroup());
					isCovered[i] = true;
				} else if (Config.OpHelper.implies(selList[j].getOperator(), selList[i].getOperator())) {
					chilEquivalenceNodes[0] = selList[i].getGroup();
					addDerivedOperator(Config.OpHelper.cloneSelectOperator(selList[j].getOperator(),
							selList[i].getOperator(),null), chilEquivalenceNodes, selList[j].getGroup());
					isCovered[j] = true;
				}
			}
		}
		
		// merge all the predicate which are not covered by any other predicate and add a path from 
		// output of this new predicate to the all parent select operator's logEqNode
		Object condExpr = null;
		boolean newPred = false;
		// the predicate which is not covered for some predicate 
		for (int i = 0 ; i < numSel ; i ++) {
			if (!isCovered[i]) {
				if (condExpr == null) {
					// get a clone of condition Expression
					condExpr = Config.OpHelper.cloneSelectPredicate(selList[i].getOperator());
				} else {
					Object otherExpr = Config.OpHelper.cloneSelectPredicate(selList[i].getOperator());
					// TODO :can merge one predicate in other, that can avoid creation of unnecessary objects  
					condExpr = Config.OpHelper.mergePredicates(condExpr,otherExpr);
					newPred = true;
				}
			}
		}
		// if new predicate generated
		if (newPred == true) {
			Object subSelOp = Config.OpHelper.cloneSelectOperator(selList[0].getOperator(), null,condExpr);
			LogicalOperatorNode subSelLogOpNode = addDerivedOperator(subSelOp, new LogicalEquivalenceNode[] {logEqNode}, null);
			Object newSelOp;
			for (int i = 0 ; i < numSel ; i++ ) {
				newSelOp = Config.OpHelper.cloneSelectOperator(selList[i].getOperator(),subSelOp,null);
				addDerivedOperator(newSelOp, new LogicalEquivalenceNode[]{subSelLogOpNode.getGroup()}, selList[i].getGroup());
			}
		}




		/********************** AGGREGATE SUBSUMPTION **************************/

		boolean[] isCoveredAggr = new boolean[numAggr+1];
		for(int i = 0 ; i < numAggr ; i++)
		{
			isCoveredAggr[i] = false;
		}

		LogicalEquivalenceNode[] childEquivalenceNodesAggr = new LogicalEquivalenceNode[1];


		for (int i = 0 ; i  < numAggr ; i++ )
		{
			for (int j = i+1 ; j < numAggr ; j++)
			{
				if ( Config.OpHelper.impliesAggr(aggOpList[j].getOperator(), aggOpList[i].getOperator()) )
				{
					childEquivalenceNodesAggr[0] = aggOpList[j].getGroup();

					addDerivedOperator(Config.OpHelper.cloneAggregateOperator(aggOpList[i].getOperator(),
							aggOpList[j].getOperator(), null), childEquivalenceNodesAggr, aggOpList[i].getGroup());
					isCoveredAggr[i] = true;
				}
				else if (Config.OpHelper.impliesAggr(aggOpList[i].getOperator(), aggOpList[j].getOperator()))
				{
					childEquivalenceNodesAggr[0] = aggOpList[i].getGroup();

					addDerivedOperator(Config.OpHelper.cloneAggregateOperator(aggOpList[j].getOperator(),
							aggOpList[i].getOperator(), null), childEquivalenceNodesAggr, aggOpList[j].getGroup());
					isCoveredAggr[j] = true;
				}
				else if(Config.OpHelper.canMergeAggr(aggOpList[i].getOperator(), aggOpList[j].getOperator()))
				{
					/*LogicalEquivalenceNode[] childEquivalenceNodesAggregate = new LogicalEquivalenceNode[2];
					childEquivalenceNodesAggregate[0] = aggOpList[i].getGroup();
					childEquivalenceNodesAggregate[1] = aggOpList[j].getGroup();*/

					Object Aggop = Config.OpHelper.getAggregateOperator(aggOpList[i].getOperator(), aggOpList[j].getOperator());
					LogicalOperatorNode derivedAggOp = addDerivedOperator(Aggop, new LogicalEquivalenceNode[] {logEqNode}, null);


					Object AggCloneOp = Config.OpHelper.cloneAggregateOperator(aggOpList[i].getOperator(), Aggop, null);
					addDerivedOperator(AggCloneOp, new LogicalEquivalenceNode[]{derivedAggOp.getGroup()}, aggOpList[i].getGroup() );


					Object Projop = Config.OpHelper.getProjectOperator(aggOpList[j].getOperator(), Aggop);
					addDerivedOperator(Projop, new LogicalEquivalenceNode[]{derivedAggOp.getGroup()}, aggOpList[j].getGroup() );

				}
				else if(Config.OpHelper.canMergeAggr(aggOpList[j].getOperator(), aggOpList[i].getOperator()))
				{
					/*LogicalEquivalenceNode[] childEquivalenceNodesAggregate = new LogicalEquivalenceNode[2];
					childEquivalenceNodesAggregate[0] = aggOpList[i].getGroup();
					childEquivalenceNodesAggregate[1] = aggOpList[j].getGroup();*/

					Object Aggop = Config.OpHelper.getAggregateOperator(aggOpList[i].getOperator(), aggOpList[j].getOperator());
					LogicalOperatorNode derivedAggOp = addDerivedOperator(Aggop, new LogicalEquivalenceNode[] {logEqNode}, null);

					Object AggCloneOp = Config.OpHelper.cloneAggregateOperator(aggOpList[j].getOperator(), Aggop, null);
					addDerivedOperator(AggCloneOp, new LogicalEquivalenceNode[]{derivedAggOp.getGroup()}, aggOpList[j].getGroup() );


					Object Projop = Config.OpHelper.getProjectOperator(aggOpList[i].getOperator(), Aggop);
					addDerivedOperator(Projop, new LogicalEquivalenceNode[]{derivedAggOp.getGroup()}, aggOpList[i].getGroup() );
				}


			}
		}

		logEqNode.setSubExpanded(true);
	}

	protected void markAllTranformationRulesApplied(LogicalOperatorNode op) {
		int numOfTransformationRules = Config.OpHelper.getTransformationRules().length;
		for (int i =0 ; i < numOfTransformationRules; i++ ) {
			op.setRuleApplied(i);
		}
	}
	
	// this method add a new derived operator in the memo
	private LogicalOperatorNode addDerivedOperator(Object expr,
			LogicalEquivalenceNode[] childrenGroup,
			LogicalEquivalenceNode grp) {
		

		final LogicalOperatorNode op = new LogicalOperatorNode(expr, childrenGroup);
		op.markDrived(true);
		// set that LON already expanded
		markAllTranformationRulesApplied(op);
		
		final LogicalOperatorNode opMemo = _memo.find(op);

		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode(op.computeLogicalProperties());
				grp.markSharable(true);
				grp.setSubExpanded(true);
			}

			grp.addExpr(op);
			op.setGroup(grp);
			if (childrenGroup != null) {
				for (int i = 0; i < childrenGroup.length; i++)
					childrenGroup[i].addParent(op,true);
			}

			if (Config.IsDebug(Config.DebugLevel.HIGH)) {
				System.out.println("Added new Subsumtion Node   ");
				Config.OpHelper.printLogicalOperator(System.out, op.getOperator(), op.getGroup().getLogicalProperties());
				System.out.println();
				//op.printExpr(childGroups);
			}
			// TODO This operator may trigger more rules on ancestor operator,
			// so
			// need to add task for that.
			_memo.insert(op);
			return op;
		}
		else {
			// found matching operators. TODO:  Check if we need to consider which group is unified.
			// TODO:  Check if the root can be an expression
			if(op != opMemo)
				Config.OpHelper.unifyOperators(op.getOperator(), opMemo.getOperator());
			final LogicalEquivalenceNode grpMemo = opMemo.getGroup();
			if (grp != null && grp.getId() != grpMemo.getId()) {
				/*
				final LogicalEquivalenceNode deletedGrp = grp.unify(grpMemo, this);
				if (deletedGrp != null) {
					final LogicalOperatorNode deletedOp = _memo.isGroupExist(deletedGrp);
					if (deletedOp != null) {
						System.out.println("Bug - - ");
						deletedOp.print();
					}
				}
				*/
			}
			return opMemo;
		}

	}

	/*
	 * This method apply the Logical Subsumption on the Logical Equivalence Node
	 */
	private void expandSubsumption(LogicalEquivalenceNode logEqNode) {

		// if already expanded return 
		if (logEqNode.isSubExpanded())
			 	return;
		
		
		if (Config.IsDebug(Config.DebugLevel.LOW)){
			System.out.print("Starting Expanding Eq node : ");
			logEqNode.printshort();
			System.out.println();
		}
		
		// iterate over all logical expression in bottom up manner


		logEqNode.printshort();
		System.out.println();

		int numLogExpr = logEqNode.getNumLogExpr();

		System.out.println("NO EXP " + numLogExpr);

		int numChild ;
		for (int i = 0 ; i < numLogExpr ; i++ )
		{
			LogicalOperatorNode logOpNode = logEqNode.getLogicalExpression(i);

			if(logOpNode!=null)
			{
				System.out.println("A " + i + " " + numLogExpr);
				//Config.OpHelper.printLogicalOperator(System.out, logOpNode.getOperator(), logEqNode.getLogicalProperties());

				numChild = logOpNode.getNumChildren();
				LogicalEquivalenceNode[] childrenEquivalenceNodes = logOpNode.getChildren();

				for (int j = 0 ; j <  numChild ; j++ )
				{
					System.out.println(childrenEquivalenceNodes[j].getId() + "B");
					expandSubsumption(childrenEquivalenceNodes[j]);
				}
			}


		}
		
		// apply Subsumption Transformation Now
		ApplyLogicalSub(logEqNode);
		
		if (Config.IsDebug(Config.DebugLevel.LOW)){
			System.out.print("Finished Expanding Eq node : ");
			logEqNode.printshort();
			System.out.println();
		}
	
		logEqNode.setSubExpanded(true);
	}

	
	@Override
	public Plan optimize(Object logicalExpressionTree) {

		// Do not clear state between two queries
		
		if (Config.PrintQueryExpressionTree) {
			System.out.println("\nQuery Expression Tree");
			System.out.println("=====================");
			Config.TreeHelper.print(logicalExpressionTree);
			System.out.println();
		}


		if(Config.rootop!=null && Config.savedMemo!=null)
		{
			System.out.println("\n\nPrevious memo");
			System.out.print("============");
			Config.savedMemo.print(Config.rootop.getGroup());
		}
		/**here it copies the initial query expression (logicalExpressionTree) in memo.
		 * addExprTree creates proper treestructure with Logicaloperator node at root and its children.
		 * Adds it in the memo (also creates group) and the operator is associated with that group.
		 * Logicaloperator is added to memo, but as it holds reference to its group, so inturn group is also associated with that memo.
		 * so the returned rootOp is root of the querytree and its associated group is first of memo
		 */
		if(Config.savedMemo!=null)
			_memo = Config.savedMemo;

		final LogicalOperatorNode rootOp = addExprTree(logicalExpressionTree, null);

		if (Config.PrintInitialMemo) {
			System.out.println("\nInitial memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		System.out.println("*************************Applying Rules*********************");
		applyTransformationRules(rootOp.getGroup());
		if (Config.PrintMemoAfterTrasformation) {
			System.out.println("\nMemo after transformation");
			System.out.print("===========================");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}
		
		// Apply Logical Subsumption if enabled
		if (Config.isSubExpand) {
			expandSubsumption(rootOp.getGroup());
			if (Config.PrintSubExpandedMemo) {
				System.out.println("\nSubExpanedMemo");
				System.out.print("============");
				_memo.print(rootOp.getGroup());
				System.out.println();
				System.out.println();
			}
		}

		rootOp.getGroup().getPhysicalEqNode(null).setRoot();
		final int idCostUB = Config.CostModel.MaxCostId();

		_tasks = new Stack<Task>();
		// Add task to optimize root group.
		addTask(new OptimizePhysicalEquivalenceNode(rootOp.getGroup().getPhysicalEqNode(null), this, idCostUB, false));

		while (!_tasks.empty()) {
			final Task task = _tasks.pop();
			task.execute();
		}

		if (Config.PrintFinalMemo) {
			System.out.println("\n\nFinal memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
		}

		Config.setSavedMemo(_memo);
		Config.setRootop(rootOp);
		Config.setMax_grp(Config.cur_max_grp);

		final PhysicalEquivalenceNode phyEqNode = rootOp.getGroup().getPhysicalEqNode(null);

		if (Config.PrintFinalPlan) {
			System.out.print("\nFinal plan ");
			System.out.println("\n==========");
			phyEqNode.printCurrentBestPlan(0);
			System.out.println();
		}

		return phyEqNode.getCurrentBestPlan();
	}
		
}
