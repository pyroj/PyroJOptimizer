 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.incremental;

import java.util.Collection;
import java.util.Stack;

import pyroj.optimizer.Config;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.optimizer.PhysicalOperatorNode;

public class DfsSearch extends HeuristicOptimizer {

	@Override
	public void searchSpace(PhysicalEquivalenceNode rootEqNode) {
		final Stack<Object> tasks = new Stack<Object>();
		tasks.add(rootEqNode);

		while (!tasks.isEmpty()) {
			final Object task = tasks.pop();

			if (task instanceof PhysicalOperatorNode)
				optimizePhysicalOperatorNode((PhysicalOperatorNode) task, tasks);
			else if (task instanceof PhysicalEquivalenceNode)
				optimizePhysicalEquivalenceNode((PhysicalEquivalenceNode) task, tasks);

		}
	}

	private void optimizePhysicalOperatorNode(PhysicalOperatorNode phyOpNode, Collection<Object> tasks) {
		if (Config.PrintTaskExecuting) {
			System.out.println("Optimize ");
			System.out.print("\t\t\t");
			phyOpNode.printLong();
			System.out.println();
		}

		final int idCostUB = phyOpNode.getCostUBId();
		final int idCostLB = phyOpNode.getCostLBId();

		if (Config.CostModel.IsGreaterThanOrEqual(idCostLB, idCostUB))
			return;

		NumTasks++;

		final PhysicalEquivalenceNode[] children = phyOpNode.getChildren();
		if (children == null)
			return;

		final boolean isEnfOperator = phyOpNode.isEnfOperator();

		for (int i = children.length - 1; i >= 0; i--) {

			final int idChildCostUB = phyOpNode.getCostUBForChildId(children[i].getId());

			if (!Config.CostModel.IsValid(idChildCostUB))
				return;

			final int idChildCostLB = children[i].getCostLBId(isEnfOperator);

			// Need to optimize
			if (Config.CostModel.IsLessThan(idChildCostLB, idChildCostUB)) {
				if (!isEnfOperator)
					children[i].setEnfPlanActive();
				tasks.add(children[i]);
			}
		}

		if (Config.PrintTaskExecuting) {
			System.out.print("\t\t\t");
			phyOpNode.printLong();
			System.out.println();
		}
	}

	private void optimizePhysicalEquivalenceNode(PhysicalEquivalenceNode phyEqNode, Collection<Object> tasks) {
		if (Config.PrintTaskExecuting) {
			System.out.println("Optimize ");
			System.out.print("\t\t\t");
			phyEqNode.printLong();
			System.out.println();
		}

		final boolean onlyAlgPlan = !phyEqNode.isEnfPlanActive();

		phyEqNode.expand(onlyAlgPlan, getRules());

		if (Config.PrintTaskExecuting) {
			System.out.println("Optimize ");
			System.out.print("\t\t\t");
			phyEqNode.printLong();
			System.out.println();
		}

		final int idCostUB = phyEqNode.getCostUBId(onlyAlgPlan);
		final int idCostLB = phyEqNode.getCostLBId(onlyAlgPlan);

		if (Config.CostModel.IsGreaterThanOrEqual(idCostLB, idCostUB))
			return;

		NumTasks++;

		final int numOperators = phyEqNode.getNumberOfChildren(onlyAlgPlan);
		for (int i = numOperators - 1; i >= 0; i--) {

			final PhysicalOperatorNode phyOpNode = phyEqNode.getChild(i);

			if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
				continue;

			tasks.add(phyOpNode);
		}
		if (Config.PrintTaskExecuting) {
			System.out.print("\t\t\t");
			phyEqNode.printLong();
			System.out.println();
		}

		phyEqNode.resetEnfPlanActive();
	}
}
