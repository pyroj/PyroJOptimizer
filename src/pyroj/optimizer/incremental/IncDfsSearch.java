 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.incremental;

import java.util.ArrayList;
import java.util.BitSet;

import org.apache.commons.lang3.mutable.MutableInt;

import pyroj.optimizer.Config;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.optimizer.PhysicalOperatorNode;

public class IncDfsSearch extends HeuristicOptimizer {

	@Override
	public void searchSpace(PhysicalEquivalenceNode rootEqNode) {
		while (Config.CostModel.IsLessThan(rootEqNode.getCostLBId(false), rootEqNode.getCurrentBestPlanCostId())) {
			findBetterPlan(getNextEqNodeToExplore(rootEqNode), true);
		}
	}

	private boolean findBetterPlan(PhysicalOperatorNode phyOpNode) {
		if (Config.PrintTaskExecuting) {
			System.out.println("FindBetterPlan ");
			System.out.print("\t\t\t");
			phyOpNode.printLong();
			System.out.println();
		}

		NumTasks++;

		final PhysicalEquivalenceNode[] children = phyOpNode.getChildren();
		if (children == null)
			return false;

		final boolean isEnfOperator = phyOpNode.isEnfOperator();

		final int idCurrentBestPlanCost = Config.CostModel.Clone(phyOpNode.getCurrentBestPlanCostId());

		while (true) {

			final int idCostUB = phyOpNode.getCostUBId();
			final int idCostLB = phyOpNode.getCostLBId();

			if (Config.CostModel.IsGreaterThanOrEqual(idCostLB, idCostUB)) {
				Config.CostModel.Free(idCurrentBestPlanCost);
				if (Config.PrintTaskExecuting) {
					System.out.println("FindBetterPlan ");
					System.out.print("\t\t\t");
					phyOpNode.printLong();
					System.out.println();
				}
				return false;
			}

			for (int i = children.length - 1; i >= 0; i--) {

				final int idChildCostUB = phyOpNode.getCostUBForChildId(children[i].getId());

				if (!Config.CostModel.IsValid(idChildCostUB)) {
					if (Config.PrintTaskExecuting) {
						System.out.println("FindBetterPlan ");
						System.out.print("\t\t\t");
						phyOpNode.printLong();
						System.out.println();
					}
					return false;
				}

				final int idChildCostLB = children[i].getCostLBId(isEnfOperator);

				if (Config.CostModel.IsGreaterThanOrEqual(idChildCostLB, idChildCostUB))
					continue;

				if (!findBetterPlan(children[i], !isEnfOperator))
					continue;

				if (Config.CostModel.IsLessThan(phyOpNode.getCurrentBestPlanCostId(), idCurrentBestPlanCost)) {
					Config.CostModel.Free(idCurrentBestPlanCost);
					if (Config.PrintTaskExecuting) {
						System.out.println("FindBetterPlan ");
						System.out.print("\t\t\t");
						phyOpNode.printLong();
						System.out.println();
					}

					return true;
				}
			}
		}
	}

	private boolean findBetterPlan(PhysicalEquivalenceNode phyEqNode, boolean isEnfPlanActive) {
		if (isEnfPlanActive)
			phyEqNode.setEnfPlanActive();

		if (Config.PrintTaskExecuting) {
			System.out.println("Optimize ");
			System.out.print("\t\t\t");
			phyEqNode.printLong();
			System.out.println();
		}

		NumTasks++;

		final boolean onlyAlgPlan = !phyEqNode.isEnfPlanActive();

		final int idCurrentBestPlanCost = Config.CostModel.Clone(phyEqNode.getCurrentBestPlanCostId());

		phyEqNode.expand(onlyAlgPlan, getRules());

		if (Config.CostModel.IsLessThan(phyEqNode.getCurrentBestPlanCostId(), idCurrentBestPlanCost)) {
			Config.CostModel.Free(idCurrentBestPlanCost);
			phyEqNode.resetEnfPlanActive();
			return true;
		}

		int idCostUB = phyEqNode.getCostUBId(onlyAlgPlan);
		final int idCostLB = phyEqNode.getCostLBId(onlyAlgPlan);

		if (Config.CostModel.IsGreaterThanOrEqual(idCostLB, idCostUB)) {
			Config.CostModel.Free(idCurrentBestPlanCost);
			phyEqNode.resetEnfPlanActive();
			return false;
		}

		while (true) {

			final PhysicalOperatorNode phyOpNode = findOpNodeToExplore(phyEqNode);

			if (phyOpNode == null) {

				if (Config.PrintTaskExecuting) {
					System.out.print("\t\t\t");
					phyEqNode.printLong();
					System.out.println();
				}

				phyEqNode.resetEnfPlanActive();

				return false;
			}

			idCostUB = phyEqNode.getCostUBId(onlyAlgPlan);
			if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
				continue;

			if (Config.PrintTaskExecuting) {
				System.out.println("Optimize ");
				System.out.print("\t\t\t");
				phyEqNode.printLong();
				System.out.println();
			}

			findBetterPlan(phyOpNode);

			if (Config.CostModel.IsLessThan(phyEqNode.getCurrentBestPlanCostId(), idCurrentBestPlanCost)) {
				Config.CostModel.Free(idCurrentBestPlanCost);
				if (Config.PrintTaskExecuting) {
					System.out.println("Optimize ");
					System.out.print("\t\t\t");
					phyEqNode.printLong();
					System.out.println();
				}
				phyEqNode.resetEnfPlanActive();
				return true;
			}
		}
	}

	private PhysicalOperatorNode findOpNodeToExplore(PhysicalEquivalenceNode phyEqNode) {

		final boolean onlyAlgPlan = !phyEqNode.isEnfPlanActive();

		final int numOperators = phyEqNode.getNumberOfChildren(onlyAlgPlan);

		if (numOperators <= 0)
			return null;

		int idCostUB = phyEqNode.getCostUBId(onlyAlgPlan);

		switch (Config.SelectPhysicalOperatorOrder) {
		case SEQUENTIAL: {
			for (int i = numOperators - 1; i >= 0; i--) {

				final PhysicalOperatorNode phyOpNode = phyEqNode.getChild(i);

				if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
					continue;

				return phyOpNode;
			}
		}
		case MIN_LOCAL_COST: {
			PhysicalOperatorNode opNodeToExplore = null;

			for (int i = numOperators - 1; i >= 0; i--) {

				final PhysicalOperatorNode phyOpNode = phyEqNode.getChild(i);

				if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
					continue;

				if (opNodeToExplore == null)
					opNodeToExplore = phyOpNode;

				else if (Config.CostModel.IsLessThan(phyOpNode.getLocalCostID(), opNodeToExplore.getLocalCostID())) {
					opNodeToExplore = phyOpNode;
				}
			}

			return opNodeToExplore;
		}
		case MIN_COST_LB: {
			PhysicalOperatorNode opNodeToExplore = null;

			for (int i = numOperators - 1; i >= 0; i--) {

				final PhysicalOperatorNode phyOpNode = phyEqNode.getChild(i);

				if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
					continue;

				if (opNodeToExplore == null)
					opNodeToExplore = phyOpNode;

				else if (Config.CostModel.IsLessThan(phyOpNode.getCostLBId(), opNodeToExplore.getCostLBId())) {
					opNodeToExplore = phyOpNode;
				}
			}

			return opNodeToExplore;

		}
		case MIN_INPUT_SIZE: {
			PhysicalOperatorNode opNodeToExplore = null;
			double currentInputSize = Double.MAX_VALUE;

			for (int i = numOperators - 1; i >= 0; i--) {

				final PhysicalOperatorNode phyOpNode = phyEqNode.getChild(i);

				if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
					continue;

				if (opNodeToExplore == null) {
					opNodeToExplore = phyOpNode;
					currentInputSize = phyOpNode.getInputSizeBlocks();
				}

				else if (phyOpNode.getInputSizeBlocks() < currentInputSize) {
					opNodeToExplore = phyOpNode;
					currentInputSize = phyOpNode.getInputSizeBlocks();
				}
			}

			return opNodeToExplore;
		}
		case RANDOM: {
			BitSet opExplored = new BitSet(numOperators);
			while (opExplored.nextClearBit(0) < numOperators) {
				int randomIndex = (int) (Math.random() * (numOperators - opExplored.cardinality()));

				int bestExprIndex = -1;
				while (randomIndex >= 0) {
					bestExprIndex = opExplored.nextClearBit(bestExprIndex + 1);
					randomIndex--;
				}
				opExplored.set(bestExprIndex);

				final PhysicalOperatorNode phyOpNode = phyEqNode.getChild(bestExprIndex);

				if (Config.CostModel.IsLessThan(phyOpNode.getCostLBId(), idCostUB))
					return phyOpNode;
			}
			return null;
		}
		default:
			return null;
		}

	}

	private PhysicalEquivalenceNode getNextEqNodeToExplore(PhysicalEquivalenceNode phyEqNode) {

		switch (Config.SelectPhyscialEqNodeOrder) {
		case ROOT:
			return phyEqNode;
		case PARENT_WITH_MOSTEXPENSIVE_CHILD: {
			return getMostExpensiveParent(phyEqNode, null);
		}
		case RANDOM: {
			if (phyEqNode.getCurrentBestPlanOperator() == null)
				return phyEqNode;

			ArrayList<PhysicalOperatorNode> bestPlanOps = new ArrayList<PhysicalOperatorNode>();

			getPhysicalEqNodes(phyEqNode, bestPlanOps);

			while (true) {
				int index = (int) (Math.random() * bestPlanOps.size());

				PhysicalOperatorNode opNode = bestPlanOps.get(index);

				PhysicalEquivalenceNode eqNode = opNode.getParent();

				if (Config.CostModel.IsLessThan(eqNode.getCostLBId(false), eqNode.getCurrentBestPlanCostId()))
					return eqNode;
			}
		}
		default:
			return null;
		}
	}

	private PhysicalEquivalenceNode getMostExpensiveParent(PhysicalEquivalenceNode rootEqNode,
			MutableInt maxChildLocalCost) {
		PhysicalOperatorNode parent = rootEqNode.getCurrentBestPlanOperator();

		if (parent == null)
			return rootEqNode;

		PhysicalEquivalenceNode[] children = parent.getChildren();
		if (children == null)
			return rootEqNode;

		int mostExpensiveChildLocalCost = -1;
		PhysicalEquivalenceNode mostExpParent = rootEqNode;

		MutableInt childMaxChildCost = new MutableInt();
		boolean onlyAlgPlan = parent.isEnfOperator();

		for (int i = 0; i < children.length; i++) {
			int childLocalCost = children[i].getCurrentBestPlanOperator().getLocalCostID();

			if (mostExpensiveChildLocalCost == -1)
				mostExpensiveChildLocalCost = childLocalCost;

			else if (Config.CostModel.IsGreaterThan(childLocalCost, mostExpensiveChildLocalCost)) {
				mostExpensiveChildLocalCost = childLocalCost;
				mostExpParent = rootEqNode;
			}

			if (Config.CostModel.IsLessThan(children[i].getCostLBId(onlyAlgPlan),
					children[i].getCurrentBestPlanCostId())) {
				childMaxChildCost.setValue(0);
				PhysicalEquivalenceNode mostExpParentInChild = getMostExpensiveParent(children[i], childMaxChildCost);

				if (Config.CostModel.IsGreaterThan(childMaxChildCost.getValue(), mostExpensiveChildLocalCost)) {
					mostExpensiveChildLocalCost = childMaxChildCost.getValue();
					mostExpParent = mostExpParentInChild;
				}
			}
		}
		if (maxChildLocalCost != null)

			maxChildLocalCost.setValue(mostExpensiveChildLocalCost);
		return mostExpParent;
	}

	private void getPhysicalEqNodes(PhysicalEquivalenceNode rootEqNode, ArrayList<PhysicalOperatorNode> opNodes) {
		PhysicalOperatorNode opNode = rootEqNode.getCurrentBestPlanOperator();
		opNodes.add(opNode);

		PhysicalEquivalenceNode[] children = opNode.getChildren();
		if (children == null)
			return;

		for (int i = 0; i < children.length; i++)
			getPhysicalEqNodes(children[i], opNodes);
	}

}
