 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.incremental;

import pyroj.optimizer.Config;
import pyroj.optimizer.LogicalOperatorNode;
import pyroj.optimizer.Optimizer;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.runtime.Plan;

public abstract class HeuristicOptimizer extends Optimizer {

	public static int NumTasks = 0;

	@Override
	public Plan optimize(Object logicalExpressionTree) {
		clearState();

		if (Config.PrintQueryExpressionTree) {
			System.out.println("\nQuery Expression Tree");
			System.out.println("=====================");
			Config.TreeHelper.print(logicalExpressionTree);
			System.out.println();
		}

		final LogicalOperatorNode rootOp = addExprTree(logicalExpressionTree, null);

		if (Config.PrintInitialMemo) {
			System.out.println("\nInitial memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		applyTransformationRules(rootOp.getGroup());

		if (Config.PrintMemoAfterTrasformation) {
			System.out.println("\nMemo after transformation");
			System.out.print("===========================");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		Config.PropagateAll = true;

		final PhysicalEquivalenceNode phyEqNode = rootOp.getGroup().getPhysicalEqNode(null);
		phyEqNode.setEnfPlanActive();

		phyEqNode.setRoot();
		searchSpace(phyEqNode);

		if (Config.PrintFinalMemo) {
			System.out.println("\n\nFinal memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
		}

		if (Config.PrintFinalPlan) {
			System.out.print("\nFinal plan ");
			System.out.println("\n==========");
			phyEqNode.printCurrentBestPlan(0);
			System.out.println();
		}

		return phyEqNode.getCurrentBestPlan();
	}

	public abstract void searchSpace(PhysicalEquivalenceNode rootEqNode);

}
