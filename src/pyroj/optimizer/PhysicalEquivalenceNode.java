 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import pyroj.runtime.IImplementationRule;
import pyroj.runtime.ILocalEnforcerRule;
import pyroj.runtime.IPartitionEnforcerRule;
import pyroj.runtime.Plan;

public class PhysicalEquivalenceNode {
	static public int NumInstances = 0;

	private final int _id;

	private final LogicalEquivalenceNode _group;
	private final Object _physicalProperties;

	private ArrayList<PhysicalOperatorNode> _algOperators;
	private ArrayList<PhysicalOperatorNode> _enfOperators;

	private final ArrayList<PhysicalOperatorNode> _algParents = new ArrayList<PhysicalOperatorNode>();
	private final ArrayList<PhysicalOperatorNode> _enfParents = new ArrayList<PhysicalOperatorNode>();

	private Pair<Integer, Plan> cachedBestPlan = null;
	
	private PhysicalOperatorNode _currentBestPlanOp = null;

	private boolean _isImplementationRulesApplied = false;
	public final boolean isTransformationRulesApplied() {
		return _isImplementationRulesApplied;
	}

	public void setTransformationRulesApplied() {
		_isImplementationRulesApplied = true;
	}
	
	// Add for Greedy ----
	
	private PhysicalOperatorNode _currentBestLocalEnfPlanOp = null;
	
	// At present these are only updated at time of the Greedy Optimization
	// points to the current best algorithmic plan's root physical Operator Node
	private PhysicalOperatorNode _currentBestAlgPlanOp = null;
	
	// points to the current best algorithmic plan's root physical Operator Node
	private PhysicalOperatorNode _currentBestPartEnfPlanOp = null; 
	
	/** List of all Nodes whose prop is covered by the this PhyEq or node who can use the plan of this node, The sublist consist 
	 * of only immediate predecessor
	 * In order to traverse all the physicalEqvalence node which are covered by this physical Equivalence node one need to recursively visit sublist
	 * of elements present in the suplist of the this PhysicalEquivalence node. 
	 * */
	private List<PhysicalEquivalenceNode> subList = null;
	
	/** List of all nodes who covers the prop of this physical node. or whose plan can be used by this phyical Eq  node ,
	 * We consider only immediate successor 
	 * In order to traverse all the physicalEqvalence node which covers this physical Equivalence node one need to recursively visit superlist
	 * of elements present in the super list of the this PhysicalEquivalence node 
	 * */
	private List<PhysicalEquivalenceNode> superList = null;
	
	/** represent the number of places where the physicalEq node is being processed */
	private int _progressCount = 0 ;
	
	/** secondTraversal used to mark node a visited. We have a variable traversal which is used for same purpose. 
	 * But there are cases when we need traverse set of nodes while one traversal is going on. This variable can be used in such scenario
	 * */
	private int secondTraversal = 0; 

	/** isOptimized is true if the physical Eq node has been optimized completely*/
	private boolean _isOptimized = false;
	
	/** this method refer to the bestPlanObj in case the plan is revisited this object would be returned 
	 * At present this is used only in greedy optimization. 
	 * TODO : for SharSh we have same kind of technique. We can merge two methods (if possible)  
	 * */
	private Plan bestPlanObj = null;
	
	
	// -------

	
	public static int NumEqNodesDuringLastUpdate = 0;

	public static int NumOpNodesDuringLastUpdate = 0;

	private boolean _isRoot = false;

	private boolean _isEnfPlanActive = false;

	public boolean materialize = false;
	public void set_materialize(boolean val)
	{
		materialize =  val;
	}

	
	/**
	 * cost lower bound among algorithm operators only. _algCostLB >= _costLB
	 * Reference to costLB object of algorithmic operator with minimum costLB
	 */
	private int _idAlgCostLB = Config.CostModel.ZeroCostId();

	/**
	 * Overall cost lower bound. Minimum cost lower bound among algorithm and
	 * enforcer operators. Reference to costLB object of operator with minimum
	 * costLB
	 */
	private int _idCostLB = Config.CostModel.ZeroCostId();

	/**
	 * Cost upper from from only algorithmic parents. It is the upper bound for
	 * enforcer operators. _costUBFromAlgParents <= _costUBFromParents.
	 * Reference to childCostUB object of algorithmic parent with maximum
	 * childCostUB
	 */
	private int _idCostUBFromAlgParents = Config.CostModel.MaxCostId();

	/**
	 * overall cost upper bound from all parents, it is cost upper bound for
	 * algorithm as well as enforcer operators. Reference to childCostUB object
	 * of parent with maximum childCostUB.
	 */
	private int _idCostUBFromParent = Config.CostModel.MaxCostId();

	/**
	 * Cost of current best plan (algorithmic / enforcer) for the phyEqNode.
	 * Reference to currentBestPlanCost object of operator with min
	 * currentBestPlanCost
	 */
	private int _idCurrentBestPlanCost = Config.CostModel.MaxCostId();
	
	
	/** Number of times current PhysicalEquivalenceNode 
	 *  is used in plan
	 * */
	private int useCount = 0;
	
	private boolean shared;
	
	/** Indicates which equivalence nodes are in best plan.
	 *  This can be used for other purposes too.
	 * */
	private boolean mark = false;
		
	public PhysicalEquivalenceNode(LogicalEquivalenceNode group, Object physicalProperties) {
		_group = group;
		_physicalProperties = physicalProperties;
		_id = NumInstances++;
		//-- Added for greedy
		if (Config.getCreateSubAndSuperList() == true) {
			superList = new ArrayList<PhysicalEquivalenceNode>();
			subList = new ArrayList<PhysicalEquivalenceNode>();
		}
	}
	
	
	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	/** returns whether current PhysicalEquivalenceNode is shared*/
	public boolean isShared() {
		return shared;
	}

	/** Sets the current PhysicalEquivalenceNode as shared or unshared*/
	public void setShared(boolean shared) {
		this.shared = shared;
	}

	public void setRoot() {
		_isRoot = true;
	}

	/**
	 * Returns algorithmic parents of the PhysicalEquivalenceNode
	 * */
	public List<PhysicalOperatorNode> getAlgParents() {
		return new ArrayList<PhysicalOperatorNode>(_algParents);
	}
	
	
	/** Mark the node. The purpose of marking is dependent
	 * on user of this class.
	 * */
	public void setMarked(boolean n) {
		mark = n;
	}
	
	/** Returns true if marked is true
	 * */
	public boolean isMarked() {
		return mark;
	}
	
	/** Indicates which equivalence nodes are visited in
	 * current traversal. This can be used for other 
	 * purposes too. TraversalId is useful because
	 * otherwise, we would need to reset visited for
	 * each node in between two traversals for all nodes
	 * */
	private int traversal = 0;
	
	/** Mark the node as visited. The purpose of marking is dependent
	 * on user of this class
	 * */
	public void setTraversal(int n) {
		traversal = n;
	}
	
	/** Returns traversal id of the latest traversal
	 * */
	public boolean isTraversed(int currentTraversal) {
		return traversal == currentTraversal;
	}

	
	// Added for Greedy-----------------
	
	
	public void setSecondTraversal(int currentTraversal) {
		secondTraversal = currentTraversal;
	}
	
	public boolean isSecondTraversal(int currentTraversal) {
		return (secondTraversal == currentTraversal);
	}
	
	
	public List<PhysicalEquivalenceNode>  getSuperList() {
		return superList;
	}
	
	public List<PhysicalEquivalenceNode> getSubList() {
		return subList;
	}
	
	
	public PhysicalOperatorNode getBestPartEnforcerPlan() {
		return _currentBestPartEnfPlanOp;
	}
	
	public PhysicalOperatorNode getBestAlgorithmicPlan() {
		return _currentBestAlgPlanOp;
	}
	
	
	public void setBestAlgorithmicPlan(PhysicalOperatorNode bestAlgPlan) {
		_currentBestAlgPlanOp = bestAlgPlan;
	}
	
	/*
	public void setBestEnforcerPlan(PhysicalOperatorNode bestEnfPlan) {
		_currentBestEnfPlanOp = bestEnfPlan;
	}
	*/
	
	public boolean isInProgress() {
		if (_progressCount > 0) 
			return true;
		return false;
	}
	
	public void incrProgressCount() {
		_progressCount++;
	}
	
	public void decrProgressCount() {
		_progressCount--;
	}
	
	public void setCurrentBestPlanOp(PhysicalOperatorNode phyOp) {
		_currentBestPlanOp = phyOp;
	}
	
	public void setCurrentBestPlanCostId(int costId) { 
		_idCurrentBestPlanCost = costId;
	}
	
	public List<PhysicalOperatorNode> getAlgPlanList() {
		return _algOperators;
	} 
	public List<PhysicalOperatorNode> getEnfPlanList() {
		return _enfOperators;
	}
	
	public ArrayList<PhysicalOperatorNode> getEnfParantPlanNode() {
		return _enfParents;
	}
	
	// this method returns the parent of the localEnforcerNode. At present all local enforcers parent would have been stored in 
	// algparents. Should be fixed later
	public List<PhysicalOperatorNode> getLocalEnfParentPlanNode(){
		
		if (_algParents == null || _algParents.size() == 0)
			return  null;
		List<PhysicalOperatorNode> localEnfParents = new ArrayList<PhysicalOperatorNode>();
		
		for (PhysicalOperatorNode parOpNode : _algParents) {
			if (parOpNode.isEnfOperator() == true) {
				localEnfParents.add(parOpNode);
			}
		}
		return localEnfParents;
	}
	
	// this method return the index of the physicalEquivalenceNode in the logicalEquivalenceGroup it belongs 
	public int getIndex() {
		LogicalEquivalenceNode logEqNode = this.getLogicalEqNode();
		return logEqNode.getIndex(this);
	}
	
	// This method returns all the local enforcer (at present sort operators)
	public List<PhysicalOperatorNode> getLocalEnfPlanList() {
		// at present the localEnforcers are present in the list of the _algOperator Node. This should be separated
		if (_algOperators == null)
			return null;
		List<PhysicalOperatorNode> localEnfNode = new ArrayList<PhysicalOperatorNode>();
		for (PhysicalOperatorNode aOperator : _algOperators) {
			if (aOperator.isLocalEnf() == true ) {
				localEnfNode.add(aOperator);
			}
		}
		return localEnfNode;
	}
	
	public int getUseCostId() {
		return _group.getUseCostId(_physicalProperties);
	}
	
	/**
	 * Traverse all the algorithmic plan and set (if feasible) best algorithmic plan in the 
	 * _currentBestAlgPlanOp variable 
	 * @return
	 */
	public void  updateBestAlgorithmicPlan(boolean consideringSort) {
		int bestAlgCostId;
		bestAlgCostId = Config.CostModel.MaxCostId();
		_currentBestAlgPlanOp = null;
		PhysicalOperatorNode opNode;
		if (_algOperators != null && _algOperators.size() != 0) {
			for (int i = 0; i < _algOperators.size(); i++) {
				opNode = _algOperators.get(i);
				if (opNode.isLocalEnf() && consideringSort == false)
					continue;
				if (opNode.isLocalEnf() == false && consideringSort == true)
					continue;
				int idOpCost = opNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsGreaterThan(bestAlgCostId, idOpCost)){
					_currentBestAlgPlanOp = opNode;
					bestAlgCostId =  idOpCost; 
				}
			}
		}
		
		if ( _currentBestAlgPlanOp != null && !Config.CostModel.IsValidPlanCost(bestAlgCostId) )
			_currentBestAlgPlanOp = null;
	}
	
	/** 
	 * This method traverse all child enforcer plans and all the sort plan's and then  and set the best enf plan in 
	 * _currentBestEnfPlanOp  
	 */
	public void updateBestPartEnforcerPlan() {
		int bestEnfCostId = Config.CostModel.MaxCostId();
		_currentBestPartEnfPlanOp = null;
		PhysicalOperatorNode enfOp = null;
		int costId;
		if (_enfOperators != null && _enfOperators.size() !=0 ) {
			for( int i = 0 ; i < _enfOperators.size() ; i++  ) {
				enfOp = _enfOperators.get(i);
				costId = enfOp.getCurrentBestPlanCostId();
				if(Config.CostModel.IsGreaterThan(bestEnfCostId, costId)) {
					_currentBestPartEnfPlanOp = enfOp;
					bestEnfCostId = costId;
				}
			}
		}
		
		
		if ( _currentBestPartEnfPlanOp != null && !Config.CostModel.IsValidPlanCost(bestEnfCostId) ) {
			_currentBestPartEnfPlanOp = null;
		}
	}
	

	
	public void updateBestLocalEnfPlan() {
		int bestLocalEnfCostId;
		bestLocalEnfCostId = Config.CostModel.MaxCostId();
		_currentBestLocalEnfPlanOp = null;
		PhysicalOperatorNode opNode;
		if (_algOperators != null && _algOperators.size() != 0) {
			for (int i = 0; i < _algOperators.size(); i++) {
				opNode = _algOperators.get(i);
				if (opNode.isLocalEnf() != true)
					continue;
				int idOpCost = opNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsGreaterThan(bestLocalEnfCostId, idOpCost)){
					_currentBestLocalEnfPlanOp = opNode;
					bestLocalEnfCostId =  idOpCost; 
				}
			}
		}
		
		if ( _currentBestLocalEnfPlanOp != null && !Config.CostModel.IsValidPlanCost(bestLocalEnfCostId) )
			_currentBestLocalEnfPlanOp = null;
	}
	
	
	/**
	 * This method is called to get the best current plan in case when greedy optimization is in progress.
	 */
	public Plan getCurrentBestGreedyPlan(int currentTraversal) {
		if (isTraversed(currentTraversal))
			return bestPlanObj;
		setTraversal(currentTraversal);
		
		if (Config.CostModel.IsNull(_idCurrentBestPlanCost) || !Config.CostModel.IsValidPlanCost(_idCurrentBestPlanCost)) { 
			bestPlanObj = null;
			return bestPlanObj;
		}
		
		bestPlanObj = _currentBestPlanOp.getCurrentBestGreedyPlan(currentTraversal);
		if (this.isMarked() == true)
			bestPlanObj.setMaterialization(true);
	
		return bestPlanObj;
		
	}

	public boolean isOptimized() {
		return _isOptimized;
	}
	
	public void setOptimized(boolean flag) {
		_isOptimized = flag;
	}
	
	/**
	 * This method prints 
	 */
	public void printCurrentBestGreedyPlan(int indentLevel) {
		if (Config.CostModel.IsNull(_idCurrentBestPlanCost))
			return;
		if (_currentBestPlanOp == null)
			return;
		_currentBestPlanOp.printGreedyPlan(indentLevel);
	}
	
	/**
	 * This method only maintain the list of the _algParent and _enfParent. In future this method can be merged with addParent method
	 */
	public void addOnlyParent(PhysicalOperatorNode parent , boolean isAlgOperator ) {
		if (isAlgOperator) 
			_algParents.add(parent);
		else 
			_enfParents.add(parent);

	}
	
	//----------
	
	public void addParent(PhysicalOperatorNode parent, int idCostUBFromParent, boolean isAlgOperator) {

		if (isAlgOperator) {
			_algParents.add(parent);

			if (_algParents.size() == 1 || Config.CostModel.IsLessThan(_idCostUBFromAlgParents, idCostUBFromParent)) {
				_idCostUBFromAlgParents = idCostUBFromParent;

				if (_enfOperators != null) {
					for (int i = 0; i < _enfOperators.size(); i++)
						_enfOperators.get(i).updateCostUB();
				}
			}

		}
		else {
			_enfParents.add(parent);
		}
		if (_enfParents.size() + _algParents.size() == 1
				|| Config.CostModel.IsLessThan(_idCostUBFromParent, idCostUBFromParent)) {
			_idCostUBFromParent = idCostUBFromParent;

			if (_algOperators != null) {
				for (int i = 0; i < _algOperators.size(); i++)
					_algOperators.get(i).updateCostUB();
			}
		}
	}

	private void applyPartitionEnforcerRules(IPartitionEnforcerRule[] rules) {
		if (_enfOperators == null)
		_enfOperators = new ArrayList<PhysicalOperatorNode>();
		
		//not need as _idCostLB already updated during localEnf application
		if (Config.isPruningEnabled == true)
			_idCostLB = _idAlgCostLB;

		if (_physicalProperties == null) {
			return;
		}

		for (int ruleIndex = 0; ruleIndex < rules.length; ruleIndex++) {

			final Pair<Object, Object> outputOperator = rules[ruleIndex].getOutputOperator(_physicalProperties);

			if (outputOperator == null)
				continue;
			
			final Object outputLogOp = outputOperator.getLeft();
			final Object outputPhyOp = outputOperator.getRight();
			rules[ruleIndex].putPair(outputOperator);

			final Object logicalProperties = _group.getLogicalProperties();

			final ArrayList<Object[]> childrenReqdPropertiesArray = Config.OpHelper.getChildReqdProperties(outputPhyOp,
					 outputLogOp, logicalProperties, null, _physicalProperties);

			for (Object[] childrenReqdProperties : childrenReqdPropertiesArray) {
				final PhysicalEquivalenceNode[] children = new PhysicalEquivalenceNode[] { _group
						.getPhysicalEqNode(childrenReqdProperties[0]) };
	
				final PhysicalOperatorNode phyOpNode = new PhysicalOperatorNode(false, outputPhyOp,outputLogOp, this, children, childrenReqdProperties);
				_enfOperators.add(phyOpNode);
	
				final int idOpCostLB = phyOpNode.getCostLBId();
				
				if (Config.isPruningEnabled == true ) {
					if (Config.CostModel.IsGreaterThan(_idCostLB, idOpCostLB))
						_idCostLB = idOpCostLB;
				}
	
				final int idOpCurrentBestPlanCost = phyOpNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsGreaterThan(_idCurrentBestPlanCost, idOpCurrentBestPlanCost)) {
					_idCurrentBestPlanCost = idOpCurrentBestPlanCost;
					_currentBestPlanOp = phyOpNode;
	
					if (_isRoot) {
						NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode.NumInstances;
						NumOpNodesDuringLastUpdate = PhysicalOperatorNode.NumInstances;
					}
				}
			}
			Config.OpHelper.putPhysicalPropertiesArray(childrenReqdPropertiesArray);
		}

		if (Config.PropagateAll) {
			// Propagate cost LB.
			for (int i = 0; i < _algParents.size(); i++) {
				_algParents.get(i).updateCostLB();
			}

		}
	}

	// old method
	
	private void applyLocalEnforcerRules(ILocalEnforcerRule[] rules) {

		if (_physicalProperties == null) {
			return;
		}

		for (int ruleIndex = 0; ruleIndex < rules.length; ruleIndex++) {

			final Pair<Object, Object> outputOperator = rules[ruleIndex].getOutputOperator(_physicalProperties);

			if (outputOperator == null)
				continue;

			final Object outputLogOp = outputOperator.getLeft();
			final Object outputPhyOp = outputOperator.getRight();		
			rules[ruleIndex].putPair(outputOperator);
			
			final Object logicalProperties = _group.getLogicalProperties();
			final ArrayList<Object[]> childrenReqdPropertiesArray = Config.OpHelper.getChildReqdProperties(outputPhyOp,
					outputLogOp, logicalProperties, null, _physicalProperties);

			for (Object[] childrenReqdProperties : childrenReqdPropertiesArray) {
				final PhysicalEquivalenceNode[] children = new PhysicalEquivalenceNode[] { _group
						.getPhysicalEqNode(childrenReqdProperties[0]) };
	
				final PhysicalOperatorNode phyOpNode = new PhysicalOperatorNode(true, outputPhyOp, outputLogOp, this, children, childrenReqdProperties);
				// changed as localEnf was being added in the _agl
				_algOperators.add(phyOpNode);
				phyOpNode.setLocalEnforcer(true);
				//_enfOperators.add(phyOpNode);
	
				final int idOpCostLB = phyOpNode.getCostLBId();
				if (Config.CostModel.IsGreaterThan(_idAlgCostLB, idOpCostLB))
					_idAlgCostLB = idOpCostLB;
	
				final int idOpCurrentBestPlanCost = phyOpNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsGreaterThan(_idCurrentBestPlanCost, idOpCurrentBestPlanCost)) {
					_idCurrentBestPlanCost = idOpCurrentBestPlanCost;
					_currentBestPlanOp = phyOpNode;
	
					if (_isRoot) {
						NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode.NumInstances;
						NumOpNodesDuringLastUpdate = PhysicalOperatorNode.NumInstances;
					}
				}
			}
			
			Config.OpHelper.putPhysicalPropertiesArray(childrenReqdPropertiesArray);
		}

		if (Config.PropagateAll) {
			// Propagate cost LB.
			for (int i = 0; i < _enfParents.size(); i++) {
				_enfParents.get(i).updateCostLB();
			}

		}
	}
	
	
	/*
	// added new to add local enforcer to the enforcer list
	private void applyLocalEnforcerRules(ILocalEnforcerRule[] rules) {

		if (_physicalProperties == null) {
			return;
		}
		if (_enfOperators == null)
			_enfOperators = new ArrayList<PhysicalOperatorNode>();
		if (Config.isPruningEnabled == true )
		_idCostLB = _idAlgCostLB;
		
		for (int ruleIndex = 0; ruleIndex < rules.length; ruleIndex++) {

			final Pair<Object, Object> outputOperator = rules[ruleIndex].getOutputOperator(_physicalProperties);

			if (outputOperator == null)
				continue;

			final Object outputLogOp = outputOperator.getLeft();
			final Object outputPhyOp = outputOperator.getRight();		
			rules[ruleIndex].putPair(outputOperator);
			
			final Object logicalProperties = _group.getLogicalProperties();
			final ArrayList<Object[]> childrenReqdPropertiesArray = Config.OpHelper.getChildReqdProperties(outputPhyOp,
					outputLogOp, logicalProperties, null, _physicalProperties);

			for (Object[] childrenReqdProperties : childrenReqdPropertiesArray) {
				final PhysicalEquivalenceNode[] children = new PhysicalEquivalenceNode[] { _group
						.getPhysicalEqNode(childrenReqdProperties[0]) };
	
				final PhysicalOperatorNode phyOpNode = new PhysicalOperatorNode(false, outputPhyOp, outputLogOp, this, children, childrenReqdProperties);
				// changed as localEnf was being added in the _agl
				//_algOperators.add(phyOpNode);
				_enfOperators.add(phyOpNode);
				
	
				final int idOpCostLB = phyOpNode.getCostLBId();
				
				if (Config.isPruningEnabled == true) {
					if (Config.CostModel.IsGreaterThan(_idCostLB, idOpCostLB))
						_idCostLB = idOpCostLB;
				}
	
				final int idOpCurrentBestPlanCost = phyOpNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsGreaterThan(_idCurrentBestPlanCost, idOpCurrentBestPlanCost)) {
					_idCurrentBestPlanCost = idOpCurrentBestPlanCost;
					_currentBestPlanOp = phyOpNode;
	
					if (_isRoot) {
						NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode.NumInstances;
						NumOpNodesDuringLastUpdate = PhysicalOperatorNode.NumInstances;
					}
				}
			}
			
			Config.OpHelper.putPhysicalPropertiesArray(childrenReqdPropertiesArray);
		}

		if (Config.PropagateAll) {
			// Propagate cost LB.
			for (int i = 0; i < _enfParents.size(); i++) {
				_algParents.get(i).updateCostLB();
			}

		}
	}
	*/
	
	/**
	 * This method apply all set of rules passed on all the logical expressions present in logicalEq node.
	 * @param rules
	 */
	private void applyImplementationRules(IImplementationRule[] rules) {

		if(_isImplementationRulesApplied)
			return;

		_algOperators = new ArrayList<PhysicalOperatorNode>();
		_idAlgCostLB = Config.CostModel.MaxCostId();

		final int numLogicalExpressions = _group.getNumLogExpr();
		
		// process all logical expression 
		for (int logExprIndex = 0; logExprIndex < numLogicalExpressions; logExprIndex++) {

			final LogicalOperatorNode expr = _group.getLogicalExpression(logExprIndex);
			if (expr == null)
				continue;

			// apply all implementation rules
			for (int implementationRuleIndex = 0; implementationRuleIndex < rules.length; implementationRuleIndex++) {

				// check if rule is for the root operator of the expression 
				if (!rules[implementationRuleIndex].isApplicable(expr.getOperator()))
					continue;

				final Object logicalOperator = expr.getOperator();
				final Object logicalProperties = _group.getLogicalProperties();
				final Object[] childrenLogicalProperties = expr.getChildrenProps();
				final Object outputOperator = rules[implementationRuleIndex].getOutputOperator(logicalOperator,
						logicalProperties, childrenLogicalProperties, _physicalProperties);

				//System.out.println("COMPUTEPHY");
				//Config.OpHelper.printLogicalOperator(System.out, logicalOperator, logicalProperties);

				if (outputOperator == null) {
					Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);
					continue;
				}

				// has no more child
				if (expr.getNumChildren() == 0) {
					Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);

					final PhysicalOperatorNode phyOpNode = new PhysicalOperatorNode(true, outputOperator, logicalOperator, this, null, null);
					_algOperators.add(phyOpNode);
					
					// update algorithmic lower bound
					final int idOpCostLB = phyOpNode.getCostLBId();
					if (Config.CostModel.IsGreaterThan(_idAlgCostLB, idOpCostLB))
						_idAlgCostLB = idOpCostLB;
	
					// update currentBestPlan for this operator
					final int idOpCurrentBestPlanCost = phyOpNode.getCurrentBestPlanCostId();
					if (Config.CostModel.IsGreaterThan(_idCurrentBestPlanCost, idOpCurrentBestPlanCost)) {
						_idCurrentBestPlanCost = idOpCurrentBestPlanCost;
						_currentBestPlanOp = phyOpNode;
	
						if (_isRoot) {
							NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode.NumInstances;
							NumOpNodesDuringLastUpdate = PhysicalOperatorNode.NumInstances;
						}
					}					
				}
				
				else {
					// expr have children
					
					// calculate the children required property
					final ArrayList<Object[]> childrenReqdPropertiesArray = Config.OpHelper.getChildReqdProperties(outputOperator,
							logicalOperator, logicalProperties, childrenLogicalProperties, _physicalProperties);
	
					Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);
					
					for (Object[] childrenReqdProperties : childrenReqdPropertiesArray)
					{
						// get all child PhysicalEquivalence Node
						final PhysicalEquivalenceNode[] children = computePhyEqNodes(expr.getChildren(), childrenReqdProperties);
						final PhysicalOperatorNode phyOpNode = new PhysicalOperatorNode(true, outputOperator, logicalOperator, this, children, childrenReqdProperties);
						_algOperators.add(phyOpNode);
						
						// update algLowerBound
						final int idOpCostLB = phyOpNode.getCostLBId();
						
						// changed to disable pruning
						if (Config.isPruningEnabled == true) {
							if (Config.CostModel.IsGreaterThan(_idAlgCostLB, idOpCostLB))
							_idAlgCostLB = idOpCostLB;
						}
						
						// update currentBestPlan
						final int idOpCurrentBestPlanCost = phyOpNode.getCurrentBestPlanCostId();
						if (Config.CostModel.IsGreaterThan(_idCurrentBestPlanCost, idOpCurrentBestPlanCost)) {
							_idCurrentBestPlanCost = idOpCurrentBestPlanCost;
							_currentBestPlanOp = phyOpNode;
		
							if (_isRoot) {
								NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode.NumInstances;
								NumOpNodesDuringLastUpdate = PhysicalOperatorNode.NumInstances;
							}
						}
					}
					Config.OpHelper.putPhysicalPropertiesArray(childrenReqdPropertiesArray);
				}
			}
		}

		if (Config.PropagateAll) {
			for (int i = 0; i < _enfParents.size(); i++) {
				_enfParents.get(i).updateCostLB();
			}
		}

		_isImplementationRulesApplied = true;
	}

	private PhysicalEquivalenceNode[] computePhyEqNodes(LogicalEquivalenceNode[] groups, Object[] reqdProperties) {
		if (groups == null || groups.length == 0)
			return null;

		final PhysicalEquivalenceNode[] children = new PhysicalEquivalenceNode[groups.length];
		for (int i = 0; i < groups.length; i++)
			children[i] = groups[i].getPhysicalEqNode(reqdProperties[i]);

		return children;
	}

	/**
	 * This method apply the all the rules passed in rule set on this physicalEqNode and 
	 * generate all the possible physical plans 
	 * @param onlyAlgPlan : true if only generate algorithmic plan
	 * @param rules : set of implementation rule to be applied
	 */
	public void expand(boolean onlyAlgPlan, Rules rules) {
		
		// check if already expanded   
		if (_algOperators != null && _enfOperators != null)
			return;

		if (onlyAlgPlan && _algOperators != null && Config.isPruningEnabled == true)
			return;
		
		// generate algorithmic plan
		if (_algOperators == null) {
			applyImplementationRules(rules.getImplementationRules());
			applyLocalEnforcerRules(rules.getLocalEnforcerRules());
		}

		// generate enforcer plans
		if (!onlyAlgPlan && _enfOperators == null || (_enfOperators == null && Config.isPruningEnabled == false)) {
			//applyLocalEnforcerRules(rules.getLocalEnforcerRules());
			applyPartitionEnforcerRules(rules.getPartitionEnforcerRules());
		}

		if (Config.CostModel.IsValidPlanCost(_idCurrentBestPlanCost)) {
			if (Config.PropagateAll) {

				// Update best plan cost of parent.
				if (_algParents != null) {
					for (int i = 0; i < _algParents.size(); i++)
						_algParents.get(i).updateCurrentBestPlanCost();
				}
				if (_enfParents != null) {
					for (int i = 0; i < _enfParents.size(); i++)
						_enfParents.get(i).updateCurrentBestPlanCost();
				}

				// update child cost UB
				if (_algOperators != null && Config.CostModel.IsLessThan(_idCurrentBestPlanCost, _idCostUBFromParent)) {
					for (int i = 0; i < _algOperators.size(); i++)
						_algOperators.get(i).updateCostUB();
				}

				if (_enfOperators != null
						&& Config.CostModel.IsLessThan(_idCurrentBestPlanCost, _idCostUBFromAlgParents)) {
					for (int i = 0; i < _enfOperators.size(); i++)
						_enfOperators.get(i).updateCostUB();
				}
			}

		}

		if (Config.PrintPhysicalNodeCreation)
			printLong();
	}

	public PhysicalOperatorNode getChild(int index) {

		if (index < _algOperators.size())
			return _algOperators.get(index);
		index -= _algOperators.size();

		return _enfOperators.get(index);
	}

	public int getCostLBId(boolean onlyAlgPlan) {
		if (onlyAlgPlan && Config.isPruningEnabled == true)
			return _idAlgCostLB;
		else
			return _idCostLB;
	}

	public int getCostUBId(boolean forAlgOperator) {

		final int idCostUBFromParent = (forAlgOperator) ? _idCostUBFromParent : _idCostUBFromAlgParents;

		if (Config.CostModel.IsLessThan(idCostUBFromParent, _idCurrentBestPlanCost))
			return idCostUBFromParent;
		else
			return _idCurrentBestPlanCost;

	}

	public PhysicalOperatorNode getCurrentBestPlanOperator() {
		return _currentBestPlanOp;
	}

	public int getCurrentBestPlanCostId() {
		return _idCurrentBestPlanCost;
	}

	public int getId() {
		return _id;
	}

	public LogicalEquivalenceNode getLogicalEqNode() {
		return _group;
	}

	public Object getLogicalProperties() {
		return _group.getLogicalProperties();
	}

	public int getNumberOfChildren(boolean onlyAlgOperators) {
		int numOperators = 0;
		if (_algOperators != null)
			numOperators += _algOperators.size();

		if (!onlyAlgOperators && _enfOperators != null)
			numOperators += _enfOperators.size();

		return numOperators;
	}

	public Object getPhysicalProperties() {
		return _physicalProperties;
	}

	public boolean isEnfPlanActive() {
		return _isEnfPlanActive;
	}

	/**
	 * This method is used to print all the plans present in this Physical Equivalence node
	 */
	public void print() {
		System.out.println();
		printShort();
		System.out.println();
		System.out.println("=================================================================================");
		if (Config.getCreateSubAndSuperList() == true) {
			printSuperList();
			printSubList();
		}
		System.out.print("Best plan ");
		if (!Config.CostModel.IsNull(_idCurrentBestPlanCost)) {
			if (Config.isPruningEnabled == true)
				printCurrentBestPlan(1);
			else 
				printCurrentBestGreedyPlan(1);
		}
		System.out.println();
		
		if (_currentBestAlgPlanOp != null) 
			System.out.println("Cost of current Best Alg Plan  :" + Config.CostModel.toString( _currentBestAlgPlanOp.getCurrentBestPlanCostId() ));
		if (_currentBestLocalEnfPlanOp != null)
			System.out.println("Cost of current Best Enf Plan  :" + Config.CostModel.toString( _currentBestLocalEnfPlanOp.getCurrentBestPlanCostId() ));
		if (_currentBestPartEnfPlanOp != null)
			System.out.println("Cost of current Best Enf Plan  :" + Config.CostModel.toString( _currentBestPartEnfPlanOp.getCurrentBestPlanCostId() ));
		
		
		System.out.println("Operators");
		final int numOperators = getNumberOfChildren(false);
		for (int i = 0; i < numOperators; i++) {
			if (Config.PrintPhyiscalDetailedMemo == true)
				getChild(i).printPlan(1);
			else 
				getChild(i).print();
			System.out.println();
		}
	}

	/**
	 *  this method returns the plan object corresponding to the best current Plan. 
	 *  It compares all the enforcer and algorithmic plan with the _idCurrentBestPlanCost and
	 *  then on successful match returns the best plan. So if _idCurrentBestPlanCost is not updated
	 *  then this method will give the wrong plan object    
	 */
	public Plan getCurrentBestPlan() {
		if (Config.CostModel.IsNull(_idCurrentBestPlanCost))
			return null;

		// if best plan is cached and it represents the plan 
		// for same cost as current best plan cost then
		// return stored plan. This avoids creating duplicate
		// plans. Duplicate plans were created because a node
		// may be accessed more than once in a DAG. This 
		// creates some issues in new implementation since we
		// transfer some additional information through plans
		// which requires us to ensure that there are not
		// more than one best plan for same physical 
		// equivalence node unless the best plan cost has been
		// updated. The check on stored cost ensures that new 
		// plan is computed if the plan cost has changed
		
		//TODO : it might be cost is same but the id's are different, handle that case 
		//if(cachedBestPlan != null && cachedBestPlan.getLeft() == _idCurrentBestPlanCost)
			//return cachedBestPlan.getRight();

		//if(cachedBestPlan != null && Config.CostModel.IsEqual(cachedBestPlan.getLeft(), _idCurrentBestPlanCost))
			//return cachedBestPlan.getRight();

		
		PhysicalOperatorNode currentBestPlan = null;

		/*Config.OpHelper.printPhysicalProperties(System.out, _physicalProperties);
		System.out.println("\n");
		System.out.println(Config.CostModel.toString(_idCurrentBestPlanCost));*/

		if (_algOperators != null) {
			for (int i = 0; i < _algOperators.size(); i++) {
				final PhysicalOperatorNode opNode = _algOperators.get(i);
				final int idOpCost = opNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsEqual(idOpCost, _idCurrentBestPlanCost)) {
					currentBestPlan = opNode;
					break;
				}
			}
		}
		if (currentBestPlan == null && _enfOperators != null) {
			for (int i = 0; i < _enfOperators.size(); i++) {
				final PhysicalOperatorNode opNode = _enfOperators.get(i);
				final int idOpCost = opNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsNull(idOpCost))
					continue;
				if (Config.CostModel.IsEqual(idOpCost, _idCurrentBestPlanCost)) {
					currentBestPlan = opNode;
					break;
				}
			}

		}
		Plan bestplan = currentBestPlan.getCurrentBestPlan();

		if(materialize)
			bestplan.setMaterialization(true);

		cachedBestPlan = Pair.of(_idCurrentBestPlanCost, bestplan);
		return cachedBestPlan.getRight();
	}
		
	
	public void printCurrentBestPlan(int indentLevel) {
		if (Config.CostModel.IsNull(_idCurrentBestPlanCost) || Config.CostModel.IsGreaterThanOrEqual(_idCurrentBestPlanCost, Config.CostModel.MaxCostId()))
			return;

		PhysicalOperatorNode currentBestPlan = null;

		/*Config.OpHelper.printPhysicalProperties(System.out, _physicalProperties);
		System.out.println("\n");
		System.out.println(Config.CostModel.toString(_idCurrentBestPlanCost));*/

		if (_algOperators != null) {
			for (int i = 0; i < _algOperators.size(); i++) {
				final PhysicalOperatorNode opNode = _algOperators.get(i);
				final int idOpCost = opNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsEqual(idOpCost, _idCurrentBestPlanCost)) {
					currentBestPlan = opNode;
					break;
				}
			}
		}
		if (currentBestPlan == null && _enfOperators != null) {
			for (int i = 0; i < _enfOperators.size(); i++) {
				final PhysicalOperatorNode opNode = _enfOperators.get(i);
				final int idOpCost = opNode.getCurrentBestPlanCostId();
				if (Config.CostModel.IsNull(idOpCost))
					continue;
				if (Config.CostModel.IsEqual(idOpCost, _idCurrentBestPlanCost)) {
					currentBestPlan = opNode;
					break;
				}
			}

		}
		if(currentBestPlan == null)
			return;
		currentBestPlan.printPlan(indentLevel);
	}

	
	
	
	public void printId() {
		System.out.print(_group.getId() + "." + _id);
	}

	public int getGroupId()
	{
		return _group.getId();
	}


	/**
	 * Print all the operators contained with in a physicalEq node 
	 */
	private void printOperatorNode() {
		if (_algOperators != null ) {
			for (PhysicalOperatorNode phyOpNode : _algOperators) {
				System.out.print("\t\t");
				phyOpNode.printLong();
			}
		}
		if (_enfOperators != null ) {
			for (PhysicalOperatorNode phyOpNode : _enfOperators) {
				System.out.print("\t\t");
				phyOpNode.printLong();
			}
		}
	}
	
	/** This method print the superList associated with this Physical Equivalence node */
	public void printSuperList() {
		System.out.print(" SuperList : " );
		int count = 1;
		System.out.print(" [[ ");
		for ( PhysicalEquivalenceNode nextEq : getSuperList()) {
			System.out.print(count + " : <id: ");
			nextEq.printId();
			System.out.print(", prop: ");
			Config.OpHelper.printPhysicalProperties(System.out, nextEq.getPhysicalProperties());
			System.out.print( " > ");
			count++;
		}
		System.out.print(" ]] ");
		System.out.println();	
	}
	
	/** This method print the subList associated with this Physical Equivalence node */
	public void printSubList() {
		System.out.print(" SubList : " );
		int count = 1;
		System.out.print(" [[ ");
		for ( PhysicalEquivalenceNode nextEq : getSubList()) {
			System.out.print(count + " : <id: ");
			nextEq.printId();
			System.out.print(", prop: ");
			Config.OpHelper.printPhysicalProperties(System.out, nextEq.getPhysicalProperties());
			System.out.print( " > ");
			count++;
		}
		System.out.print(" ]] ");
		System.out.println();
	}
	
	
	public void printLong() {
		System.out.println("PRINT PHY EQ NODE:");
		printId();
		System.out.print(":PhyEqNode, properties = <");
		Config.OpHelper.printPhysicalProperties(System.out, _physicalProperties);
		System.out.println(">");
		System.out.println("\t\t\t\t _costLB = " + Config.CostModel.toString(_idCostLB) + ", _algCostLB = "
				+ Config.CostModel.toString(_idAlgCostLB));
		System.out.println("\t\t\t\t _costUBFromParent = " + Config.CostModel.toString(_idCostUBFromParent)
				+ ", _costUBFromAlgParents = " + Config.CostModel.toString(_idCostUBFromAlgParents));
		System.out.println("\t\t\t\t _currentBestPlanCost = " + Config.CostModel.toString(_idCurrentBestPlanCost));
		if (Config.PrintSuperList == true) {
			printSuperList();
		}
		if (Config.PrintDetailedPhyEq == true)
			printOperatorNode();
	}

	public void printShort() {
		printId();
		System.out.print(":PhyEqNode, properties = <");
		Config.OpHelper.printPhysicalProperties(System.out, _physicalProperties);
		System.out.print(">");
	}

	public void resetEnfPlanActive() {
		_isEnfPlanActive = false;
	}

	public void setEnfPlanActive() {
		_isEnfPlanActive = true;
	}

	public void updateCostLB(int idCostLB, boolean algOperator) {

		if (algOperator) {
			// update algorithm cost lb.
			if (idCostLB == _idAlgCostLB) {

				for (int i = 0; i < _algOperators.size(); i++) {
					final int idOpCostLB = _algOperators.get(i).getCostLBId();
					if (Config.CostModel.IsGreaterThan(_idAlgCostLB, idOpCostLB))
						_idAlgCostLB = idOpCostLB;
				}

				if (Config.PropagateAll) {
					for (int i = 0; i < _enfParents.size(); i++) {
						_enfParents.get(i).updateCostLB();
					}
				}
			}
		}

		// if the costLb of current min CostLb operator has been modified,
		// recompute costLB.
		if (idCostLB == _idCostLB) {
			_idCostLB = _idAlgCostLB;
			for (int i = 0; i < _enfOperators.size(); i++) {
				final int idOpCostLB = _enfOperators.get(i).getCostLBId();
				if (Config.CostModel.IsGreaterThan(_idCostLB, idOpCostLB))
					_idCostLB = idOpCostLB;
			}
			if (Config.PropagateAll) {
				for (int i = 0; i < _algParents.size(); i++) {
					_algParents.get(i).updateCostLB();
				}
			}
		}
	}

	/**
	 * Called when _childCostUB has been modified (reduced or increased) for an
	 * existing parent.
	 * 
	 * @param idCostUBFromParent
	 * @param algParent
	 */
	public void updateCostUBFromParent(int idCostUBFromParent, boolean algParent) {
		if (algParent) {

			// If the childcostUB of current max childCostUB has been changed.
			if (_idCostUBFromAlgParents == idCostUBFromParent) {
				for (int i = 0; i < _algParents.size(); i++) {
					final int idCostUB = _algParents.get(i).getCostUBForChildId(_id);
					if (Config.CostModel.IsLessThan(_idCostUBFromAlgParents, idCostUB))
						_idCostUBFromAlgParents = idCostUB;
				}

				if (_enfOperators != null)
					for (int i = 0; i < _enfOperators.size(); i++)
						_enfOperators.get(i).updateCostUB();
			}

			// If the childCostUB of some other parent has been increased.
			else if (Config.CostModel.IsGreaterThan(idCostUBFromParent, _idCostUBFromAlgParents)) {
				_idCostUBFromAlgParents = idCostUBFromParent;

				if (_enfOperators != null)
					for (int i = 0; i < _enfOperators.size(); i++)
						_enfOperators.get(i).updateCostUB();

			}
		}

		// If the childcostUB of current max childCostUB has been changed.
		if (_idCostUBFromParent == idCostUBFromParent) {
			_idCostUBFromParent = _idCostUBFromAlgParents;

			for (int i = 0; i < _enfParents.size(); i++) {
				final int idCostUB = _enfParents.get(i).getCostUBForChildId(_id);
				if (Config.CostModel.IsLessThan(_idCostUBFromParent, idCostUB))
					_idCostUBFromParent = idCostUB;
			}

			if (_algOperators != null) {
				for (int i = 0; i < _algOperators.size(); i++)
					_algOperators.get(i).updateCostUB();
			}
		}

		// If the childCostUB of some other parent has been increased.
		else if (Config.CostModel.IsGreaterThan(idCostUBFromParent, _idCostUBFromParent)) {
			_idCostUBFromParent = idCostUBFromParent;

			if (_algOperators != null) {
				for (int i = 0; i < _algOperators.size(); i++)
					_algOperators.get(i).updateCostUB();
			}
		}

	}

	public void compbestplancost()
	{
		for(PhysicalOperatorNode op: _algOperators)
		{
			op.compbestplancost();

			if(Config.CostModel.IsLessThan(op.getCurrentBestPlanCostId(), _idCurrentBestPlanCost))
				Config.CostModel.Assign(_idCurrentBestPlanCost, op.getCurrentBestPlanCostId());
		}
	}
	/**
	 * This method is called by the child Physical Operator node if the cost of the child operator has been changed.
	 * 
	 * @param idOpBestPlanCost : costId of the called operator node
	 * @param phyOpNode : object of the calling operator node
	 */
	
	public void updateCurrentBestPlanCost(int idOpBestPlanCost, PhysicalOperatorNode phyOpNode) {

		// if the newChildCostId is less then the currentBestPlanId then update the currentBestPlanCostId 
		// and currentBestPlanCost operator
		if (Config.CostModel.IsLessThan(idOpBestPlanCost, _idCurrentBestPlanCost)) {
			_idCurrentBestPlanCost = idOpBestPlanCost;
			_currentBestPlanOp = phyOpNode;
		}
		else if (_idCurrentBestPlanCost != idOpBestPlanCost)
			return;

		if (_isRoot) {
			NumEqNodesDuringLastUpdate = PhysicalEquivalenceNode.NumInstances;
			NumOpNodesDuringLastUpdate = PhysicalOperatorNode.NumInstances;
		}

		if (Config.PropagateAll) {
			// Update best plan cost of parent.
			if (_algParents != null) {
				for (int i = 0; i < _algParents.size(); i++)
					_algParents.get(i).updateCurrentBestPlanCost();
			}
			if (_enfParents != null) {
				for (int i = 0; i < _enfParents.size(); i++)
					_enfParents.get(i).updateCurrentBestPlanCost();
			}

			// update child cost UB, only if costUB has been changed
			if (_algOperators != null && Config.CostModel.IsLessThan(_idCurrentBestPlanCost, _idCostUBFromParent)) {
				for (int i = 0; i < _algOperators.size(); i++)
					_algOperators.get(i).updateCostUB();
			}

			if (_enfOperators != null && Config.CostModel.IsLessThan(_idCurrentBestPlanCost, _idCostUBFromAlgParents)) {
				for (int i = 0; i < _enfOperators.size(); i++)
					_enfOperators.get(i).updateCostUB();
			}
		}
	}


	
}
