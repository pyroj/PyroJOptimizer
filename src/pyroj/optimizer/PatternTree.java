 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class PatternTree {

	public static void getBindingsNonRoot(Object patternTree,
			LogicalOperatorNode opNode,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode> bindingGroup) throws Exception {

		if (Config.TreeHelper.isRootMatches(patternTree, opNode.getOperator()))
			getBindings(patternTree, opNode,
					new ImmutablePair<Integer, LogicalOperatorNode>(
							Config.TreeHelper.getId(patternTree), opNode),
					bindings, bindingGroup);

		final Object[] children = Config.TreeHelper.getChildren(patternTree);

		if (children != null) {
			for (int i = 0; i < children.length; i++)
				getBindingsNonRoot(children[i], opNode, bindings, bindingGroup);
		}
	}

	public static void getBindings(Object patternTree,
			LogicalOperatorNode opNode,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode> bindingGroup) {

		getBindingsDesc(patternTree, opNode,
				Config.TreeHelper.getNumNodes(patternTree), null, bindings,
				bindingGroup);
	}

	protected static void getBindings(Object patternTree,
			LogicalOperatorNode opNode,
			Pair<Integer, LogicalOperatorNode> fixedBinding,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode> bindingGroup) throws Exception {

		if (Config.TreeHelper.getParent(patternTree) == null) {
			getBindingsDesc(patternTree, opNode,
					Config.TreeHelper.getNumNodes(patternTree), fixedBinding,
					bindings, bindingGroup);
			return;
		}

		if (opNode == null
				|| !Config.TreeHelper.isRootMatches(patternTree,
						opNode.getOperator()))
			return;

		final LogicalEquivalenceNode eqNode = opNode.getGroup();

		for (int i = 0; i < eqNode.getNumParents(); i++) {
			final LogicalOperatorNode parentExpr = eqNode
					.getParentExpression(i);
			if (parentExpr != null)
				getBindings(Config.TreeHelper.getParent(patternTree),
						parentExpr, fixedBinding, bindings, bindingGroup);
		}
	}

	private static void crossProductAndAssignToFirst(
			ArrayList<Pair<Object, Object>[]> left,
			ArrayList<Pair<Object, Object>[]> right) {

		final int numLeftBnd = left.size();
		// Multiple remaining right bnd.
		for (int i = 1; i < right.size(); i++) {
			final Pair<Object, Object>[] rightBnd = right.get(i);

			for (int j = 0; j < numLeftBnd; j++) {
				final Pair<Object, Object>[] leftBnd = left.get(j);

				final Pair<Object, Object>[] resultBnd = MemoryManager
						.getTempBinding(leftBnd.length);
				crossProductAndAssignToFirst(resultBnd, leftBnd);
				crossProductAndAssignToFirst(resultBnd, rightBnd);

				left.add(resultBnd);
			}
		}

		// Multiply first element of rightBnd with all left
		final Pair<Object, Object>[] firstRightBnd = right.get(0);
		for (int k = 0; k < numLeftBnd; k++) {
			crossProductAndAssignToFirst(left.get(k), firstRightBnd);
		}
	}

	private static void crossProductAndAssignToFirst(
			Pair<Object, Object>[] first, Pair<Object, Object>[] second) {

		if (first.length != second.length)
			throw new IllegalArgumentException("Binding length must be same in left and right");

		for (int i = 0; i < first.length; i++) {
			if (second[i] == null)
				continue;
			if (first[i] == null)
				first[i] = MemoryManager.getTempBindingPair(
						second[i].getLeft(), second[i].getRight());
			else if (second[i] != null && first[i] != second[i])
				throw new IllegalArgumentException("Different binding in left and right array");
		}
	}

	private static void getBindingsDesc(Object patternTree,
			LogicalOperatorNode opNode, int numElements,
			Pair<Integer, LogicalOperatorNode> fixedBinding,
			ArrayList<Pair<Object, Object>[]> bindings,
			ArrayList<LogicalEquivalenceNode> bindingGroup) {

		if (opNode == null
				|| !Config.TreeHelper.isRootMatches(patternTree,
						opNode.getOperator()))
			return;

		final int id = Config.TreeHelper.getId(patternTree);

		// if we have fixed binding for this
		if (fixedBinding != null && id == fixedBinding.getKey()
				&& opNode != fixedBinding.getValue())
			return;

		final Object[] children = Config.TreeHelper.getChildren(patternTree);
		if (children == null) {
			final Pair<Object, Object>[] binding = MemoryManager
					.getTempBinding(numElements);
			binding[id] = MemoryManager.getTempBindingPair(
					opNode.getOperator(), opNode.getGroup()
							.getLogicalProperties());
			if (bindingGroup != null)
				bindingGroup.add(opNode.getGroup());
			bindings.add(binding);
			return;
		}
		else if(opNode.getNumChildren() != children.length)
			return;
		else {

			final ArrayList<Pair<Object, Object>[]> allBindings = MemoryManager
					.getTempBindingArray();

			for (int i = 0; i < children.length; i++) {

				if (children[i] == null)
					continue;

				final ArrayList<Pair<Object, Object>[]> childResult = MemoryManager
						.getTempBindingArray();
				final LogicalEquivalenceNode childEq = opNode.getChild(i);

				if (!Config.TreeHelper.shouldRootBeLogicalOperator(children[i])) {
					final Pair<Object, Object>[] binding = MemoryManager
							.getTempBinding(numElements);
					binding[Config.TreeHelper.getId(children[i])] = MemoryManager
							.getTempBindingPair(childEq,
									childEq.getLogicalProperties());
					childResult.add(binding);
				} else {
					for (int exprIndex = 0; exprIndex < childEq.getNumLogExpr(); exprIndex++) {
						final LogicalOperatorNode childExpr = childEq
								.getLogicalExpression(exprIndex);
						if (childExpr == null)
							continue;
						getBindingsDesc(children[i], childExpr, numElements,
								fixedBinding, childResult, null);
					}
				}

				if (childResult == null || childResult.size() == 0) {
					MemoryManager.putTempBindingArrayRecurse(childResult);
					MemoryManager.putTempBindingArrayRecurse(allBindings);
					return;
				}
				if (allBindings.size() == 0) {
					allBindings.addAll(childResult);
					childResult.clear();
				} else {
					crossProductAndAssignToFirst(allBindings, childResult);
				}

				MemoryManager.putTempBindingArrayRecurse(childResult);

			}
			for (int i = 0; i < allBindings.size(); i++) {
				allBindings.get(i)[id] = MemoryManager.getTempBindingPair(
						opNode.getOperator(), opNode.getGroup()
								.getLogicalProperties());
				if (bindingGroup != null)
					bindingGroup.add(opNode.getGroup());
			}

			bindings.addAll(allBindings);
			allBindings.clear();
			MemoryManager.putTempBindingArrayRecurse(allBindings);
		}

	}
}
