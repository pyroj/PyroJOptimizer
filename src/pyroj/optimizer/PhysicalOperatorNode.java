 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import pyroj.costmodel.ICostModel;
import pyroj.runtime.Plan;

public class PhysicalOperatorNode {
	public static int NumInstances;

	private final int _id;

	private final boolean _isAlgOrLocalEnfOperator;
	private final Object _rootOperator;
	private final Object _logicalOperator;
	private final PhysicalEquivalenceNode _parent;
	private PhysicalEquivalenceNode[] _children;
	
	// Added for Greedy --- 
	private PhysicalEquivalenceNode[] _actualChildren =null ;
	
	private boolean _isLocalEnf = false;
	//---

	private final int _idLocalCost = Config.CostModel.AllocZeroCostObj();

	private final int _idCostLB;

	private final int _idCurrentBestPlanCost;

	private final int[] _idChildrenCostUB;

	public PhysicalOperatorNode(
				boolean isAlgOrLocalEnfOperator, 
				Object rootOperator,
				Object logicalOperator,
				PhysicalEquivalenceNode parent,
				PhysicalEquivalenceNode[] children,
				Object[] childrenPhysicalProperties) {
		_id = NumInstances++;
		_isAlgOrLocalEnfOperator = isAlgOrLocalEnfOperator;
		_rootOperator = rootOperator;
		_logicalOperator = logicalOperator;
		_parent = parent;
		_children = children;

		computeLocalCost(childrenPhysicalProperties);

		if (_children == null) {
			_idChildrenCostUB = null;
			_idCostLB = _idLocalCost;
			_idCurrentBestPlanCost = _idLocalCost;
		}
		else {
			_idCostLB = Config.CostModel.Clone(_idLocalCost);
			_idCurrentBestPlanCost = Config.CostModel.AllocMaxCostObj();

			_idChildrenCostUB = new int[_children.length];
			for (int i = 0; i < _children.length; i++)
				_idChildrenCostUB[i] = Config.CostModel.AllocZeroCostObj();

			if (Config.PropagateAll == false && Config.BuildParentList == true) {
				for (int i = 0 ; i < _children.length ; i++ ) { 
					_children[i].addOnlyParent(this,_isAlgOrLocalEnfOperator);
				}
			}
			
			if (Config.PropagateAll) {
				computeCostLB();
				computeCurrentBestPlanCost();
				computeCostUBForChild();

				for (int i = 0; i < _children.length; i++)
					// AddParent will propagate required fields to children
					_children[i].addParent(this, _idChildrenCostUB[i], _isAlgOrLocalEnfOperator);
			}
		}
	}

	// Added for Greedy --
	
	
	public void setLocalEnforcer(boolean flag) {
		this._isLocalEnf = true;
	}

	public boolean isLocalEnf() {
		return _isLocalEnf;
	}

	public void setChild(int i, PhysicalEquivalenceNode child)
	{
		if(_children == null)
			_children = new PhysicalEquivalenceNode[1];

		_children[i] = child;
	}
	
	public void setActualChild(int i, PhysicalEquivalenceNode child) {
		if (_actualChildren == null) 
			_actualChildren = new PhysicalEquivalenceNode[_children.length];
		_actualChildren[i] = child;
	}
	
	public PhysicalEquivalenceNode getActualChild(int i) {
		if ( _actualChildren ==  null || _actualChildren.length == 0 ) 
			return null;
		return _actualChildren[i];
	}
	
	/**
	 * This method is called to get the current best plan rooted at this physical Operator Node.
	 * 
	 */
	public Plan getCurrentBestGreedyPlan(int currentTraversal) {
		if (_children == null) 
				return new Plan (
						_logicalOperator,
						_rootOperator,
						_parent.getLogicalProperties(),
						_parent.getPhysicalProperties(),
						Config.CostModel.toString(_idLocalCost),
						Config.CostModel.toString(_idCurrentBestPlanCost),
						null,
						this	
						);
		Plan[] childrenBestPlans = new Plan[_children.length];
		for (int i = 0 ; i < _children.length ; i++) {
			if (_actualChildren != null && _actualChildren[i] != null) {
				// assumed that the _actual children is maintained onlyu in case of the greddy optimization
				childrenBestPlans[i] = _actualChildren[i].getCurrentBestGreedyPlan(currentTraversal);
			} else {
				childrenBestPlans[i] = _children[i].getCurrentBestGreedyPlan(currentTraversal);
			}
		}
		
		return new Plan(
				_logicalOperator,
				_rootOperator,
				_parent.getLogicalProperties(),
				_parent.getPhysicalProperties(),
				Config.CostModel.toString(_idLocalCost),
				Config.CostModel.toString(_idCurrentBestPlanCost),
				childrenBestPlans,
				this
				);
	}
	
	public void printGreedyPlan(int indentLevel){
		System.out.println();
		for (int i = 0; i < indentLevel; i++)
			System.out.print("| ");
		print();		
		if (_children == null) 
			return;
		for (int i = 0; i < _children.length; i++) {
			if (_actualChildren != null && _actualChildren[i] != null) {
			 _actualChildren[i].printCurrentBestGreedyPlan(indentLevel + 1);
			} else {
				_children[i].printCurrentBestGreedyPlan(indentLevel + 1);
			}
		}
	}
	

	
	// ---
	
	public int getId() {
		return _id;
	}

	public Object getOperator() {
		return _rootOperator;
	}
	
	public Object getLogicalOperator() {
		return _logicalOperator;
	}

	public boolean isAlgOrLocalEnfOperator() {
		return _isAlgOrLocalEnfOperator;
	}

	public boolean isEnfOperator() {
		return !_isAlgOrLocalEnfOperator;
	}

	public int getLocalCostID() {
		return _idLocalCost;
	}

	public PhysicalEquivalenceNode getParent() {
		return _parent;
	}

	public PhysicalEquivalenceNode[] getChildren() {
		return _children;
	}

	public int getCostLBId() {
		return _idCostLB;
	}

	public boolean computeCostLB() {
		final int[] idChildCostLB = MemoryManager.getCostArray(_children.length);

		for (int i = 0; i < _children.length; i++)
			idChildCostLB[i] = _children[i].getCostLBId(!_isAlgOrLocalEnfOperator);

		boolean modified = Config.CostModel.computePartialCost(_idCostLB, _rootOperator, _idLocalCost, idChildCostLB);
		MemoryManager.putCostArray(idChildCostLB);

		return modified;
	}

	public void updateCostLB() {
		boolean modified = computeCostLB();
		if (modified)
			_parent.updateCostLB(_idCostLB, _isAlgOrLocalEnfOperator);
		// else
		// System.out.println("costLB");
	}

	public int getCurrentBestPlanCostId() {
		return _idCurrentBestPlanCost;
	}
	
	
	/**
	 * recompute the cost of current best plan and set the costId in _idCurrentBestPlanCost. This method assumes the 
	 * all child equivalence node have already updated there currentBestPlanCostId.
	 * 
	 * @return true : if the cost of currentBestPlanChanges
	 * 		   false : if the valid plan does not exist or the cost remains same
	 */
	public boolean computeCurrentBestPlanCost() {

        if (_children != null) {
            final int[] idChildCurrentBestPlanCost = MemoryManager.getCostArray(_children.length);

            for (int i = 0; i < _children.length; i++) {
                idChildCurrentBestPlanCost[i] = _children[i].getCurrentBestPlanCostId();

                if (!Config.CostModel.IsValidPlanCost(idChildCurrentBestPlanCost[i]))
                    return false;
            }

            boolean modified = Config.CostModel.computeTotalCost(_idCurrentBestPlanCost, _rootOperator, _idLocalCost,
                    idChildCurrentBestPlanCost);

            MemoryManager.putCostArray(idChildCurrentBestPlanCost);

            return modified;
        }
        return false;
	}

	public void compbestplancost()
	{
		if(_children!=null)
		{
			final int[] idChildCurrentBestPlanCost = MemoryManager.getCostArray(_children.length);

			for (int i = 0; i < _children.length; i++) {
				//compute child cost
				_children[i].compbestplancost();
				idChildCurrentBestPlanCost[i] = _children[i].getCurrentBestPlanCostId();

				//if (!Config.CostModel.IsValidPlanCost(idChildCurrentBestPlanCost[i]))
					//return false;
			}

			boolean modified = Config.CostModel.computeTotalCost(_idCurrentBestPlanCost, _rootOperator, _idLocalCost,
					idChildCurrentBestPlanCost);

			MemoryManager.putCostArray(idChildCurrentBestPlanCost);

			//return modified;
		}
	}

	
	/**
	 * This method recompute the currentBestPlanCostId. In case the cost of currentBestPlanChanged 
	 * in that case it recursively calls update method for that parent equivalence node.
	 * This method should be called in case of children cost has  been changes
	 */
	public void updateCurrentBestPlanCost() {

		boolean modified = computeCurrentBestPlanCost();
		if (modified)
			_parent.updateCurrentBestPlanCost(_idCurrentBestPlanCost, this);
		// else
		// System.out.println("currentBestPlanCost");
	}

	public int getCostUBId() {
		return _parent.getCostUBId(_isAlgOrLocalEnfOperator);
	}

	public void updateCostUB() {
		if (_children != null) {
			final int idCostUB = _parent.getCostUBId(_isAlgOrLocalEnfOperator);
			final int[] idChildCostLB = MemoryManager.getCostArray(_children.length);

			for (int i = 0; i < _children.length; i++)
				idChildCostLB[i] = _children[i].getCostLBId(!_isAlgOrLocalEnfOperator);

			for (int i = 0; i < _children.length; i++) {
				final int idCurrentChildCostLB = idChildCostLB[i];
				idChildCostLB[i] = ICostModel.NULL;

				boolean modified = Config.CostModel.computeRemainingCostUB(_idChildrenCostUB[i], _rootOperator,
						_idLocalCost, idChildCostLB, idCostUB);

				if (!Config.CostModel.IsValid(_idChildrenCostUB[i]))
					Config.CostModel.SetToZero(_idChildrenCostUB[i]);

				idChildCostLB[i] = idCurrentChildCostLB;

				if (modified)
					_children[i].updateCostUBFromParent(_idChildrenCostUB[i], _isAlgOrLocalEnfOperator);
				// else
				// System.out.println("UpdateChildCostUB");
			}

			MemoryManager.putCostArray(idChildCostLB);
			computeCostUBForChild();
		}
	}

	public int getCostUBForChildId(int childId) {
		for (int i = 0; i < _children.length; i++)
			if (_children[i].getId() == childId)
				return _idChildrenCostUB[i];

		return ICostModel.NULL;
	}

	public void computeCostUBForChild() {

		final int idCostUB = _parent.getCostUBId(_isAlgOrLocalEnfOperator);
		final int[] idChildCostLB = MemoryManager.getCostArray(_children.length);

		for (int i = 0; i < _children.length; i++)
			idChildCostLB[i] = _children[i].getCostLBId(!_isAlgOrLocalEnfOperator);

		for (int i = 0; i < _children.length; i++) {
			final int idCurrentChildCostLB = idChildCostLB[i];
			idChildCostLB[i] = ICostModel.NULL;

			Config.CostModel.computeRemainingCostUB(_idChildrenCostUB[i], _rootOperator, _idLocalCost, idChildCostLB,
					idCostUB);

			if (!Config.CostModel.IsValid(_idChildrenCostUB[i]))
				Config.CostModel.SetToZero(_idChildrenCostUB[i]);

			idChildCostLB[i] = idCurrentChildCostLB;
		}

		MemoryManager.putCostArray(idChildCostLB);
	}

	public double getInputSizeBlocks() {
		if (_children == null)
			return 0;

		double inputSize = 0;
		for (int i = 0; i < _children.length; i++) {
			inputSize += Config.OpHelper.getNumBlocks(_children[i].getLogicalProperties());
		}

		return inputSize;
	}

	public void printShort() {
		_parent.printId();
		System.out.print("." + _id + ".");
		Config.OpHelper.printPhysicalOperator(System.out, _rootOperator, _logicalOperator, _parent.getLogicalProperties());
		if (isEnfOperator()) {
			System.out.print("\t\tEnforcerOperator  ");
		}else if (isAlgOrLocalEnfOperator()) {
			System.out.print("\t\tAlgorithmic ");
		}else {
			System.out.print("\t\tLocal Enforcer ");
		}
	}

	public void printLong() {
		printShort();
		System.out.println();
		System.out.println("\t\t\t\t _localCost = " + Config.CostModel.toString(_idLocalCost));
		System.out.println("\t\t\t\t _costLB = " + Config.CostModel.toString(_idCostLB));
		System.out.println("\t\t\t\t _costUB = " + Config.CostModel.toString(getCostUBId()));
		System.out.println("\t\t\t\t _currentBestPlanCost = " + Config.CostModel.toString(_idCurrentBestPlanCost));
		if (_idChildrenCostUB != null) {
			System.out.print("\t\t\t\t _childCostUB = ");
			for (int i = 0; i < _idChildrenCostUB.length; i++)
				System.out.print(Config.CostModel.toString(_idChildrenCostUB[i]) + " ");
			System.out.println();
		}
	}

	public void print() {
		printShort();
		System.out.print(" <totalCost = " + Config.CostModel.toString(_idCurrentBestPlanCost) + ", localCost = "
				+ Config.CostModel.toString(_idLocalCost) + ", ");
		Config.OpHelper.printPhysicalProperties(System.out, _parent.getPhysicalProperties());
		System.out.print(",");
		Config.OpHelper.printLogicalProperties(System.out, _parent.getLogicalProperties());
		System.out.print(">");
	}

	/** 
	 * This method traverse the DAG in bottom up manner and return the Plan-DAG.
	 * Plan DAG is different from AND or graph.  
	 */
	public Plan getCurrentBestPlan() {

		/*System.out.println("PHY OP");
		Config.OpHelper.printPhysicalOperator(System.out, _rootOperator, _logicalOperator, _parent.getLogicalProperties() );
		System.out.println("\n");
		print();
		System.out.println("\n");*/

		if (_children == null)
			return new Plan(
					_logicalOperator,
					_rootOperator,
					_parent.getLogicalProperties(),
					_parent.getPhysicalProperties(),
					Config.CostModel.toString(_idLocalCost),
					Config.CostModel.toString(_idCurrentBestPlanCost),
					null,
					this
					);

		Plan[] childrenBestPlans = new Plan[_children.length];

		for (int i = 0; i < _children.length; i++) {
			childrenBestPlans[i] = _children[i].getCurrentBestPlan();
		}

		return new Plan(
				_logicalOperator,
				_rootOperator,
				_parent.getLogicalProperties(),
				_parent.getPhysicalProperties(),
				Config.CostModel.toString(_idLocalCost),
				Config.CostModel.toString(_idCurrentBestPlanCost),
				childrenBestPlans,
				this
				);
	}



	
	
	
	/**
	 * Print the best plan. 
	 * @param indentLevel
	 */
	public void printPlan(int indentLevel) {


		/*System.out.println("PHY OP");
		Config.OpHelper.printPhysicalOperator(System.out, _rootOperator, _logicalOperator, _parent.getLogicalProperties() );
		System.out.println("\n");
		print();
		System.out.println("\n");*/


		System.out.println();
		for (int i = 0; i < indentLevel; i++)
			System.out.print("| ");
		print();		
		if (_children == null) 
			return;
		for (int i = 0; i < _children.length; i++) {
			//if (_actualChildren != null && _actualChildren[i] != null) {
			 //_actualChildren[i].printCurrentBestPlan(indentLevel + 1);
			//} else {
				_children[i].printCurrentBestPlan(indentLevel + 1);
			//}
		}
	}

	
	
	private void computeLocalCost(Object[] childrenPhysicalProperties) {
		Object[] childrenLogicalProperties = null;
		if (_children != null) {
			childrenLogicalProperties = Config.OpHelper.getLogicalPropertiesArray(_children.length);
			for (int i = 0; i < _children.length; i++)
				childrenLogicalProperties[i] = _children[i].getLogicalProperties();
		}

		Config.CostModel.computeLocalCost(_idLocalCost, _rootOperator, _logicalOperator,
				_parent.getLogicalProperties(), childrenLogicalProperties, _parent.getPhysicalProperties(), childrenPhysicalProperties);

		Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);
	}
}
