 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.junit;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.BitSet;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import pyroj.optimizer.Config;
import pyroj.optimizer.LogicalEquivalenceNode;
import pyroj.optimizer.LogicalOperatorNode;
import pyroj.optimizer.PatternTree;

public class PatternTreeTestNoRoot {

	DummyPatternTree getPattern() {
		DummyPatternTree innerJoinPattern = new DummyPatternTree("JoinOp",
				new DummyPatternTree("groupNode"), new DummyPatternTree(
						"groupNode"));
		DummyPatternTree getPattern = new DummyPatternTree("GetOp");

		return new DummyPatternTree("JoinOp", innerJoinPattern, getPattern);
	}

	LogicalOperatorNode createOpNode(DummyLogicalOperator op,
			LogicalEquivalenceNode child0, LogicalEquivalenceNode child1) {
		LogicalOperatorNode opNode = new LogicalOperatorNode(op, child0, child1);
		child0.addParent(opNode);
		child1.addParent(opNode);
		return opNode;
	}

	LogicalOperatorNode createOpNode(DummyLogicalOperator op,
			LogicalEquivalenceNode child0) {
		LogicalOperatorNode opNode = new LogicalOperatorNode(op, child0);
		child0.addParent(opNode);
		return opNode;
	}

	LogicalEquivalenceNode createGroupNode(LogicalOperatorNode child0) {
		LogicalEquivalenceNode group = new LogicalEquivalenceNode(null);
		group.addExpr(child0);
		child0.setGroup(group);
		return group;
	}

	LogicalEquivalenceNode createGroupNode(LogicalOperatorNode child0,
			LogicalOperatorNode child1) {
		LogicalEquivalenceNode group = new LogicalEquivalenceNode(null);
		child0.setGroup(group);
		child1.setGroup(group);
		group.addExpr(child0);
		group.addExpr(child1);
		return group;
	}

	LogicalEquivalenceNode createGroupNode(LogicalOperatorNode child0,
			LogicalOperatorNode child1, LogicalOperatorNode child2) {
		LogicalEquivalenceNode group = new LogicalEquivalenceNode(null);
		group.addExpr(child0);
		group.addExpr(child1);
		group.addExpr(child2);
		child0.setGroup(group);
		child1.setGroup(group);
		child2.setGroup(group);
		return group;
	}

	LogicalOperatorNode getDAG(Pair<Object, Object>[][] expectedBinding,
			LogicalEquivalenceNode[] expectedBindingGroup) {

		LogicalEquivalenceNode group_0 = new LogicalEquivalenceNode(null);
		LogicalEquivalenceNode group_1 = new LogicalEquivalenceNode(null);
		LogicalOperatorNode opNode_0 = createOpNode(new DummyLogicalOperator(
				"JoinOp", 0), group_0, group_1);

		LogicalEquivalenceNode group_2 = new LogicalEquivalenceNode(null);
		LogicalEquivalenceNode group_3 = new LogicalEquivalenceNode(null);
		LogicalOperatorNode opNode_1 = createOpNode(new DummyLogicalOperator(
				"JoinOp", 1), group_2, group_3);

		LogicalEquivalenceNode group_4 = createGroupNode(opNode_0, opNode_1);

		LogicalOperatorNode opNode_2 = new LogicalOperatorNode(
				new DummyLogicalOperator("GetOp", 2));

		LogicalEquivalenceNode group_5 = new LogicalEquivalenceNode(null);
		LogicalOperatorNode opNode_3 = createOpNode(new DummyLogicalOperator(
				"SelectOp", 3), group_5);

		LogicalOperatorNode opNode_4 = new LogicalOperatorNode(
				new DummyLogicalOperator("GetOp", 4));

		LogicalEquivalenceNode group_6 = createGroupNode(opNode_2, opNode_3,
				opNode_4);

		LogicalOperatorNode opNode_5 = new LogicalOperatorNode(
				new DummyLogicalOperator("JoinOp", 5), group_4, group_6);

		LogicalEquivalenceNode group_7 = new LogicalEquivalenceNode(null);
		LogicalEquivalenceNode group_8 = new LogicalEquivalenceNode(null);
		LogicalOperatorNode opNode_6 = createOpNode(new DummyLogicalOperator(
				"JoinOp", 6), group_7, group_8);

		LogicalEquivalenceNode group_9 = createGroupNode(opNode_6);

		LogicalOperatorNode opNode_7 = new LogicalOperatorNode(
				new DummyLogicalOperator("GetOp", 7));

		LogicalEquivalenceNode group_10 = createGroupNode(opNode_7);

		LogicalOperatorNode opNode_8 = new LogicalOperatorNode(
				new DummyLogicalOperator("JoinOp", 8), group_9, group_10);

		LogicalEquivalenceNode group_11 = createGroupNode(opNode_5, opNode_8);

		LogicalEquivalenceNode group_12 = new LogicalEquivalenceNode(null);
		LogicalOperatorNode opNode_9 = createOpNode(new DummyLogicalOperator(
				"SelectOp", 9), group_12);
		LogicalEquivalenceNode group_13 = createGroupNode(opNode_9);

		LogicalOperatorNode opNode_10 = createOpNode(new DummyLogicalOperator(
				"JoinOp", 10), group_11, group_13);
		LogicalEquivalenceNode group_14 = createGroupNode(opNode_10);

		LogicalOperatorNode opNode_11 = new LogicalOperatorNode(
				new DummyLogicalOperator("GetOp", 11));
		LogicalEquivalenceNode group_15 = createGroupNode(opNode_11);

		LogicalOperatorNode opNode_12 = createOpNode(new DummyLogicalOperator(
				"JoinOp", 12), group_11, group_15);
		LogicalEquivalenceNode group_16 = createGroupNode(opNode_12);

		LogicalOperatorNode opNode_13 = new LogicalOperatorNode(
				new DummyLogicalOperator("GetOp", 13));
		LogicalEquivalenceNode group_17 = createGroupNode(opNode_13);

		LogicalOperatorNode opNode_14 = createOpNode(new DummyLogicalOperator(
				"JoinOp", 14), group_11, group_17);
		LogicalEquivalenceNode group_18 = createGroupNode(opNode_14);

		group_18.printDAG(0);
		group_16.printDAG(0);
		group_14.printDAG(0);

		expectedBinding[0][0] = new MutablePair<Object, Object>(
				opNode_5.getOperator(), opNode_5.getGroup()
						.getLogicalProperties());
		expectedBinding[0][1] = new MutablePair<Object, Object>(
				opNode_0.getOperator(), opNode_0.getGroup()
						.getLogicalProperties());
		expectedBinding[0][2] = new MutablePair<Object, Object>(group_0,
				group_0.getLogicalProperties());
		expectedBinding[0][3] = new MutablePair<Object, Object>(group_1,
				group_1.getLogicalProperties());
		expectedBinding[0][4] = new MutablePair<Object, Object>(
				opNode_2.getOperator(), opNode_2.getGroup()
						.getLogicalProperties());
		expectedBindingGroup[0] = opNode_5.getGroup();

		expectedBinding[1][0] = new MutablePair<Object, Object>(
				opNode_5.getOperator(), opNode_5.getGroup()
						.getLogicalProperties());
		expectedBinding[1][1] = new MutablePair<Object, Object>(
				opNode_1.getOperator(), opNode_1.getGroup()
						.getLogicalProperties());
		expectedBinding[1][2] = new MutablePair<Object, Object>(group_2,
				group_2.getLogicalProperties());
		expectedBinding[1][3] = new MutablePair<Object, Object>(group_3,
				group_3.getLogicalProperties());
		expectedBinding[1][4] = new MutablePair<Object, Object>(
				opNode_2.getOperator(), opNode_2.getGroup()
						.getLogicalProperties());
		expectedBindingGroup[1] = opNode_5.getGroup();

		expectedBinding[2][0] = new MutablePair<Object, Object>(
				opNode_5.getOperator(), opNode_5.getGroup()
						.getLogicalProperties());
		expectedBinding[2][1] = new MutablePair<Object, Object>(
				opNode_0.getOperator(), opNode_0.getGroup()
						.getLogicalProperties());
		expectedBinding[2][2] = new MutablePair<Object, Object>(group_0,
				group_0.getLogicalProperties());
		expectedBinding[2][3] = new MutablePair<Object, Object>(group_1,
				group_1.getLogicalProperties());
		expectedBinding[2][4] = new MutablePair<Object, Object>(
				opNode_4.getOperator(), opNode_4.getGroup()
						.getLogicalProperties());
		expectedBindingGroup[2] = opNode_5.getGroup();

		expectedBinding[3][0] = new MutablePair<Object, Object>(
				opNode_5.getOperator(), opNode_5.getGroup()
						.getLogicalProperties());
		expectedBinding[3][1] = new MutablePair<Object, Object>(
				opNode_1.getOperator(), opNode_1.getGroup()
						.getLogicalProperties());
		expectedBinding[3][2] = new MutablePair<Object, Object>(group_2,
				group_2.getLogicalProperties());
		expectedBinding[3][3] = new MutablePair<Object, Object>(group_3,
				group_3.getLogicalProperties());
		expectedBinding[3][4] = new MutablePair<Object, Object>(
				opNode_4.getOperator(), opNode_4.getGroup()
						.getLogicalProperties());
		expectedBindingGroup[3] = opNode_5.getGroup();

		expectedBinding[4][0] = new MutablePair<Object, Object>(
				opNode_12.getOperator(), opNode_12.getGroup()
						.getLogicalProperties());
		expectedBinding[4][1] = new MutablePair<Object, Object>(
				opNode_5.getOperator(), opNode_5.getGroup()
						.getLogicalProperties());
		expectedBinding[4][2] = new MutablePair<Object, Object>(group_4,
				group_4.getLogicalProperties());
		expectedBinding[4][3] = new MutablePair<Object, Object>(group_6,
				group_6.getLogicalProperties());
		expectedBinding[4][4] = new MutablePair<Object, Object>(
				opNode_11.getOperator(), opNode_11.getGroup()
						.getLogicalProperties());
		expectedBindingGroup[4] = opNode_12.getGroup();

		expectedBinding[5][0] = new MutablePair<Object, Object>(
				opNode_14.getOperator(), opNode_14.getGroup()
						.getLogicalProperties());
		expectedBinding[5][1] = new MutablePair<Object, Object>(
				opNode_5.getOperator(), opNode_5.getGroup()
						.getLogicalProperties());
		expectedBinding[5][2] = new MutablePair<Object, Object>(group_4,
				group_4.getLogicalProperties());
		expectedBinding[5][3] = new MutablePair<Object, Object>(group_6,
				group_6.getLogicalProperties());
		expectedBinding[5][4] = new MutablePair<Object, Object>(
				opNode_13.getOperator(), opNode_13.getGroup()
						.getLogicalProperties());
		expectedBindingGroup[5] = opNode_14.getGroup();

		return opNode_5;
	}

	private void printBinding(Pair<Object, Object>[] binding,
			LogicalEquivalenceNode group) {

		group.printshort();
		System.out.print("  -  ");
		for (int j = 0; j < binding.length; j++) {
			System.out.print("[");
			if (binding[j].getLeft() instanceof LogicalEquivalenceNode) {
				LogicalEquivalenceNode node = (LogicalEquivalenceNode) binding[j]
						.getLeft();
				node.printshort();
			} else {
				DummyLogicalOperator node = (DummyLogicalOperator) binding[j]
						.getLeft();
				node.print();

			}
			System.out.print("] ");
		}
		System.out.println();

	}

	private boolean findbinding(Pair<Object, Object>[] actualBinding,
			LogicalEquivalenceNode actualBindingGroup,
			Pair<Object, Object>[][] expectedBinding,
			LogicalEquivalenceNode[] expectedBindingGroup, BitSet alreadyMatched) {

		for (int i = 0; i < expectedBinding.length; i++) {

			if (alreadyMatched.get(i))
				continue;

			Pair<Object, Object>[] expected = expectedBinding[i];

			if (expected.length != actualBinding.length)
				continue;

			boolean matched = true;

			for (int j = 0; j < expected.length; j++) {
				if (expected[j].getLeft() != actualBinding[j].getLeft())
					matched = false;
				if (expected[j].getRight() != actualBinding[j].getRight())
					matched = false;
			}

			if (!matched)
				continue;

			if (actualBindingGroup.getId() != expectedBindingGroup[i].getId())
				return false;

			alreadyMatched.set(i);
			return true;
		}
		return false;
	}

	@Test
	public void test() {

		Config.OpHelper = new DummyOpHelper();
		Config.TreeHelper = new DummyTreeHelper();

		@SuppressWarnings("unchecked")
		Pair<Object, Object>[][] expectedBinding = new MutablePair[6][5];
		LogicalEquivalenceNode[] expectedBindingGroup = new LogicalEquivalenceNode[6];
		LogicalOperatorNode opNode = getDAG(expectedBinding,
				expectedBindingGroup);
		System.out.println();
		System.out.println("DAG");
		System.out.println("=======");
		opNode.printDAG(0);
		System.out.println();
		System.out.println();

		DummyPatternTree pattern = getPattern();
		System.out.println("Pattern");
		System.out.println("=======");
		pattern.print();
		System.out.println();

		ArrayList<Pair<Object, Object>[]> actualBinding = new ArrayList<Pair<Object, Object>[]>();
		ArrayList<LogicalEquivalenceNode> actualBindingGroup = new ArrayList<LogicalEquivalenceNode>();

		try {
			System.out.println("\nExpected bindings");
			for (int i = 0; i < expectedBinding.length; i++) {
				printBinding(expectedBinding[i], expectedBindingGroup[i]);
			}

			pattern.initializeIds();
			PatternTree.getBindingsNonRoot(pattern, opNode, actualBinding,
					actualBindingGroup);

			if (actualBinding.size() != expectedBinding.length
					|| actualBindingGroup.size() != expectedBindingGroup.length) {
				System.out.println("\nActual bindings");
				for (int i = 0; i < actualBinding.size(); i++) {
					printBinding(actualBinding.get(i),
							actualBindingGroup.get(i));
				}
				fail("Size doesn't match");
			}

			BitSet alreadyMatched = new BitSet(expectedBinding.length);
			System.out.println("\nActual bindings");
			for (int i = 0; i < actualBinding.size(); i++) {
				printBinding(actualBinding.get(i), actualBindingGroup.get(i));
				if (!findbinding(actualBinding.get(i),
						actualBindingGroup.get(i), expectedBinding,
						expectedBindingGroup, alreadyMatched))
					fail("Binding is not expected");
			}

		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}
