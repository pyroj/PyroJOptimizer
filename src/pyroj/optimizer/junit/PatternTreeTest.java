 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.junit;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.BitSet;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import pyroj.optimizer.Config;
import pyroj.optimizer.LogicalEquivalenceNode;
import pyroj.optimizer.LogicalOperatorNode;
import pyroj.optimizer.PatternTree;

public class PatternTreeTest {

	@Test
	public void test() {
		try {

			Config.TreeHelper = new DummyTreeHelper();
			Config.OpHelper = new DummyOpHelper();

			ArrayList<Pair<Object, Object>[]> expectedBinding = new ArrayList<Pair<Object, Object>[]>();
			LogicalOperatorNode opNode = getDAG1(expectedBinding);
			System.out.println("DAG");
			System.out.println("=======");
			opNode.printDAG(0);
			System.out.println();
			System.out.println();

			// Select <Join <Join <Select <null>, Get>, Select <null>>>
			DummyPatternTree pattern = getPattern1();
			System.out.println("Pattern");
			System.out.println("=======");
			pattern.print();
			System.out.println();

			BitSet alreadyMatched = new BitSet(expectedBinding.size());
			System.out.println("\n\nPattern tree test bindings : ");

			ArrayList<Pair<Object, Object>[]> bindings = new ArrayList<Pair<Object, Object>[]>();
			PatternTree.getBindings(pattern, opNode, bindings, null);
			for (int i = 0; i < bindings.size(); i++) {
				printBinding(bindings.get(i));
				if (!findbinding(bindings.get(i), expectedBinding,
						alreadyMatched))
					fail("Binding is not expected");
			}

			if (alreadyMatched.nextClearBit(0) < expectedBinding.size())
				fail("Number of bindings not same");

		} catch (Exception e) {
			System.out.println();
			e.printStackTrace();
			fail(e.getMessage());
		}

	}

	private boolean findbinding(Pair<Object, Object>[] actual,
			ArrayList<Pair<Object, Object>[]> expectedBinding,
			BitSet alreadyMatched) {

		for (int i = 0; i < expectedBinding.size(); i++) {

			if (alreadyMatched.get(i))
				continue;

			Pair<Object, Object>[] expected = expectedBinding.get(i);

			if (expected.length != actual.length)
				continue;

			boolean matched = true;

			for (int j = 0; j < expected.length; j++) {
				if (expected[j].getLeft() != actual[j].getLeft())
					matched = false;
				if (expected[j].getRight() != actual[j].getRight())
					matched = false;
			}

			if (!matched)
				continue;

			alreadyMatched.set(i);
			return true;
		}
		return false;
	}

	private LogicalOperatorNode getDAG1(
			ArrayList<Pair<Object, Object>[]> expectedBinding) throws Exception {

		@SuppressWarnings("unchecked")
		Pair<Object, Object>[] binding1 = new ImmutablePair[8];
		@SuppressWarnings("unchecked")
		Pair<Object, Object>[] binding2 = new ImmutablePair[8];

		// Select <Join <Join <Select <null>, Get>, Select <null>>>

		LogicalEquivalenceNode group_0 = new LogicalEquivalenceNode(null);

		LogicalEquivalenceNode group_1 = new LogicalEquivalenceNode(null);

		DummyLogicalOperator op_0 = new DummyLogicalOperator("SelectOp", 0);
		LogicalOperatorNode opNode_0 = new LogicalOperatorNode(op_0, group_0);
		group_0.addParent(opNode_0);

		group_1.addExpr(opNode_0);
		opNode_0.setGroup(group_1);

		binding1[3] = new ImmutablePair<Object, Object>(op_0, opNode_0
				.getGroup().getLogicalProperties());
		binding1[4] = new ImmutablePair<Object, Object>(group_0,
				group_0.getLogicalProperties());

		DummyLogicalOperator op_1 = new DummyLogicalOperator("GetOp", 1);
		LogicalOperatorNode opNode_1 = new LogicalOperatorNode(op_1);
		group_1.addExpr(opNode_1);
		opNode_1.setGroup(group_1);

		LogicalEquivalenceNode group_2 = new LogicalEquivalenceNode(null);

		DummyLogicalOperator op_2 = new DummyLogicalOperator("GetOp", 2);
		LogicalOperatorNode opNode_2 = new LogicalOperatorNode(op_2);
		group_2.addExpr(opNode_2);
		opNode_2.setGroup(group_2);

		binding1[5] = new ImmutablePair<Object, Object>(op_2, opNode_2
				.getGroup().getLogicalProperties());

		LogicalEquivalenceNode group_3 = new LogicalEquivalenceNode(null);

		DummyLogicalOperator op_3 = new DummyLogicalOperator("SelectOp", 3);
		LogicalOperatorNode opNode_3 = new LogicalOperatorNode(op_3, group_3);
		group_3.addParent(opNode_3);
		group_2.addExpr(opNode_3);
		opNode_3.setGroup(group_2);

		DummyLogicalOperator op_4 = new DummyLogicalOperator("JoinOp", 4);
		LogicalOperatorNode opNode_4 = new LogicalOperatorNode(op_4, group_1,
				group_2);
		group_1.addParent(opNode_4);
		group_2.addParent(opNode_4);

		LogicalEquivalenceNode group_4 = new LogicalEquivalenceNode(null);
		group_4.addExpr(opNode_4);
		opNode_4.setGroup(group_4);

		binding1[2] = new ImmutablePair<Object, Object>(op_4, opNode_4
				.getGroup().getLogicalProperties());

		LogicalEquivalenceNode group_5 = new LogicalEquivalenceNode(
				group_4.getLogicalProperties());

		DummyLogicalOperator op_5 = new DummyLogicalOperator("SelectOp", 5);
		LogicalOperatorNode opNode_5 = new LogicalOperatorNode(op_5, group_5);
		group_5.addParent(opNode_5);
		group_4.addExpr(opNode_5);
		opNode_5.setGroup(group_4);

		LogicalEquivalenceNode group_6 = new LogicalEquivalenceNode(null);

		DummyLogicalOperator op_6 = new DummyLogicalOperator("JoinOp", 6);
		LogicalOperatorNode opNode_6 = new LogicalOperatorNode(op_6, group_1,
				group_2);
		group_1.addParent(opNode_6);
		group_2.addParent(opNode_6);
		group_6.addExpr(opNode_6);
		opNode_6.setGroup(group_6);

		LogicalEquivalenceNode group_7 = new LogicalEquivalenceNode(
				group_6.getLogicalProperties());

		DummyLogicalOperator op_7 = new DummyLogicalOperator("SelectOp", 7);
		LogicalOperatorNode opNode_7 = new LogicalOperatorNode(op_7, group_7);
		group_7.addParent(opNode_7);

		group_6.addExpr(opNode_7);
		opNode_7.setGroup(group_6);

		binding1[6] = new ImmutablePair<Object, Object>(op_7, opNode_7
				.getGroup().getLogicalProperties());
		binding1[7] = new ImmutablePair<Object, Object>(group_7,
				group_7.getLogicalProperties());

		LogicalEquivalenceNode group_8 = new LogicalEquivalenceNode(null);

		DummyLogicalOperator op_8 = new DummyLogicalOperator("JoinOp", 8);
		LogicalOperatorNode opNode_8 = new LogicalOperatorNode(op_8, group_4,
				group_6);
		group_4.addParent(opNode_8);
		group_6.addParent(opNode_8);

		group_8.addExpr(opNode_8);
		opNode_8.setGroup(group_8);

		binding1[1] = new ImmutablePair<Object, Object>(op_8, opNode_8
				.getGroup().getLogicalProperties());

		binding2 = binding1.clone();

		LogicalEquivalenceNode group_9 = new LogicalEquivalenceNode(null);

		DummyLogicalOperator op_9 = new DummyLogicalOperator("GetOp", 9);
		LogicalOperatorNode opNode_9 = new LogicalOperatorNode(op_9);
		group_9.addExpr(opNode_9);
		opNode_9.setGroup(group_9);

		LogicalEquivalenceNode group_10 = new LogicalEquivalenceNode(
				group_9.getLogicalProperties());

		DummyLogicalOperator op_10 = new DummyLogicalOperator("SelectOp", 10);
		LogicalOperatorNode opNode_10 = new LogicalOperatorNode(op_10, group_10);
		group_10.addParent(opNode_10);

		group_9.addExpr(opNode_10);
		opNode_10.setGroup(group_9);

		binding2[6] = new ImmutablePair<Object, Object>(op_10, opNode_10
				.getGroup().getLogicalProperties());
		binding2[7] = new ImmutablePair<Object, Object>(group_10,
				group_10.getLogicalProperties());

		DummyLogicalOperator op_11 = new DummyLogicalOperator("JoinOp", 11);
		LogicalOperatorNode opNode_11 = new LogicalOperatorNode(op_11, group_4,
				group_9);
		group_4.addParent(opNode_11);
		group_9.addParent(opNode_11);

		group_8.addExpr(opNode_11);
		opNode_11.setGroup(group_8);

		binding2[1] = new ImmutablePair<Object, Object>(op_11, opNode_11
				.getGroup().getLogicalProperties());

		DummyLogicalOperator root = new DummyLogicalOperator("SelectOp", 12);
		LogicalOperatorNode op_root = new LogicalOperatorNode(root, group_8);
		group_8.addParent(op_root);

		LogicalEquivalenceNode root_eq = new LogicalEquivalenceNode(null);
		op_root.setGroup(root_eq);
		root_eq.addExpr(op_root);

		binding1[0] = binding2[0] = new ImmutablePair<Object, Object>(root,
				op_root.getGroup().getLogicalProperties());

		expectedBinding.add(binding1);
		expectedBinding.add(binding2);

		return op_root;
	}

	/**
	 * Select <Join <Join <Select <null>, Get>, Select <null>>>
	 * 
	 * @return
	 * @throws Exception
	 */

	private DummyPatternTree getPattern1() throws Exception {

		DummyPatternTree select1 = new DummyPatternTree("SelectOp",
				new DummyPatternTree("groupNode"));
		DummyPatternTree rel1 = new DummyPatternTree("GetOp");

		DummyPatternTree innerJoin = new DummyPatternTree("JoinOp", select1,
				rel1);

		DummyPatternTree select2 = new DummyPatternTree("SelectOp",
				new DummyPatternTree("groupNode"));

		DummyPatternTree outerJoin = new DummyPatternTree("JoinOp", innerJoin,
				select2);

		DummyPatternTree outerSelect = new DummyPatternTree("SelectOp",
				outerJoin);

		outerSelect.initializeIds();

		return outerSelect;
	}

	private void printBinding(Pair<Object, Object>[] binding) {
		for (int j = 0; j < binding.length; j++) {
			System.out.print("[");
			if (binding[j].getLeft() instanceof LogicalEquivalenceNode) {
				LogicalEquivalenceNode node = (LogicalEquivalenceNode) binding[j]
						.getLeft();
				node.printshort();
			} else {
				DummyLogicalOperator node = (DummyLogicalOperator) binding[j]
						.getLeft();
				node.print();

			}
			System.out.print("] ");
		}
		System.out.println();

	}
}
