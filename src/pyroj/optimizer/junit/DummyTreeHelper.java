 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.junit;

import pyroj.runtime.ITreeHelper;

public class DummyTreeHelper implements ITreeHelper {

	@Override
	public boolean isRootMatches(Object patternTree, Object logicalOperator) {
		return ((DummyPatternTree) patternTree).isRootMatches(logicalOperator);
	}

	@Override
	public boolean shouldRootBeLogicalOperator(Object patternTree) {
		return ((DummyPatternTree) patternTree).shouldRootBeLogicalOperator();
	}

	@Override
	public int getNumChildren(Object tree) {
		return ((DummyPatternTree) tree).getNumChildren();
	}

	@Override
	public Object[] getChildren(Object tree) {
		return ((DummyPatternTree) tree).getChildren();
	}

	@Override
	public Object getChild(Object tree, int index) {
		return ((DummyPatternTree) tree).getChild(index);
	}

	@Override
	public Object getParent(Object patternTree) {
		return ((DummyPatternTree) patternTree).getParent();
	}

	@Override
	public int getId(Object tree) {
		return ((DummyPatternTree) tree).getId();
	}

	@Override
	public int getNumNodes(Object tree) {
		return ((DummyPatternTree) tree).getNumNodes();
	}

	@Override
	public Object getRootOperator(Object exprTree) {
		return null;
	}

	@Override
	public void print(Object tree) {
		((DummyPatternTree) tree).print();

	}

	@Override
	public int getRootOperatorType(Object patternTree) {
		// TODO Auto-generated method stub
		return 0;
	}
}
