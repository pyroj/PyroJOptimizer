 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.junit;

public class DummyPatternTree {
	private final String _name;
	private final DummyPatternTree[] _children;
	private DummyPatternTree _parent;
	private int _id;
	private int _totalNumNodes = -1;

	public DummyPatternTree(String name) {
		_name = name;
		_children = null;
	}

	public DummyPatternTree(String name, DummyPatternTree child0) {
		_name = name;
		_children = new DummyPatternTree[] { child0 };
		child0._parent = this;
	}

	public DummyPatternTree(String name, DummyPatternTree child0,
			DummyPatternTree child1) {
		_name = name;
		_children = new DummyPatternTree[] { child0, child1 };
		child0._parent = child1._parent = this;
	}

	public boolean isRootMatches(Object logicalOperator) {
		return _name.equalsIgnoreCase(((DummyLogicalOperator) logicalOperator)
				.getName());
	}

	public boolean shouldRootBeLogicalOperator() {
		return !_name.equalsIgnoreCase("groupNode");
	}

	public int getNumChildren() {
		if (_children == null)
			return 0;
		else
			return _children.length;
	}

	public Object[] getChildren() {
		return _children;
	}

	public Object getChild(int index) {
		return _children[index];
	}

	public Object getParent() {
		return _parent;
	}

	public int getId() {
		return _id;
	}

	public int getNumNodes() {
		if (_totalNumNodes == 0)
			initializeIds();
		return _totalNumNodes;
	}

	public void initializeIds() {
		initializeIds(0);
	}

	private int initializeIds(int id) {
		_id = id++;

		if (_children != null) {
			for (int i = 0; i < _children.length; i++) {
				if (_children[i] != null)
					id = _children[i].initializeIds(id);
			}
		}

		_totalNumNodes = id - _id;
		return id;
	}

	public void print() {
		print(0);
	}

	private void print(int indentLevel) {
		System.out.println();
		for (int i = 0; i < indentLevel; i++)
			System.out.print("| ");
		System.out.print(_name + "." + _id);

		if (_children != null)
			for (int i = 0; i < _children.length; i++)
				_children[i].print(indentLevel + 1);
	}
}
