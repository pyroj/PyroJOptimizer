 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.junit;

import java.io.PrintStream;
import java.util.ArrayList;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.runtime.IImplementationRule;
import pyroj.runtime.ILocalEnforcerRule;
import pyroj.runtime.IOperatorHelper;
import pyroj.runtime.IPartitionEnforcerRule;
import pyroj.runtime.ITransformationRule;

public class DummyOpHelper implements IOperatorHelper {

	@Override
	public Object computeOutputLogicalProperties(Object logicalOperator, Object[] childrenLogicalProperties) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printLogicalOperator(PrintStream ps, Object logicalOperator, Object logicalProperties) {
		((DummyLogicalOperator) logicalOperator).print();

	}

	@Override
	public void printPhysicalOperator(PrintStream ps, Object physicalOperator, Object logicalOperator, Object logicalProperties) {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<Object[]> getChildReqdProperties(Object physicalOperator, Object logicalOperator,
			Object logicalProperties, Object[] childrenLogicalProperties, Object reqdProperties) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void computeLocalCost(Object physicalOperator, Object logicalOperator, Object logicalProperties, Object[] childrenLogicalProperties,
			Object reqdProperties, Object[] childrenPhysicalProperties, MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getNumBlocks(Object logicalProperties) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void mergeProperty(Object srcLogicalProperty, Object logicalPropertyToMerge) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILocalEnforcerRule[] getLocalEnforcerRules() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IImplementationRule[] getImplementationRules() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ITransformationRule[] getTransformationRules() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clearState() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLogOperatorEqual(Object logicalOperator,
			Object otherLogicalOperator) {
		// TODO Auto-generated method stub
		return logicalOperator.equals(otherLogicalOperator);
	}

	@Override
	public int getLogOperatorLocalHashCode(Object logicalOperator) {
		// TODO Auto-generated method stub
		return logicalOperator.hashCode();
	}

	@Override
	public IPartitionEnforcerRule[] getPartitionEnforcerRules() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isPhysicalPropertyEqual(Object phyProp1, Object phyProp2) {
		return phyProp1.equals(phyProp2);
	}

	@Override
	public boolean isPhysicalPropertyEqualHive(Object phyProp1, Object phyProp2, Object logOp) {
		return phyProp1.equals(phyProp2);
	}

	@Override
	public void printInstanceCounts() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void putLogicalPropertyObject(Object logicalProperty) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void printLogicalProperties(PrintStream ps, Object logicalProperties) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void printPhysicalProperties(PrintStream ps,
			Object physicalProperties) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getLogicalPropertiesArray(int numElements) {
		return new Object[numElements];
	}

	@Override
	public void putLogicalPropertiesArray(Object[] logicalPropertyArray) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void putPhysicalPropertiesArray(
			ArrayList<Object[]> physicalPropertyArray) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void computeUseCost(Object logicalProperties,Object physicalProperties,
			MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void computeMaterializationCost(Object logicalOperator,
			Object logicalProperties, Object physicalOperator, Object physicalProperty,
			MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unifyOperators(Object fromLogOp, Object toLogOp) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isCovered(Object phyProp1, Object phyProp2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object getEmptyPropery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isExcludedForSharing(Object operator) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSelectOperator(Object op) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean implies(Object operator1, Object operator2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object cloneSelectPredicate(Object operator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object mergePredicates(Object condExpr1, Object condExpr2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isCorrelated(Object ob) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAggregateOperator(Object op) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object cloneSelectOperator(Object operator, Object child,
			Object predicate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void computeLocalCost_FirstTuple(Object physicalOperator,
			Object logicalOperator, Object logicalProperties,
			Object[] childrenLogicalProperties, Object reqdProperties,
			Object[] childrenPhysicalProperties, MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency,
			MutableDouble networkLatency) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void computeMaterializationCost_FirstTuple(Object logicalOperator,
			Object logicalProperties, Object physicalOperator,
			Object physicalProperty, MutableDouble memoryLatency,
			MutableDouble processingLatency, MutableDouble diskLatency,
			MutableDouble networkLatency) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getnewOp(Object expr)
	{
		return null;
	}

	@Override
	public Object cloneAggregateOperator(Object operator, Object newChild, Object newGrpbyExp)
	{
		return null;
	}

	@Override
	public boolean impliesAggr(Object op1, Object op2)
	{
		return false;
	}

	@Override
	public Object cloneAggregateGroupByAttributes(Object operator)
	{
		return null;
	}


	@Override
	public Object mergeGroupByAttributes(Object set1, Object set2)
	{
		return null;
	}

	@Override
	public boolean canMergeAggr(Object operator1, Object operator2)
	{
		return false;
	}

	@Override
	public Object getAggregateOperator(Object operator1, Object operator2)
	{
		return false;
	}

	@Override
	public Object getProjectOperator(Object op1 ,Object op2){ return false;}

	@Override
	public boolean IsVirtualOperator(Object op) {
		return false;
	}

	@Override
	public Object getFilterPhysicalOperator(Object op)
	{
		return null;
	}

	@Override
	public boolean isLogOperatorNotEqualHive(Object logicalOperator, Object otherLogicalOperator, int grp1, int grp2){return false;}
}
