 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;


public class DegShar{
	LogicalEquivalenceNode logEqNode;
	int degree;
	
	public DegShar( LogicalEquivalenceNode  logEq , int d ) {
		logEqNode  = logEq;
		degree = d;
	}
	
	public LogicalEquivalenceNode getLogicalEquivalenceNode() {
		return logEqNode;
	}
	
	public int getDegree() {
		return degree;
	}
};