 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.Pair;

import pyroj.costmodel.ICostModel;
import pyroj.optimizer.cascade.Task;
import pyroj.runtime.IOperatorHelper;
import pyroj.runtime.ITransformationRule;
import pyroj.runtime.ITreeHelper;
import pyroj.runtime.Plan;

public abstract class Optimizer {

	static public void printInstanceCounts(long timeTaken) {
		System.out.println();
		System.out.printf("%1$-35s = %2$d ms\n", "Time taken", timeTaken);
		System.out.printf("%1$-35s = %2$d \n", "LogicalEquivalenceNode instances", LogicalEquivalenceNode.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "LogicalOperatorNode instances", LogicalOperatorNode.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "PhysicalEquivalenceNode instances",
				PhysicalEquivalenceNode.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "PhysicalOperatorNode instances", PhysicalOperatorNode.NumInstances);
		System.out.printf("%1$-35s = %2$d \n", "CostOrd instances", Config.CostModel.getNumCostInstance());
		System.out.printf("%1$-35s = %2$d \n", "Binding array instances", MemoryManager.NumBindingArrayCreated);
		System.out.printf("%1$-35s = %2$d \n", "Binding grouparray instances",
				MemoryManager.NumBindingGroupArrayCreated);
		System.out.printf("%1$-35s = %2$d \n", "Binding instances", MemoryManager.NumBindingCreated);
		System.out.printf("%1$-35s = %2$d \n", "Binding pair instances", MemoryManager.NumBindingPairs);
		
		Config.OpHelper.printInstanceCounts();
	}

	static protected void clearState() {
		LogicalEquivalenceNode.NumInstances = 0;
		LogicalOperatorNode.NumInstances = 0;
		PhysicalEquivalenceNode.NumInstances = 0;
		PhysicalOperatorNode.NumInstances = 0;
		Task.NumInstances = 0;
		MemoryManager.NumBindingArrayCreated = 0;
		MemoryManager.NumBindingGroupArrayCreated = 0;
		MemoryManager.NumBindingCreated = 0;
		MemoryManager.NumBindingPairs = 0;
		Config.OpHelper.clearState();
	}

	protected Memo _memo = null;

	protected Rules _rules = null;

	public void initialize(Rules rules, IOperatorHelper opHelper, ICostModel costModel, ITreeHelper treeHelper) {
		_memo = new Memo();
		_rules = rules;
		//_memo = new Memo();
		Config.OpHelper = opHelper;
		Config.setCostModel(costModel);
		Config.TreeHelper = treeHelper;
		MemoryManager.initialize();
	}

	public void initialize(Rules rules, IOperatorHelper opHelper, ITreeHelper treeHelper) {
		_memo = new Memo();
		_rules = rules;
		//_memo = new Memo();
		Config.OpHelper = opHelper;
		//Config.setCostModel(costModel);
		Config.TreeHelper = treeHelper;
		//MemoryManager.initialize();
	}

	protected LogicalOperatorNode addExprTree(Object exprTree, LogicalEquivalenceNode grp) {

		final Object expr = Config.TreeHelper.getRootOperator(exprTree);

		LogicalEquivalenceNode[] childrenGroup = null;

		if (Config.TreeHelper.getNumChildren(exprTree) > 0) {
			final Object[] children = Config.TreeHelper.getChildren(exprTree);
			childrenGroup = new LogicalEquivalenceNode[children.length];

			for (int i = 0; i < children.length; i++) {
				final Object childRootOp = Config.TreeHelper.getRootOperator(children[i]);
				if (childRootOp instanceof LogicalEquivalenceNode)
					childrenGroup[i] = (LogicalEquivalenceNode) childRootOp;
				else
					childrenGroup[i] = addExprTree(children[i], null).getGroup();
			}
		}

		return addOperator(expr, childrenGroup, grp);
	}

	protected LogicalOperatorNode addOperator(LogicalOperatorNode op) {

		LogicalEquivalenceNode grp = op.getGroup();
		final LogicalOperatorNode opMemo = _memo.find(op);

		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode(op.computeLogicalProperties());
				grp.addExpr(op);
				op.setGroup(grp);
			}

			_memo.insert(op);
			return op;
		}
		else {
			if (grp != null) {
				grp.removeExpr(op);
				for (int i = 0; i < op.getNumChildren(); i++) {
					op.getChild(i).removeParent(op);
				}

				op.setUnifiedTo(opMemo);

				final LogicalEquivalenceNode grpMemo = opMemo.getGroup();
				if (grp.getId() != grpMemo.getId()) {

					grpMemo.unify(grp, this);
				}
			}
			return opMemo;
		}
	}

	
	/**creates new LogicalOperatorNode from root given in expr and its childrens in childrenGroup and adds it to grp
	and adds this operator to memo. as grp is associated with this added op in memo, so grp is also part of memo 
	or if the op is already present in memo but associated with doff group, then unification of the two groups is done.
	and finally returns the LogicalOperatorNode formed with expr */
	protected LogicalOperatorNode addOperator(Object expr, LogicalEquivalenceNode[] childrenGroup,
			LogicalEquivalenceNode grp) {

		final LogicalOperatorNode op = new LogicalOperatorNode(expr, childrenGroup);
		final LogicalOperatorNode opMemo = _memo.find(op);

		if (opMemo == null) {
			if (grp == null) {
				grp = new LogicalEquivalenceNode(op.computeLogicalProperties());
			}

			grp.addExpr(op);
			op.setGroup(grp);
			if (childrenGroup != null) {
				for (int i = 0; i < childrenGroup.length; i++)
					childrenGroup[i].addParent(op,true);
			}

			// TODO This operator may trigger more rules on ancestor operator,
			// so
			// need to add task for that.
			_memo.insert(op);
			return op;
		}
		else {
			final LogicalEquivalenceNode grpMemo = opMemo.getGroup();
			if (grp != null && grp.getId() != grpMemo.getId()) {
				final LogicalEquivalenceNode deletedGrp = grp.unify(grpMemo, this);
				if (deletedGrp != null) {
					// following code is to catch bug. commented to improve the performance
					/*
					final LogicalOperatorNode deletedOp = _memo.isGroupExist(deletedGrp);
					if (deletedOp != null) {
						System.out.println("Bug - - ");
						deletedOp.print();
					}
					*/
				}
			}

			return opMemo;
		}

	}

	public Rules getRules() {
		return _rules;
	}

	public abstract Plan optimize(Object logicalExpressionTree);

	public LogicalOperatorNode removeOp(LogicalOperatorNode op) {
		return _memo.remove(op);
	}

	private LogicalEquivalenceNode applyRule(ITransformationRule rule, LogicalOperatorNode logExpr,
			LogicalEquivalenceNode group, int ruleIndex) {
		final Object rulePattern = rule.getInputPattern();

		boolean ruleApplied = false;

		final ArrayList<Pair<Object, Object>[]> bindings = MemoryManager.getTempBindingArray();
		final ArrayList<LogicalEquivalenceNode> bindingGroup = MemoryManager.getTempBindingGroupArray();
		PatternTree.getBindings(rulePattern, logExpr, bindings, bindingGroup);
		for (int bndIndex = 0; bindings != null && bndIndex < bindings.size(); bndIndex++) {

			final Pair<Object, Object>[] binding = bindings.get(bndIndex);

			final Object[] outputExpr = rule.getOutputExpr(binding);

			if (outputExpr == null)
				continue;

			for (int i = 0; i < outputExpr.length; i++) {

				group = addExprTree(outputExpr[i], group).getGroup();

			}
			ruleApplied = true;
		}

		MemoryManager.putTempBindingArrayRecurse(bindings);
		MemoryManager.putTempBindingGroupArray(bindingGroup);

		if (ruleApplied)
			logExpr.setRuleApplied(ruleIndex);
		return group;
	}

	protected void applyTransformationRules(LogicalEquivalenceNode group) {

		if (group.isTransformationRulesApplied())
			return;

		final ITransformationRule[] rules = _rules.getTransformationRules();

		for (int exprIndex = 0; exprIndex < group.getNumLogExpr(); exprIndex++) {
			final LogicalOperatorNode logExpr = group.getLogicalExpression(exprIndex);

			if (logExpr == null)
				continue;

			final int numChildren = logExpr.getNumChildren();
			for (int childIndex = 0; childIndex < numChildren; childIndex++) {

				final LogicalEquivalenceNode childGroup = logExpr.getChild(childIndex);
				if (!childGroup.isTransformationRulesApplied())
					applyTransformationRules(childGroup);
			}

			int ruleIndex = logExpr.getFirstUnAppliedRuleIndex(0);
			while (ruleIndex < rules.length) {

				group = applyRule(rules[ruleIndex], logExpr, group, ruleIndex);
				ruleIndex = logExpr.getFirstUnAppliedRuleIndex(ruleIndex + 1);
			}
		}

		group.setTransformationRulesApplied();
	}
}
