 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.volcano;

import org.apache.commons.lang3.tuple.Pair;
import pyroj.optimizer.Config;
import pyroj.optimizer.LogicalOperatorNode;
import pyroj.optimizer.MemoryManager;
import pyroj.optimizer.Optimizer;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.optimizer.PhysicalOperatorNode;
import pyroj.runtime.Plan;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Volcano extends Optimizer {

	@Override
	public Plan optimize(Object logicalExpressionTree) {
		clearState();

		if (Config.PrintQueryExpressionTree) {
			System.out.println("\nQuery Expression Tree");
			System.out.println("=====================");
			Config.TreeHelper.print(logicalExpressionTree);
			System.out.println();
		}

		final LogicalOperatorNode rootOp = addExprTree(logicalExpressionTree, null);

		if (Config.PrintInitialMemo) {
			System.out.println("\nInitial memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		applyTransformationRules(rootOp.getGroup());

		if (Config.PrintMemoAfterTrasformation) {
			System.out.println("\nMemo after transformation");
			System.out.print("===========================");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		//return null;

		final int idCostUB = Config.CostModel.MaxCostId();

		final PhysicalEquivalenceNode phyEqNode = rootOp.getGroup().getPhysicalEqNode(null);
		phyEqNode.setRoot();
		optimizePhysicalEquivalenceNode(phyEqNode, false, idCostUB);
		// TODO Auto-generated method stub
		if (Config.PrintFinalMemo) {
			System.out.println("\n\nFinal memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
		}

		if(Config.PrintPlanCount)
		{
			System.out.println(" \n\nPlan Count for the Final Memo ");
			System.out.println("============");
			int plancount = getPlanCountPhysicaLEqNode(phyEqNode);
			System.out.println(plancount);
			System.out.println("");
		}



        /*try {
            BufferedWriter out = new BufferedWriter(new FileWriter("/home/rashmitha/all_plans_countResults.txt", true));
            out.write("Plan count for the query is : ");
            out.write(String.valueOf(plancount));
            out.newLine();
            out.close();
        }
        catch (IOException e) {
            System.out.println("exception occured"+ e);
        }*/

		if (Config.PrintFinalPlan) {
			System.out.print("\nFinal plan ");
			System.out.println("\n==========");
			phyEqNode.printCurrentBestPlan(0);
			System.out.println();
		}

		return phyEqNode.getCurrentBestPlan();


	}

	private int getPlanCountPhysicalOpNode(PhysicalOperatorNode phyOpNode){
        PhysicalEquivalenceNode[] children = phyOpNode.getChildren();
        if(children==null || children.length==0){
            return 1;
        }
        int sum = 1;
        for(int i=0;i<children.length;i++){
            sum=sum*getPlanCountPhysicaLEqNode(children[i]);
        }
        return sum;
    }
	private int getPlanCountPhysicaLEqNode(PhysicalEquivalenceNode phyEqNode){
	    int numchildren = phyEqNode.getNumberOfChildren(false);

        int sum=0;
        for( int i=0;i<numchildren;i++){
            sum+=getPlanCountPhysicalOpNode(phyEqNode.getChild(i));
        }
        return sum;
    }

	private void optimizePhysicalOperatorNode(PhysicalOperatorNode phyOpNode, int idCostUB) {
		if (Config.PrintTaskExecuting) {
			System.out.print("OPERATOR TASK Exec:Find best plan - ");
			phyOpNode.printShort();
			System.out.print(", " + Config.CostModel.toString(idCostUB));
			System.out.println();
		}

		final Object phyOp = phyOpNode.getOperator();
		final int idLocalCost = phyOpNode.getLocalCostID();

		final PhysicalEquivalenceNode[] children = phyOpNode.getChildren();
		if (children == null) {
			phyOpNode.updateCurrentBestPlanCost();
			return;
		}

		final boolean isEnfOperator = phyOpNode.isEnfOperator();
		final int[] idChildrenCost = MemoryManager.getCostArray(children.length);
		final int idChildCostUB = MemoryManager.getTempCostObject();

		for (int i = 0; i < children.length; i++) {
			Config.CostModel.computeRemainingCostUB(idChildCostUB, phyOp, idLocalCost, idChildrenCost, idCostUB);

			idChildrenCost[i] = children[i].getCurrentBestPlanCostId();

			int idCurrentCostUB = idChildrenCost[i];
			if (Config.CostModel.IsGreaterThan(idCurrentCostUB, idChildCostUB))
				idCurrentCostUB = idChildCostUB;

			final int idCostLB = children[i].getCostLBId(isEnfOperator);

			// Need to optimize
			if (Config.CostModel.IsLessThan(idCostLB, idCurrentCostUB)) {
				optimizePhysicalEquivalenceNode(children[i], isEnfOperator, idChildCostUB);

				idChildrenCost[i] = children[i].getCurrentBestPlanCostId();
			}

			if (Config.CostModel.IsGreaterThanOrEqual(idChildrenCost[i], idChildCostUB)) {
				phyOpNode.updateCostLB();

				MemoryManager.putCostArray(idChildrenCost);
				MemoryManager.putTempCostObject(idChildCostUB);
				return;
			}
		}

		phyOpNode.updateCostLB();
		phyOpNode.updateCurrentBestPlanCost();
		MemoryManager.putCostArray(idChildrenCost);
		MemoryManager.putTempCostObject(idChildCostUB);
		return;
	}

	private void optimizePhysicalEquivalenceNode(PhysicalEquivalenceNode phyEqNode, boolean onlyAlgPlan, int idCostUB) {
		if (Config.PrintTaskExecuting) {
			System.out.print("NODE TASK EXECUTING: Find best plan - ");
			phyEqNode.printShort();
			System.out.print(", " + Config.CostModel.toString(idCostUB));
			System.out.println();
		}

		/*if(phyEqNode.isMarked() && phyEqNode.getCurrentBestPlanOperator()!=null)
		{
			int useCost = phyEqNode.getUseCostId();


			PhysicalOperatorNode phyOpNode = phyEqNode.getCurrentBestPlanOperator();
			Object logicalProperties = phyEqNode.getLogicalProperties();
			Object Lop = phyOpNode.getLogicalOperator();

			int matCost = Config.CostModel.computeMaterializationCost(Lop, logicalProperties, phyOpNode.getOperator(),
					phyOpNode.getParent().getPhysicalProperties());

			int cost = 0;
			if(useCost < phyEqNode.getCurrentBestPlanCostId())
				cost = useCost + matCost;
			else
				cost = phyEqNode.getCurrentBestPlanCostId();



			_idCostUBFromParent = cost;


			Plan bestplan =  phyOpNode.getCurrentBestPlan();
			cachedBestPlan = Pair.of(_idCurrentBestPlanCost, bestplan);
			return cachedBestPlan.getRight();
		}*/

		phyEqNode.expand(onlyAlgPlan, getRules());

		final int numOperators = phyEqNode.getNumberOfChildren(onlyAlgPlan);
		for (int i = 0; i < numOperators; i++) {

			final int idCurrentBestPlanCost = phyEqNode.getCurrentBestPlanCostId();
			if (Config.CostModel.IsGreaterThan(idCostUB, idCurrentBestPlanCost))
				idCostUB = idCurrentBestPlanCost;

			final PhysicalOperatorNode phyOpNode = phyEqNode.getChild(i);

			if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(), idCostUB))
				continue;

			optimizePhysicalOperatorNode(phyOpNode, idCostUB);

			if (Config.PrintTaskExecuting) {
				System.out.print("OPERATOR TASK EXECUTION:Find best plan - ");
				phyEqNode.printShort();
				System.out.print(", " + Config.CostModel.toString(idCostUB));
				System.out.println();
			}

		}
	}
}
