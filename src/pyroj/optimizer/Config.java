 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;

import com.sosnoski.util.array.DoubleArray;
import pyroj.costmodel.ICostModel;
import pyroj.runtime.IOperatorHelper;
import pyroj.runtime.ITreeHelper;

public class Config {

	public static boolean useRU = true;
	public static boolean isRu()
	{
		return useRU;
	}

	public  static Memo savedMemo = null;
	public static void setSavedMemo(Memo memo)
	{
		savedMemo = memo;
	}

	public static LogicalOperatorNode rootop = null;
	public static void setRootop(LogicalOperatorNode op)
	{
		rootop = op;
	}

	public static int max_grp = -1;
	public static void setMax_grp(int id)
	{
		max_grp = id;
	}

	public static int cur_max_grp = -1;
	public static void setCur_max_grp(int id)
	{
		cur_max_grp = id;
	}

	public static DoubleArray CostObjList = null;

	public enum ChildOrder {
		SEQUENTIAL, RANDOM
	};

	public enum DebugLevel {
		NONE, ALL, ALL1, HIGH, MEDIUM, LOW 
	}

	public enum LogExprOrder {
		SEQUENTIAL, MIN_HEURISTIC, MAX_HEURISTIC, RANDOM
	};

	public enum PhysicalOperatorOrder {
		SEQUENTIAL, MIN_COST_LB, MIN_LOCAL_COST, MIN_INPUT_SIZE, RANDOM
	};

	public enum PhysicalEquivalenceNodeOrder {
		ROOT, PARENT_WITH_MOSTEXPENSIVE_CHILD, RANDOM
	};

	//--mayuri
	public static int OBJECTIVE ; //1=total cost based 2= response time based
	//--mayuri added for testing 
	public static BufferedWriter querryResultOutputBW = null;	
	//public static String outputPlanFilename = "/home/hduser/Desktop/stage2Testing/generatedPlan.txt";
	
	//---added for approximation
	
	public static boolean APPROXIMATION_ENABLED =false;
	public static double APPROXIMATION_FACTOR =0.0;  //>0
	//--
	
	public static boolean PropagateAll = true;


	public static DebugLevel _debugLevel = DebugLevel.ALL;


	public static LogExprOrder SelectLogExprOrder = LogExprOrder.MIN_HEURISTIC;

	public static ChildOrder SelectChildOrder = ChildOrder.SEQUENTIAL;

	public static boolean PrintInitialMemo = true;//false;
	public static boolean PrintMemoAfterTrasformation = true;//false;
	public static boolean PrintFinalMemo = true;//false;
	public static boolean PrintQueryExpressionTree = true;//false;
	public static boolean PrintTaskExecuting = true;//false;
	public static boolean PrintFinalPlan = true;//false;
	public static boolean Printdetailed_plan_in_outputFile = true;//false;
	public static boolean PrintPlanAdding = true;//false;
	public static boolean PrintPhysicalNodeCreation = true;//false;
	public static PhysicalOperatorOrder SelectPhysicalOperatorOrder = PhysicalOperatorOrder.SEQUENTIAL;
	public static PhysicalEquivalenceNodeOrder SelectPhyscialEqNodeOrder = PhysicalEquivalenceNodeOrder.ROOT;

	public static  boolean PrintPlanCount = true;
	
	public static IOperatorHelper OpHelper;
	public static ICostModel CostModel;
	public static ITreeHelper TreeHelper;
	

	private static HashMap<String, PrintWriter> logFiles = null;

	// Used for Greedy
	//---------------
	
	public static double relThreshold = 0.000;
	public static double absoluteThreshold = 0.50;
	
	// Check if counter needed
	private static int counter;
	
	public static void resetCounter() {
		counter = 0;
	}
	
	public static int nextCounter() {
		return ++counter;
	}
	// to enable and disable pruning
	public static boolean isPruningEnabled = false ;//true;//false
	
	// Whether to use the sharability optimization
	public static boolean isSharPrune = true ;
	
	// Maintain the sub and superList in Physical Equivalence Nodes
	private static boolean createSubAndSuperList = false;

	/** Print the all plans in memo while printing memo*/
	public static boolean PrintPhyiscalDetailedMemo = true;

	// print the all plan in physical Eq node when print long is called
	public static boolean PrintDetailedPhyEq = true;

	// print the super list associated with the physical Eq Node 
	public static boolean PrintSuperList = false;

	// print final memo after physical optimization considering subsumtion. This is being used in the greedy optimization
	//only
	public static boolean PrintFinalMemoAfterUpdatingBestPlan= true;

	// if Build Parent is true in that case the each physicalEquivalence node will maintain the _algParent and _enfParent list
	public static boolean BuildParentList = true;


	public static boolean getCreateSubAndSuperList () {
		return createSubAndSuperList;
	}
	
	public static void setCreateSubAndSuperList (boolean flag) {
		createSubAndSuperList = flag;	
	}
	
	// following are for subsumption derivation
	// to apply the  subsumption derivation. by default disabled because no TPCH query can take advantage of this
	public static boolean isSubExpand = false;

	// to print the memo after the subsumtion derivation
	public static boolean PrintSubExpandedMemo = true;
	//--------------------
	
	public static void initLog(String logname, String filename, boolean append) {
		if (logFiles == null)
			logFiles = new HashMap<String, PrintWriter>();
		if (!logFiles.containsKey(logname)) {
			try {
				logFiles.put(logname, new PrintWriter(new FileWriter(filename, append)));
			}
			catch (final Exception e) {
				e.printStackTrace();
				System.out.println("Cannot open file: " + filename);
			}

		}
		else {
			System.out.println("Log " + logname + " already open!");
		}
	}

	public static void printToLog(String logname, String out) {
		if (logFiles == null || !logFiles.containsKey(logname) || logFiles.get(logname) == null) {
			System.out.println("Log not initiated!");
		}
		else {
			logFiles.get(logname).print(out);
		}
	}

	public static void closeLog(String logname) {
		if (logFiles != null && logFiles.containsKey(logname) && logFiles.get(logname) != null) {
			logFiles.get(logname).close();
			logFiles.remove(logname);
		}
		else {
			System.out.println("Log not initiated!");
		}
	}

	public static boolean IsDebug(DebugLevel debugLevel) {
		return debugLevel.ordinal() <= _debugLevel.ordinal();
	}

	public static void setPrintNone() {
		//_debugLevel = DebugLevel.NONE;
		PrintInitialMemo = false;
		PrintMemoAfterTrasformation = false;
		PrintFinalMemo = false;
		PrintQueryExpressionTree = false;
		PrintTaskExecuting = false;
		PrintFinalPlan = false;
	}

	public static void setCostModel(ICostModel costModel) {
		CostModel = costModel;
	}
	
	
}
