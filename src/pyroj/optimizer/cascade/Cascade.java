 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.cascade;

import java.util.Stack;

import pyroj.optimizer.Config;
import pyroj.optimizer.LogicalOperatorNode;
import pyroj.optimizer.Optimizer;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.runtime.Plan;

public class Cascade extends Optimizer {

	private Stack<Task> _tasks;

	/**
	 * Find best plan for the group with the costUB
	 * 
	 * @param group
	 * @param costUB
	 * @return Expression tree of physical operators.
	 * @throws Exception
	 */

	// TODO Add partitioning info.
	// TODO Need to add support for nested query.
	// TODO Re-explore pruned plans with a higher cost.

	public void addTask(Task task) {
		_tasks.push(task);
	}

	@Override
	public Plan optimize(Object logicalExpressionTree) {

		clearState();

		if (Config.PrintQueryExpressionTree) {
			System.out.println("\nQuery Expression Tree");
			System.out.println("=====================");
			Config.TreeHelper.print(logicalExpressionTree);
			System.out.println();
		}

		final LogicalOperatorNode rootOp = addExprTree(logicalExpressionTree, null);

		if (Config.PrintInitialMemo) {
			System.out.println("\nInitial memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		applyTransformationRules(rootOp.getGroup());
		if (Config.PrintMemoAfterTrasformation) {
			System.out.println("\nMemo after transformation");
			System.out.print("===========================");
			_memo.print(rootOp.getGroup());
			System.out.println();
			System.out.println();
		}

		rootOp.getGroup().getPhysicalEqNode(null).setRoot();
		final int idCostUB = Config.CostModel.MaxCostId();

		_tasks = new Stack<Task>();
		// Add task to optimize root group.
		addTask(new OptimizePhysicalEquivalenceNode(rootOp.getGroup().getPhysicalEqNode(null), this, idCostUB, false));

		while (!_tasks.empty()) {
			final Task task = _tasks.pop();
			task.execute();
		}

		if (Config.PrintFinalMemo) {
			System.out.println("\n\nFinal memo");
			System.out.print("============");
			_memo.print(rootOp.getGroup());
		}

		final PhysicalEquivalenceNode phyEqNode = rootOp.getGroup().getPhysicalEqNode(null);

		if (Config.PrintFinalPlan) {
			System.out.print("\nFinal plan ");
			System.out.println("\n==========");
			phyEqNode.printCurrentBestPlan(0);
			System.out.println();
		}

		return phyEqNode.getCurrentBestPlan();
	}
}
