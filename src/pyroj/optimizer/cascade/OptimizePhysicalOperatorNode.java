 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.cascade;

import java.util.BitSet;

import pyroj.costmodel.ICostModel;
import pyroj.optimizer.Config;
import pyroj.optimizer.MemoryManager;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.optimizer.PhysicalOperatorNode;

public class OptimizePhysicalOperatorNode extends Task {

	private final PhysicalOperatorNode _phyOpNode;
	private final PhysicalEquivalenceNode[] _children;
	private final boolean _isEnforcerExpression;

	private int _childIndex = -1;
	private final BitSet _exploredChildIndex = new BitSet();

	private int _idOperatorLocalCost = ICostModel.NULL;
	private int[] _idChildrenCost = null;
	private int _idChildCostUB = ICostModel.NULL;

	public OptimizePhysicalOperatorNode(PhysicalOperatorNode phyOpNode, Cascade optimizer, int idCostUB) {
		super(idCostUB, optimizer);
		_phyOpNode = phyOpNode;
		_isEnforcerExpression = _phyOpNode.isEnfOperator();
		_children = _phyOpNode.getChildren();
	}

	private void getNextChildIndex() {
		switch (Config.SelectChildOrder) {
		case SEQUENTIAL:
			_childIndex++;
			_exploredChildIndex.set(_childIndex);
			break;
		case RANDOM:
			final int totalExplored = _exploredChildIndex.cardinality();
			int randomIndex = (int) (Math.random() * (_children.length - totalExplored));
			int nextChildIndex = -1;

			while (randomIndex >= 0) {
				nextChildIndex = _exploredChildIndex.nextClearBit(nextChildIndex + 1);
				randomIndex--;
			}

			_childIndex = nextChildIndex;
			_exploredChildIndex.set(_childIndex);
			break;
		default:
			break;
		}
		// System.out.println("id=" + this._group.getId() + " next child=" +
		// this._childIndex);
	}

	@Override
	protected String getName() {
		return "Optimize PhysicalOperatorNode";
	}

	@Override
	public void print() {
		_phyOpNode.printShort();
	}

	@Override
	protected void perform() {

		if (Config.CostModel.IsNull(_idOperatorLocalCost)) {

			_idOperatorLocalCost = _phyOpNode.getLocalCostID();
			
			// if no child update  best plan 
			if (_children == null) {
				_phyOpNode.updateCurrentBestPlanCost();
				return;
			}

			_idChildrenCost = MemoryManager.getCostArray(_children.length);
			_idChildCostUB = MemoryManager.getTempCostObject();
			// changed to disable pruning 
			if (Config.isPruningEnabled == false)
				Config.CostModel.Assign(_idChildCostUB, _idCostUB);
		}
		// notice _childIndex initialized to -1. 
		// this condition check if the last optimized child's best plan have overshoot the buget allocated to that in that case stop this optimization task
		if (_childIndex >= 0) {

			_idChildrenCost[_childIndex] = _children[_childIndex].getCurrentBestPlanCostId();
 			
			
			// if pruning is diabled in that case the costUB for the child would be Max and in that case if the child is failed , it means
			// that plan does not exist because of the other constrains.( Example :As the operator can not be applied because high  less no of available buffer )
			
			if (Config.CostModel.IsGreaterThanOrEqual(_idChildrenCost[_childIndex], _idChildCostUB)) {
				
				// changed to disable pruning
				if (Config.isPruningEnabled == true)
					_phyOpNode.updateCostLB();
				
				if (Config.IsDebug(Config.DebugLevel.LOW)) {
					System.out.println();
					System.out.print("\t\tAborting Optiomization of : ");
					_phyOpNode.print();
					System.out.println();
					System.out.print("\t\tLast child optimized   :");
					_children[_childIndex].printShort();
					System.out.print(" BestPlanCost = " + Config.CostModel.toString(_idChildrenCost[_childIndex]));
					System.out.println("  Budget Allocated = " + Config.CostModel.toString(_idChildCostUB));
				}

				MemoryManager.putCostArray(_idChildrenCost);
				MemoryManager.putTempCostObject(_idChildCostUB);
				return;
			}
		}

		// get next child index to be explored
		getNextChildIndex();

		while (_exploredChildIndex.cardinality() <= _children.length) {
			
			
			
			// update the cost upper bound for current child, in case pruning is disabled all the children should be allocated maximum budget
			
			// changed to disable pruning
			if (Config.isPruningEnabled == true) 
				Config.CostModel.computeRemainingCostUB(_idChildCostUB, _phyOpNode.getOperator(), _idOperatorLocalCost,
						_idChildrenCost, _idCostUB);

			// Otherwise the current best plan is the best plan.
			_idChildrenCost[_childIndex] = _children[_childIndex].getCurrentBestPlanCostId();

			
			
			int idCostUB = _idChildrenCost[_childIndex];
			if (Config.CostModel.IsGreaterThan(idCostUB, _idChildCostUB))
				idCostUB = _idChildCostUB;

			
			final int idCostLB = _children[_childIndex].getCostLBId(_isEnforcerExpression);
			
			
			if (Config.IsDebug(Config.DebugLevel.LOW)) {
				System.out.print("\t\t Considering optimization of :: " );
				_children[_childIndex].printShort();
				System.out.println();
				System.out.println("\t\tLB = " + Config.CostModel.toString(idCostLB) + 
						" UB = " + Config.CostModel.toString(idCostUB) +
						" Budget = "  + Config.CostModel.toString(_idChildCostUB) );
			}

			// Need to optimize-- if lb < Ub or the pruning is disabled
			if (Config.CostModel.IsLessThan(idCostLB, idCostUB)) {
				_optimizer.addTask(this);
				_optimizer.addTask(new OptimizePhysicalEquivalenceNode(_children[_childIndex], _optimizer,
						_idChildCostUB, _isEnforcerExpression));
				return;
			} else  {
				if (Config.IsDebug(Config.DebugLevel.LOW)) { 
					System.out.println("\t\tSkipping the optimization : LB < UB" );
					System.out.println();
				}
			}

			// child is expensive and pruning is enabled then no need to explore more child  
			if (Config.CostModel.IsGreaterThanOrEqual(_idChildrenCost[_childIndex], _idChildCostUB)) {

				// changed to disable pruning 
				if (Config.isPruningEnabled == true)
					_phyOpNode.updateCostLB();

				MemoryManager.putCostArray(_idChildrenCost);
				MemoryManager.putTempCostObject(_idChildCostUB);
				return;
			}

			getNextChildIndex();
		}

		if (Config.isPruningEnabled == true)
			_phyOpNode.updateCostLB();
		_phyOpNode.updateCurrentBestPlanCost();
		
		if (Config.IsDebug(Config.DebugLevel.LOW)) {
			System.out.print("\t\t");
			_phyOpNode.print();
			System.out.println();
		}

		MemoryManager.putCostArray(_idChildrenCost);
		MemoryManager.putTempCostObject(_idChildCostUB);
		return;
	}
}
