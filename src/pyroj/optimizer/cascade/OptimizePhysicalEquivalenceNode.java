 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.cascade;

import java.util.BitSet;

import pyroj.optimizer.Config;
import pyroj.optimizer.PhysicalEquivalenceNode;
import pyroj.optimizer.PhysicalOperatorNode;

public class OptimizePhysicalEquivalenceNode extends Task {

	private final boolean _onlyAlgPlan;
	private final PhysicalEquivalenceNode _phyEqNode;

	private final BitSet _phyOpExplored = new BitSet();

	public OptimizePhysicalEquivalenceNode(PhysicalEquivalenceNode phyEqNode,
			Cascade optimizer, int idCostUB, boolean onlyAlgPlan) {
		super(idCostUB, optimizer);
		_phyEqNode = phyEqNode;
		_onlyAlgPlan = onlyAlgPlan;
	}

	@Override
	protected String getName() {
		return "Optimize PhysicalEquivalenceNode";
	}

	@Override
	public void print() {
		if (_onlyAlgPlan)
			System.out.print("OnlyAlgorithmPlan, ");
		_phyEqNode.printShort();
	}

	private int findPhysicalOperatorToExplore(int numPhysicalOperators) {
		int bestExprIndex = -1;
		switch (Config.SelectLogExprOrder) {
		case RANDOM:
			final int totalExplored = _phyOpExplored.cardinality();
			int randomIndex = (int) (Math.random() * (numPhysicalOperators - totalExplored));

			while (randomIndex >= 0) {
				bestExprIndex = _phyOpExplored.nextClearBit(bestExprIndex + 1);
				randomIndex--;
			}
			break;
		case SEQUENTIAL:
			bestExprIndex = _phyOpExplored.nextClearBit(0);
			break;
		case MIN_HEURISTIC:
			double currentMinInputSize = Double.MAX_VALUE;

			for (int i = _phyOpExplored.nextClearBit(0); i < numPhysicalOperators; i = _phyOpExplored
					.nextClearBit(i + 1)) {

				final PhysicalOperatorNode expr = _phyEqNode.getChild(i);
				final double inputSize = expr.getInputSizeBlocks();
				if (bestExprIndex == -1 || inputSize < currentMinInputSize) {
					bestExprIndex = i;
					currentMinInputSize = inputSize;
				}
			}
			break;
		case MAX_HEURISTIC:
			final double currentMaxInputSize = 0;

			for (int i = _phyOpExplored.nextClearBit(0); i < numPhysicalOperators; i = _phyOpExplored
					.nextClearBit(i + 1)) {

				final PhysicalOperatorNode expr = _phyEqNode.getChild(i);
				final double inputSize = expr.getInputSizeBlocks();
				if (bestExprIndex == -1 || inputSize > currentMaxInputSize) {
					bestExprIndex = i;
					currentMinInputSize = inputSize;
				}
			}
			break;
		default:
			System.out.println("Error: Unknown SelectLogExprOrder");
			break;
		}

		return bestExprIndex;
	}

	/**
	 * Apply one rule on one expression in each call.
	 */
	@Override
	protected void perform() {
		
		// added during disabling prune
		 if (_phyEqNode.isOptimized() && Config.isPruningEnabled == false) {
			 if (Config.IsDebug(Config.DebugLevel.LOW)) {
				 System.out.println("Skipping Optimization, Already Optimized..");
			 }
			 return;
		 }
		 
			 
		// generate all possible physical plans
		_phyEqNode.expand(_onlyAlgPlan, _optimizer.getRules());

		if (Config.IsDebug(Config.DebugLevel.LOW)) {
			_phyEqNode.printLong();
		}
		
		// get the current best cost
		final int idCurrentCostUB = _phyEqNode.getCurrentBestPlanCostId();
		
		if (Config.IsDebug(Config.DebugLevel.LOW)) {
			System.out.println("\t\t Current Upper Bound : " + Config.CostModel.toString(_idCostUB) +
					"   current plan cost = " + Config.CostModel.toString(idCurrentCostUB));
		}
		
		// Changed to disable pruning - do not update the upper bound. Actual upper bound should be passed to next child
		// in case pruning is disabled
		if (Config.CostModel.IsLessThan(idCurrentCostUB, _idCostUB) && (Config.isPruningEnabled == true))
			_idCostUB = idCurrentCostUB;

		final int numPhysicalOperators = _phyEqNode
				.getNumberOfChildren(_onlyAlgPlan);
		
		if (Config.IsDebug(Config.DebugLevel.LOW)) {
			System.out.println("\t\t Upper bound for next plan :  " + Config.CostModel.toString(_idCostUB) +"\n");
		}
		
		// iterate over each generated plan
		for (int phyOpIndex = findPhysicalOperatorToExplore(numPhysicalOperators); phyOpIndex >= 0
				&& phyOpIndex < numPhysicalOperators; phyOpIndex = findPhysicalOperatorToExplore(numPhysicalOperators)) {

			_phyOpExplored.set(phyOpIndex);
			
			final PhysicalOperatorNode phyOpNode = _phyEqNode
					.getChild(phyOpIndex);
			
			if (Config.IsDebug(Config.DebugLevel.LOW)) {
				System.out.print("\t\t Next plan to explore   : ");
				phyOpNode.printShort();
				System.out.println();
			}
			// check physical plan lower bound is greater the the upper bound then if 
			// pruning enabled the do not explore the plan further, but in case of pruning disabled 
			// the exploration will continue
			if (Config.CostModel.IsGreaterThanOrEqual(phyOpNode.getCostLBId(),
					_idCostUB)) {
				if (Config.IsDebug(Config.DebugLevel.LOW)) {
					System.out.println("\t\t Skipping next Operator : LB >= Budget,");
					System.out.println("\t\t\t LB = " + Config.CostModel.toString(phyOpNode.getCostLBId()) +
										" , Local Cost = " + Config.CostModel.toString(phyOpNode.getLocalCostID())  + 
										" , UB = " +  Config.CostModel.toString(_idCostUB));
				}		
				continue;
			}
			
			if (Config.isPruningEnabled == false) {
				if (phyOpNode.getChildren() == null) {
					if (Config.IsDebug(Config.DebugLevel.LOW)) {
						System.out.println("\t\t Skipping next Operator : Has Zero Children,");
					}
					continue;
				}
			}
			
			// add same task 
			_optimizer.addTask(this);
			// and new optimization task  
			_optimizer.addTask(new OptimizePhysicalOperatorNode(phyOpNode,
					_optimizer, _idCostUB));
			return;
		}
		if (Config.IsDebug(Config.DebugLevel.LOW)) {
			_phyEqNode.printLong();
		}
		_phyEqNode.setOptimized(true);
	}
}
