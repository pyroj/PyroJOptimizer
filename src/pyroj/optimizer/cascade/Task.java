 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer.cascade;

import pyroj.optimizer.Config;

public abstract class Task {

	static public int NumInstances = 0;

	protected final int _id;

	protected int _idCostUB;
	protected Cascade _optimizer;

	protected Task(int idCostUB, Cascade optimizer) {
		_idCostUB = idCostUB;
		_optimizer = optimizer;
		_id = NumInstances++;
	}

	public void execute() {
		if (Config.PrintTaskExecuting) {
			System.out.printf("Task: %1$-4d: %2$25s, costUB =%3$s, ", _id,
					getName(), Config.CostModel.toString(_idCostUB));
			print();
			System.out.println();
		}

		perform();

	}

	public abstract void print();

	protected abstract String getName();

	protected abstract void perform();

}
