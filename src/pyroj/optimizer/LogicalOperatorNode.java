 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

public class LogicalOperatorNode {

	public static int NumInstances = 0;

	private final Object _rootOperator;
	private LogicalEquivalenceNode[] _children;

	private LogicalEquivalenceNode _grp;

	private final int _id;
	private final BitSet _rulesApplied = new BitSet();
	private LogicalOperatorNode _unifiedTo = null;

	// Changes for Greedy -------
	/** it is true if the degree list for the Eq node has been calculated  	 
	 * this variable shows that the degree list for all the parent has been calcuated
	 * so it is safe to delete the degree list associated with current Node
	 * TODO : Add this feature, (Not added yet)
	 * */
	private boolean isDegreeCalculated = false;
	
	/** degree list contains the child equivalence node and there degree
	 * degree list always sorted by the topological number
	 * (No separate sorting is performed , creation method inserts the element they
	 * are automatically in sorted order) 
	 */
	private List<DegShar> degreeList = null;
	
	/** mark that degree list for the equivalece node has been calculated 
	 * @param flag
	 */
	public void setDegreeCalculated(boolean flag) {
		isDegreeCalculated = flag;
	}
	
	public boolean isDegreeCalculated() {
		return isDegreeCalculated ;
	}
	
	/**
	 * return the degree list associated with the  this node
	 * @return 
	 */
	public List<DegShar> getDegreeList() {
		if (degreeList == null) {
			degreeList = createDegreeList();
		}
		return degreeList;
	}
	
	
	/**
	 * Thise method create the degree list of not yet created
	 * See Prasan thesis for details
	 * @return
	 */
	private List<DegShar> createDegreeList() {
		List <DegShar> list = new ArrayList<DegShar>();
		if (degreeList != null)
			return degreeList;
		
		int numChildren = getNumChildren();
		if ( numChildren > 0 ) {
			// input lists
			List<List<DegShar>> inpList = new ArrayList<List<DegShar>>(numChildren);
			
			// iterator for each input list
			List<Iterator<DegShar>>  inpIter = new ArrayList<Iterator<DegShar>>(numChildren);
			
			// set iterator for each inputList
			for (int i = 0; i< numChildren ; i++ ) {
				LogicalEquivalenceNode logEqNode = getChild(i);
				inpList.add(i, logEqNode.getDegreeList());
				inpIter.add(i, inpList.get(i).iterator());
 			}
			
			// initialize the currentEntries
			// currEntry point the DagShar in each inpList  which has the lowest topNo
			// (Notice by construction of algo we ensure that each degree list is sorted 
			// according to the topological No)
			List<DegShar> curEntry = new ArrayList<DegShar>(numChildren);
			for (int i = 0 ; i < numChildren ; i++ ) {
				if (inpIter.get(i).hasNext()) {
					curEntry.add(i, inpIter.get(i).next());
				} else {
					curEntry.add(i, null);
				}
			}
			
			// traverse all the child degree list and take sum of all logical eq node with same topological no.
			for (;;) {
				int minTopNo = -1;
				
				// get the minimum topological number
				for ( int i = 0 ; i < numChildren ; i++  ) {
					if (curEntry.get(i) == null) 
						continue;
					
					LogicalEquivalenceNode childEq = curEntry.get(i).getLogicalEquivalenceNode();
					int eqTopNo = childEq.getToplogocalNo();
					if ( minTopNo == -1 || eqTopNo < minTopNo ) 
						minTopNo = eqTopNo;
					
				}
				
				// if no more eq node left 
				if ( minTopNo == -1 )
					break;
				
				int sumDegree = 0 ;
				DegShar degShar = null;
				
				for ( int i = 0 ; i < numChildren; i++) {
					if (curEntry.get(i) == null) 
						continue;
					LogicalEquivalenceNode eqNode = curEntry.get(i).getLogicalEquivalenceNode();
					
					int eqTopNo = eqNode.getToplogocalNo();
					
					if (eqTopNo != minTopNo) 
						continue;
					
					int currDegree = curEntry.get(i).getDegree();
					
					sumDegree += currDegree;
					degShar = curEntry.get(i);
					
					
					if (inpIter.get(i).hasNext()) {
						curEntry.set(i, inpIter.get(i).next());
					} else {
						curEntry.set(i, null);
					}
						
				}
				
				if (sumDegree == degShar.getDegree()) {
					list.add(degShar);
				} else {
					DegShar newDegShar = new DegShar(degShar.getLogicalEquivalenceNode(), sumDegree);
					list.add(newDegShar);
				}	
			}	
		}
		
		// TODO :Check if all the parent have calculated the dgree list delete existing
		// degree list
		setDegreeCalculated(true);
		degreeList = list;
		return list;
	}
	//------------------------
	
	// Changes of subsumption derivation
	
	/**
	 * isDrived Denotes whether the logical expression node is
	 * denerated during subsumption derivation, this is used in MQO
	 * true = drived by sumsumption
	 */
	private boolean _isDrived = false;
	
	public boolean isDrived() {
		return _isDrived;
	}
	
	public void markDrived(boolean flag) {
		_isDrived = false;
	}
	
	// ------------------------
	
	public LogicalOperatorNode(Object root) {
		_rootOperator = root;
		_children = null;
		_id = NumInstances++;
	}

	public LogicalOperatorNode(Object root, LogicalEquivalenceNode child0) {
		_rootOperator = root;
		_children = new LogicalEquivalenceNode[1];
		_children[0] = child0;
		_id = NumInstances++;
	}

	public LogicalOperatorNode(Object root, LogicalEquivalenceNode child0, LogicalEquivalenceNode child1) {
		_rootOperator = root;
		_children = new LogicalEquivalenceNode[2];
		_children[0] = child0;
		_children[1] = child1;
		_id = NumInstances++;
	}

	public LogicalOperatorNode(Object root, LogicalEquivalenceNode[] children) {
		_rootOperator = root;
		_children = children;
		_id = NumInstances++;
	}

	public Object computeLogicalProperties() {
		final Object[] childrenLogicalProperties = getChildrenProps();
		Object logProp = Config.OpHelper.computeOutputLogicalProperties(_rootOperator, childrenLogicalProperties);
		Config.OpHelper.putLogicalPropertiesArray(childrenLogicalProperties);
		return logProp;
	}

	@Override
	public boolean equals(Object obj) {

		//System.out.println("In Equals LOGOP");
		//this.print();

		if (!(obj instanceof LogicalOperatorNode))
			return false;

		final LogicalOperatorNode other = (LogicalOperatorNode) obj;

		int grp1 = Config.cur_max_grp;
		if(_grp!=null && _grp.getId()>=0)
			grp1 = _grp.getId();

		int grp2 = Config.cur_max_grp;
		if(other.getGroup()!=null && other.getGroup().getId()>=0)
			grp2 = other.getId();


		//Config.OpHelper.isLogOperatorNotEqualHive(_rootOperator, other._rootOperator, grp1, grp2);
		//if(!Config.OpHelper.isLogOperatorNotEqualHive(_rootOperator, other._rootOperator, grp1, grp2))
			//return false;

		if (!Config.OpHelper.isLogOperatorEqual(_rootOperator, other._rootOperator))
			return false;
		
		if (_children != null) {
			for (int i = 0; i < _children.length; i++) {
				if (_children[i].getId() != other._children[i].getId()) {
					return false;
				}
			}
		}
		//System.out.println("Returning TRUE EQUALS LOGOPNODE****************************");
		return true;
	}

	public LogicalEquivalenceNode getChild(int index) {
		return _children[index];
	}

	public LogicalEquivalenceNode[] getChildren() {
		return _children;
	}

	public void set_children(int i, LogicalEquivalenceNode lEq)
	{
		_children[i] = lEq;
	}

	public Object[] getChildrenProps() {
		if (_children == null)
			return null;

		final Object[] childProps = Config.OpHelper.getLogicalPropertiesArray(_children.length);

		for (int i = 0; i < _children.length; i++)
			childProps[i] = _children[i].getLogicalProperties();

		return childProps;
	}

	public LogicalOperatorNode getCurrentActiveOperator() {
		if (_unifiedTo == null)
			return this;
		return _unifiedTo.getCurrentActiveOperator();
	}

	public int getFirstUnAppliedRuleIndex() {
		return _rulesApplied.nextClearBit(0);
	}

	public int getFirstUnAppliedRuleIndex(int startIndex) {
		return _rulesApplied.nextClearBit(startIndex);
	}

	public LogicalEquivalenceNode getGroup() {
		return _grp;
	}

	public int getId() {
		return _id;
	}

	public double getInputSizeBlocks() {
		if (_children == null)
			return 0;

		double inputSize = 0;
		for (int i = 0; i < _children.length; i++) {
			inputSize += Config.OpHelper.getNumBlocks(_children[i].getLogicalProperties());
		}

		return inputSize;
	}

	public int getNumChildren() {
		if (_children == null)
			return 0;
		return _children.length;
	}

	public Object getOperator() {
		return _rootOperator;
	}

	public boolean getRuleApplied(int ruleIndex) {
		return _rulesApplied.get(ruleIndex);
	}

	@Override
	public int hashCode() {
		int hashCode = Config.OpHelper.getLogOperatorLocalHashCode(_rootOperator);
		if (_children != null) {
			for (int i = 0; i < _children.length; i++)
				hashCode *= _children[i].getId();
		}
		
		return hashCode;
	}

	public void print() {
		int grpId = -1;
		if (_grp != null)
			grpId = _grp.getId();

		System.out.print(grpId + "." + _id + ".");
		Config.OpHelper.printLogicalOperator(System.out, _rootOperator, _grp.getLogicalProperties());
	}

	public void printDAG(int indentLevel) {
		System.out.println();
		for (int i = 0; i < indentLevel; i++)
			System.out.print("| ");
		print();

		if (_children != null) {
			for (int i = 0; i < _children.length; i++) {
				_children[i].printDAG(indentLevel + 1);
			}
		}
	}

	public void printExpr(PriorityQueue<LogicalEquivalenceNode> childGroups) {
		print();

		if (_children != null) {
			System.out.println();
			System.out.print("  <");
			for (int i = 0; i < _children.length; i++) {
				if (i > 0)
					System.out.print(", ");
				final LogicalEquivalenceNode child = _children[i];
				child.printshort();
				if (childGroups != null)
					childGroups.add(child);
			}
			System.out.print(">");
		}
		System.out.println();
	}

	public void setChild(int index, LogicalEquivalenceNode child) {
		_children[index] = child;
	}

	public void setGroup(LogicalEquivalenceNode grp) {
		_grp = grp;
	}

	public void setRuleApplied(int ruleIndex) {
		_rulesApplied.set(ruleIndex);
	}

	public void setUnifiedTo(LogicalOperatorNode op) {
		_unifiedTo = op;
	}
}
