 
/* 
 * Copyright 2011-2014 Indian Institute of Technology Bombay, Mumbai
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
 
package pyroj.optimizer;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;

//TODO Should memo contain only logical operator or physical operators as well ?
public class Memo {
	static final int MEMOSIZE = 100;

	public final HashMap<LogicalOperatorNode, LogicalOperatorNode> _operators = new HashMap<LogicalOperatorNode, LogicalOperatorNode>(
			MEMOSIZE, 10);

	public Memo() {
	}

	/**
	 * searches for the logical operator
	 * 
	 * @param op
	 * @return returns the existing operator if found; NULL otherwise
	 */
	public final LogicalOperatorNode find(LogicalOperatorNode op) {
		assert null != op;

		final LogicalOperatorNode retval = _operators.get(op);
		return retval;
	}

	/**
	 * inserts the operator into the table
	 * 
	 * @param op
	 */
	public final void insert(LogicalOperatorNode op) {
		if (_operators.put(op, op) == null) {
			// System.out.println("Operator " + op.getGroup().getId() + "." +
			// op.getId() + " added to memo");
		}
	}

	public LogicalOperatorNode isGroupExist(LogicalEquivalenceNode grp) {
		for (final Iterator<LogicalOperatorNode> opIter = _operators.values()
				.iterator(); opIter.hasNext();) {
			final LogicalOperatorNode op = opIter.next();
			if (op.getGroup().getId() == grp.getId())
				return op;
		}

		return null;
	}

	public void print(LogicalEquivalenceNode root) {
		final BitSet groupsPrinted = new BitSet(
				LogicalEquivalenceNode.NumInstances);

		final PriorityQueue<LogicalEquivalenceNode> groupsToPrint = new PriorityQueue<LogicalEquivalenceNode>();
		groupsToPrint.add(root);

		while (!groupsToPrint.isEmpty()) {
			final LogicalEquivalenceNode grp = groupsToPrint.remove();

			if (groupsPrinted.get(grp.getId()))
				continue;

			groupsPrinted.set(grp.getId());
			grp.printAllExpr(groupsToPrint);
		}

	}

	/**
	 * removes the logical operator
	 * 
	 * @param op
	 */
	public final LogicalOperatorNode remove(LogicalOperatorNode op) {
		op = _operators.remove(op);
		// System.out.println("Operator " + op.getGroup().getId() + "." +
		// op.getId() + " removed from memo");
		return op;
	}
}
